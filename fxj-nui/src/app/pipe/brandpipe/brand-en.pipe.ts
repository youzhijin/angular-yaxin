import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'brandEnPipe'})
export class BrandEnPipe implements PipeTransform {

  transform(value: any, args?: any): string {    
    switch (value) {
        case 'BOLON太阳镜':
          return "bolon sunglass";
        case 'BOLON光学镜':
          return "bolon optical frame";
        case 'MOLSION太阳镜':
          return "molsion sunglass";
        case '陌森光学架':
          return "molsion optical frame";
        case '保圣太阳镜':    
          return 'prosun sunglass';
        case '保圣光学镜':
          return 'prosun optical frame';
        case '亓那太阳镜':
          return 'qina sunglass';
        case '亓那光学镜':
          return 'qina optical frame';   
        default: 
          return value; 
    }    
  }
}