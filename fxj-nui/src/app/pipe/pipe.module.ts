import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { BrandEnPipe } from 'app/pipe/brandpipe/brand-en.pipe';
import { UserGroupEnPipe } from 'app/pipe/usergrouppipe/usergroup-en.pipe';
import { OrderStatusPipe } from 'app/pipe/order-status.pipe';
import { CurrencyTypePipe } from 'app/pipe/currency-type.pipe';




@NgModule({
  imports: [    
  ],
  declarations: [  
    BrandEnPipe,
    UserGroupEnPipe,
    OrderStatusPipe,
    CurrencyTypePipe
  ],
  exports: [  
    BrandEnPipe,
    UserGroupEnPipe,
    OrderStatusPipe,
    CurrencyTypePipe
  ]
})
export class PipeModule {
  
}
