import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'userGroupEnPipe'})
export class UserGroupEnPipe implements PipeTransform {

  transform(value: any, args?: any): string {    
    switch (value) {
        case '男款':
          return "MALE";
        case '女款':
          return "FAMALE";
        case '儿童款':
          return "KIDS";
        case '中性款':
          return "UNISEX";        
        default: 
          return value; 
    }    
  }
}