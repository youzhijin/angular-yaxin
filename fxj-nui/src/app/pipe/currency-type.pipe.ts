import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'CurrencyType'})
export class CurrencyTypePipe implements PipeTransform {

  transform(value: any, args?: any): string {    
    switch (value) {
        case 'USD':
          return "$";
        case 'CNY':
          return "¥";
        case 'EUR':
          return "€";
        default: 
          return '';
    }    
  }
}