import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'OrderStatus'})
export class OrderStatusPipe implements PipeTransform {

  transform(value: any, args?: any): string {    
    switch (value) {
        case 'NOCHECK':
          return "order-nocheck";
        case 'CHECKED':
          return "order-checked";
        case 'SENDING': 
          return "order-sending";
        case 'COMPLETE':
          return "order-complete";
        case 'CANCEL':
          return "order-cancel";
        default: 
          return value;
    }    
  }
}