import { CommService } from "../service/comm/comm.service";
import { ServiceResult } from "@core/constant";
import { Injector } from "@angular/core";
import { SettingsService } from "@delon/theme";
declare var _: any;
declare var $: any;
export class BaseController {

    authMap = {};

    constructor(public injector: Injector) {

    }

    loadMenuType() {
        return this.injector.get(CommService).getMenuType();
    }

    loadSex() {        
        return this.injector.get(CommService).getSex()
    }

    loadElementType() {
        return this.injector.get(CommService).getElementType();
    }

    getAuths(code): boolean {        
        if (this.authMap[code]) {
            console.log(`${code} ${this.authMap[code]}`);
            return this.authMap[code];
        }
        let i = this.injector.get(SettingsService).user.elements.findIndex((v, i) => {
             return v.code === code 
        });
        if (i > -1) {
            this.authMap[code] = true;
        } else {
            this.authMap[code] = false;
        }        
        return this.authMap[code];
    }

    replaceKeysDeep(obj, keysMap) { // keysMap = { oldKey1: newKey1, oldKey2: newKey2, etc...
        let that = this;
        return _.transform(obj, function(result, value, key) { // transform to a new object
      
          var currentKey = keysMap[key] || key; // if the key is in keysMap use the replacement, if not use the original key
      
          result[currentKey] = _.isObject(value) ? that.replaceKeysDeep(value, keysMap) : value; // if the key is an object run it through the inner function - replaceKeys
        });
    }

    print(url, token) {
        // 创建Form
        let form = $('<form></form>');
        // 设置属性
        form.attr('action', url);
        form.attr('method', 'get');
        // _self -> 当前页面 _blank -> 新页面
        form.attr('target', 'hideFrame');
        // 创建Input
        let qp = $('<input type="hidden" name="token" />');        
        qp.attr('value', token);
        // 附加到Form
        form.append(qp);
        form.appendTo("body")
        form.css('display', 'none')
        // 提交表单
        form.submit();
        form.remove();
      }

}