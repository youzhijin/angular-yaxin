import { Component, OnInit } from '@angular/core';
import { NzMessageService, NzModalService, UploadFile } from 'ng-zorro-antd';
import { EmitterService } from '../../../service/emit.service';
import { ProductService } from '../../../service/prod/product.service';
import {of} from "rxjs/index";
import {catchError} from "rxjs/operators";
import {syspjService} from "../../../service/syspj/syspj.service";

@Component({
    selector: 'product-sku-edit-v2',
    styleUrls: ['./product-sku-edit-v2.component.less'],
    templateUrl: './product-sku-edit-v2.component.html'
})


export class ProductSkuEditV2Component implements OnInit {
    message = "  ";
    attrList = [ '政治素质', '职业操守', '作风建设', '廉洁从业',
        '科学决策能力', '推动执行能力', '学习创新能力',
        '团队建设能力', '履职绩效', '协同成效'];
    attrListc = [ '政治素质', '职业操守', '作风建设', '廉洁从业',
    '科学决策能力', '推动执行能力', '学习创新能力',
    '团队建设能力', '履职绩效', '协同成效', '协同成效'];
    dataTable = [];
    uploadUrl = '';
    batch = [];


    constructor(
      private syspjService: syspjService,

      private msg: NzMessageService,
        private modalService: NzModalService,
        private emit: EmitterService,
        private productService: ProductService
    ) {

    }

    ngOnInit() {
        // this.uploadUrl = this.productService.uploadUrl();
        // 构建数据表 默认5行
        // this.dataTable = [["胡中朝","1","1","1","1","1","1","1","1","1","1"], ["胡中朝"], ["胡中朝"], ["胡中朝"], ["胡中朝"]];
        this.batch = [];


    }



    // mergeStyleColor() {
    //     for (let row = 0; row < this.dataTable.length; row++) {
    //         // if (this.dataTable[row][1] || this.dataTable[row][2]) {
    //         //     this.dataTable[row][3] = ((this.dataTable[row][1]+"") + (this.dataTable[row][2] + "")).replace('/', '');
    //         // }
    //     }
    // }

    append(rwocount) {
        if (this.dataTable.length >= 10) {
            return;
        }
        for(let row = 0;row<=rwocount;row++){
          this.dataTable.push([]);
        }



    }

    batchEdit(index) {
        for (let row = 0; row < this.dataTable.length; row++) {
          // if(this.dataTable[row][index+1]===""){
            this.dataTable[row][index+2] = this.batch[index];

          // }
        }
    }




  pasteExcel(row, col, e) {
    let CHAR = '\r\n';
    if ("Windows" !== this.getOS()) {
      CHAR = '\n';
    }
    for (let item of e.clipboardData.items) {
      if (item.type === 'text/plain') {
        item.getAsString((text) => {
          let x = col,
            y = row,
            obj = {};
          text = text.trim(CHAR);
          text.split(CHAR).forEach((v2, i2) => {
            v2.split('\t').forEach((v3, i3) => {
              let newRow = y + i2, newCol = x + i3;
              if (this.dataTable[newRow]) {
                this.dataTable[newRow][newCol] = v3;
              }

            })
          })
        })
      }
    }
  }


    getOS() {
        var userAgent = window.navigator.userAgent,
            platform = window.navigator.platform,
            macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
            windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
            iosPlatforms = ['iPhone', 'iPad', 'iPod'],
            os = null;

        if (macosPlatforms.indexOf(platform) !== -1) {
          os = 'Mac OS';
        } else if (iosPlatforms.indexOf(platform) !== -1) {
          os = 'iOS';
        } else if (windowsPlatforms.indexOf(platform) !== -1) {
          os = 'Windows';
        } else if (/Android/.test(userAgent)) {
          os = 'Android';
        } else if (!os && /Linux/.test(platform)) {
          os = 'Linux';
        }

        return os;
    }
}
