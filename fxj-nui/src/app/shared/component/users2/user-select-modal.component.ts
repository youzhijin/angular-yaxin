import { Component, Input, Output, EventEmitter, ViewChild, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { DepartService } from '../../../service/depart/depart.service';
import { NzTreeComponent, NzModalRef, NzMessageService, NzTreeNode, NzFormatEmitEvent } from 'ng-zorro-antd';
import { ModalHelper, _HttpClient } from '@delon/theme';
import { Subject, Observable } from 'rxjs';
import { UserService } from '../../../service/user/user.service';
import { SimpleTableComponent, SimpleTableColumn } from '@delon/abc';
import { SysUserService } from '../../../service/sys/sys-user.service';
declare var _: any;

@Component({
    selector: 'users-select-v2',
    templateUrl: './user-select-modal-v2.component.html'
})
export class UserSelectModalV2Component implements OnInit {        
    private subject = new Subject();
    ps = 10;
    q: any = {   
        isAppManager: '0',                  
        name: ''    
    };
    url = this.userService.list()    
    @ViewChild('st') st: SimpleTableComponent;
    data = [];

    columns: SimpleTableColumn[] = [
        { title: '', index: 'key', type: 'checkbox' },
        { title: '姓名', index: 'name' },        
        { title: '账户', index: 'username'},            
        { title: '性别', index: 'sex' },        
        { title: '备注', index: 'description' }          
    ];
    
    constructor(
        private modal: NzModalRef,        
        public msg: NzMessageService, 
        private modalSrv: ModalHelper,
        private userService: SysUserService,                
        private http: _HttpClient
    ) {        
        
    }

    ngOnInit() {
        
    }

    checkChange(list) {
        this.data = list.map(v => {
            return v.id;
        });
    }
    

    close() {
        this.modal.close(true);
        this.modal.destroy();
    }


    save() {        
        this.subject.next(this.data.toString());
        this.close();        
    }

    getSubject() {
        return this.subject;
    }
}