import { Component, Input, Output, EventEmitter, ViewChild, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { DepartService } from '../../../service/depart/depart.service';
import { NzTreeComponent, NzModalRef, NzMessageService, NzTreeNode, NzFormatEmitEvent } from 'ng-zorro-antd';
import { ModalHelper, _HttpClient, SettingsService } from '@delon/theme';
import { Subject, Observable } from 'rxjs';
import { UserService } from '../../../service/user/user.service';
import { SimpleTableComponent, SimpleTableColumn } from '@delon/abc';
declare var _: any;

@Component({
    selector: 'users-select',
    templateUrl: './user-select-modal.component.html'
})
export class UserSelectModalComponent implements OnInit {        
    private subject = new Subject();
    ps = 10;
    q: any = { 
        userLevel: null,
        noChild: true,                    
        name: ''    
    };
    url = this.userService.list2()    
    @ViewChild('st') st: SimpleTableComponent;
    data = [];
    @Input() selectLevel;
    @Input() lockLevel = false;
    @Input() multi = true;
    switchValue = false;
    
    types = [{text: '一级用户', value: 'FIRST'}, {text: '二级用户', value: 'SECOND'},  {text: '三级用户', value: 'THIRD'}]

    columns: SimpleTableColumn[] = [
        { title: '', index: 'key', type: this.multi ? 'checkbox' : 'radio' },
        { title: '名称', index: 'name' },        
        { title: '账户', index: 'username' },    
        { title: '级别', index: 'userLevel', render: 'level' },
        { title: '父级', index: 'bodyUsername' }
    ];
    
    constructor(
        private modal: NzModalRef,        
        public msg: NzMessageService, 
        private modalSrv: ModalHelper,
        private userService: UserService,                
        private http: _HttpClient,
        private settings: SettingsService
    ) {        
        
    }

    ngOnInit() {
        if (this.selectLevel) {
            this.q.userLevel = this.selectLevel;
        }
        if (this.lockLevel) {
            this.columns = [
                { title: '', index: 'key', type: this.multi ? 'checkbox' : 'radio' },
                { title: '名称', index: 'name' },        
                { title: '账户', index: 'username'}            
            ]; 
            if (!this.settings.user.isManager) {
                if (this.settings.user.bodyId) {
                    this.q.parentId = this.settings.user.bodyId;
                } else {
                    this.q.parentId = this.settings.user.userinfoId
                }                
            } else {
                this.q.parentId = "-1";
            }            
        }
    }

    checkChange(list) {       
        this.data = list.map(v => {
            let userInfo = JSON.parse(v.userId)
            return userInfo.id;
        });
    }

    radioChange(data) {
        this.data = [data];
    }
    

    close() {
        this.modal.close(true);
        this.modal.destroy();
    }


    save() {  
        if (this.data.length == 0) {
            this.msg.warning('Please select one');
            return;
        }      
        this.subject.next(this.data);
        this.close();        
    }

    getSubject() {
        return this.subject;
    }

    change(val) {        
        this.q.noChild = !val;
        this.st.load(1, this.q);
    }
}