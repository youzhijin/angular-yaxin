import { Component, OnInit, Output, Input } from '@angular/core';
import {Injectable,EventEmitter} from '@angular/core'; 
import { fromEvent } from 'rxjs';
import { NzModalService } from 'ng-zorro-antd';

declare var _: any;

@Component({
    selector: 'lxm-view',    
    templateUrl: './view.html',
    styleUrls: ['./view.less']
})
export class ViewComponent implements OnInit {
    innerWidth: number; // 屏幕宽度
    isPicture = false;
    isClose = false;
    rotea = ''
    num = 0;
    jiaodu = '';
    isDelete = false;
    
    @Input() fromParentData;
    @Input() imgUrl;
    @Input() img;

    pdfList : string[] = [];
    imgList : string[] = [];
    otherList : string[] = [];

    @Output() changeNumber: EventEmitter<number> = new EventEmitter(); 
    @Output() delete: EventEmitter<string> = new EventEmitter(); 

    subscribeScoll:any;
    scrollDis:any = {
        _top:0
    }
    
    constructor(
        private confirmServ: NzModalService,
    ) {}

    ngOnInit() {
        // console.log(this.fromParentData);
        // let getWindow = function(){
        //     return window.innerWidth;
        // };

        // window.onresize = function() {
        //     console.log(getWindow());
        // };
        // console.log(window.innerWidth);
        // console.log(window.innerHeight);
        this.subscribeScoll = fromEvent(window, 'scroll')
        .subscribe((event) => {
            this.onWindowScroll();//调用
        });
    }


    // 点击图片    
    checkBigImg(e,arg){
        this.isPicture = true;
        this.isDelete = true;
        console.log(this.isPicture);
        console.log(e);
        console.log(arg);
    }
    // 下载
    downloadImg(){
        console.log('下载');
        window.location.href = this.img;
    }
    download(item) {
        console.log(item);
        var path = item.src;
        window.location.href = path;
    }
    // 返回
    backoff(){
        console.log('返回');
        this.isPicture = false;
        // this.rotea = '';
        this.num = 0;
    }
    // 右转
    rotate(e){
        console.log('顺时针旋转');
        console.log(e);
        if((this.num)<5){
            this.num++;
            console.log(this.num);
            if((this.num) % 5 == 0){
                this.rotea = 'rotee';
            }
            if((this.num) % 5 == 1){
                this.rotea = 'rotea'
            }
            if((this.num) % 5 == 2){
                this.rotea = 'roteb'
            }
            if((this.num) % 5 == 3){
                this.rotea = 'rotec'
            }
            if((this.num) % 5 == 4){
                this.rotea = 'roted'
            }
        }
        
    }
    // 左转
    rotateA(){
        console.log('逆时针');
        if((this.num)>0){
            this.num--;
            console.log(this.num);
            if((this.num) % 5 == 0){
                this.rotea = 'rotee';
            }
            if((this.num) % 5 == 1){
                this.rotea = 'rotea';
            }
            if((this.num) % 5 == 2){
                this.rotea = 'roteb';
            }
            if((this.num) % 5 == 3){
                this.rotea = 'rotec';
            }
            if((this.num) % 5 == 4){
                this.rotea = 'rotec';
            }
        }
    }
    //删除
    deleteA() {
        this.confirmServ.confirm({
            nzTitle: `您确定要删除?`,
            nzOkText: '确定',
            nzCancelText: '取消',
            nzOnOk: () => {
                console.log('shanchu');
            }
        });
    }
    scollPostion() {
        var t, l, w, h;
        if (document.documentElement && document.documentElement.scrollTop) {
            t = document.documentElement.scrollTop;
            l = document.documentElement.scrollLeft;
            w = document.documentElement.scrollWidth;
            h = document.documentElement.scrollHeight;
        } else if (document.body) {
            t = document.body.scrollTop;
            l = document.body.scrollLeft;
            w = document.body.scrollWidth;
            h = document.body.scrollHeight;
        }
        return {
            top: t,
            left: l,
            width: w,
            height: h
        };
    }
    onWindowScroll(){
        this.scrollDis._top = this.scollPostion().top;
        console.log(this.scrollDis._top);
    }
    //
    getChangeImg(e,a){
        console.log(e);
        console.log(a);
        const file = e.srcElement.files[0]; // 获取图片这里只操作一张图片
        this.img = window.URL.createObjectURL(file); // 获取上传的图片临时路径
    }
    
    
}