import { Component, OnInit } from '@angular/core';
import {NzMessageService, NzModalService} from "ng-zorro-antd";
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ProductService } from '../../../service/prod/product.service';

@Component({
  selector: 'user-price-config-modal',
  templateUrl: './user-price-config-modal.component.html'
})
export class UserPriceConfigModalComponent implements OnInit {
    mode;
    calc = null;
    radioValue = null;
    selectedValue = 'PLUS';
    constructor() {}
    ngOnInit() {}

}