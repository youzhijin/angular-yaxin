export function Timeout(milliseconds: number = 0) {
    // target: 实例对象; key: 被修饰的方法名; descriptor: 对象描述, 同Object.getOwnPropertyDescriptor()
    return function(target, key, descriptor) {
        var orgMethod = descriptor.value;
        descriptor.value = function(...args) {
            setTimeout(() => {
                orgMethod.apply(this, args);
            }, milliseconds)
        }
        return descriptor;
    }
}