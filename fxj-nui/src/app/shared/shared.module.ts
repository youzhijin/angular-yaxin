import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
// delon
import { AlainThemeModule } from '@delon/theme';
import { DelonABCModule } from '@delon/abc';
import { DelonACLModule } from '@delon/acl';
import { DelonFormModule } from '@delon/form';
// i18n
import { TranslateModule } from '@ngx-translate/core';

// region: third libs
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { CountdownModule } from 'ngx-countdown';
import { UEditorModule } from 'ngx-ueditor';
import { NgxTinymceModule } from 'ngx-tinymce';
import { DepartsModalComponent } from '@shared/component/departs/departs-modal.component';
import { UserSelectModalComponent } from '@shared/component/users/user-select-modal.component';
import { AuthModalComponent } from '@shared/component/auth/auth-modal.component';
import { AuthorizeModalComponent } from '@shared/component/authorize/authorize-modal.component';
import { ProductSkuEditV2Component } from '@shared/component/product-sku-edit-v2/product-sku-edit-v2.component';
import { UserPriceConfigModalComponent } from '@shared/component/userprice/user-price-config-modal.component';
// color-sketch
import { ColorSketchModule } from 'ngx-color/sketch';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { UserSelectModalV2Component } from '@shared/component/users2/user-select-modal.component';
import { TimelineComponent } from '@shared/component/timeline/timeline';
import { ViewComponent } from '@shared/component/view/view';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

const THIRDMODULES = [
  NgZorroAntdModule,
  CountdownModule,
  UEditorModule,
  NgxTinymceModule,
  ColorSketchModule,
  InfiniteScrollModule,
  FontAwesomeModule
];
// endregion

// region: your componets & directives
const COMPONENTS = [
  DepartsModalComponent, 
  UserSelectModalComponent, 
  AuthModalComponent, 
  AuthorizeModalComponent, 
  ProductSkuEditV2Component, 
  UserPriceConfigModalComponent, 
  UserSelectModalV2Component, 
  ViewComponent,
  TimelineComponent];
const DIRECTIVES = [];
// endregion

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    AlainThemeModule.forChild(),
    DelonABCModule,
    DelonACLModule,
    DelonFormModule,
    // third libs
    ...THIRDMODULES,
  ],
  declarations: [
    // your components
    ...COMPONENTS,
    ...DIRECTIVES,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AlainThemeModule,
    DelonABCModule,
    DelonACLModule,
    DelonFormModule,
    // i18n
    TranslateModule,
    // third libs
    ...THIRDMODULES,
    // your components
    ...COMPONENTS,
    ...DIRECTIVES,
  ],
  entryComponents: [
    DepartsModalComponent,
    UserSelectModalComponent,
    AuthModalComponent, 
    AuthorizeModalComponent,
    ProductSkuEditV2Component,
    UserPriceConfigModalComponent,
    UserSelectModalV2Component
  ]
})
export class SharedModule {}
