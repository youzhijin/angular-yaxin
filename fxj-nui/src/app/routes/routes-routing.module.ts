import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { environment } from '@env/environment';

// layout
import { LayoutDefaultComponent } from '../layout/default/default.component';
import { LayoutFullScreenComponent } from '../layout/fullscreen/fullscreen.component';
import { LayoutPassportComponent } from '../layout/passport/passport.component';
import { UserLoginComponent } from './passport/login/login.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { HomeComponent } from './home/home.component';
import { CwComponent } from './cw/cw.component';
import { CwbiComponent } from './cwbi/cwbi.component';

import { LogComponent } from 'app/routes/changelog/log.component';
import {DashboardWorkplaceComponent} from "./dashboard/workplace/workplace.component";
import {DashboardV1Component} from "./dashboard/v1/monitor.component";
import {DashboardAnalysisComponent} from "./dashboard/analysis/analysis.component";
import {DashboardMonitorComponent} from "./dashboard/monitor/monitor.component";

// import {syspjAddComponent} from "./syspj/syspj-list/edit/syspj-add.component";
// import {home1Component} from "./home1/home1.component";



const routes: Routes = [
  {
    path: '',
    component: LayoutDefaultComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'cw/cw', component: CwComponent },
      { path: 'cwbi/cwbi', component: CwbiComponent },
      { path: 'dashboard', redirectTo: 'dashboard/v1', pathMatch: 'full' },
      { path: 'dashboard/v1', component: DashboardV1Component },
      { path: 'dashboard/analysis', component: DashboardAnalysisComponent },
      { path: 'dashboard/monitor', component: DashboardMonitorComponent },
      { path: 'dashboard/workplace', component: DashboardWorkplaceComponent },
      { path: 'log', component: LogComponent },
      { path: 'extras', loadChildren: './extras/extras.module#ExtrasModule' },
      { path: 'admin', canActivateChild: [AuthGuardService], loadChildren: './admin/admin.module#AdminModule' },
      { path: 'pl',  canActivateChild: [AuthGuardService], loadChildren: './pl/pl.module#plModule' },
      { path: 'plo',  canActivateChild: [AuthGuardService], loadChildren: './plo/plo.module#ploModule' },
      { path: 'plop',  canActivateChild: [AuthGuardService], loadChildren: './plop/plop.module#plopModule' },
      { path: 'ma',  canActivateChild: [AuthGuardService], loadChildren: './ma/ma.module#maModule' },
      { path: 'workbench',  canActivateChild: [AuthGuardService], loadChildren: './workbench/workbench.module#workbenchModule' },
      { path: 'yewu',  canActivateChild: [AuthGuardService], loadChildren: './yewu/yewu.module#yewuModule' },

      { path: 'promain',  canActivateChild: [AuthGuardService], loadChildren: './promain/promain.module#promainModule' },
      { path: 'pos',  canActivateChild: [AuthGuardService], loadChildren: './pos/pos.module#posModule' },
      // { path: 'orderMan', canActivateChild: [AuthGuardService], loadChildren: './orderMan/orderMan.module#orderManModule' },
      { path: 'prodList', canActivateChild: [AuthGuardService], loadChildren: './prodList/prodList.module#prodListModule' },

      { path: 'spList', canActivateChild: [AuthGuardService], loadChildren: './spList/spList.module#spListModule' },

      { path: 'jcbg', canActivateChild: [AuthGuardService], loadChildren: './jcbg/jcbg.module#jcbgModule' },

      { path: 'cspList', canActivateChild: [AuthGuardService], loadChildren: './csList/csList.module#csListModule' },
      { path: 'cszx', canActivateChild: [AuthGuardService], loadChildren: './csList/csList.module#csListModule' },
      { path: 'csgl', canActivateChild: [AuthGuardService], loadChildren: './csList/csList.module#csListModule' },
      { path: 'cscl', canActivateChild: [AuthGuardService], loadChildren: './csList/csList.module#csListModule' },
      { path: 'cstj', canActivateChild: [AuthGuardService], loadChildren: './csList/csList.module#csListModule' },

      { path: 'fk',  canActivateChild: [AuthGuardService], loadChildren: './fk/fk.module#fkModule' },
      { path: 'file',  canActivateChild: [AuthGuardService], loadChildren: './file/file.module#fileModule' },
      // { path: 'syspj',  canActivateChild: [AuthGuardService], loadChildren: './syspj/syspj.module#syspjModule' },
      { path: 'dhcar',  canActivateChild: [AuthGuardService], loadChildren: './dhcar/dhcar.module#dhcarModule' },
      { path: 'sd',  canActivateChild: [AuthGuardService], loadChildren: './sd/sd.module#sdModule' },

      { path: 'ins',  canActivateChild: [AuthGuardService], loadChildren: './ins/ins.module#insModule' },
      { path: 'prod',  canActivateChild: [AuthGuardService], loadChildren: './prod/prod.module#ProdModule' },
      { path: 'order', canActivateChild: [AuthGuardService], loadChildren: './order/order.module#OrderModule' },
      { path: 'pop', canActivateChild: [AuthGuardService], loadChildren: './pop/pop.module#PopModule' },
      { path: 'sys', canActivateChild: [AuthGuardService], loadChildren: './sys/sys.module#SysModule' },
      { path: 'warehouse', canActivateChild: [AuthGuardService], loadChildren: './warehouse/warehouse.module#WarehouseModule'},
      // { path: 'workplace', canActivateChild: [AuthGuardService], loadChildren: './workplace/workplace.module#WorkplaceModule'},
      { path: 'report', canActivateChild: [AuthGuardService], loadChildren: './report/report.module#ReportModule'},

      { path: 'notice', canActivateChild: [AuthGuardService], loadChildren: './notice/notice.module#noticeModule'},

      { path: 'repay', canActivateChild: [AuthGuardService], loadChildren: './repay/repay.module#repayModule'},
      { path: 'gps', canActivateChild: [AuthGuardService], loadChildren: './gps/gps.module#gpsModule'},
      { path: 'wfgl', canActivateChild: [AuthGuardService], loadChildren: './wfgl/wfgl.module#wfglModule'},
      { path: 'qcgl', canActivateChild: [AuthGuardService], loadChildren: './qcgl/qcgl.module#qcglModule'},
      { path: 'zcgl', canActivateChild: [AuthGuardService], loadChildren: './zcgl/zcgl.module#zcglModule'},
      { path: 'htjq', canActivateChild: [AuthGuardService], loadChildren: './htjq/htjq.module#htjqModule'},

      { path: 'xsjk', canActivateChild: [AuthGuardService], loadChildren: './xsjk/xsjk.module#xsjkModule'},
      { path: 'fxyj', canActivateChild: [AuthGuardService], loadChildren: './fxyj/fxyj.module#fxyjModule'},

      { path: 'cwgl', canActivateChild: [AuthGuardService], loadChildren: './cwgl/cwgl.module#cwglModule'},
      { path: 'dh', canActivateChild: [AuthGuardService], loadChildren: './dh/dh10.module#dh10Module'},
      { path: 'qylb', canActivateChild: [AuthGuardService], loadChildren: './qylb/qylb.module#qylbModule'},



    ],
  },
  // {
  //   path: 'workbench',
  //   component: LayoutFullScreenComponent,
  //   children: [
  //     { path: '', canActivateChild: [AuthGuardService], loadChildren: './workbench/workbench.module#workbenchModule' }
  //   ],
  // },
  {
    path: 'passport',
    component: LayoutPassportComponent,
    children: [
      {
        path: 'login',
        component: UserLoginComponent,
        data: { title: '登录', titleI18n: 'pro-login' },
      },
    ],
  },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: environment.useHash })],
  exports: [RouterModule],
})
export class RouteRoutingModule {}
