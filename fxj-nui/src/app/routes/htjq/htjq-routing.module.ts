import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { htjqListComponent } from './htjq-list/htjq-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { htjqViewEditComponent } from 'app/routes/htjq/htjq-list/edit/htjq-edit.component';

const routes: Routes = [
  {
    path: 'htjq',
    children: [
      { path: '', component: htjqListComponent },
      { path: 'edit/:id', component: htjqViewEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class htjqRoutingModule { }
