import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { htjqRoutingModule } from './htjq-routing.module';
import { htjqListComponent } from './htjq-list/htjq-list.component';
import { htjqViewEditComponent } from 'app/routes/htjq/htjq-list/edit/htjq-edit.component';

const COMPONENTS = [
  htjqListComponent
];
const COMPONENTS_NOROUNT = [
  htjqViewEditComponent
];

@NgModule({
  imports: [
    SharedModule,
    htjqRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class htjqModule { }
