import { Component, OnInit, ViewChild, Inject, Injector } from '@angular/core';
import { NzModalRef, NzMessageService } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { SFSchema, SFUISchema } from '@delon/form';
import { UserService } from '../../../../service/user/user.service';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { deepCopy } from '@delon/util';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { CommService } from '../../../../service/comm/comm.service';
import { ServiceResult, Constant } from '@core/constant';
import { SmsService } from '../../../../service/sms/sms.service';
import { SimpleTableComponent, SimpleTableColumn } from '@delon/abc';
import { DA_SERVICE_TOKEN, TokenService } from '@delon/auth';
import { BaseController } from '@shared/base-controller';
import { OrderService } from '../../../../service/order/order.service';


@Component({
  selector: 'delivery-view-modal',
  templateUrl: './delivery-view.component.html',
  styleUrls: ['./delivery-view.component.less'],
  providers: [TranslatePipe]
})
export class DeliveryViewModalComponent extends BaseController implements OnInit {
  exportUrl = "";
  filename = '';
  record: any = {};
  delivery: { [propName: string]: any } = { deliveryId: null, qty: 0, sendQty: 0, brand: null, deliveryDate: null, wf: 0 };
  loading = false;
  data = [];
  @ViewChild('st') st: SimpleTableComponent;
  isGz = true;
  gzBtn = false;
  exportBtn = false;
  printBtn = false;

  columns: SimpleTableColumn[] = [    
    { title: 'Style', index: 'STYLE'},
    { title: 'ColorCd', index: 'COLOR_CD' },
    { title: '发货数量', type: 'number', index: 'QTY', i18n: 'qty' },
    { title: '已发数量', type: 'number', index: 'SEND_QTY', i18n: 'sendQty' },
    { title: '未发数量', type: 'number', index: 'UNSEND_QTY', i18n: 'wf' },
  ];

  constructor(
      private sms: SmsService,
      public msg: NzMessageService,
      private modal: NzModalRef,
      private http: _HttpClient,
      private cnt: Constant,
      private orderServ: OrderService,
      @Inject(DA_SERVICE_TOKEN) private tokenService: TokenService,
      public injector: Injector
  ) {
      super(injector);
  }

  ngOnInit() {
      this.gzBtn = this.getAuths("sms_delivery_gz_btn");
      this.exportBtn = this.getAuths("sms_delivery_detail_export");
      this.printBtn = this.getAuths("sms_delivery_detail_print_btn");
      this.loadDelivery(this.record.deliveryId);
      this.loadDeliveryDetail(this.record.deliveryId);
      this.exportUrl = this.sms.exportDeliveryDetail(this.record.deliveryId);
      this.filename = "发货" + this.record.deliveryId + ".xls";
  }

  loadDelivery(id) {
    if (id) {
        this.sms.getDelivery(id).pipe(
            catchError(err => {
                return of(err)
            })
        ).subscribe(data => {
            if (data.status === 200) {
                this.delivery = data.data; 
                if (this.delivery.status == '3') {
                    this.isGz = true;
                } else {
                    this.isGz = false;
                }
            } else {
                this.msg.error(data.message);
            }
        })
    }
  }

  loadDeliveryDetail(id) {
    if (id) {
        this.loading = true;
        this.sms.getDeliveryDetail(id).pipe(
            catchError(err => {
                return of(err)
            })
        ).subscribe(data => {
            this.loading = false;
            if (data.status === 200) {
                this.data = data.data.rows;
            } else {
                this.msg.error(data.message);
            }
        })
    }
  }

  close() {
    this.modal.close(true);
    this.modal.destroy();
  }

  print() {
    super.print(`${this.cnt.baseUrl}/api/sms/log/delivery/print/detail/${this.record.deliveryId}`, 
        this.tokenService.get().token);
  }

  deliveryGz() {
      this.loading = true;
      this.orderServ.deliveryGz(this.record.deliveryId).pipe(
          catchError(err => {
              return of(err)
          })
      ).subscribe(data => {
          this.loading = false;
          if (data.status === 200) {
              this.msg.success('Posting successful');
              this.isGz = true;
          } else {
              this.msg.error('Posting failed:' + data.message);
          }
      })
  }
}