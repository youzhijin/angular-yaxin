import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WarehouseDeliveryOrderComponent } from './delivery-order/delivery-order.component';

const routes: Routes = [
  { path: 'delivery', component: WarehouseDeliveryOrderComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WarehouseRoutingModule { }
