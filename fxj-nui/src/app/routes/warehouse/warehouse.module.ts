import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { WarehouseRoutingModule } from './warehouse-routing.module';
import { WarehouseDeliveryOrderComponent } from './delivery-order/delivery-order.component';
import { DeliveryViewModalComponent } from 'app/routes/warehouse/delivery-order/delivery-view/delivery-view.component';
import { PipeModule } from 'app/pipe/pipe.module';

const COMPONENTS = [
  WarehouseDeliveryOrderComponent
];
const COMPONENTS_NOROUNT = [
  DeliveryViewModalComponent
];

@NgModule({
  imports: [
    SharedModule,
    PipeModule,
    WarehouseRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class WarehouseModule { }
