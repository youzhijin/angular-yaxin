import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { SFSchema } from '@delon/form';
import { StockoutService } from '../../../service/order/stockout.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import {CommService} from "../../../service/comm/comm.service";

@Component({
  selector: 'report-sdjqxyj',
  templateUrl: './sdjqxyj.component.html',
})
export class sdjqxyjComponent implements OnInit, OnDestroy {
  inter;
  syncing = false;
  lastDate = null
  checkedList = [];

  ps = 10;
  q: any = {
    bitype: 'sdjqxyj',
    cusName: '',
    cusMobile: '',
    cusIncard: '',
    orderNo: ''
  };
  url = this.comm.getbiUrl();
  data = [];
  columns: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: 'CARVIN', index: 'fxjx1' },
    { title: '车主', index: 'fxjx2' },
    { title: '车牌', index: 'fxjx3' },
    { title: '到期日', index: 'fxjx4' },
    { title: '交强险过期天数', index: 'fxjx5' }

  ];
  @ViewChild('st') st: SimpleTableComponent;


  constructor(
    private comm: CommService,

    private stockoutSku: StockoutService,
    private modal: ModalHelper
  ) { }

  ngOnInit() {

  }

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

}
