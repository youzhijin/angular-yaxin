import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { SFSchema } from '@delon/form';
import { StockoutService } from '../../../service/order/stockout.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import {CommService} from "../../../service/comm/comm.service";

@Component({
  selector: 'ficonj-ficonjyj',
  templateUrl: './ficonjyj.component.html',
})
export class ficonjyjComponent implements OnInit, OnDestroy {
  inter;
  syncing = false;
  lastDate = null
  checkedList = [];

  ps = 10;
  q: any = {
    bitype: 'ficonj',
    cusName: '',
    cusMobile: '',
    cusIncard: '',
    orderNo: ''
  };
  url = this.comm.getbiUrl();
  data = [];
  columns: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '客户姓名', index: 'fxjx1' },
    { title: '订单编号', index: 'fxjx2' },
    { title: '订单信息', index: 'fxjx3' },
    { title: '年检日期', index: 'fxjx4' },

    { title: '逾期天数', index: 'fxjx5' }

  ];
  @ViewChild('st') st: SimpleTableComponent;


  constructor(
    private comm: CommService,

    private stockoutSku: StockoutService,
    private modal: ModalHelper
  ) { }

  ngOnInit() {

  }

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

}
