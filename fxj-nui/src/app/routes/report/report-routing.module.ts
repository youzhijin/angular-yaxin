import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportStockoutskuComponent } from './stockoutsku/stockoutsku.component';
import {OrderlistComponent} from "../report/orderlist/orderlist.component";
import {fklistComponent} from "../report/fklist/fklist.component";
import {yqlistComponent} from "./yqlist/yqlist.component";
import {cpxstjlistComponent} from "./cpxstjlist/cpxstjlist.component";
import {dddklistComponent} from "./dddklist/dddklist.component";
import {fktjlistComponent} from "./fktjlist/fktjlist.component";
import {hktjlistComponent} from "./hktjlist/hktjlist.component";
import {sdbxyjComponent} from "./sdbxyj/sdbxyj.component";
import {sdbyyjComponent} from "./sdbyyj/sdbyyj.component";
import {sdjqxyjComponent} from "./sdjqxyj/sdjqxyj.component";
import {sdlqyjComponent} from "./sdlqyj/sdlqyj.component";
import {ficobxyjComponent} from "./ficobxyj/ficobxyj.component";
import {ficonjyjComponent} from "./ficonjyj/ficonjyj.component";
import {ficoyqyjComponent} from "./ficoyqyj/ficoyqyj.component";

const routes: Routes = [
  { path: 'stockoutsku', component: ReportStockoutskuComponent },
  { path: 'orderlist', component: OrderlistComponent },
  { path: 'fklist', component: fklistComponent },
  { path: 'cpxstjlist', component: cpxstjlistComponent },

  { path: 'dddklist', component: dddklistComponent },

  { path: 'fktjlist', component: fktjlistComponent },

  { path: 'hktjlist', component: hktjlistComponent },
  { path: 'yqlist', component: yqlistComponent},

  { path: 'sdbxyj', component: sdbxyjComponent},
  { path: 'sdbyyj', component: sdbyyjComponent},
  { path: 'sdjqxyj', component: sdjqxyjComponent},
  { path: 'sdlqyj', component: sdlqyjComponent},
  { path: 'ficobxyj', component: ficobxyjComponent},
  { path: 'ficonjyj', component: ficonjyjComponent},
  { path: 'ficoyqyj', component: ficoyqyjComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
