import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { SFSchema } from '@delon/form';
import { StockoutService } from '../../../service/order/stockout.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'report-stockoutsku',
  templateUrl: './stockoutsku.component.html',
})
export class ReportStockoutskuComponent implements OnInit, OnDestroy {
  inter;
  syncing = false;
  lastDate = null
  checkedList = [];

  ps = 10;
  q: any = {
    sap: null
  };
  url = this.stockoutSku.list();
  data = [];
  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: 'SAP', index: 'sap' },
    { title: '订单数量', index: 'qty'},
    { title: '库存', index: 'stock' },
    { title: '缺额', index: 'ed', render: 'ed' }
  ];
  @ViewChild('st') st: SimpleTableComponent;


  constructor(
    private stockoutSku: StockoutService,
    private modal: ModalHelper
  ) { }

  ngOnInit() {

  }

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }







}
