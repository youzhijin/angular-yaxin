import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { ReportRoutingModule } from './report-routing.module';
import {OrderlistComponent} from "./orderlist/orderlist.component";
import {fklistComponent} from "./fklist/fklist.component";

import {ReportStockoutskuComponent} from "./stockoutsku/stockoutsku.component";
import {cpxstjlistComponent} from "./cpxstjlist/cpxstjlist.component";
import {dddklistComponent} from "./dddklist/dddklist.component";
import {fktjlistComponent} from "./fktjlist/fktjlist.component";
import {hktjlistComponent} from "./hktjlist/hktjlist.component";
import {yqlistComponent} from "./yqlist/yqlist.component";
import {ficonjyjComponent} from "./ficonjyj/ficonjyj.component";
import {ficoyqyjComponent} from "./ficoyqyj/ficoyqyj.component";
import {sdjqxyjComponent} from "./sdjqxyj/sdjqxyj.component";
import {sdbyyjComponent} from "./sdbyyj/sdbyyj.component";
import {sdbxyjComponent} from "./sdbxyj/sdbxyj.component";
import {ficobxyjComponent} from "./ficobxyj/ficobxyj.component";
import {sdlqyjComponent} from "./sdlqyj/sdlqyj.component";

const COMPONENTS = [
  OrderlistComponent,
  fklistComponent,
  cpxstjlistComponent,
  dddklistComponent,
  fktjlistComponent,
  hktjlistComponent,
  yqlistComponent,
    sdbxyjComponent,
    sdbyyjComponent,
   sdjqxyjComponent,
    sdlqyjComponent,
    ficobxyjComponent,
  ficonjyjComponent,
    ficoyqyjComponent,
  ReportStockoutskuComponent
];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [
    SharedModule,
    ReportRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class ReportModule { }
