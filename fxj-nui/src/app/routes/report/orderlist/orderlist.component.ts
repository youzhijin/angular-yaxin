import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { SFSchema } from '@delon/form';
import { StockoutService } from '../../../service/order/stockout.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import {CommService} from "../../../service/comm/comm.service";

@Component({
  selector: 'report-orderlist',
  templateUrl: './orderlist.component.html',
})
export class OrderlistComponent implements OnInit, OnDestroy {
  inter;
  syncing = false;
  lastDate = null
  checkedList = [];

  ps = 10;
  q: any = {
    bitype: 'orderdetail',
    cusName: '',
    cusMobile: '',
    cusIncard: '',
    orderNo: ''
  };
  url = this.comm.getbiUrl();
  data = [];
  columns: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '订单编号', index: 'fxjx1' },
    { title: '客户姓名', index: 'fxjx2' },
    { title: '订单类型', index: 'fxjx3' },
    { title: '联系电话', index: 'fxjx4' },
    { title: '车辆信息', index: 'fxjx5' },
    { title: '期数', index: 'fxjx6' },
    { title: '评估价', index: 'fxjx7' },
    { title: '还款方式', index: 'fxjx8' },
    { title: '录入业务员', index: 'fxjx9' },
    { title: '产品种类', index: 'fxjx10' },
    { title: '录入组织', index: 'fxjx11' },
    { title: '订单提交时间', index: 'fxjx12' },
    { title: '订单状态', index: 'fxjx13' }

  ];
  @ViewChild('st') st: SimpleTableComponent;


  constructor(
    private comm: CommService,

    private stockoutSku: StockoutService,
    private modal: ModalHelper
  ) { }

  ngOnInit() {

  }

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

}
