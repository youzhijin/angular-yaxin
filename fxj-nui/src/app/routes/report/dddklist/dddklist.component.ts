import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { SFSchema } from '@delon/form';
import { StockoutService } from '../../../service/order/stockout.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import {CommService} from "../../../service/comm/comm.service";

@Component({
  selector: 'report-dddklist',
  templateUrl: './dddklist.component.html',
})
export class dddklistComponent implements OnInit, OnDestroy {
  inter;
  syncing = false;
  lastDate = null
  checkedList = [];

  ps = 10;
  q: any = {
    bitype: 'dddk',
    cusName: '',
    cusMobile: '',
    cusIncard: '',
    orderNo: ''
  };
  url = this.comm.getbiUrl();
  data = [];
  columns: SimpleTableColumn[] = [
    { title: '订单编号', index: 'fxjx1' },
    { title: '客户姓名', index: 'fxjx2' },
    { title: '订单类型', index: 'fxjx3' },
    { title: '联系电话', index: 'fxjx4' },
    { title: '车辆信息', index: 'fxjx5' },
    { title: '期数', index: 'fxjx6' },
    { title: '垫款日期', index: 'fxjx7' },
    { title: '垫款金额', index: 'fxjx8' },
    { title: '还款日期', index: 'fxjx9' }

  ];
  @ViewChild('st') st: SimpleTableComponent;


  constructor(
    private comm: CommService,

    private stockoutSku: StockoutService,
    private modal: ModalHelper
  ) { }

  ngOnInit() {

  }

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

}
