import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { maService } from '../../../../service/ma/ma.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {insService} from "../../../../service/ins/ins.service";
import {ProductService} from "../../../../service/prod/product.service";
import {dhcarListComponent} from "../dhcar-list.component";
import {SimpleTableColumn, SimpleTableComponent} from "@delon/abc";

@Component({
  selector: 'dhcar-detail-edit',
  templateUrl: './dhcar-detail.component.html',
  styleUrls: ['./dhcar-detail.component.less']
})
export class dhcardetailViewEditComponent implements OnInit, OnDestroy {
  product:  any = {
    id:null,
    carNo: null,
    frameno:null
  };

  checkedList = [];

  @ViewChild('st') st: SimpleTableComponent;
  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox',exported:false },

    // { title: '订单编号', index: 'orderNo' },
    { title: '车牌号 ', index: 'carNo'},
    // { title: '车架号 ', index: 'frameno' },
    // { title: '发动机号', index: 'engineno' },

    { title: '违章时间 ', index: 'time'},
    { title: '违章地点 ', index: 'address' },
    { title: '违章行为', index: 'content' },
    { title: '违章扣分 ', index: 'score'},
    { title: '违章罚款 ', index: 'price' },
    { title: '违章城市', index: 'csmc' }
    // { title: '城市代码 ', index: 'csdm' },
    // { title: '违章代码', index: 'wzid' },
    // { title: '查询批号', index: 'queryOrderno' }

  ];
  loading = false;
  constructor(
    private productService: ProductService,

    public setting: SettingsService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private maService: maService,
    private emit: EmitterService,
    private notice: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService
  ) {}
  ps=10;
  q: any = {
    carNo: '',
    frameno:''
  };
  private sub: any;
url="";
  ngOnInit(): void {
    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"];
      // 获取商品详情
              this.url=this.maService.getwzdetailurl(this.product.id);
    });
    // 隐藏reusetab
    setTimeout(() => {
      this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);
    this.titleService.setTitle("详情显示")

  }

  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }
  back() {
    this.router.navigate(['/dhcar/dhcar']);
  }
  checkboxChange(list: any[]) {
    this.checkedList = list;
  }
  exportdetail() {
    if (this.checkedList.length > 0) {
      console.log("1*******");
      this.st.export(this.checkedList, { filename: '查询汇总.xlsx' });
    } else {
      this.notice.create('error', '失败', '请选择进行导出');

    }
  }
}
