import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { maService } from '../../../../service/ma/ma.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {insService} from "../../../../service/ins/ins.service";
import {ProductService} from "../../../../service/prod/product.service";
import {dhcarListComponent} from "../dhcar-list.component";
import {SimpleTableColumn, SimpleTableComponent} from "@delon/abc";

@Component({
  selector: 'dhcar-view-edit',
  templateUrl: './dhcar-edit.component.html',
  styleUrls: ['./dhcar-edit.component.less']
})
export class dhcarViewEditComponent implements OnInit, OnDestroy {
  product:  any = {
    fkname:"do360wz",
    lsnum: '闽',
    frameno:null,
    engineno: null,
    cartype:null
  };

  resulthead:any = {

    code:null,
    charge: null,
    msg:null
  };
  resultdetail:any = {
      code:null,
      msg:null,
      cph:null,
      wzts:null,
      wzfkhj:null,
      wzjfhj:null
  };
wzdata=[];
  @ViewChild('st') st: SimpleTableComponent;

  column: SimpleTableColumn[] = [
    // { title: '订单编号', index: 'orderNo' },
    { title: '违章时间 ', index: 'wzsj'},
    { title: '违章地点 ', index: 'wzdd' },
    { title: '违章行为', index: 'wzxw' },
    { title: '违章扣分 ', index: 'wzjf'},
    { title: '违章罚款 ', index: 'wzfk' },
    { title: '违章城市', index: 'csmc' },
    { title: '城市代码 ', index: 'csdm' },
    { title: '违章代码', index: 'wfdm' }

  ];
  loading = false;
  fknames = [{ text: '服务器1', value: 'do360wz' }, { text: '服务器2', value: 'doclwz' }];
  constructor(
    private productService: ProductService,

    public setting: SettingsService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private maService: maService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService
  ) {}
  q: any = {
    bpmStatus:"",
    cusName: '',
    cusMobile: '',
    cusIncard: '',
    proNo: '',
    orderNo:''
  };
  ngOnInit(): void {
    // 隐藏reusetab
    setTimeout(() => {
      this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 1000);
  }

  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }
  back() {
    this.router.navigate(['/dhcar/dhcar']);
  }
  wzquery() {
    if(this.product.fkname===''||this.product.fkname===null ){
      this.msg.warning("请选择服务器");
      return;
    }
    if(this.product.lsnum===''||this.product.lsnum===null){
      this.msg.warning("请输入车牌号");
      return;
    }
    if(this.product.frameno===''||this.product.frameno===null){
      this.msg.warning("请输入车架号");
      return;
    }
    if(this.product.engineno===''||this.product.engineno===null){
      this.msg.warning("请输入发动机号");
      return;
    }
    // if(this.product.cusMobile===''||this.product.cusMobile===null){
    //   this.msg.warning("请输入电话");
    //   return;
    // }

    let fkinput = {
      createBy:this.setting.user.username,//openid
      lsnum:this.product.lsnum,// 车牌号
      frameno:this.product.frameno,//车架号
      engineno:this.product.engineno,//发动机号
      shiietype:this.product.cartype//默认02
    };
    this.loading = true;

    this.productService.rmquery(this.product.fkname,fkinput).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.loading = false;

      console.log("data.ok"+data.ok);
      if (data.ok === true) {
        console.log("data******"+JSON.stringify(data));

this.product.lsnum=null;
        this.product.frameno=null;

        this.product.engineno=null;

        this.wzdata = data.data.result.data;
        console.log("wzdata******"+JSON.stringify(this.wzdata));
        this.st.load(1, this.q);
        //
        this.resulthead = data.data;
        console.log("resulthead******"+JSON.stringify(this.resulthead));
        //
        this.resultdetail = data.data.result;
        console.log("resultdetail******"+JSON.stringify(this.resultdetail));

        this.msg.success('成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })
  }



}
