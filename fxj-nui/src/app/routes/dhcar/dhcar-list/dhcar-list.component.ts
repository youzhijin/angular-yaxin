import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { maService } from '../../../service/ma/ma.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';




@Component({
  selector: 'dhcar-list',
  templateUrl: './dhcar-list.component.html',
  styleUrls: ['./dhcar-list.component.less']
})
export class dhcarListComponent implements OnInit, OnDestroy {
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    impFile:"",

    fiName: '',
    cusMobile: '',
    cusIncard: '',
    orderNo: ''
  };

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.maService.listdhcar();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  seasons = [];
  checkedList = [];
  data = [];
  downurl='https://zhaodui.oss-cn-shenzhen.aliyuncs.com/doc/fxbbcartempv1.xls';

  uploadExcelUrl = "";

  uploadDateUrl = this.maService.batchRepdate();




  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    // { title: '订单编号', index: 'orderNo' },
    { title: '查询人', index: 'createBy'},
    { title: '导入文件名', index: 'impFile' },
    { title: '查询批号', index: 'id' },
    { title: '导入条数' , index: 'impCount' },
    // { title: '附件', index: 'fiAtta' },
    { title: '已查询条数', index: 'queryCount' },
    {
        title: '操作',
        buttons: [
            {
              text: '汇总显示',
              type: 'link',
              click: (record: any) => this.router.navigate(['/dhcar/dhcar/queryhead/'+record.id])
            },
          {
            text: '详情显示',
            type: 'link',
                click: (record: any) => this.router.navigate(['/dhcar/dhcar/querydetail/'+record.id])
          }
        ]
    }
  ];

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private maService: maService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  inter
  ngOnInit() {
    this.uploadExcelUrl = this.maService.uploadcarinfo();
    this.imgSuffix = this.comm.getGoodsUrl();

    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })


  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }

  gotoone(){
    this.router.navigate(['/dhcar/dhcar/queryone']);

  }
  exportPop(){


    if(this.checkedList.length>1){
      this.notice.create('error', '失败', '只能选择一批进行查询');
return;
    }

    if(this.checkedList.length<1){
      this.notice.create('error', '失败', '请选择一批进行查询');
      return;
    }
    this.loading = true;
    console.log("this.checkedList[0].id"+this.checkedList[0].id);

    /////

    this.maService.getwzqueryg(this.checkedList[0].id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {

      if (data.ok === true) {

        this.msg.success("数据已经提交后台，请点击查询按钮刷新");
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })


    /////


    this.st.load();

    this.loading = false;

  }
  uploadProd(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
    }
    if (info.file.status === 'done') {
      this.loading = false;
      this.st.load();
      this.notice.create('success', '成功', '车辆信息导入成功');
    }
    if (info.file.status === 'error') {
      this.loading = false;
      this.notice.create('error', '失败', '车辆信息导入失败');
    }
  }
  checkboxChange(list: any[]) {
    this.checkedList = list;
  }


  close(): void {
    this.visible = false;
  }



  cancel() {
    this.repdate = null;
  }
  repdate = null;



}
