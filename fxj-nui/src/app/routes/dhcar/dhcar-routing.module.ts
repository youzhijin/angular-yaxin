import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import {dhcarListComponent} from "./dhcar-list/dhcar-list.component";
import {dhcarViewEditComponent} from "./dhcar-list/edit/dhcar-edit.component";
import {dhcarheadViewEditComponent} from "./dhcar-list/edit/dhcar-head.component";
import {dhcardetailViewEditComponent} from "./dhcar-list/edit/dhcar-detail.component";

const routes: Routes = [
  {
    path: 'dhcar',
    children: [
      { path: '', component: dhcarListComponent },
      { path: 'queryhead/:id', component: dhcarheadViewEditComponent },
      { path: 'querydetail/:id', component: dhcardetailViewEditComponent },

      { path: 'queryone', component: dhcarViewEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class dhcarRoutingModule { }
