import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { dhcarRoutingModule } from './dhcar-routing.module';
import { dhcarListComponent } from './dhcar-list/dhcar-list.component';
import { dhcarViewEditComponent } from 'app/routes/dhcar/dhcar-list/edit/dhcar-edit.component';
import {dhcarheadViewEditComponent} from "./dhcar-list/edit/dhcar-head.component";
import {dhcardetailViewEditComponent} from "./dhcar-list/edit/dhcar-detail.component";

const COMPONENTS = [
  dhcarListComponent
];
const COMPONENTS_NOROUNT = [
  dhcarViewEditComponent,
  dhcarheadViewEditComponent,
  dhcardetailViewEditComponent

];

@NgModule({
  imports: [
    SharedModule,
    dhcarRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class dhcarModule { }
