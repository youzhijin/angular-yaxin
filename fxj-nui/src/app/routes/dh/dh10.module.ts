import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { dh10RoutingModule } from './dh10-routing.module';
import { dh10ListComponent } from './dh10-list/dh10-list.component';
//import { noticeViewEditComponent } from 'app/routes/notice/notice-list/edit/notice-edit.component';

const COMPONENTS = [
  dh10ListComponent
];
const COMPONENTS_NOROUNT = [
  //noticeViewEditComponent
];

@NgModule({
  imports: [
    SharedModule,
    dh10RoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class dh10Module { }
