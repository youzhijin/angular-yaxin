import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { dh10ListComponent } from './dh10-list/dh10-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
//import { noticeViewEditComponent } from 'app/routes/notice/notice-list/edit/notice-edit.component';

const routes: Routes = [
  {
    path: 'dh10',
    children: [
      { path: '', component: dh10ListComponent }
     ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class dh10RoutingModule { }
