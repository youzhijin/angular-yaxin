import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import {ActivatedRoute, Router} from '@angular/router';
import { noticeService } from '../../../service/notice/notice.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';
import {formatDate} from "@angular/common";
import {dh10Service} from "../../../service/dh10/dh10.service";




@Component({
  selector: 'dh10-list',
  templateUrl: './dh10-list.component.html',
  styleUrls: ['./dh10-list.component.less']
})
export class dh10ListComponent implements OnInit, OnDestroy {
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {

    fxjOut01: '',
    fxjOut02: '',
    dateRange: null,
    startDate:'',
    endDate:''

    // cusIncard: '',
    // orderNo: ''
  };

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.dh10Service.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  seasons = [];
  checkedList = [];
  data = [];

  // uploadUrl = this.maService.uploadProds();
  // uploadDateUrl = this.noticeService.batchRepdate();
  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '提醒日期', index: 'fxjOut11' },
    { title: '订单编号', index: 'fxjOut12' },
     { title: '姓名', index: 'fxjOut01'},
    { title: '电话', index: 'fxjOut02'},
    { title: '客户处理结果', index: 'fxjOut03'},
    { title: '月供逾期', index: 'fxjOut04'},
    { title: '保险逾期', index: 'fxjOut05'},
    { title: '年检逾期', index: 'fxjOut06'},
    { title: '违章分数', index: 'fxjOut07'},
    { title: '违章金额', index: 'fxjOut08' },
    { title: '商业保险', index: 'fxjOut09' },
    { title: '交强险', index: 'fxjOut10' },
    {
        title: '操作',
        buttons: [
            {
              text: '详情',
              type: 'link',
              click: (record: any) => this.router.navigate(['/prod/product/dhview/'+record.fxjOut12])
            },
      {
        text: '处理',
        type: 'link',
        click: (record: any) => this.router.navigate(['/notice/notice/query/'+record.fxjOut12])
      }
        ]
    }
  ];

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private dh10Service: dh10Service,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
    private route: ActivatedRoute,

  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }
  private sub: any;

  inter
  ngOnInit() {
    this.imgSuffix = this.comm.getGoodsUrl();

    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      let varinpur = params["id"];
      if(varinpur){
        this.q.orderNo = params["id"];
      }
    });

    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })

    // 隐藏reusetab
    setTimeout(() => {
      this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);
  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }

  back() {
    this.router.navigate(['/prod/product/80']);
  }
  change(args: NzTabChangeEvent) {
    switch (args.index) {
      case 0:
        this.q.ntType = "";
        break;
      case 1:
        this.q.ntType = "路桥";
        break;
      case 2:
        this.q.ntType = "保险";
        break;
      case 3:
        this.q.ntType = "保养";
        break;
      case 4:
        this.q.ntType = "还款";
        break;

      default:
        break;
    }
  this.st.load(1, this.q);
}




  checkboxChange(list: any[]) {
    this.checkedList = list;
  }


  close(): void {
    this.visible = false;
  }



  cancel() {
    this.repdate = null;
  }
  repdate = null;

  dateChange(result: Date[]): void {
    if (result.length !== 0) {
      this.q.startDate = formatDate(result[0], 'yyyy-MM-dd', 'en-US');
      this.q.endDate = formatDate(result[1], 'yyyy-MM-dd', 'en-US');
    } else {
      this.q.startDate = '';
      this.q.endDate = '';
    }
  }

}
