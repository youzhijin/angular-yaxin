import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { ProductService } from '../../../../service/prod/product.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {plService} from "../../../../service/pl/pl.service";

@Component({
  selector: 'replan-view-yuqi',
  templateUrl: './replan-yuqi.component.html',
  styleUrls: ['./replan-yuqi.component.less']
})
export class plViewYuqiComponent implements OnInit, OnDestroy {
  product:  any = {
    id:null,
    orderNo: null,
    bpmStatus:null,
    mStatus:null,
    rchStatus:null,
    orderInfo: null,
    cusName: null,
    cusMobile:null,
    refPeriod: null,
    refTotalPeriod: null /*No show*/,
    refDate: null,
    refFbenjin: null,
    refAmount: null,
    refLixi: null,
    refSybenjin: null,
    reMark: null,
    payAmount: null
  };
  // 预览图片地址
  bpmStatus =  [{text: '未还款', value: '1'}, {text: '已还款', value: '10'}];
  mStatus =  [{text: 'M1', value: 'M1'}, {text: 'M2', value: 'M2'}, {text: 'M3', value: 'M3'}, {text: 'M4', value: 'M4'}, {text: 'M5', value: 'M5'}];
  rchStatus =  [{text: '人车失联', value: '1'}, {text: '人失联', value: '10'}, {text: '车失联', value: '20'}];
  picUrl = '';
  // 图片上传地址
  uploadUrl = '';


  MAX_COUNT = 60;
  tabIndex = 0;
  saleing = false;
  private sub: any;

  constructor(
    public setting: SettingsService,

    private msg: NzMessageService,
    private modalService: NzModalService,
    private plService: plService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService
  ) {}

  ngOnInit(): void {

    // this.loadbpmStatus();


    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"];
      // 获取商品详情
      this.getProductInfo();
    });
    // 隐藏reusetab
    setTimeout(() => {
        this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);
    this.titleService.setTitle("编辑产品");


    
  }

  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  changeTab(index) {
    // 使用异步的方式更新组件, 防止ngIf错误
    Promise.resolve(null).then(() => this.tabIndex = index);
  }



  getProductInfo() {
    this.plService.getProduct(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      // console.log('数据==='+JSON.stringify(data.data))
      this.product = data.data;

    })
  }

  loadbpmStatus() {
    this.bpmStatus = [];
    this.comm.getType("bpm_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.bpmStatus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  beforeUpload = (file: File) => {
      const isJPG = file.type === 'image/jpeg';
      const isPNG = file.type === 'image/png';
      if (!isJPG && !isPNG) {
          this.msg.warning(`不支持的格式: ${file.type}`);
      }
      const isLt2M = file.size / 1024 / 1024 < 2;
      if (!isLt2M) {
          this.msg.warning('图片不能超过2MB!');
      }
      return (isJPG || isPNG) && isLt2M;
  }

  private getBase64(img: File, callback: (img: any) => void) {
      if (img) {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
      }
  }

  handleChange(row, col, info: { file: UploadFile }) {
      if (info.file.status === 'uploading') {
          return;
      }
      if (info.file.status === 'done') {
          let that = this;
          let f = info;
          this.getBase64(info.file.originFileObj, (img: any) => {
            that.picUrl = img;
            that.product.img =  f.file.response.rows[0].id;
          });
      }
      if (info.file.status === 'error') {
          this.msg.error(info.file.error.error);
      }
  }

  formatterYuan = value => `¥ ${value}`;
  parserYuan = value => value.replace('¥', '');

  formatterDollor = value => `${value}`;
  parserDollor = value => value.replace('$', '');

  back() {
    this.router.navigate(['/prod/product/80']);
  }
  save() {
    let fxjPlReplan = {
      updateBy: this.setting.user.username,
      // updateName:this.setting.user.username,
      // sysorgCode:this.setting.user.sysorgCode,
      id: this.product.id,
      bpmStatus: this.product.bpmStatus,
      // updateDate: new Date().getTime(),
      fxjOut11: this.product.mStatus,
      fxjOut12: this.product.rchStatus,
      fxjOut13: this.product.orderInfo,
      fxjOut14: this.product.cusName,
      fxjOut15: this.product.cusMobile,
      fxjOut16: this.product.refPeriod,
      fxjOut17: this.product.refTotalPeriod /*No show*/,
      fxjOut18: this.product.refDate,
      fxjOut19: this.product.refFbenjin,
      fxjOut20: this.product.refAmount,
      fxjOut21: this.product.refLixi,
      fxjOut22: this.product.refSybenjin,
      fxjOut23: this.product.reMark,
      fxjOut24: this.product.payAmount,
      fxjOut25: this.product.orderNo
    };
    // console.log(fxjPlReplan);
    // 调用接口
    this.plService.yuqiPro(fxjPlReplan).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if (data.ok === true) {
        this.msg.success('保存成功');
        this.back();
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data['message']);
      }
    })
  }

  changeColor(res) {
    this.product.colorSort = res.color.hex;
  }
}
