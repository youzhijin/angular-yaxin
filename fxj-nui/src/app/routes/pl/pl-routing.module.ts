import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { plListComponent } from './replan-list/replan-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
// import { plAddComponent } from 'app/routes/pl/replan-list/edit/replan-add.component';
import { plViewEditComponent } from 'app/routes/pl/replan-list/edit/replan-edit.component';

import { plViewYuqiComponent } from 'app/routes/pl/replan-list/edit/replan-yuqi.component';
import { GoodsImgUploadComponent } from './goods-upload/goods-img-upload.component';

const routes: Routes = [
  {
    path: 'replan',
    children: [
      { path: '', component: plListComponent },
      { path: 'query/:id', component: plListComponent },

      // { path: 'add', component: plAddComponent },
      { path: 'edit/:id', component: plViewEditComponent },
      { path: 'yuqi/:id', component: plViewYuqiComponent },
      { path: 'upload', component: GoodsImgUploadComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class plRoutingModule { }
