import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { plRoutingModule } from './pl-routing.module';
import { plListComponent } from './replan-list/replan-list.component';
// import { plAddComponent } from 'app/routes/pl/replan-list/edit/replan-add.component';
import { plViewEditComponent } from 'app/routes/pl/replan-list/edit/replan-edit.component';
import { plViewYuqiComponent } from 'app/routes/pl/replan-list/edit/replan-yuqi.component';
import { GoodsImgUploadComponent } from './goods-upload/goods-img-upload.component';

const COMPONENTS = [
  plListComponent,
  GoodsImgUploadComponent
];
const COMPONENTS_NOROUNT = [
  // plAddComponent,
  plViewEditComponent,
  plViewYuqiComponent
];

@NgModule({
  imports: [
    SharedModule,
    plRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class plModule { }
