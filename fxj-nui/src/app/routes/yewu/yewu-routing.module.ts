import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { yewuListComponent } from './yewu-list/yewu-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import {yewuEditComponent} from './yewu-list/edit/yewu-edit.component';
import {yewurmComponent} from './yewu-list/edit/yewu-rm.component';

const routes: Routes = [
  {
    path: 'yewu',
    children: [
      { path: '', component: yewuListComponent },
      { path: 'edit/:id', component: yewuEditComponent },
      { path: 'rm/:id', component: yewurmComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class yewuRoutingModule { }
