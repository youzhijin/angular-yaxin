import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { yewuRoutingModule } from './yewu-routing.module';
import { yewuListComponent } from './yewu-list/yewu-list.component';
import { yewuEditComponent } from './yewu-list/edit/yewu-edit.component';
import {yewurmComponent} from './yewu-list/edit/yewu-rm.component';

const COMPONENTS = [
  yewuListComponent
];
const COMPONENTS_NOROUNT = [
  yewuEditComponent,
  yewurmComponent
];

@NgModule({
  imports: [
    SharedModule,
    yewuRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class yewuModule { }
