import { NzMessageService } from 'ng-zorro-antd';
import {Component, OnInit, ChangeDetectorRef, Injector, ViewChild} from '@angular/core';
import {_HttpClient, SettingsService, TitleService} from '@delon/theme';
import { LoginService } from '../../service/login.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Router } from '@angular/router';

import {ReuseTabService, SimpleTableColumn, SimpleTableComponent} from '@delon/abc';
import { getTimeDistance, yuan } from '@delon/util';
import {ProductService} from "../../service/prod/product.service";
import {workbenchService} from "../../service/workbench/workbench.service";
import {fkService} from "../../service/fk/fk.service";
import {Constant} from "@core/constant";
import {CommService} from "../../service/comm/comm.service";
import {formatDate} from "@angular/common";

@Component({
    selector: 'cw-home',
    templateUrl: './cw.component.html',
    styleUrls: ['./cw.component.less']
})
export class CwComponent implements OnInit {

  inter;
  home:any={
    fxjOut11:"0",
    fxjOut12:"0",
    fxjOut13:"0",
    fxjOut14:"0",
    fxjOut15:"0",
    fxjOut16:"0",
    fxjOut17:"0",
    fxjOut18:"0",
    fxjOut19:"0",
  };
  checkedList = [];
  @ViewChild('st') st: SimpleTableComponent;
  urlpro =this.productService.list0();
  // urlpro = this.comm.getbiUrl();

  // @ViewChild('st2') st2: SimpleTableComponent;
  // urlloan=this.fkService.list();
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    fxjOut01:"",
    fxjOut02: "",
    fxjOut03: "",
    fxjOut48: "",
    fxjOut49: "",
    // fxjOut06: "",
    // fxjOut07: "",
    // fxjOut08: null,//日期范围
    // fxjOut09:'',//开始时间
    // fxjOut10:''//结束时间
  };

  // loading = true;

  columns: SimpleTableColumn[] = [

    {
      title: '操作',
      buttons: [
        // {
        //   text: '审批',
        //   type: 'link',
        //   click: (record: any) => this.router.navigate([record.wbUrl+record.id]),
        //   iif: (item: any, btn: SimpleTableButton, column: SimpleTableColumn) => {
        //     return item.bpmStatus === '1'
        //   }
        // },
        {
          text: '查看',
          type: 'link',
          click: (record: any) => this.router.navigate(['/prod/product/cwcheck/'+record.id])
        }                                                                                                                                                                                                                                                                                                                          
      ]
    } ,
    //  { title: '', index: 'key', type: 'checkbox',exported:false },
     { title: '供应商', index: 'fxjOut01',width:500 },
     { title: '所属区域', index: 'fxjOut02' },
    { title: '车型', index: 'fxjOut03' },
    { title: '下车单日期', index: 'fxjOut04' },
    { title: '车牌号', index: 'fxjOut05' },
    { title: '车架号', index: 'fxjOut06' },
    { title: '日期', index: 'fxjOut07' },
    { title: '车身价', index: 'fxjOut08' },
    { title: '购置税', index: 'fxjOut09' },
    { title: '商业险', index: 'fxjOut10' },
    { title: '交强险', index: 'fxjOut11'},
    { title: '车船税', index: 'fxjOut12' },
   { title: '第二年商业险', index: 'fxjOut13' },
   { title: '第二年交强险', index: 'fxjOut14' },
   { title: '第二年车船税', index: 'fxjOut15' },
   { title: '第三年商业险', index: 'fxjOut16' },
   { title: '第三年交强险', index: 'fxjOut17' },
   { title: '第三年车船税', index: 'fxjOut18' },
   { title: '承运险', index: 'fxjOut19' },
   { title: '驾意险', index: 'fxjOut20' },
    { title: '上牌费', index: 'fxjOut21'},
    { title: '拖车费', index: 'fxjOut22' },
   { title: '牌照快递费', index: 'fxjOut23' },
   { title: '处理违章费用', index: 'fxjOut24' },
   { title: '洗车费', index: 'fxjOut25' },
   { title: '停车费', index: 'fxjOut26' },
   { title: '保证保险', index: 'fxjOut27' },
   { title: '人身意外险', index: 'fxjOut28' },
   { title: '变更行驶证', index: 'fxjOut29' },
   { title: '底盘装甲', index: 'fxjOut30' },
    { title: '装饰费', index: 'fxjOut31'},
    { title: '营业证', index: 'fxjOut32' },
   { title: '北斗GPS', index: 'fxjOut33' },
   { title: '风控GPS', index: 'fxjOut34' },
   { title: 'GPS安装费', index: 'fxjOut35' },
   { title: '车辆检测费', index: 'fxjOut36' },
   { title: '返点佣金', index: 'fxjOut37' },
   { title: '销售奖励', index: 'fxjOut38' },
   { title: '车行返点', index: 'fxjOut39' },
   { title: '运输证费用', index: 'fxjOut40' },
    { title: '洗车身照片', index: 'fxjOut41'},
    { title: '总支出费用', index: 'fxjOut42' },
   { title: '总售价', index: 'fxjOut43' },
   { title: '首付订金', index: 'fxjOut44' },
   { title: '尾款', index: 'fxjOut45' },
   { title: '车辆维修', index: 'fxjOut46' },
   { title: '营利', index: 'fxjOut47' },
   { title: '销售方式', index: 'fxjOut48' },
   { title: '业务人员', index: 'fxjOut49' },
   { title: '备注', index: 'fxjOut50' },
     { title: '回款月份', index: 'fxjOut51' },
     { title: '全款回收期', index: 'fxjOut52' },
    //  { title: '代理模式', index: 'orderBy1' },
    //  { title: '客户姓名 ', index: 'cusName'},
    //  { title: '身份证号 ', index: 'cusIncard' },
    //  { title: '联系电话', index: 'cusMobile' },
    //  { title: '联系地址 ', index: 'cusResAddr' },
    //  { title: '车牌', index: 'orderBy61' },
    //  { title: '品牌型号', index: 'orderBy62' },
    //  { title: '车身颜色', index: 'orderBy37' },
    //  { title: '所属公司', index: 'orderBy35' },
    //  { title: '合同金额 ', index: 'orderBy48' },
    //  { title: '申请期数 ', index: 'orderPeriod' },
    //  { title: '录入业务员  ' , index: 'qiandanUser' },
     { title: '订单提交时间', index: 'createDate' },
    //  { title: '审批人员', index: 'updateBy' }
   ];
  // columns: SimpleTableColumn[] = [
  //   // { title: '', index: 'key', type: 'checkbox' },
  //   { title: '订单编号', index: 'fxjx1' },
  //   { title: '客户姓名', index: 'fxjx2' },
  //   { title: '订单类型', index: 'fxjx3' },
  //   { title: '联系电话', index: 'fxjx4' },
  //   { title: '公司', index: 'fxjx11' },
  //   { title: '订单提交时间', index: 'fxjx12' },
  //   { title: '订单状态', index: 'fxjx13' },
  //   { title: '放款金额' , index: 'fxjx14' },
  //   { title: '逾期金额', index: 'fxjx14' },
  //   { title: '保险金额', index: 'fxjx14' },
  //   { title: 'GPS费用', index: 'fxjx14' },
  //   { title: '利润', index: 'fxjx14' },
  //   { title: '资金成本', index: 'fxjx14' },
  //   { title: '佣金', index: 'fxjx14' },
  // ];
  // uploadDateUrl = this.productService.batchRepdate();

  constructor(
    private comm: CommService,

    private http: _HttpClient,
    public msg: NzMessageService,
    private cd: ChangeDetectorRef,
    private productService: ProductService,
    private fkService: fkService,
    private title: TitleService,
    public setting: SettingsService,
    private router: Router,

  ) {}
  ngOnInit() {
    // this.inter = setInterval(() => {
    // }, 10000);
    this.geiCw();

  }
  list=[];
  geiCw(){
    // this.productService.getbiCwUrl(this.q.fxjOut09,this.q.fxjOut10).pipe(catchError(err => {
      this.productService.getCwUrl(this.q.fxjOut01,this.q.fxjOut02,this.q.fxjOut03,this.q.fxjOut48,this.q.fxjOut49).pipe(catchError(err => {
      return of(err)
    })).subscribe(data => {
      if(data.ok){
        // this.loading = false;
        if(data.data.length){
          this.list = data.data;
          this.st.load(1, this.q);
          // this.home.fxjOut11=data.data[0].fxjOut11;
          // this.home.fxjOut12=data.data[0].fxjOut12;
          // this.home.fxjOut13=data.data[0].fxjOut13;
          // this.home.fxjOut14=data.data[0].fxjOut14;
          // this.home.fxjOut15=data.data[0].fxjOut15;
          // this.home.fxjOut16=data.data[0].fxjOut16;
          // this.home.fxjOut17=data.data[0].fxjOut17;
          // this.home.fxjOut18=data.data[0].fxjOut18;
          // this.home.fxjOut19=data.data[0].fxjOut19;
        }
      }
    });
  }

  reloadBi(){
    // this.loading = false;
    // this.loading = true;
    this.geiCw();
    this.st.load(1, this.q);

  }

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }



  _activeTab = 0;
  _tabChange(value: any) {}

  dateChange(result: Date[]): void {
    if (result.length !== 0) {
      this.q.fxjOut09 = formatDate(result[0], 'yyyy-MM-dd', 'en-US');
      this.q.fxjOut10 = formatDate(result[1], 'yyyy-MM-dd', 'en-US');
    } else {
      this.q.fxjOut09 = '';
      this.q.fxjOut10 = '';
    }
  }

}
