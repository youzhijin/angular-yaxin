import { NgModule } from '@angular/core';

import { SharedModule } from '@shared/shared.module';
import { RouteRoutingModule } from './routes-routing.module';
import { UserLoginComponent } from './passport/login/login.component';
import { HomeComponent } from './home/home.component';
import { CwComponent } from './cw/cw.component';
import { CwbiComponent } from './cwbi/cwbi.component';
import { LogComponent } from 'app/routes/changelog/log.component';
import {DashboardWorkplaceComponent} from "./dashboard/workplace/workplace.component";
import {DashboardMonitorComponent} from "./dashboard/monitor/monitor.component";
import {DashboardAnalysisComponent} from "./dashboard/analysis/analysis.component";
import {DashboardV1Component} from "./dashboard/v1/monitor.component";


const COMPONENTS = [


  DashboardV1Component,
  DashboardAnalysisComponent,
  DashboardMonitorComponent,
  DashboardWorkplaceComponent,

  UserLoginComponent,

  HomeComponent,
  CwComponent,
  CwbiComponent,

  LogComponent
];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [SharedModule, RouteRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT
})
export class RoutesModule {}
