import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { ExtrasRoutesModule } from './extras-routes.module';
import { SettingComponent } from './setting/setting.component';


const COMPONENT_NOROUNT = [];

@NgModule({
  imports: [
    SharedModule,
    ExtrasRoutesModule
  ],
  declarations: [
    SettingComponent,
    ...COMPONENT_NOROUNT
  ],
  entryComponents: COMPONENT_NOROUNT
})
export class ExtrasModule { }
