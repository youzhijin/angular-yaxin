import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { PopRoutingModule } from './pop-routing.module';
import { PopOrderComponent } from './order/order.component';
import { PopListComponent } from './list/list.component';
import { PopListAddComponent } from './list/add/add.component';
import { PopProductComponent } from './product/product.component';
import { PopCartComponent } from './cart/cart.component';
import { PopOrderViewComponent } from './order/view/view.component';
import { PopMyOrderComponent } from './my-order/my-order.component';
import { PipeModule } from '../../pipe/pipe.module';
import { PopMyOrderViewComponent } from './my-order/view/view.component';
import { PopPopsComponent } from './pops/pops.component';
import { PopPopsAddPopItemComponent } from './pops/edit/add-pop-item.component';
import { PopPopsListComponent } from './pops/list/list.component';

const COMPONENTS = [
  PopOrderComponent,
  PopListComponent,
  PopListAddComponent,
  PopProductComponent,
  PopCartComponent,
  PopOrderViewComponent,
  PopMyOrderComponent,
  PopMyOrderViewComponent,
  PopPopsComponent,
  PopPopsAddPopItemComponent,
  PopPopsListComponent,
];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [SharedModule, PopRoutingModule, PipeModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class PopModule {}
