import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PopOrderComponent } from './order/order.component';
import { PopListComponent } from './list/list.component';

import { PopListAddComponent } from './list/add/add.component';
import { PopProductComponent } from './product/product.component';
import { PopCartComponent } from './cart/cart.component';
import { PopOrderViewComponent } from './order/view/view.component';
import { PopMyOrderComponent } from './my-order/my-order.component';
import { PopMyOrderViewComponent } from './my-order/view/view.component';
import { PopPopsComponent } from './pops/pops.component';
import { PopPopsAddPopItemComponent } from './pops/edit/add-pop-item.component';
import { PopPopsListComponent } from './pops/list/list.component';
const routes: Routes = [
  {
    path: 'list',
    children: [
      { path: '', component: PopListComponent },
      { path: 'add', component: PopListAddComponent },
      { path: 'edit/:id', component: PopListAddComponent },
      {
        path: 'pops',
        children: [
          { path: '', component: PopPopsComponent },
          { path: 'list', component: PopPopsListComponent },
          { path: 'add', component: PopPopsAddPopItemComponent },
          { path: 'edit/:id', component: PopPopsAddPopItemComponent },
        ],
      },
    ],
  },
  {
    path: 'order',
    children: [
      { path: '', component: PopOrderComponent },
      { path: 'view/:billId', component: PopOrderViewComponent },
    ],
  },
  {
    path: 'myorder',
    children: [
      { path: '', component: PopMyOrderComponent },
      { path: 'view/:billId', component: PopMyOrderViewComponent },
    ],
  },
  { path: 'product', component: PopProductComponent },
  { path: 'cart', component: PopCartComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PopRoutingModule {}
