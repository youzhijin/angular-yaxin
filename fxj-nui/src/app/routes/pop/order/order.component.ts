import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient, ModalHelper, SettingsService } from '@delon/theme';

import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { ProductService } from '../../../service/prod/product.service';
import { Router } from '@angular/router';
import { PopService } from '../../../service/pop/pop.service';
import { EmitterService } from '../../../service/emit.service';

import { UserSelectModalComponent } from '@shared/component/users/user-select-modal.component';
import { NzModalService } from 'ng-zorro-antd';
import { ServiceResult } from '@core/constant';
import { TranslateService } from '@ngx-translate/core';
import { CommService } from '../../../service/comm/comm.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'pop-order',
  templateUrl: './order.component.html',
})
export class PopOrderComponent implements OnInit {
  record: any = {};
  url = this.popService.list();
  ps = 10;

  // statuss = [
  //   { text: 'Pending', value: 'NOCHECK' },
  //   { text: 'normal', value: 'CHECKED' },
  //   { text: 'Canceled', value: 'CANCEL' },
  // ];

  // 搜索表单
  q: any = {
    mode: 'other',
    billCd: null,
    username: null,
    brand: null,
    status: null,
    dateRange: null,
  };
  brands = [];
  loading = false;

  @ViewChild('st') st: SimpleTableComponent;
  columns: SimpleTableColumn[] = [
    { title: '订单号', index: 'billId', i18n: 'order_number' },
    { title: '创建日期', index: 'createDate', i18n: 'createDate' },
    { title: '代理商', index: 'username', i18n: 'agent' },
    // { title: '品牌', index: 'brand', i18n: 'brand', render: 'brand' },
    { title: '数量', index: 'totalQty', i18n: 'qty' }, // 此处qty为订单数量
    { title: 'EXW', index: 'totalExw' },
    { title: 'CIF', index: 'totalCif' },

    {
      title: '状态',
      index: 'billStatus',
      type: 'badge',
      i18n: 'status',
      // filters: this.statuss,
      // filterMultiple: false,
      // filter: () => true,
      // filterConfirmText: 'Ok',
      // filterClearText: 'Reset',
      badge: {
        NOCHECK: { text: 'Pending', color: 'processing' },
        CHECKED: { text: 'Confirmed', color: 'success' },
        CANCEL: { text: 'Canceled', color: 'error' },
      },
    },
    {
      title: '操作',
      i18n: 'operator',
      buttons: [
        {
          text: '明细',
          i18n: 'view',
          type: 'link',

          click: (record: any) => {
            this.router.navigate(['/pop/order/view/' + record.billId]);
          },
        },
      ],
    },
  ];

  constructor(
    public http: _HttpClient,
    private modal: ModalHelper,
    private router: Router,
    private productService: ProductService,
    private emit: EmitterService,
    private popService: PopService,
    private setting: SettingsService,
    private modalService: NzModalService,
    private trans: TranslateService,
    private comm: CommService,
  ) {}

  ngOnInit() {
    this.loadBrands();

    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_ORDERS_LIST') {
        this.st.reload(1, this.q);
      }
    });
    this.st.load(1, this.q);
  }

  _onReuseInit() {
    this.st.load(1, this.q);
  }

  // 加载品牌列表
  loadBrands() {
    const lang = this.trans.currentLang || this.trans.defaultLang;
    this.brands = [];
    this.comm.getBrand().subscribe(data => {
      const ret: ServiceResult = <ServiceResult>data;
      if (ret.status === 200) {
        for (const row of ret['data']['rows']) {
          this.brands.push({
            text: lang === 'en' ? row.labelEnUs : row.labelDefault,
            value: row.value,
          });
        }
      }
    });
  }

  dateChange(result: Date[]): void {
    if (result.length !== 0) {
      this.q.startDate = formatDate(result[0], 'yyyy-MM-dd', 'en-US');
      this.q.endDate = formatDate(result[1], 'yyyy-MM-dd', 'en-US');
    } else {
      this.q.startDate = '';
      this.q.endDate = '';
    }
  }

  showUserSelect() {
    let level = 'FIRST';
    if (this.setting.user.isManager) {
      level = 'FIRST';
    } else if (this.setting.user.level === 'FIRST') {
      level = 'SECOND';
    } else if (this.setting.user.level === 'SECOND') {
      level = 'THIRD';
    }
    const modal = this.modalService.create({
      nzContent: UserSelectModalComponent,
      nzComponentParams: { selectLevel: level, lockLevel: true, multi: false },

      nzFooter: null,
    });
    modal.afterOpen.subscribe(() => {
      modal
        .getContentComponent()
        .getSubject()
        .subscribe(data => {
          const users = data as Array<any>;
          if (users && users.length !== 0) {
            this.q.username = users[0].username;
          }
        });
    });
  }
}
