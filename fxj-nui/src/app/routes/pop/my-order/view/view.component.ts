import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  _HttpClient,
  ModalHelper,
  SettingsService,
  TitleService,
} from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { SFSchema } from '@delon/form';
import { ActivatedRoute, Router } from '@angular/router';
import { CommService } from '../../../../service/comm/comm.service';
import { EmitterService } from '../../../../service/emit.service';
import { OrderService } from '../../../../service/order/order.service';
import {
  NzMessageService,
  NzModalService,
  NzNotificationService,
} from 'ng-zorro-antd';
import { ProductService } from '../../../../service/prod/product.service';
import { Constant } from '@core/constant';
import { of } from 'rxjs/index';
import { catchError } from 'rxjs/operators';
import { PopService } from '../../../../service/pop/pop.service';

@Component({
  selector: 'pop-my-order-view',
  templateUrl: './view.component.html',
})
export class PopMyOrderViewComponent implements OnInit, OnDestroy {
  loading = false;
  private sub: any;
  private billId;
  public order: [{ [proname: string]: any }] = [{}];
  // list: {[proname: string]: any}[] = [];
  private totalQty = 0;
  private totalExwPrice = 0;
  private totalCifPrice = 0;
  imgSuffix = '';
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';

  @ViewChild('st') st: SimpleTableComponent;
  columns: SimpleTableColumn[] = [
    { title: '图片', index: 'img', i18n: 'img', render: 'img', width: '100px' },
    { title: 'SKU', index: 'sku', i18n: 'sku_number' },
    { title: '描述', index: 'description', i18n: 'description' },
    { title: '净重', index: 'netWeight', i18n: 'net_weight' },
    { title: '毛重', index: 'grossWeight', i18n: 'gross_weight' },
    { title: '数量', index: 'qty', i18n: 'qty' },
    { title: 'EXW', index: 'exw' },
    { title: 'TOTAL EXW', index: 'totalExw' },
    { title: 'CIF', index: 'cif' },
    { title: 'TOTAL CIF', index: 'totalCif' },
  ];

  constructor(
    private http: _HttpClient,
    private modal: ModalHelper,
    private route: ActivatedRoute,
    private popService: PopService,
    private emit: EmitterService,
    private titleService: TitleService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private router: Router,
    private comm: CommService,
  ) {}

  ngOnInit() {
    this.imgSuffix = this.comm.getGoodsUrl();
    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.billId = params['billId'];
      console.log(this.billId);
      this.info();
    });

    // 隐藏reusetab
    setTimeout(() => {
      this.emit.sendMessage('HIDE_REUSE_TAB');
    }, 100);
    this.titleService.setTitle('订单明细');
  }

  ngOnDestroy() {
    this.emit.sendMessage('SHOW_REUSE_TAB');
  }

  info() {
    this.popService
      .detail(this.billId)
      .pipe(
        catchError(err => {
          return of(err);
        }),
      )
      .subscribe(data => {
        if (data.status === 200) {
          this.order = data.data.rows;
          this.qtyCount();
          this.calcTotalPrice();
          // this.setOrderStep();
        } else {
          this.msg.error(data.message);
        }
      });
  }

  qtyCount() {
    this.totalQty = 0;
    for (let i = 0; i < this.order.length; i++) {
      this.totalQty += this.order[i].qty;
    }
  }

  calcTotalPrice() {
    this.totalExwPrice = 0;
    this.totalCifPrice = 0;
    for (let i = 0; i < this.order.length; i++) {
      this.totalExwPrice += this.order[i].totalExw;
      this.totalCifPrice += this.order[i].totalCif;
    }
  }
  back() {
    this.router.navigate(['/pop/myorder']);
  }

  showImage(src) {
    this.previewImage = src;
    this.previewVisible = true;
  }
}
