import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { csListRoutingModule } from './csList-routing.module';
import { csListComponent } from './csList-list/csList-list.component';
import { cspComponent } from './csList-list/csp-list.component';
import { csglComponent } from './csList-list/csgl-list.component';
import { csclComponent } from './csList-list/cscl-list.component';
import { cstjComponent } from './csList-list/cstj-list.component';

import { csListEditComponent } from 'app/routes/csList/csList-list/edit/csList-edit.component';
import { csListAddoneComponent } from 'app/routes/csList/csList-list/edit/csList-addone.component';


const COMPONENTS = [
  csListComponent,
  cspComponent,
  csglComponent,
  csclComponent,
  cstjComponent
];
const COMPONENTS_NOROUNT = [
  csListEditComponent,
  csListAddoneComponent
];

@NgModule({
  imports: [
    SharedModule,
    csListRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class csListModule { }
