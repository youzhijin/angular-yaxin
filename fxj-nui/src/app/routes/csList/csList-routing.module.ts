import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { csListComponent } from './csList-list/csList-list.component';

import { cspComponent } from './csList-list/csp-list.component';
import { csglComponent } from './csList-list/csgl-list.component';
import { csclComponent } from './csList-list/cscl-list.component';
import { cstjComponent } from './csList-list/cstj-list.component';

import { AuthGuardService } from '@core/auth/auth-guard.service';
import { csListEditComponent } from 'app/routes/csList/csList-list/edit/csList-edit.component';
import { csListAddoneComponent } from 'app/routes/csList/csList-list/edit/csList-addone.component';

const routes: Routes = [
  {
    path: 'cspList',
    children: [
      { path: '', component: csListComponent },
      { path: 'edit/:id', component: csListEditComponent },
      { path: 'add', component:csListAddoneComponent }
    ]
  },{
    path: 'cszx', component:cspComponent
  },{
    path: 'csgl', component:csglComponent
  },{
    path: 'cscl', component:csclComponent
  },{
    path: 'cstj', component:cstjComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class csListRoutingModule { }
