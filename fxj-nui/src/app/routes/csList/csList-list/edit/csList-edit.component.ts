// import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { maService } from '../../../../service/ma/ma.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import { TranslateService } from '@ngx-translate/core';
import { csListService } from "../../../../service/csList/csList.service";

import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';

@Component({
  selector: 'csList-view-edit',
  templateUrl: './csList-edit.component.html',
  styleUrls: ['./csList-edit.component.less']
})
export class csListEditComponent implements OnInit, OnDestroy {
  
  isfullscreen = true; //是否全屏显示
  onShow = 0;
  selectedIndex = 0;
  selectedIndexa = 0;
  smcsIndex = 0;
  
  changeIndex = 0;
  checked = true;

  percent = '20'
  color = "#ff1600"

  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;

  loanerInfo = {
    // 贷款人信息
    // cusName:'', //姓名
    cusName:'dsda', //姓名
    cusGender:'', //性别
    cusEthnical:'',//民族
    cusBirthday:'',//出生
    cusIdValidto:'',//有效期限

    issueOffice:'',//签发机关
    cusResAddr:'',//地址
    workUnit:'',//工作单位
    cusMobile:'',//联系电话
    spouseName:'',//配偶姓名
    spouseNumber:'',//配偶联系电话
    workAddress:'',//工作地址
    workNumber:'',//单位电话
    
    lxMobile1:'',//紧急联系人1
    lxMobile2:'',//紧急联系人2
  }
  loanInfo = {
    // 贷款信息
    orderNo:'00000000000001',//订单编号
    loanAmount:'￥150000.00',//贷款金额
    loanDbmonth:'￥16666.00',//利息金额

    kbje:'￥1500.00',//空白金额
    ajlx:'车抵贷',//案件类型

    yhbj:'￥100000.00',//已还本金
    qkje:'￥66666.00',//欠款金额
    jqje:'￥68266.00',//结清金额
    fxdj:'M2',//风险等级
    sybj:'￥50000.00',//剩余本金
    ljhk:'￥5000.00',//累计还款
    yhsq:'￥1500.00',//优惠申请
  }

  params = {
    
    // 共借人信息
    commonName:'',//姓名
    commonSex:'',//性别
    commonGation:'',//民族
    commonBirth:'',//出生
    commonPeriod:'',//有效期限
    commonOffice:'',//签发机关
    commonAddress:'',//地址
    commonRelative:'',//关系属性
    commonWorkUnit:'',//工作单位
    commonWorkNumber:'',//单位电话
    commonNumber:'',//联系电话
  }
  loanInfoShow = false;//借款人信息展开
  commonBorrowerShow = false;//共借人信息展开
  guarantorInfoShow = true;//担保人信息展开
  loanMaterialsShow = true;//贷款材料展开
  bodyPhotosShow = true;//车身照片

  // 还款记录
  repayment = {

  }
  hkmxbShow = true;//还款明细表展开
  hkmxbOfData = [
    {
      qs: '001',
      hkyf: '2018年01月',
      mqhke: '4420.80',
      yhbj: '3941.64',
      yhlx: '479.17',
      dkye: '96058.36',
      hkqk: '本期已还',
      sjhksj: '2018-04-20',
      sjhk: '4420.80',
      sybj: '0',
      znj: '0',
      csfk: '',
      csjl: ''
    },{
      qs: '002',
      hkyf: '2018年02月',
      mqhke: '4420.80',
      yhbj: '3941.64',
      yhlx: '479.17',
      dkye: '96058.36',
      hkqk: '本期已还',
      sjhksj: '2018-04-20',
      sjhk: '4420.80',
      sybj: '0',
      znj: '0',
      csfk: '',
      csjl: ''
    },
  ]
  
  // 催收操作
  // 贷款人联系电话
  dxcsbOfData = [
    {
      xuhao:'01',
      dhhm:'13800000001',
      lxr:'彭于晏',
      yjkrgx:'本人',
      gxsj:'2019-01-01 12:01:01',
      hfbz:'明天一定准时还款'
    },{
      xuhao:'02',
      dhhm:'13800000001',
      lxr:'彭于晏',
      yjkrgx:'朋友',
      gxsj:'2019-01-01 12:01:01',
      hfbz:'会提醒对方及时还款'
    },{
      xuhao:'03',
      dhhm:'13800000001',
      lxr:'彭于晏',
      yjkrgx:'父母',
      gxsj:'2019-01-01 12:01:01',
      hfbz:'承诺下周一替对方还款5000元'
    },
  ]
  dxthxdbOfData = [
    {
      xuhao:'01',
      dhhm:'13800000168',
      gsd:'深圳',
      yjkrgx:'同事',
      gxsj:'2019-01-01 12:01:01',
      hfbz:'明天一定准时还款'
    },{
      xuhao:'02',
      dhhm:'13800000141',
      gsd:'湖南',
      yjkrgx:'朋友',
      gxsj:'2019-01-02 13:31:41',
      hfbz:'会提醒对方及时还款'
    },{
      xuhao:'03',
      dhhm:'13800014591',
      gsd:'泉州',
      yjkrgx:'同事',
      gxsj:'2019-01-01 12:01:01',
      hfbz:'会提醒对方及时还款'
    }
  ]
  loanInfoShowa = false;
  loanInfoShowb = false;
  commonBorrowerShowa = false;
  
  // 催收记录
  csjlbOfData = [
    {
      id:'001',
      xuhao:'1',
      cslx:'电话催收',
      cssj:'2019-01-31 09:30',
      csry:'彭于晏',
      lxr:'彭于晏1',
      dhhm:'13500000001',
      yjkrgx:'本人',
      beizhu:'明天一定准时还款',
      csdz:'1',
      cspj:'查看'
    },{
      id:'002',
      xuhao:'2',
      cslx:'电话催收',
      cssj:'2019-01-31 19:30',
      csry:'彭于晏',
      lxr:'吴彦祖',
      dhhm:'13500000001',
      yjkrgx:'本人',
      beizhu:'明天一定准时还款',
      csdz:'2',
      cspj:'查看'
    },
    {
      id:'003',
      xuhao:'3',
      cslx:'电话催收',
      cssj:'2019-01-31 19:30',
      csry:'彭于晏',
      lxr:'吴彦祖',
      dhhm:'13500000001',
      yjkrgx:'本人',
      beizhu:'明天一定准时还款',
      csdz:'3',
      cspj:'查看'
    },
    {
      id:'004',
      xuhao:'4',
      cslx:'电话催收',
      cssj:'2019-01-31 19:30',
      csry:'彭于晏',
      lxr:'吴彦祖',
      dhhm:'13500000001',
      yjkrgx:'本人',
      beizhu:'明天一定准时还款',
      csdz:'4',
      cspj:'查看'
    },
  ]
  // 录音
  csjlLy(id){
    console.log("催收记录录音"+id);
  }
  // 播放
  csjlBf(id){
    console.log("催收记录播放"+id);
  }
  // 查看
  csjlView(id){
    console.log("催收记录查看"+id);
  }
  // 搜索表单
  // q: any = {
  //   bpmStatus:"",

  //   fiName: '',
  //   cusMobile: '',
  //   cusIncard: '',
  //   orderNo: ''
  // };

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  // url = this.csListService.list();
  // region end

  // change(args) {
  //   console.log(args.index);
  //   this.changeIndex = args.index
  // }

  changea(args) {
    this.onShow = args.index;
  }
  changeb(args) {
    this.selectedIndex = args.index;
  }
  changec(args) {
    this.selectedIndexa = args.index;
  }
  changed(args) {
    this.smcsIndex = args.index;
  }

  // checkboxchange(args){
  //   console.log(args);
  // }

  private sub: any;

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private maService: maService,
    private emit: EmitterService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NzModalService,
    private csListService: csListService,
    private notice: NzNotificationService,
  ) {}
  ngOnInit(): void {
    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      console.log(params["id"])
      // 获取商品详情
      this.getProductInfo(params["id"]);
      this.getLoanInfo(params["id"])
    });
  }
  

  ngOnDestroy() {
    // this.emit.sendMessage("SHOW_REUSE_TAB");
  }
  // 贷款人信息
  getProductInfo(id) {
    this.csListService.getprodetail(id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.loanerInfo = data.data;
      console.log(this.loanerInfo)
    })
  }
  // 贷款信息
  getLoanInfo(id) {
    this.csListService.getLoanInfo(id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.loanInfo = data.data;
      console.log(this.loanInfo);
    })
  }








  back() {
//     this.router.navigate(['/csList/csList']);
  }




}
