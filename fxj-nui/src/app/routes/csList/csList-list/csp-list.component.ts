import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { maService } from '../../../service/ma/ma.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';
import {csListService} from "../../../service/csList/csList.service";




@Component({
  selector: 'csp-list',
  templateUrl: './csp-list.component.html',
  styleUrls: ['./csp-list.component.less']
})
export class cspComponent implements OnInit, OnDestroy {

  isfullscreen = true; //是否全屏显示

  // q = { searchProvince:'',typesOfCooperation: '',csQuotientQuery:'' };
  // region 公共字段
  selectedIndex=0;
  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    // bpmStatus:"",

    // fiName: '',
    // cusMobile: '',
    // cusIncard: '',
    // orderNo: ''
    searchProvince:'',
    typesOfCooperation: '',
    csQuotientQuery:''
  };

  listOfData = [
    {
      customer: '彭于晏',
      mobile: '13666080099',
      yqze: '￥65432',
      bqje: '￥12345',
      dqzt: 'M3',
      rczt: '人车失联',
      csdz: '前往催收'
    },{
      customer: '彭于晏',
      mobile: '13666080099',
      yqze: '￥65432',
      bqje: '￥12345',
      dqzt: 'M3',
      rczt: '人车失联',
      csdz: '前往催收'
    },{
      customer: '彭于晏',
      mobile: '13666080099',
      yqze: '￥65432',
      bqje: '￥12345',
      dqzt: 'M3',
      rczt: '人车失联',
      csdz: '前往催收'
    },{
      customer: '彭于晏',
      mobile: '13666080099',
      yqze: '￥65432',
      bqje: '￥12345',
      dqzt: 'M3',
      rczt: '人车失联',
      csdz: '前往催收'
    },{
      customer: '彭于晏',
      mobile: '13666080099',
      yqze: '￥65432',
      bqje: '￥12345',
      dqzt: 'M3',
      rczt: '人车失联',
      csdz: '前往催收'
    },{
      customer: '彭于晏',
      mobile: '13666080099',
      yqze: '￥65432',
      bqje: '￥12345',
      dqzt: 'M3',
      rczt: '人车失联',
      csdz: '前往催收'
    },{
      customer: '彭于晏',
      mobile: '13666080099',
      yqze: '￥65432',
      bqje: '￥12345',
      dqzt: 'M3',
      rczt: '人车失联',
      csdz: '前往催收'
    },{
      customer: '彭于晏',
      mobile: '13666080099',
      yqze: '￥65432',
      bqje: '￥12345',
      dqzt: 'M3',
      rczt: '人车失联',
      csdz: '前往催收'
    },{
      customer: '彭于晏',
      mobile: '13666080099',
      yqze: '￥65432',
      bqje: '￥12345',
      dqzt: 'M3',
      rczt: '人车失联',
      csdz: '前往催收'
    },{
      customer: '彭于晏',
      mobile: '13666080099',
      yqze: '￥65432',
      bqje: '￥12345',
      dqzt: 'M3',
      rczt: '人车失联',
      csdz: '前往催收'
    },
  ];

  MobData = [
    {x: "M1",y: 800},
    {x: "M2",y: 620},
    {x: "M3",y: 650},
    {x: "M4",y: 495}
  ]

  rankingListData: any[] = Array(7)
  .fill({})
  .map((item, i) => {
    return {
      title: "111",
      total: 30123234,
    };
  });


  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.csListService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  seasons = [];
  checkedList = [];
  data = [];
  // uploadUrl = this.maService.uploadProds();
  uploadDateUrl = this.maService.batchRepdate();
  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '机构编号', index: 'paNo' },
    { title: '机构归属', index: 'paMainno'},
    { title: '机构名称', index: 'paName' },
    { title: '机构地址', index: 'paAddr' },
    { title: '联系人' , index: 'paUser' },
    { title: '联系电话', index: 'paMobile' },
    { title: '机构等级', index: 'paClass' },
    { title: '平均返佣比例', index: 'paProfit' },
    { title: '合作开始时间', index: 'paBdate' },
    { title: '合作结束时间', index: 'paEdate' },
    { title: '备注', index: 'content' },
    {
        title: '操作',
        buttons: [
            {
              text: '详情',
              type: 'link',
              click: (record: any) => this.router.navigate(['/csp/csp/edit/'+record.proNo])
            }
        ]
    }
  ];

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private maService: maService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private csListService: csListService,
    private notice: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }
  inter
  ngOnInit() {
    this.imgSuffix = this.comm.getGoodsUrl();
    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })
  }

  goCsHome(){
    this.router.navigate(['/cspList/cspList']);
  }

  goCscCenter(){ //前往催收中心
    this.router.navigate(['/cszx/cszx']);
  }

  goCsGl(){ //前往催收管理
    this.router.navigate(['/csgl/csgl']);
  }
  goCsCl(){ //前往催收策略
    this.router.navigate(['/cscl/cscl']);
  }

  goCsTj(){ //前往数据统计
    this.router.navigate(['/cstj/cstj']);
  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }
  change(args: NzTabChangeEvent) {
        switch (args.index) {
          case 0:
            this.q.bpmStatus = "";
            break;
          case 1:
            this.q.bpmStatus = 1;
            break;
          case 2:
            this.q.bpmStatus = 10;
            break;
          case 3:
            this.q.bpmStatus = 20;
            break;
          case 4:
            this.q.bpmStatus = 30;
            break;
          case 5:
            this.q.bpmStatus = 40;
            break;
          case 6:
            this.q.bpmStatus = 50;
            break;
          case 7:
            this.q.bpmStatus = 60;
            break;
          case 8:
            this.q.bpmStatus = 70;
            break;
          case 9:
            this.q.bpmStatus = 80;
            break;
          case 10:
            this.q.bpmStatus = 90;
            break;
          case 11:
            this.q.bpmStatus = 99;
            break;
          case 12:
            this.q.bpmStatus = 100;
            break;
          default:
            break;
        }
      this.st.load(1, this.q);
  }
  checkboxChange(list: any[]) {
    this.checkedList = list;
  }
  close(): void {
    this.visible = false;
  }
  cancel() {
    this.repdate = null;
  }
  repdate = null;
}
