import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { maService } from '../../../service/ma/ma.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';
import {csListService} from "../../../service/csList/csList.service";




@Component({
  selector: 'cscl-list',
  templateUrl: './cscl-list.component.html',
  styleUrls: ['./cscl-list.component.less']
})
export class csclComponent implements OnInit, OnDestroy {

  isfullscreen = true; //是否全屏显示

  // q = { searchProvince:'',typesOfCooperation: '',csQuotientQuery:'' };
  // region 公共字段
  selectedIndex=0;
  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    searchYear: '',
    searchMonth: '',
    searchQds: '',
    orderNo: '',
  };

 

  rankingListData: any[] = Array(7)
  .fill({})
  .map((item, i) => {
    return {
      title: "111",
      total: 30123234,
    };
  });


  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.csListService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  seasons = [];
  checkedList = [];
  data = [];
  // uploadUrl = this.maService.uploadProds();
  uploadDateUrl = this.maService.batchRepdate();
  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '订单编号', index: 'paNo' },
    { title: '客户姓名', index: 'paMainno'},
    { title: '订单类型', index: 'paName' },
    { title: '联系电话', index: 'paAddr' },
    { title: '车辆信息' , index: 'paUser' },
    { title: '评估价值', index: 'paMobile' },
    { title: 'GPS状态', index: 'paClass' },
    { title: '贷款期数', index: 'paProfit' },
    { title: '融资金额', index: 'paBdate' },
    { title: '融资利息', index: 'paEdate' },
    { title: '已还期数', index: 'content' },
    { title: '已还金额', index: 'content' },
    { title: '未还金额', index: 'content' },
    { title: '录入单位', index: 'content' },
    { title: '负责人', index: 'content' },
    { title: '联系电话', index: 'content' },
    { title: '历史欠款', index: 'content' },
    { title: '本期应还', index: 'content' },
    { title: '当前应还', index: 'content' },
    { title: '到期还款日', index: 'content' },
    { title: 'MOB状态', index: 'content' },
    {
        title: '操作',
        buttons: [
            {
              text: '查看',
              type: 'link',
              click: (record: any) => this.router.navigate(['/cscl/cscl/edit/'+record.proNo])
            },{
              text: '邮件',
              type: 'link',
              click: (record: any) => this.router.navigate(['/cscl/cscl/edit/'+record.proNo])
            },{
              text: '微信',
              type: 'link',
              click: (record: any) => this.router.navigate(['/cscl/cscl/edit/'+record.proNo])
            },
        ]
    }
  ];

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private maService: maService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private csListService: csListService,
    private notice: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }
  inter
  ngOnInit() {
    this.imgSuffix = this.comm.getGoodsUrl();
    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })
  }

  goCsHome(){
    this.router.navigate(['/cspList/cspList']);
  }

  goCscCenter(){ //前往催收中心
    this.router.navigate(['/cszx/cszx']);
  }

  goCsGl(){ //前往催收管理
    this.router.navigate(['/csgl/csgl']);
  }

  goCsCl(){ //前往催收策略
    this.router.navigate(['/cscl/cscl']);
  }

  goCsTj(){ //前往数据统计
    this.router.navigate(['/cstj/cstj']);
  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }
  change(args: NzTabChangeEvent) {
        switch (args.index) {
          case 0:
            this.q.bpmStatus = "";
            break;
          case 1:
            this.q.bpmStatus = 1;
            break;
          case 2:
            this.q.bpmStatus = 10;
            break;
          case 3:
            this.q.bpmStatus = 20;
            break;
          case 4:
            this.q.bpmStatus = 30;
            break;
          case 5:
            this.q.bpmStatus = 40;
            break;
          case 6:
            this.q.bpmStatus = 50;
            break;
          case 7:
            this.q.bpmStatus = 60;
            break;
          default:
            break;
        }
      this.st.load(1, this.q);
  }
  checkboxChange(list: any[]) {
    this.checkedList = list;
  }
  close(): void {
    this.visible = false;
  }
  cancel() {
    this.repdate = null;
  }
  repdate = null;
}
