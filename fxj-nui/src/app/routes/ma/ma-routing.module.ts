import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { maListComponent } from './ma-list/ma-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { maViewEditComponent } from 'app/routes/ma/ma-list/edit/ma-edit.component';

const routes: Routes = [
  {
    path: 'ma',
    children: [
      { path: '', component: maListComponent },
      { path: 'edit/:id', component: maViewEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class maRoutingModule { }
