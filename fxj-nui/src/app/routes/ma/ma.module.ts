import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { maRoutingModule } from './ma-routing.module';
import { maListComponent } from './ma-list/ma-list.component';
import { maViewEditComponent } from 'app/routes/ma/ma-list/edit/ma-edit.component';

const COMPONENTS = [
  maListComponent
];
const COMPONENTS_NOROUNT = [
  maViewEditComponent
];

@NgModule({
  imports: [
    SharedModule,
    maRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class maModule { }
