import { Component, OnInit, ViewChild } from '@angular/core';
import { _HttpClient, SettingsService } from '@delon/theme';
import { OrderService } from '../../../service/order/order.service';
import { SimpleTableComponent, SimpleTableColumn } from '@delon/abc/src/simple-table';
import { CommService } from '../../../service/comm/comm.service';
import { ServiceResult } from '@core/constant';
import { formatDate } from '@angular/common';
import { Router } from '@angular/router';
import { EmitterService } from 'app/service/emit.service';

@Component({
  selector: 'order-list',
  templateUrl: './order-list.component.html',
})
export class OrderListComponent implements OnInit {
  dateFormat = 'yyyy/MM/dd';
  ps = 10;
  q: any = { 
     mode: 'my',
     orderId: '',
     dateRange: null
  };  
  url = this.orderServ.list();
  data = [];
  checkedList = [];
  loading = false;
  statuss = [{text: 'Pending', value: 'NOCHECK'}, {text: 'Confirmed', value: 'CHECKED'}, 
  {text: 'Shipping', value: 'SENDING'}, { text: 'Complete', value: 'COMPLETE'}, { text: 'Canceled', value: 'CANCEL'}];

  @ViewChild('st') st: SimpleTableComponent;
  columns: SimpleTableColumn[] = [];


  constructor(
    private orderServ: OrderService,
    private comm: CommService,
    private setting: SettingsService,
    private emit: EmitterService,
    private router: Router
  ) { }

  ngOnInit() {
    this.buildColumn();    
    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_ORDERS_LIST') {
        this.st.reload(this.q);
      }
    })
  }

  checkboxChange(list: any[]) {
    this.checkedList = list;
  }

  buildColumn() {
    this.columns = [
      { title: '', index: 'key', type: 'checkbox' },
      { title: '单号', index: 'orderId', i18n: 'orderId' },
      { title: '提交日期', index: 'createDate', i18n: 'createDate' },      
      { title: '数量', index: 'qty', i18n: 'qty' },
      { title: '金额', index: 'price', i18n: 'price' },
      { title: '状态', index: 'status',  type: 'badge', i18n: 'status',
        filters: this.statuss,
        filterMultiple: false,
        filter: () => true,        
        filterConfirmText: 'Ok',
        filterClearText: 'Reset',
        badge: {
          'NOCHECK': { text: 'Pending', color: 'processing' },
          'CHECKED': { text: 'Confirmed', color: 'warning' },
          'SENDING': { text: 'Shipping', color: 'warning'},
          'COMPLETE': { text: 'Complete', color: 'success'},
          'CANCEL': { text: 'Canceled', color: 'error' },        
        }
      },
      { 
        title: '',
        buttons: [  
            {
              text: '明细',
              i18n: 'view',
              type: 'link',
              click: (record: any) => this.router.navigate(['/order/myorder/view/'+record.id])
            }                      
        ]
      }  
    ];
  }

  dateChange(result: Date[]): void {
    console.log(result);
    if (result.length != 0) {
      this.q.startDate = formatDate(result[0], 'yyyy-MM-dd', 'en-US');
      this.q.endDate = formatDate(result[1], 'yyyy-MM-dd', 'en-US');
    } else {
      this.q.startDate = '';
      this.q.endDate = '';
    }
  }
}
