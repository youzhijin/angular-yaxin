import { Component, OnInit } from '@angular/core';
import { SettingsService } from '@delon/theme/src/src/services/settings/settings.service';
import { SmsService } from 'app/service/sms/sms.service';
import { NzMessageService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { trimTrailingNulls } from '@angular/compiler/src/render3/view/util';

@Component({
    selector: 'order-logistics',
    templateUrl: './logistics-list.component.html',
    styleUrls: ['./logistics-list.component.less']
})
export class LogisticsListComponent implements OnInit {
    code = "";
    loading = false;
    data: { [proname: string]: any } = null

    constructor(
        public msg: NzMessageService,
        private setting: SettingsService,
        private sms: SmsService
    ) {}
    ngOnInit() { }

    search() {
      this.code = this.code.trim()
      if (!this.code) {
          this.msg.warning("The security code is required");
          return;
      }
      let acode = '';
      let barcode = '';
      if (this.code.length == 16) {
          barcode = this.code;
      } else {
          acode = this.code;
      }
      this.loading = true;
      this.sms.queryLogistic(acode, barcode).pipe(catchError(err => {
          return of(err)
      })).subscribe(data => {
          this.loading = false;
          this.code = '';
          if (data.status === 200) {
              console.log(data.data);
              this.data = data.data;            
          } else {
              this.msg.error(data.message);
          }
      })
    }
}