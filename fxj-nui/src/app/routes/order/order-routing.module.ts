import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderProductsComponent } from './products/products.component';
import { OrderCartListComponent } from './cart-list/cart-list.component';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderViewComponent } from 'app/routes/order/order-list/view/order-view.component';
import { ClientOrderListComponent } from 'app/routes/order/client-order/client-order-list.component';
import { ClientOrderViewComponent } from 'app/routes/order/client-order/view/order-view.component';
import { LogisticsListComponent } from './logistics/logistics-list.component';

const routes: Routes = [

  { path: 'products', component: OrderProductsComponent },
  { path: 'carts', component: OrderCartListComponent },
  { path: 'myorder', 
    children: [
      { path: '', component: OrderListComponent },
      { path: 'view/:id', component: OrderViewComponent },                 
    ]
  },
  { path: 'clientorder', 
    children: [
      { path: '', component: ClientOrderListComponent },
      { path: 'view/:id', component: ClientOrderViewComponent }
    ]
  },
  {
    path: 'logistic',
    component: LogisticsListComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule { }
