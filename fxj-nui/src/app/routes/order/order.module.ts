import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { OrderRoutingModule } from './order-routing.module';
import { OrderProductsComponent } from './products/products.component';
import { PipeModule } from 'app/pipe/pipe.module';
import { OrderCartListComponent } from './cart-list/cart-list.component';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderViewComponent } from 'app/routes/order/order-list/view/order-view.component';
import { ClientOrderViewComponent } from 'app/routes/order/client-order/view/order-view.component';
import { ClientOrderListComponent } from 'app/routes/order/client-order/client-order-list.component';
import { LogisticsListComponent } from 'app/routes/order/logistics/logistics-list.component';

const COMPONENTS = [
  OrderProductsComponent,
  OrderCartListComponent,
  OrderListComponent,
  OrderViewComponent,
  ClientOrderListComponent,
  ClientOrderViewComponent,
  LogisticsListComponent
];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [
    SharedModule,
    OrderRoutingModule,
    PipeModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class OrderModule { }
