import { Component, ChangeDetectionStrategy, OnInit, ChangeDetectorRef, } from '@angular/core';
// import { NzMessageService } from 'ng-zorro-antd';
// import { STColumn } from '@delon/abc';
import { getTimeDistance } from '@delon/util';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { yuan } from '@shared';
import { format } from 'date-fns';

import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { dashboardService } from '../../../service/dashboard/dashboard.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';

@Component({
  selector: 'app-dashboard-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardAnalysisComponent implements OnInit {
  // loanAmount=1000; //历史借贷金额
  loanAmount = this.setLoanAmount(); // 获取历史借贷金额
  loanMonth='14'; //历史借贷金额月同比
  loanUp= 'up';//历史借贷金额的箭头

  // loanNalance = '126,560';//贷款余额
  loanNalance = this.setLoanNalance()
  loanNalanceWeek='18'; //贷款余额周同比
  loanNalanceWeekUp='down'; //贷款余额周同比箭头
  loanNalanceData='8'; //贷款余额日同比
  loanNalanceDataUp='up'; //贷款余额日同比箭头
  loanNalanceMonth='26'; //贷款余额月同比
  loanNalanceMonthUp='up'; //贷款余额月同比箭头

  // loss='6,560';//损失金额
  loss = this.setLoss();
  lossMonth='10'; //损失金额月同比
  lossMonthUp='up'; //损失金额月同比箭头

  // defective='30';//不良率
  defective = this.setDefective();
  defectiveMonth='13'; //不良率月同比
  defectiveMonthUp='up'; //不良率月同比箭头

  data: any = {};
  loading = true;
  date_range: Date[] = [];
  rankingListData: any[] = Array(7)
    .fill({})
    .map((item, i) => {
      return {
        title: "111",
        total: 323234,
      };
    });

  constructor(
    private http: _HttpClient,
    public msg: NzMessageService,
    private cdr: ChangeDetectorRef,

    private trans: TranslateService,
    
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private dashboardService: dashboardService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
  ) {}

  loanAmountData: any[] = [];//历史借贷金额
  lossData: any[] = [];//损失金额
  offlineData: any[] = [];//门店数据

  ngOnInit() {
    const beginDay = new Date().getTime();
    const fakeY = [7, 5, 4, 2, 4, 7, 5, 6, 5, 9, 6, 3, 1, 5, 3, 6, 5];
    //历史借贷金额
    for (let i = 0; i < fakeY.length; i += 1) {
      this.loanAmountData.push({
        x: format(new Date(beginDay + 1000 * 60 * 60 * 24 * i), 'YYYY-MM-DD'),
        y: fakeY[i],
      });
    }
    //损失金额
    for (let i = 0; i < fakeY.length; i += 1) {
      this.lossData.push({
        x: format(new Date(beginDay + 1000 * 60 * 60 * 24 * i), 'YYYY-MM-DD'),
        y: fakeY[i],
      });
    }
    //门店数据
    for (let i = 0; i < 10; i += 1) {
      this.offlineData.push({
        name: `门店${i}`,
        cvr: Math.ceil(Math.random() * 9) / 10,
      });
      this.cdr.detectChanges();
    }
    // this.http.get('/chart').subscribe((res: any) => {
    //   res.offlineData.forEach((item: any, idx: number) => {
    //     item.show = idx === 0;
    //     item.chart = Object.assign([], res.offlineChartData);
    //   });
    //   this.data = res;
    //   this.loading = false;
    //   this.changeSaleType();
    // });
    this.loading = false;
    this.data.salesData = [
      {x: "1月",y: 1022},
      {x: "2月",y: 1115},
      {x: "3月",y: 373},
      {x: "4月",y: 504},
      {x: "5月",y: 894},
      {x: "6月",y: 910},
      {x: "7月",y: 614},
      {x: "8月",y: 635},
      {x: "9月",y: 618},
      {x: "10月",y: 791},
      {x: "11月",y: 616},
      {x: "12月",y: 428},
    ]
    // console.log(this.data)
  }
  // 获取历史借贷金额
  setLoanAmount() {
    this.dashboardService.Loan().subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if(ret.ok){
        console.log(ret)
        this.loanAmount = ret.data;
      }
    });
  }
  // 获取贷款余额
  setLoanNalance() {
    this.dashboardService.loanNalance().subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if(ret.ok){
        this.loanNalance = ret.data;
      }
    });
  }
  // 获取损失金额
  setLoss() {
    this.dashboardService.loss().subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if(ret.ok){
        this.loss = ret.data;
      }
    })
  }
  // 获取不良率
  setDefective(){
    this.dashboardService.defective().subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if(ret.ok){
        this.defective = ret.data;
      }
    })
  }

  setDate(type: any) {
    this.date_range = getTimeDistance(type);
    setTimeout(() => this.cdr.detectChanges());
  }

  salesType = 'all';
  salesPieData: any;
  salesTotal = 0;
  changeSaleType() {
    this.salesPieData =
      this.salesType === 'all'
        ? this.data.salesTypeData
        : this.salesType === 'online'
        ? this.data.salesTypeDataOnline
        : this.data.salesTypeDataOffline;
    if (this.salesPieData) {
      this.salesTotal = this.salesPieData.reduce((pre, now) => now.y + pre, 0);
    }
    this.cdr.detectChanges();
  }

  saleTabs: any[] = [
    { key: '进件数', show: true },
    { key: '拒单数' },
    { key: '放款数' },
    { key: '放款金额' }
  ];

  salesChange(idx: number) {
    console.log(this.saleTabs[idx])
    if (this.saleTabs[idx].show !== true) {
      this.saleTabs[idx].show = true;
      // this.cdr.detectChanges();
    }
  }

  offlineIdx = 0;
  offlineChange(idx: number) {
    if (this.data.offlineData[idx].show !== true) {
      this.data.offlineData[idx].show = true;
      this.cdr.detectChanges();
    }
  }
}
