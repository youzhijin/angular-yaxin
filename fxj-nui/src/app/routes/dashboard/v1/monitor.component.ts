import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd';
import { zip } from 'rxjs';
import { _HttpClient } from '@delon/theme';

@Component({
  selector: 'app-dashboard-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardV1Component implements OnInit, OnDestroy {

  //url = `/users?total=100`;
  params = { a: 1, b: 2 };
  // columns: STColumn[] = [
  //   { title: '省市', index: 'id' },
  //   { title: '历史放款量',index: 'picture.thumbnail' },
  //   { title: 'MOB3金额', index: 'email' },
  //   { title: '不良率', index: 'phone' },
  //   { title: '同比%', index: 'registered' },
  //   { title: '详情查看',
  //     buttons: [
  //       {
  //         text: '查看'
  //       }
  //     ]
  //   }
  // ];

  constructor(
    private http: _HttpClient,
    public msg: NzMessageService,
    private cdr: ChangeDetectorRef,
  ) {}


  ngOnInit() {

  }

  ngOnDestroy(){

  }
}
