import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd';
import { zip } from 'rxjs';
import { _HttpClient } from '@delon/theme';

@Component({
  selector: 'app-dashboard-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardMonitorComponent implements OnInit {
  M1Numbe='126';//本月M1账户数
  M1Data='12'; //M1日同比
  M1DataUp='down'; //M1日同比箭头
  M1Week='12'; //M1周同比
  M1WeekUp='up'; //M1周同比箭头
  M1Month='12'; //M1月同比
  M1MonthUp='up'; //M1月同比箭头

  M2Numbe='616';//本月M2账户数
  M2Data='12'; //M2日同比
  M2DataUp='down'; //M2日同比箭头
  M2Week='12'; //M2周同比
  M2WeekUp='up'; //M2周同比箭头
  M2Month='12'; //M2月同比
  M2MonthUp='up'; //M1月同比箭头

  todatOverdue = '29';//本日逾期客户数
  todayRisk = '29';//本日风险监测预警事件
  todayGps = '29';//本日GPS报警数
  //高风险客户预警名单列表
  highRiskList = [
    {name:'彭于晏',amount:'1336555',riskLevel:'不良'},
    {name:'彭于晏',amount:'1336555',riskLevel:'不良'},
    {name:'彭于晏',amount:'1336555',riskLevel:'不良'},
    {name:'彭于晏',amount:'1336555',riskLevel:'不良'},
    {name:'彭于晏',amount:'1336555',riskLevel:'不良'}
  ]
  //借款人预警列表
  borrowerList = [
    {name:'彭于晏',state:'失联',date:'2019-03-27'},
    {name:'彭于晏',state:'失联',date:'2019-03-27'},
    {name:'彭于晏',state:'失联',date:'2019-03-27'},
    {name:'彭于晏',state:'失联',date:'2019-03-27'},
    {name:'彭于晏',state:'失联',date:'2019-03-27'}
  ]
  //GPS列表
  gpsList = [
    {title:'闵DNB368',state:'设备异常',date:'2019-03-27'},
    {title:'闵DNB368',state:'设备异常',date:'2019-03-27'},
    {title:'闵DNB368',state:'设备异常',date:'2019-03-27'},
    {title:'闵DNB368',state:'设备异常',date:'2019-03-27'},
    {title:'闵DNB368',state:'设备异常',date:'2019-03-27'}
  ]


  constructor(
    private http: _HttpClient,
    public msg: NzMessageService,
    private cdr: ChangeDetectorRef,
  ) {}
  salesPieData = [
    {
      x: '广东省',
      y: 4544,
    },
    {
      x: '福建省',
      y: 3321,
    },
    {
      x: '浙江省',
      y: 3113,
    },
    {
      x: '江苏省',
      y: 2341,
    }
  ];
  ngOnInit(): void {
  }
}
