import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { zip } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import {SimpleTableColumn} from "@delon/abc";

@Component({
  selector: 'app-dashboard-workplace',
  templateUrl: './workplace.component.html',
  styleUrls: ['./workplace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardWorkplaceComponent implements OnInit {
  onSw = 1;
  columns: SimpleTableColumn[] = [
    { title: '订单编号', index: 'orderNum' },
    { title: '客户姓名', index: 'name' },
    { title: '订单类型', index: 'orderType' },
    { title: '联系电话', index: 'telephone'},
    { title: '车辆信息', index: 'vehicleInfo'},
    { title: '期数', index: 'periodsNum'},
    { title: '融资额', index: 'financingAmount'},
    { title: '受益人变更', index: 'beneficiaryChanged'},
    { title: '录入业务员', index: 'salesman'},
    { title: '产品种类', index: 'productCategory'},
    { title: '录入单位', index: 'entryUnit'},
    { title: '订单提交时间', index: 'time'},
    { title: '订单状态', index: 'orderStatus'},
    { title: '详情查看',width:'140px',fixed:'right',
      buttons: [
        {
          text: '查看'
        },
        {
          text: '认领'
        }
      ]
    }
  ];


  listOfData = [
    {
      orderNum:'GM01001',
      name: '刘德华',
      orderType:'售后回租',
      telephone: '136600000',
      vehicleInfo:'宝马5系列',
      periodsNum:'36期限',
      financingAmount:'135455元',
      beneficiaryChanged:'有',
      salesman:'张学友',
      productCategory:'车抵贷',
      entryUnit:'平安银行',
      time:'2019-03-28',
      orderStatus:'已认领'
    },{
      orderNum:'GM01001',
      name: '刘德华',
      orderType:'售后回租',
      telephone: '136600000',
      vehicleInfo:'宝马5系列',
      periodsNum:'36期限',
      financingAmount:'135455元',
      beneficiaryChanged:'有',
      salesman:'张学友',
      productCategory:'车抵贷',
      entryUnit:'平安银行',
      time:'2019-03-28',
      orderStatus:'已认领'
    },{
      orderNum:'GM01001',
      name: '刘德华',
      orderType:'售后回租',
      telephone: '136600000',
      vehicleInfo:'宝马5系列',
      periodsNum:'36期限',
      financingAmount:'135455元',
      beneficiaryChanged:'有',
      salesman:'张学友',
      productCategory:'车抵贷',
      entryUnit:'平安银行',
      time:'2019-03-28',
      orderStatus:'已认领'
    },{
      orderNum:'GM01001',
      name: '刘德华',
      orderType:'售后回租',
      telephone: '136600000',
      vehicleInfo:'宝马5系列',
      periodsNum:'36期限',
      financingAmount:'135455元',
      beneficiaryChanged:'有',
      salesman:'张学友',
      productCategory:'车抵贷',
      entryUnit:'平安银行',
      time:'2019-03-28',
      orderStatus:'已认领'
    },{
      orderNum:'GM01001',
      name: '刘德华',
      orderType:'售后回租',
      telephone: '136600000',
      vehicleInfo:'宝马5系列',
      periodsNum:'36期限',
      financingAmount:'135455元',
      beneficiaryChanged:'有',
      salesman:'张学友',
      productCategory:'车抵贷',
      entryUnit:'平安银行',
      time:'2019-03-28',
      orderStatus:'已认领'
    }
  ];

  loading = true;
  // endregion
  selectHero( id ):void{
    console.log(id);
    this.onSw = id;
  }

  constructor(
    private http: _HttpClient,
    public msg: NzMessageService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.loading = false;
    this.cdr.detectChanges();
  }
}
