import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { qylbListComponent } from './qylb-list/qylb-list.component';
import { qylbList1Component } from './qylb-list/1/qylb-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { qylbViewEditComponent } from 'app/routes/qylb/qylb-list/edit/qylb-edit.component';
import { qylbViewEdit1Component } from 'app/routes/qylb/qylb-list/edit/qylb-edit1.component';

const routes: Routes = [
  {
    path: 'qylb',
    children: [
      { path: '', component: qylbListComponent },
      { path: 'dk', component: qylbList1Component },

      { path: 'view/:id', component: qylbViewEditComponent },
      { path: 'dkview/:id', component: qylbViewEdit1Component }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class qylbRoutingModule { }
