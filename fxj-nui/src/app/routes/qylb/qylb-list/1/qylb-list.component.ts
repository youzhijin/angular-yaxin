import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { qylbService } from '../../../../service/qylb/qylb.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';




@Component({
  selector: 'qylb-list',
  templateUrl: './qylb-list.component.html',
  styleUrls: ['./qylb-list.component.less']
})
export class qylbList1Component implements OnInit, OnDestroy {
  sjtj = 123
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    bpmStatus:"",
    fxjOut06:'',
    fxjOut02:'',
    fxjOut03:''
  };

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.qylbService.pllist();
  // region end

  // 上传贷后信息导入
  uploadreplanExcelUrl='';
  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  seasons = [];
  checkedList = [];

  data = [];
  downurl='https://zhaodui.oss-cn-shenzhen.aliyuncs.com/doc/fxbbcartempv1.xls';

  mbdownurl='https://zhaodui.com.cn/hkdr.xls';

  // uploadUrl = this.qylbService.uploadProds();
  uploadDateUrl = this.qylbService.batchRepdate();




  columns: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '还款计划ID', index: 'fxjOut01' },
    { title: '客户姓名', index: 'fxjOut02' },
    { title: '电话', index: 'fxjOut03' },
    { title: '还款方式', index: 'fxjOut04' },
    { title: '期数', index: 'fxjOut05' },
    { title: '订单编号', index: 'fxjOut06' },
    { title: '还款金额', index: 'fxjOut07' },
    { title: '备注', index: 'fxjOut08' },
    {
        title: '操作',
        buttons: [
            {
              text: '查看',
              type: 'link',
              click: (record: any) => this.router.navigate(['/qylb/qylb/dkview/'+record.id])
            }
        ]
    }
  ];


  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private qylbService: qylbService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  inter
  ngOnInit() {
    this.uploadreplanExcelUrl = this.qylbService.uploadreplaninfo();

    this.imgSuffix = this.comm.getGoodsUrl();

    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })


  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }






  checkboxChange(list: any[]) {
    this.checkedList = list;
  }


  close(): void {
    this.visible = false;
  }



  cancel() {
    this.repdate = null;
  }
  repdate = null;

  uploadProd(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
    }
    if (info.file.status === 'done') {
      this.loading = false;
      this.st.load();
      this.notice.create('success', '成功', '还款信息导入成功');
    }
    if (info.file.status === 'error') {
      this.loading = false;
      this.notice.create('error', '失败', '还款信息导入失败');
    }
  }

}
