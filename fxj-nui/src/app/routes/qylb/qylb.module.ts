import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { qylbRoutingModule } from './qylb-routing.module';
import { qylbListComponent } from './qylb-list/qylb-list.component';
import { qylbList1Component } from './qylb-list/1/qylb-list.component';
import { qylbViewEditComponent } from 'app/routes/qylb/qylb-list/edit/qylb-edit.component';
import { qylbViewEdit1Component } from 'app/routes/qylb/qylb-list/edit/qylb-edit1.component';

const COMPONENTS = [
  qylbListComponent,
  qylbList1Component
];
const COMPONENTS_NOROUNT = [
  qylbViewEditComponent,
  qylbViewEdit1Component
];

@NgModule({
  imports: [
    SharedModule,
    qylbRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class qylbModule { }
