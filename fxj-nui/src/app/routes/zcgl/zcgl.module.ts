import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { zcglRoutingModule } from './zcgl-routing.module';
import { zcglListComponent } from './zcgl-list/zcgl-list.component';
import { zcglViewEditComponent } from 'app/routes/zcgl/zcgl-list/edit/zcgl-edit.component';

const COMPONENTS = [
  zcglListComponent
];
const COMPONENTS_NOROUNT = [
  zcglViewEditComponent
];

@NgModule({
  imports: [
    SharedModule,
    zcglRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class zcglModule { }
