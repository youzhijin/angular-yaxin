import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { zcglListComponent } from './zcgl-list/zcgl-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { zcglViewEditComponent } from 'app/routes/zcgl/zcgl-list/edit/zcgl-edit.component';

const routes: Routes = [
  {
    path: 'zcgl',
    children: [
      { path: '', component: zcglListComponent },
      { path: 'edit/:id', component: zcglViewEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class zcglRoutingModule { }
