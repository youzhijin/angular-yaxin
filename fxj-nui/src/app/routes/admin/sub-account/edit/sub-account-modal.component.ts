import { Component, OnInit, ViewChild } from '@angular/core';
import { NzModalRef, NzMessageService } from 'ng-zorro-antd';
import { _HttpClient, SettingsService } from '@delon/theme';
import { SFSchema, SFUISchema } from '@delon/form';
import { UserService } from '../../../../service/user/user.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { deepCopy } from '@delon/util';
import { ServiceResult } from '@core/constant';
import { CommService } from '../../../../service/comm/comm.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sub-account-edit',
  templateUrl: './sub-account-modal.component.html',
  styles: [`:host ::ng-deep .ant-form-item { 
        margin-bottom: 12px;
    }`]
})
export class SubAccountModalComponent implements OnInit {
  loading = false;
  record: any = {};
  userInfo = null;
  parentId;
  constructor(
    private trans: TranslateService,
    private comm: CommService,
    private setting: SettingsService,
    private msg: NzMessageService,
    private user: UserService,
    private modal: NzModalRef,
    public msgSrv: NzMessageService,
    public http: _HttpClient,
  ) {}

  ngOnInit(): void {        
    if (this.record.id) {
        this.user.info(this.record.id).pipe(
          catchError(err => { return of(err); })
        ).subscribe(ret=>{
          if (ret.status === 200) {
              const data = ret.data;
              this.record = data;
              this.userInfo = JSON.parse(this.record.userId);
              if (typeof this.userInfo === 'object' && this.userInfo) {
                  this.record.userId = this.userInfo.id;                
              } 
          } else {
              this.msg.error(ret.message);
          }
        });
    }   
  }


  
  save() {
    let userForm = deepCopy(this.record);        
    let lang = this.trans.currentLang || this.trans.defaultLang;
    if (!userForm.name) {     
        if (lang === 'en') {
            this.msg.warning('This name is required');
        } else {
            this.msg.warning('请输入名称');
        }
        return;
    }
        
    if (!userForm.id && !userForm.password) {
        this.msg.warning(lang === 'en' ? 'This password is required' : '密码不能空');
        return;
    }
    if (!userForm.id && (userForm.password.length < 4 || userForm.password.length > 16)) {
        this.msg.warning(lang === 'en' ? 'Password must have at least 4 characters' : '密码长度需4-16个字符');
        return;
    }

    userForm.level = "FIRST_CHILD";
    userForm.parentId = this.parentId;        
    this.loading = true;
    console.log('xxx');
    if (userForm.id) {        
        this.user.modify(userForm).pipe(
            catchError(err => {
                return of(err)
            })
        ).subscribe(ret => {
            this.loading = false;
            if (ret.status == 200) {
                this.msg.success(lang === 'en' ? 'Save successful' : '操作成功');
                this.close();
            } else {
                this.msg.error(ret.message);
            }
        })
    } else {
        if (!userForm.parentId) {
            this.msg.warning(lang === 'en' ? 'Parent user is required' : '父级用户不能空');
            return;
        }
        this.user.add(userForm).pipe(
            catchError(err => {
                return of(err);
            })
        ).subscribe(ret => {
            this.loading = false;
            if (ret.status == 200) {
                this.msg.success(lang === 'en' ? 'Save successful' : '操作成功');
                this.close();
            } else {
                this.msg.error(ret.message);
            }
        })
    }
  }

  close() {
    this.modal.close(true);
    this.modal.destroy();
  }
}
