import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgentUserListComponent } from './agent-list/agent-list.component';
import { AdminClientListComponent } from './client-list/client-list.component';
import { ShopListComponent } from 'app/routes/admin/shop-list/shop-list.component';
import { GroupComponent } from './group/group.component';
import { SubAccountListComponent } from './sub-account/sub-account.component';

const routes: Routes = [  
  { 
    path: 'agents', children: [
      { path: '', component: AgentUserListComponent },
      { path: 'children', component: SubAccountListComponent }
    ]    
  },
  { path: 'clients', component: AdminClientListComponent },
  { path: 'shops', component: ShopListComponent },
  { path: 'role', component: GroupComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
