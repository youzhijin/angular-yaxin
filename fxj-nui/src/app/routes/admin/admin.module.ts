import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { AdminRoutingModule } from './admin-routing.module';
import { AgentUserListComponent } from './agent-list/agent-list.component';
import { AgentUserListEditComponent } from './agent-list/edit/edit.component';
import { AdminClientListComponent } from './client-list/client-list.component';
import { ClientEditComponent } from 'app/routes/admin/client-list/edit/edit.component';
import { ShopListComponent } from 'app/routes/admin/shop-list/shop-list.component';
import { ShopEditComponent } from 'app/routes/admin/shop-list/edit/edit.component';
import { GroupComponent } from './group/group.component';
import { GroupMemberModalComponent } from 'app/routes/admin/group/member/group-member-modal.component';
import { SubAccountListComponent } from './sub-account/sub-account.component';
import { SubAccountModalComponent } from './sub-account/edit/sub-account-modal.component';

const COMPONENTS = [
  AgentUserListComponent,
  AdminClientListComponent,
  ShopListComponent,
  GroupComponent,
  SubAccountListComponent
];
const COMPONENTS_NOROUNT = [
  AgentUserListEditComponent,
  ClientEditComponent,
  ShopEditComponent,
  GroupMemberModalComponent,
  SubAccountModalComponent
];

@NgModule({
  imports: [
    SharedModule,
    AdminRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class AdminModule { }
