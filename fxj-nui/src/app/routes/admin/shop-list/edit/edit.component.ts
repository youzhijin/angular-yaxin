import { Component, OnInit, ViewChild } from '@angular/core';
import { NzModalRef, NzMessageService } from 'ng-zorro-antd';
import { _HttpClient } from '@delon/theme';
import { SFSchema, SFUISchema } from '@delon/form';
import { UserService } from '../../../../service/user/user.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { deepCopy } from '@delon/util';

@Component({
  selector: 'shop-user-edit',
  templateUrl: './edit.component.html',
  styles: [`:host ::ng-deep .ant-form-item { 
        margin-bottom: 12px;
    }`]
})
export class ShopEditComponent implements OnInit {
  record: any = {};
  userInfo = null;
  parentId;
  loading = false;
  constructor(
    private msg: NzMessageService,
    private user: UserService,
    private modal: NzModalRef,
    public msgSrv: NzMessageService,
    public http: _HttpClient,
  ) {}

  ngOnInit(): void {    
    if (this.record.child_id) {
      this.parentId = this.record.parent_id;
      this.user.info(this.record.child_id).pipe(
        catchError(err => { return of(err); })
      ).subscribe(ret=>{
        if (ret.status === 200) {
            const data = ret.data;
            this.record = data;

            this.userInfo = JSON.parse(this.record.userId);
            if (typeof this.userInfo === 'object' && this.userInfo) {
                this.record.userId = this.userInfo.id;                
            } 
        } else {
            this.msg.error(ret.message);
        }
      });
    }
    
  }

  save() {
    let userForm = deepCopy(this.record);        
    if (!userForm.name) {
        this.msg.warning('This name is required');
        return;
    }
        
    if (!userForm.id && !userForm.password) {
        this.msg.warning('This password is required');
        return;
    }
    if (!userForm.id && (userForm.password.length < 4 || userForm.password.length > 16)) {
        this.msg.warning('Password must have at least 4 characters');
        return;
    }
    userForm.level = "THIRD";        
    userForm.parentId = this.parentId;
    if (userForm.id) {     
        this.loading = true;   
        this.user.modify(userForm).pipe(
            catchError(err => {
                return of(err)
            })
        ).subscribe(ret => {
            this.loading = false;
            if (ret.status == 200) {
                this.msg.success('Successful');
                this.close();
            } else {
                this.msg.error(ret.message);
            }
        })
    } else {
        this.loading = true;
        this.user.addThird(userForm).pipe(
            catchError(err => {
                return of(err);
            })
        ).subscribe(ret => {
            this.loading = false;
            if (ret.status == 200) {
                this.msg.success('Successful');
                this.close();
            } else {
                this.msg.error(ret.message);
            }
        })
    }
  }

  close() {
    this.modal.close(true);
    this.modal.destroy();
  }
}
