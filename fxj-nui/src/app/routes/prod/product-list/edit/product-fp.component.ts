import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { ProductService } from '../../../../service/prod/product.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {SimpleTableColumn} from "@delon/abc";
import {fkService} from "../../../../service/fk/fk.service";

@Component({
  selector: 'product-view-fp',
  templateUrl: './product-fp.component.html',
  styleUrls: ['./product-fp.component.less']
})
export class ProductfpComponent implements OnInit, OnDestroy {
  product:  any = {
    id:null,
    bmDate: null,
    bmTime: null,
    bxRq: null,
    carColor: null,
    carDetail: null,
    carVin: null,
    conJson: null,
    conNo: null,
    conType: null,
    conUrl: null,
    createSysorg: null,
    createUser: null,
    cusAge: null,
    cusBirthday: null,
    cusDeu: null,
    cusEthnical: null,
    cusGender: null,
    cusIdValidfrom: null,
    cusIdValidto: null,
    cusIncard: null,
    cusMaritalStatus: null,
    cusMobile: null,
    cusName: null,
    cusQqid: null,
    cusRemark: null,
    cusResAddr: null,
    cusWechatid: null,
    cusrIdtype: null,
    danganNo: null,
    diyaStatus: null,
    diyaUser: null,
    fangkBank: null,
    fangkId: null,
    fangkName: null,
    fangkType: null,
    fkAmount: null,
    gpsInsAddr: null,
    gpsInsData: null,
    gpsInsMobile: null,
    gpsInsUser: null,
    gpsStatus: null,
    gpsUser: null,
    guohuStatus: null,
    guohuUser: null,
    luqiaoRq: null,
    lxMobile1: null,
    lxMobile2: null,
    lxMobile3: null,
    lxName1: null,
    lxName2: null,
    lxName3: null,
    mpName: null,
    mpUrl: null,
    mpbackUrl: null,
    nianjiaRq: null,
    orderAmount: null,
    orderArea: null,
    orderBy1: null,
    orderBy10: null,
    orderBy11: null,
    orderBy12: null,
    orderBy13: null,
    orderBy14: null,
    orderBy15: null,
    orderBy16: null,
    orderBy17: null,
    orderBy18: null,
    orderBy2: null,
    orderBy3: null,
    orderBy4: null,
    orderBy5: null,
    orderBy6: null,
    orderBy7: null,
    orderBy8: null,
    orderBy9: null,
    orderCarno: null,
    orderCartype: null,
    orderInfo: null,
    orderNo: null,
    orderOper: null,
    orderPeriod: null,
    orderStatus: null,
    jiafangUser:null,
    proClass: null,
    proIsdb: null,
    proName: null,
    proNo: null,
    proType: null,
    proWf: null,
    qiandanUser: null,
    sfAnpaijiafang: null,
    sfDianshen: null,
    wbName: null,
    wbUrl: null,
    wbbackUrl: null,
    yinhangUser: null,
    dypname: null,
    dypvalue: null,
    dypyear: null,
    dyptype: null,
    dypclass: null,

    dypremark1: null,
    dypremark2: null,
    dypremark3: null,
    emergencyRelation:  null,
    emergencyContact: null,
    emergencyMobile: null,
    emergencyOffice:null,
    emergencyResAddr:null,
    spcomment:'同意',

  };
  productfk:  any = {
    bpmStatus: null,
    bxFy: null,
    bxRq: null,
    carChengshu: null,
    carPinggujia: null,
    carRongzie: null,
    conJine: null,
    createBy: null,
    createDate: null,
    createName: null,
    cusId: null,
    cusMobile: null,
    cusName: null,
    daoshouJine: null,
    fkBy1: null,
    fkBy10: null,
    fkBy11: null,
    fkBy12: null,
    fkBy13: null,
    fkBy2: null,
    fkBy3: null,
    fkBy4: null,
    fkBy5: null,
    fkBy6: null,
    fkBy7: null,
    fkBy8: null,
    fkBy9: null,
    gpsFy: null,
    id: null,
    liulFy: null,
    loanAmount: null,
    loanAtta: null,
    loanDate: null,
    loanDbmonth: null,
    loanMonth: null,
    loanMonthSum: null,
    loanPeriod: null,
    loanReMethod: '',
    loanRemark: null,
    loanSbamount: null,
    loanSum: null,
    luqiaoRq: null,
    lydbFuwfei: null,
    nianjianRq: null,
    orderNo: null,
    qitaFy: null,
    sysCompanyCode: null,
    sysOrgCode: null,
    updateBy: null,
    updateDate: null,
    updateName: null,
    ygJine: null,

  };
  ps = 100;
  // 搜索表单
  q: any = {
    bpmStatus:"",
    cusName: '',
    cusMobile: '',
    cusIncard: '',
    proNo: '',
    orderNo:''
  };


  columns: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '操作状态', index: 'bpmStatus' },
    { title: '订单编号', index: 'orderNo' },
    { title: '操作人 ', index: 'logUser'},
    { title: '日志日期 ', index: 'logDate' },
    { title: '日志内容', index: 'logContent' }

  ];
  // 预览图片对象
  previewImage = '';
  // 是否可以预览
  previewVisible = false;
  // 预览图片地址
  propName = "";
  picUrl = '';
  // 图片上传地址

  data = [];
  downurl='';
  // downurl='https://zhaodui.oss-cn-shenzhen.aliyuncs.com/doc/fxbbcartempv1.xls';

  proNos = [];
  cusrIdtypes = [];
  cusGenders = [];
  cusMaritalStatuss = [];
  cusDeus = [];
  cusEthnicals = [];
  bpmStatus = [];
  dyptypes = [];
  dypclasss = [];
  relTypes=[];
  dyrs=[];
  ghrs=[];
  yhfrys=[];
  gpsazys=[];
  qdys=[];
  jiafangUsers=[];

  avatarUrl: string;

  avatarUrl1:string;
  avatarUrl2:string;

  avatarUrl3:string;
  logurl:string;
  avatarUrl4:string;
  avatarUrl5:string;
  loading = false;
  uploadUrl = '';
  uploadUr5 = '';
  orbitUrl='';
  avatarParam1 = { sbtype: '2', filetype:'sb',orderNo: '2'};
  avatarParam2 = { sbtype: '3', filetype:'sb', orderNo: '3'};
  avatarParam3 = { sbtype: '6',  filetype:'sb',orderNo: '6'};
  avatarParam4 = { sbtype: '2', filetype:'sb', orderNo: '2'};
  avatarParam5 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};

  fileList = [ ];
  backUrl='';
  MAX_COUNT = 60;
  tabIndex = 0;
  saleing = false;
  private sub: any;

  constructor(
    public setting: SettingsService,
    private fkService: fkService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private productService: ProductService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService,
    private confirmServ: NzModalService,
  ) {}

  ngOnInit(): void {
    this.loadproNos();
    this.loadcusrIdtypes();
    this.loadcusGenders();
    this.loadcusMaritalStatuss();
    this.loadcusDeus();
    this.loadcusEthnicals();
    this.loadbpmStatus();
    this.loaddyptypes();
    this.loaddypclasss();
    this.loadrelTypes();
    this.loadqdys();

    this.uploadUrl = this.productService.uploadsbUrl();
    this.uploadUr5 = this.productService.uploadUrl();
    this.orbitUrl = "http://120.78.150.43/spmq";
    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"];
      // 获取商品详情
      this.getProductInfo();
    });
    // 隐藏reusetab
    setTimeout(() => {
        this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);

  }

  // 生成合同
  packattac(){
    this.confirmServ.confirm({
      nzTitle: '确定要打包合同?',
      nzOnOk: () => {
        this.packatta();
        },
    });
  }
  packatta(){

    let conmapList = [];

    try{
      let dto1 ={
        conKey:"htbh",
        conValue:this.product.orderNo,
      }
      conmapList.push(dto1);
    }catch (e) {

    }
    try{
      let dto2 ={
        conKey:"xm",
        conValue:this.product.cusName,
      }
      conmapList.push(dto2);
    }catch (e) {

    }
    try{
      let dto3 ={
        conKey:"sfz",
        conValue:this.product.cusIncard,
      }
      conmapList.push(dto3);
    }catch (e) {

    }
    try{
      let dto4 ={
        conKey:"lxdz",
        conValue:this.product.cusResAddr,
      }
      conmapList.push(dto4);
    }catch (e) {

    }
    try{
      let dto5 ={
        conKey:"lxdh",
        conValue:this.product.cusMobile,
      }
      conmapList.push(dto5);
    }catch (e) {

    }
    try{
      let dto6 ={
        conKey:"cphm",
        conValue:this.product.dypvalue,
      }
      conmapList.push(dto6);
    }catch (e) {

    }
    try{
      let dto7 ={
        conKey:"cjh",
        conValue:this.product.dypremark2,
      }
      conmapList.push(dto7);
    }catch (e) {

    }
    try{
      let dto8 ={
        conKey:"fdjh",
        conValue:this.product.dypremark3,
      }
      conmapList.push(dto8);
    }catch (e) {

    }
    try{
      let dto9 ={
        conKey:"cpxh",
        conValue:this.product.dypname,
      }
      conmapList.push(dto9);
    }catch (e) {

    }
    try{
      let dto10 ={
        conKey:"htje",
        conValue:this.product.loanSum,
      }
      conmapList.push(dto10);
    }catch (e) {

    }
    try{
      let dto11 ={
        conKey:"cllx",
        conValue:this.product.dypclass,
      }
      conmapList.push(dto11);
    }catch (e) {

    }
    try{
      let dto12 ={
        conKey:"zjze",
        conValue:this.productfk.loanSum,
      }
      conmapList.push(dto12);
    }catch (e) {

    }
    try{
      let dto13 ={
        conKey:"gzsj",
        conValue:this.product.dyptype,
      }
      conmapList.push(dto13);
    }catch (e) {

    }
    try{
      let dto14 ={
        conKey:"zlqx",
        conValue:this.productfk.loanPeriod,
      }
      conmapList.push(dto14);
    }catch (e) {

    }
    try{
      let dto15 ={
        conKey:"yzxl",
        conValue:this.productfk.loanMonth,
      }
      conmapList.push(dto15);
    }catch (e) {

    }


    let  conhead={
      contempPath:"https://zhaodui.oss-cn-shenzhen.aliyuncs.com/doc/htmb/hxhtmb.docx",
      conmapList:JSON.parse(JSON.stringify(conmapList)),
    }
    this.comm.gencon(this.product.orderNo,conhead).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.msg.success('生成成功');
        if(data.ok){
          console.log(JSON.stringify("data"+JSON.stringify(data)));
          this.downurl = data.data.contoUrl;
          if(this.downurl!=null||this.downurl==null){
            this.opennewwindow(this.downurl);
          }
        }


    })




  }
  opennewwindow(url){

    const newWindow = window.open(url, '_blank');
    newWindow.document.title = '合同';

  }
  fyccount() {
    let sum = 0;
    sum  = Number(sum) + Number(this.productfk.carPinggujia);
    let chengshu  =   Number(this.productfk.carChengshu);
    if(this.productfk.carRongzie ==''|| this.productfk.carRongzie  ==null){
      this.productfk.carRongzie=chengshu*sum;
    }
    if(this.productfk.bxFy ==''|| this.productfk.bxFy  ==null){
      this.productfk.bxFy = sum*0.05;
    }
    this.fybcount();
    this.fyzhcbcount();
  }

  fybcount() {
    let sum = 0;
    sum  =   Number(this.productfk.carRongzie);
    let jixjine  = Number(sum) + Number(this.productfk.bxFy);

    if(this.productfk.loanSum ==''|| this.productfk.loanSum  ==null){
      this.productfk.loanSum = jixjine;
    }
  }

  fyzhcbcount() {
    let lixi = 0;
    lixi  = Number(lixi) + Number(this.productfk.loanMonth);
    let loanPeriod  =  Number(this.productfk.loanPeriod);
    let benjin  =  Number(this.productfk.loanSum);

    this.productfk.conJine = Math.round(benjin*(1+lixi*loanPeriod/100));

    let jxjine = Number(this.productfk.loanSum);

    this.productfk.loanAmount  = Math.round(benjin*(1+lixi*loanPeriod/100)/1.1034/100)*100;

    this.productfk.lydbFuwfei =  Math.round(benjin*(1+lixi*loanPeriod/100)/1.1034/100)*100 - jxjine;
    this.productfk.daoshouJine =  Math.round( ((Number(this.productfk.loanSum ) - Number(this.productfk.bxFy) - Number(this.productfk.gpsFy) -  Number(this.productfk.conJine)/loanPeriod))*10 )/10;
  }
  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  changeTab(index) {
    // 使用异步的方式更新组件, 防止ngIf错误
    Promise.resolve(null).then(() => this.tabIndex = index);
  }
  loadrelTypes() {
    this.relTypes = [];
    this.comm.getType("reltype").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.relTypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
// 加载产品
  loadproNos() {
    this.proNos = [];
    this.comm.getType("proNo").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.proNos.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载产品
  loadqdys() {
    this.qdys = [];
    this.comm.getType("qdy").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.qdys.push({ text: row.typename, value: row.typecode });
        }
      }
    })

    this.ghrs = [];
    this.comm.getType("ghr").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.ghrs.push({ text: row.typename, value: row.typecode });
        }
      }
    })
    this.yhfrys = [];
    this.comm.getType("yhfry").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.yhfrys.push({ text: row.typename, value: row.typecode });
        }
      }
    })
    this.gpsazys = [];
    this.comm.getType("gpsazy").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.gpsazys.push({ text: row.typename, value: row.typecode });
        }
      }
    })
    this.dyrs = [];
    this.comm.getType("dyr").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.dyrs.push({ text: row.typename, value: row.typecode });
        }
      }
    })
    this.jiafangUsers = [];
    this.comm.getType("jiafang").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.jiafangUsers.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载证件类型
  loadcusrIdtypes() {
    this.cusrIdtypes = [];
    this.comm.getType("idType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusrIdtypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载性别
  loadcusGenders() {
    this.cusGenders = [];
    this.comm.getType("sex").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusGenders.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载婚姻
  loadcusMaritalStatuss() {
    this.cusMaritalStatuss = [];
    this.comm.getType("marType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusMaritalStatuss.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载学历
  loadcusDeus() {
    this.cusDeus = [];
    this.comm.getType("eduType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusDeus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
//加载民族
  loadcusEthnicals() {
    this.cusEthnicals = [];
    this.comm.getType("ethType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusEthnicals.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载证件类型
  loadbpmStatus() {
    this.bpmStatus = [];
    this.comm.getType("bpm_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.bpmStatus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载证件类型
  loaddyptypes() {
    this.dyptypes = [];
    this.comm.getType("dyptype").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.dyptypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loaddypclasss() {
    this.dypclasss = [];
    this.comm.getType("dypclass").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.dypclasss.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  getProductInfo() {
    this.productService.getProduct(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.product = data.data;
      this.backUrl = data.data.wbbackUrl;
      this.product.yinhangUser="admin";
      this.titleService.setTitle("编辑订单("+this.product.orderNo+")");
      this.logurl = this.productService.logurl()+this.product.orderNo;
      this.getProductInfofk();

      this.avatarParam1 = { sbtype: '2', filetype:'sb',orderNo: this.product.orderNo};
      this.avatarParam2 = { sbtype: '3', filetype:'sb', orderNo: this.product.orderNo};
      this.avatarParam3 = { sbtype: '6',  filetype:'sb',orderNo: this.product.orderNo};
      this.avatarParam4 = { sbtype: '2', filetype:'sb', orderNo: this.product.orderNo};
      this.avatarParam5 = { sbtype: 'No',  filetype:'mq',orderNo: this.product.orderNo};
      if (data.data.fxjOrderEmerList!=null&&data.data.fxjOrderEmerList.length > 0) {
        this.product.emergencyRelation = data.data.fxjOrderEmerList[0].emergencyRelation;
        this.product.emergencyContact = data.data.fxjOrderEmerList[0].emergencyContact;
        this.product.emergencyMobile = data.data.fxjOrderEmerList[0].emergencyMobile;
        this.product.emergencyOffice = data.data.fxjOrderEmerList[0].emergencyOffice;
        this.product.emergencyResAddr = data.data.fxjOrderEmerList[0].emergencyResAddr;
      }
  console.log("data.data.fxjOrderEmerList[0].emergencyRelation"+data.data.fxjOrderEmerList[0].emergencyRelation);
      if (data.data.fxjOrderLoanList.length > 0) {
        let flag = 0;
        for (const detail of data.data.fxjOrderLoanList) {
          console.log("detail"+detail);

          if(detail.elementNo=='10'){
            this.product.dypname =detail.elementContent;
          }
          if(detail.elementNo=='20'){
            this.product.dypvalue =detail.elementContent;

          }
          if(detail.elementNo=='30'){
            this.product.dypyear =detail.elementContent;

          }
          if(detail.elementNo=='40'){
            this.product.dyptype =detail.elementContent;

          }if(detail.elementNo=='50'){
            this.product.dypclass =detail.elementContent;

          }
          if(detail.elementNo=='60'){
            this.product.dypremark1 =detail.elementContent;

          }if(detail.elementNo=='70'){
            this.product.dypremark2 =detail.elementContent;

          }if(detail.elementNo=='80'){
            this.product.dypremark3 =detail.elementContent;
          }
        }
      };

      if (data.data.fxjOrderAttaList.length > 0) {
        let flag = 0;
        for (const detail of data.data.fxjOrderAttaList) {
          this.fileList.push( {
            uid: flag,
            name: detail.attaName,
            status: 'done',
            url: detail.attaUrl
          } )
          flag++;
        }
      };
    })
  }

  getProductInfofk() {
    this.fkService.getProduct(this.product.orderNo).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if(data.ok){
        this.productfk = data.data;
        console.log("this.productfk"+JSON.stringify(this.productfk));
      }else{
        this.productfk.loanReMethod='10';
      }

      // this.productfk.loanDate="";
    })
  }
  beforeUpload = (file: File) => {
      const isJPG = file.type === 'image/jpeg';
      const isPNG = file.type === 'image/png';
      if (!isJPG && !isPNG) {
          this.msg.warning(`不支持的格式: ${file.type}`);
      }
      const isLt2M = file.size / 1024 / 1024 < 10;
      if (!isLt2M) {
          this.msg.warning('图片不能超过2MB!');
      }
      return (isJPG || isPNG) && isLt2M;
  }

  private getBase64(img: File, callback: (img: any) => void) {
      if (img) {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
      }
  }



  formatterYuan = value => `¥ ${value}`;
  parserYuan = value => value.replace('¥', '');

  formatterDollor = value => `${value}`;
  parserDollor = value => value.replace('$', '');

  back() {
    this.router.navigate([ this.backUrl]);
  }


  savestc(orderOper,title) {
    if(orderOper=='N'){
      if(this.product.spcomment == '' || this.product.spcomment == null){
        this.msg.warning("请填写审批意见");
        return;
      }
    }
    if(orderOper=='R'){
      if(this.product.spcomment == '' || this.product.spcomment == null){
        this.msg.warning("请填写审批意见");
        return;
      }
    }
    this.confirmServ.confirm({
      nzTitle: '确定'+title+'?',
      nzOnOk: () => {
        this.savest(orderOper);
        },
    });
  }

  savest(orderOper) {
    // 调用接口
    if(this.product.spcomment==''||this.product.spcomment==null){
      this.product.spcomment="同意";
    }
    if(this.product.qiandanUser==''||this.product.qiandanUser==null){
      this.msg.warning("请选择签单员");
      return;
    }

    if(this.product.diyaUser==''||this.product.diyaUser==null){
      this.msg.warning("请选择抵押人");
      return;
    }


    if(this.product.gpsUser=='weifenpei' || this.product.gpsUser=='' || this.product.gpsUser== null){
      this.msg.warning("请选择GPS安装人员");
      return;
    }


    if(this.product.qiandanUser==''||this.product.qiandanUser==null){
      this.msg.warning("请选择签单员");
      return;
    }

    // product.spcomment
    let fxjOrderLogList = [];
    if(this.product.spcomment!=''){
      // console.log("this.product.dypname*********"+this.product.dypname);

      let dto ={
        orderNo:this.product.orderNo,
        logUser:this.setting.user.username,
        logContent:this.product.spcomment
      }
      fxjOrderLogList.push(dto);
    }


    let fxjOrderMainPage = {
      updateBy:this.setting.user.username,
      id:this.product.id,
      orderNo:this.product.orderNo,
      orderOper:orderOper,

      bmDate: this.product.bmDate,
      bmTime: this.product.bmTime,
      bxRq: this.product.bxRq,
      carColor: this.product.carColor,
      carDetail: this.product.carDetail,
      carVin: this.product.carVin,
      conJson: this.product.conJson,
      conNo: this.product.conNo,
      conType: this.product.conType,
      conUrl: this.product.conUrl,
      createSysorg: this.product.createSysorg,
      createUser: this.product.createUser,
      cusAge: this.product.cusAge,
      cusBirthday: this.product.cusBirthday,
      cusDeu: this.product.cusDeu,
      cusEthnical: this.product.cusEthnical,
      cusGender: this.product.cusGender,
      cusIdValidfrom: this.product.cusIdValidfrom,
      cusIdValidto: this.product.cusIdValidto,
      cusIncard: this.product.cusIncard,
      cusMaritalStatus: this.product.cusMaritalStatus,
      cusMobile: this.product.cusMobile,
      cusName: this.product.cusName,
      cusQqid: this.product.cusQqid,
      cusRemark: this.product.cusRemark,
      cusResAddr: this.product.cusResAddr,
      cusWechatid: this.product.cusWechatid,
      cusrIdtype: this.product.cusrIdtype,
      danganNo: this.product.danganNo,
      diyaStatus: this.product.diyaStatus,
      diyaUser: this.product.diyaUser,
      fangkBank: this.product.fangkBank,
      fangkId: this.product.fangkId,
      fangkName: this.product.fangkName,
      fangkType: this.product.fangkType,
      fkAmount: this.product.fkAmount,
      gpsInsAddr: this.product.gpsInsAddr,
      gpsInsData: this.product.gpsInsData,
      gpsInsMobile: this.product.gpsInsMobile,
      gpsInsUser: this.product.gpsInsUser,
      gpsStatus: this.product.gpsStatus,
      gpsUser: this.product.gpsUser,
      guohuStatus: this.product.guohuStatus,
      guohuUser: this.product.guohuUser,
      luqiaoRq: this.product.luqiaoRq,
      lxMobile1: this.product.lxMobile1,
      lxMobile2: this.product.lxMobile2,
      lxMobile3: this.product.lxMobile3,
      lxName1: this.product.lxName1,
      lxName2: this.product.lxName2,
      lxName3: this.product.lxName3,
      mpName: this.product.mpName,
      mpUrl: this.product.mpUrl,
      mpbackUrl: this.product.mpbackUrl,
      nianjiaRq: this.product.nianjiaRq,
      orderAmount: this.product.orderAmount,
      orderArea: this.product.orderArea,
      orderBy1: this.product.orderBy1,
      orderBy10: this.product.orderBy10,
      orderBy11: this.product.orderBy11,
      orderBy12: this.product.orderBy12,
      orderBy13: this.product.orderBy13,
      orderBy14: this.product.orderBy14,
      orderBy15: this.product.orderBy15,
      orderBy16: this.product.orderBy16,
      orderBy17: this.product.orderBy17,
      orderBy18: this.product.orderBy18,
      orderBy2: this.product.orderBy2,
      orderBy3: this.product.orderBy3,
      orderBy4: this.product.orderBy4,
      orderBy5: this.product.orderBy5,
      orderBy6: this.product.orderBy6,
      orderBy7: this.product.orderBy7,
      orderBy8: this.product.orderBy8,
      orderBy9: this.product.orderBy9,
      orderCarno: this.product.orderCarno,
      orderCartype: this.product.orderCartype,
      orderInfo: this.product.orderInfo,
      jiafangUser:this.product.jiafangUser,
      orderPeriod: this.product.orderPeriod,
      orderStatus: this.product.orderStatus,
      proClass: this.product.proClass,
      proIsdb: this.product.proIsdb,
      proName: this.product.proName,
      proNo: this.product.proNo,
      proType: this.product.proType,
      proWf: this.product.proWf,
      qiandanUser: this.product.qiandanUser,
      sfAnpaijiafang: this.product.sfAnpaijiafang,
      sfDianshen: this.product.sfDianshen,
      wbName: this.product.wbName,
      wbUrl: this.product.wbUrl,
      wbbackUrl: this.product.wbbackUrl,
      yinhangUser: this.product.yinhangUser,
      fxjOrderLogList:JSON.parse(JSON.stringify(fxjOrderLogList))

    };
console.log(this.product.gpsUser)
console.log(fxjOrderMainPage)

    this.productService.editPro(fxjOrderMainPage).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      console.log("data.ok"+data.ok);
      if (data.ok === true) {
        this.back();
        this.msg.success('分配成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })
  }



  changeColor(res) {
    this.product.colorSort = res.color.hex;
  }




  handleChange1(info: { file: UploadFile }) {


    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl1 = img;
        console.log("info1*********"+JSON.stringify(info.file.response[0].piccontent));

        var sfxx=JSON.parse(info.file.response[0].piccontent)

        this.product.cusName=sfxx.result.cardsinfo[0].items[1].content;
        this.product.cusGender=sfxx.result.cardsinfo[0].items[2].content;
        this.product.cusEthnical=sfxx.result.cardsinfo[0].items[3].content;
        this.product.cusBirthday=sfxx.result.cardsinfo[0].items[4].content;
        this.product.cusResAddr=sfxx.result.cardsinfo[0].items[5].content;
        this.product.cusIncard=sfxx.result.cardsinfo[0].items[6].content;
        info.file=null;
      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange2(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }


    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl2 = img;


        var sfxx=JSON.parse(info.file.response[0].piccontent)

        console.log("varfrom*********"+JSON.stringify(sfxx));
        this.product.cusIdValidfrom= sfxx.result.cardsinfo[0].items[3].content;
        this.product.cusIdValidto=sfxx.result.cardsinfo[0].items[4].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange3(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl3 = img;

        var sfxx=JSON.parse(info.file.response[0].piccontent)

        this.product.dyptype = sfxx.result.cardsinfo[0].items[5].content +"/"+  sfxx.result.cardsinfo[0].items[2].content;

        this.product.dypname = sfxx.result.cardsinfo[0].items[1].content;
        this.product.dypvalue = sfxx.result.cardsinfo[0].items[3].content;
        this.product.dypyear =  sfxx.result.cardsinfo[0].items[8].content + "/"+sfxx.result.cardsinfo[0].items[9].content;
        this.product.dypclass =  sfxx.result.cardsinfo[0].items[10].content;

        this.product.dypremark1 = sfxx.result.cardsinfo[0].items[4].content;
        this.product.dypremark2 = sfxx.result.cardsinfo[0].items[6].content;
        this.product.dypremark3   = sfxx.result.cardsinfo[0].items[7].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange4(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl4 = img;
        var sfxx=JSON.parse(info.file.response[0].piccontent)

        console.log("varfrom*********"+JSON.stringify(sfxx));
        this.product.emergencyContact=sfxx.result.cardsinfo[0].items[1].content;

        this.product.emergencyResAddr=sfxx.result.cardsinfo[0].items[5].content;
        this.product.emergencyOffice=sfxx.result.cardsinfo[0].items[6].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange5(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl5 = img;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }

  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  }
}
