import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { ProductService } from '../../../../service/prod/product.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {SimpleTableColumn} from "@delon/abc";

@Component({
  selector: 'product-rmview-view',
  templateUrl: './product-rmview.component.html',
  styleUrls: ['./product-rmview.component.less']
})
export class ProductrmviewComponent implements OnInit, OnDestroy {
  mqurl:any;
  rmid:any;
  private sub: any;

  constructor(
    public setting: SettingsService,

    private msg: NzMessageService,
    private modalService: NzModalService,
    private productService: ProductService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService
  ) {}

  ngOnInit(): void {


    this.sub = this.route.params.subscribe(params => {
      this.rmid= params["id"];
      this.getrm();
      // 获取商品详情
    });
    // 隐藏reusetab
    setTimeout(() => {
        this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);

  }
  getrm(){
  this.productService.getrmbyid(this.rmid).pipe(
    catchError(err => {
  return of(err);
})
).subscribe(data => {
  console.log("*************************"+JSON.stringify(data.data));
  this.mqurl = data.data.rmBy13+this.rmid;


})
}
  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  changeTab(index) {
    // 使用异步的方式更新组件, 防止ngIf错误
  }


}
