import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { ProductService } from '../../../../service/prod/product.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {SimpleTableColumn, SimpleTableComponent} from "@delon/abc";
import {fkService} from "../../../../service/fk/fk.service";
import {plService} from "../../../../service/pl/pl.service";

@Component({
  selector: 'product-view-fk',
  templateUrl: './product-fk.component.html',
  styleUrls: ['./product-fk.component.less']
})
export class ProductfkComponent implements OnInit, OnDestroy {
  product:  any = {
    id:null,
    bmDate: null,
    bmTime: null,
    bxRq: null,
    carColor: null,
    carDetail: null,
    carVin: null,
    conJson: null,
    conNo: null,
    conType: null,
    conUrl: null,
    createSysorg: null,
    createUser: null,
    cusAge: null,
    cusBirthday: null,
    cusDeu: null,
    cusEthnical: null,
    cusGender: null,
    cusIdValidfrom: null,
    cusIdValidto: null,
    cusIncard: null,
    cusMaritalStatus: null,
    cusMobile: null,
    cusName: null,
    carPinggujia:null,
    cusQqid: null,
    cusRemark: null,
    cusResAddr: null,
    cusWechatid: null,
    cusrIdtype: null,
    danganNo: null,
    diyaStatus: null,
    diyaUser: null,
    fangkBank: null,
    fangkId: null,
    fangkName: null,
    fangkType: null,
    fkAmount: null,
    gpsInsAddr: null,
    gpsInsData: null,
    gpsInsMobile: null,
    gpsInsUser: null,
    gpsStatus: null,
    gpsUser: null,
    guohuStatus: null,
    guohuUser: null,
    luqiaoRq: null,
    lxMobile1: null,
    lxMobile2: null,
    lxMobile3: null,
    lxName1: null,
    lxName2: null,
    lxName3: null,
    mpName: null,
    mpUrl: null,
    mpbackUrl: null,
    nianjiaRq: null,
    orderAmount: null,
    orderArea: null,
    orderBy1: null,
    orderBy2: null,
    orderBy3: null,
    orderBy4: null,
    orderBy5: null,
    orderBy6: null,
    orderBy7: null,
    orderBy8: null,
    orderBy9: null,
    orderBy10: null,
    orderBy11: null,
    orderBy12: null,
    orderBy13: null,
    orderBy14: null,
    orderBy15: null,
    orderBy16: null,
    orderBy17: null,
    orderBy18: null,
    orderBy19:null,
    orderBy20: null,
    orderBy21: null,
    orderBy22: null,
    orderBy23: null,
    orderBy24: null,
    orderBy25: null,
    orderBy26: null,
    orderBy27: null,
    orderBy28: null,
    orderBy29:null,
    orderBy30: null,
    orderBy31: null,
    orderBy32: null,
    orderBy33: null,
    orderBy34: null,
    orderBy35: null,
    orderBy36: null,
    orderBy37: null,
    orderBy38: null,
    orderBy39:null,
    orderBy40: null,
    orderBy41: null,
    orderBy42: null,
    orderBy43: null,
    orderBy44: null,
    orderBy45: null,
    orderBy46: null,
    orderBy47: null,
    orderBy48: null,
    orderBy49:null,
    orderBy50: null,
    orderBy51: null,
    orderBy52: null,
    orderBy53: null,
    orderBy54: null,
    orderBy55: null,
    orderBy56: null,
    orderBy57: null,
    orderBy58: null,
    orderBy59:null,
    orderBy60:null,
    orderCarno: null,
    orderCartype: null,
    orderInfo: null,
    orderNo: null,
    orderOper: null,
    orderPeriod: null,
    orderStatus: null,
    proClass: null,
    proIsdb: null,
    proName: null,
    proNo: null,
    proType: null,
    proWf: null,
    qiandanUser: null,
    sfAnpaijiafang: null,
    sfDianshen: null,
    wbName: null,
    wbUrl: null,
    wbbackUrl: null,
    yinhangUser: null,
    dypname: null,
    dypvalue: null,
    dypyear: null,
    dyptype: null,
    dypclass: null,

    dypremark1: null,
    dypremark2: null,
    dypremark3: null,
    emergencyRelation:  null,
    emergencyContact: null,
    emergencyMobile: null,
    emergencyOffice:null,
    emergencyResAddr:null,
    spcomment:'同意',
  };

  productfk:  any = {
    bpmStatus: null,
    bxFy: null,
    bxRq: null,
    carChengshu: 0.8,
    carPinggujia: null,
    loanSum:null,
    carRongzie: null,
    conJine: null,
    createBy: null,
    createDate: null,
    createName: null,
    cusId: null,
    cusMobile: null,
    cusName: null,
    daoshouJine: null,
    fkBy1: null,
    fkBy10: null,
    fkBy11: null,
    fkBy12: null,
    fkBy13: null,
    fkBy2: null,
    fkBy3: null,
    fkBy4: null,
    fkBy5: null,
    fkBy6: null,
    fkBy7: null,
    fkBy8: null,
    fkBy9: null,
    gpsFy: null,
    id: null,
    liulFy: null,
    loanAmount: null,
    loanAtta: null,
    loanDate: null,
    loanDbmonth: null,
    loanMonth: null,
    loanMonthSum: null,
    loanPeriod: null,
    loanReMethod: null,
    loanRemark: null,
    loanSbamount: null,
    luqiaoRq: null,
    lydbFuwfei: null,
    nianjianRq: null,
    orderNo: null,
    qitaFy: null,
    sysCompanyCode: null,
    sysOrgCode: null,
    updateBy: null,
    updateDate: null,
    updateName: null,
    ygJine: null,

  };
  ps = 100;
  // 搜索表单
  q: any = {
    bpmStatus:"",
    cusName: '',
    cusMobile: '',
    cusIncard: '',
    proNo: '',
    orderNo:''
  };

  // loanReMethods =  [{text: '等本等息', value: '10'}, {text: '先息后本', value: '20'}];
  loanReMethods =  [{text: '等额本息', value: '10'}];

  columns: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '操作状态', index: 'bpmStatus' },
    { title: '订单编号', index: 'orderNo' },
    { title: '操作人 ', index: 'logUser'},
    { title: '日志日期 ', index: 'logDate' },
    { title: '日志内容', index: 'logContent' }

  ];
  // 预览图片对象
  previewImage = '';
  previewImage1 = '';
  previewImage2 = '';
  previewImage3 = '';
  previewImage4 = '';
  previewImage5 = '';
  // 是否可以预览
  previewVisible = false;
  previewVisible1 = false;
  previewVisible2 = false;
  previewVisible3 = false;
  previewVisible4 = false;
  previewVisible5 = false;
  // 预览图片地址
  propName = "";
  picUrl = '';
  // 图片上传地址

  proNos = [];
  cusrIdtypes = [];
  cusGenders = [];
  cusMaritalStatuss = [];
  cusDeus = [];
  cusEthnicals = [];
  bpmStatus = [];
  dyptypes = [];
  dypclasss = [];
  relTypes=[];
  avatarUrl: string;
  avatarUrl1:string;
  avatarUrl2:string;
  avatarUrl3:string;
  logurl:string;
  avatarUrl4:string;
  avatarUrl5:string;
  loading = false;
  uploadUrl = '';
  uploadUr1 = '';
  uploadUr5 = '';
  uploadUr6 = '';
  uploadUr7 = '';
  uploadUr8 = '';
  uploadUr9 = '';
  orbitUrl='';
  avatarParam1 = { sbtype: '2', filetype:'sb',orderNo: '2'};
  avatarParam2 = { sbtype: '3', filetype:'sb', orderNo: '3'};
  avatarParam3 = { sbtype: '6',  filetype:'sb',orderNo: '6'};
  avatarParam4 = { sbtype: '2', filetype:'sb', orderNo: '2'};
  avatarParam5 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam6 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam7 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam8 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam9 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};

  fileList = [ ];
  fileList1 = [ ];
  fileList2 = [ ];
  fileList3 = [ ];
  fileList4 = [ ];
  fileList5 = [ ];
  backUrl='';
  MAX_COUNT = 60;
  tabIndex = 0;
  saleing = false;
  private sub: any;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  urlreplan = this.plService.listorder("11");


  columnsreplan: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    // { title: '产品种类', index: 'proClass' },
    // { title: '融租类型', index: 'proType'},
    { title: '车辆信息', index: 'orderInfo' },
    { title: '订单编号 ', index: 'orderNo' },
    { title: '客户姓名' , index: 'cusName' },
    { title: '期数', index: 'refPeriod' },
    { title: '总期数', index: 'refTotalPeriod' },
    { title: '还款日期', index: 'refDate' },
    { title: '期初金额', index: 'refFbenjin' },
    { title: '还款金额', index: 'refAmount' },
    // { title: '归还利息', index: 'refLixi' },
    { title: '剩余本金', index: 'refSybenjin' },
    { title: '备注', index: 'reMark' },
    // { title: '年份', index: 'year' },
    // { title: '广告款', index: 'isAd', render: 'isAd' },
    { title: '状态', index: 'bpmStatus', render: 'bpmStatus' },

  ];

  constructor(
    public setting: SettingsService,
    private fkService: fkService,
    private plService: plService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private productService: ProductService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService,
    private confirmServ: NzModalService,
    private notice: NzNotificationService,

  ) {}

  ngOnInit(): void {
    this.loadproNos();
    this.loadcusrIdtypes();
    this.loadcusGenders();
    this.loadcusMaritalStatuss();
    this.loadcusDeus();
    this.loadcusEthnicals();
    this.loadbpmStatus();
    this.loaddyptypes();
    this.loaddypclasss();
    this.loadrelTypes();
    this.uploadUrl = this.productService.uploadsbUrl();
    this.uploadUr1 = this.productService.uploadUrl();
    this.uploadUr5 = this.productService.uploadUrl();
    this.uploadUr6 = this.productService.uploadUrl();
    this.uploadUr7 = this.productService.uploadUrl();
    this.uploadUr8 = this.productService.uploadUrl();
    this.uploadUr9 = this.productService.uploadUrl();
    this.orbitUrl = "http://120.78.150.43/spmq";
    this.uploadreplanExcelUrl = this.plService.uploadreplaninfo();

    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"];
      // 获取商品详情
      this.getProductInfo();
      console.log(this.getProductInfo);
    });
    // 隐藏reusetab
    setTimeout(() => {
        this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);

  }

  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  changeTab(index) {
    // 使用异步的方式更新组件, 防止ngIf错误
    Promise.resolve(null).then(() => this.tabIndex = index);
  }
  loadrelTypes() {
    this.relTypes = [];
    this.comm.getType("reltype").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.relTypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
// 加载产品
  loadproNos() {
    this.proNos = [];
    this.comm.getType("proNo").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.proNos.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载证件类型
  loadcusrIdtypes() {
    this.cusrIdtypes = [];
    this.comm.getType("idType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusrIdtypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载性别
  loadcusGenders() {
    this.cusGenders = [];
    this.comm.getType("sex").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusGenders.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载婚姻
  loadcusMaritalStatuss() {
    this.cusMaritalStatuss = [];
    this.comm.getType("marType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusMaritalStatuss.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载学历
  loadcusDeus() {
    this.cusDeus = [];
    this.comm.getType("eduType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusDeus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
//加载民族
  loadcusEthnicals() {
    this.cusEthnicals = [];
    this.comm.getType("ethType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusEthnicals.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载证件类型
  loadbpmStatus() {
    this.bpmStatus = [];
    this.comm.getType("bpm_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.bpmStatus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载证件类型
  loaddyptypes() {
    this.dyptypes = [];
    this.comm.getType("dyptype").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.dyptypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loaddypclasss() {
    this.dypclasss = [];
    this.comm.getType("dypclass").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.dypclasss.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  } 

  getProductInfofk() {
    this.fkService.getProduct(this.product.orderNo).pipe(
      catchError(err => {
        return of(err);
        console.log(this.product.orderNo)
      })
    ).subscribe(data => {
      if (data.ok === true) {
        this.productfk = data.data;
        this.productfk.orderNo=this.product.orderNo;
        this.productfk.cusName=this.product.cusName;
        this.productfk.carPinggujia=this.product.orderBy32;
        this.productfk.loanSum=this.product.orderBy51;
        this.productfk.carRongzie=this.product.orderBy34;
        this.productfk.loanAmount=this.product.orderBy48;
        this.productfk.conJine=this.product.orderBy48;
        this.productfk.loanMonth = 0;
        // alert(this.productfk.carPinggujia);
        this.productfk.cusMobile=this.product.cusMobile;
        // this.productfk.carChengshu= 0.8;
          this.productfk.loanPeriod= this.product.orderBy50;
        //   this.productfk.gpsFy= 2000;
      }

    })
  }

  getProductInfo() {
    this.productService.getProduct(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      console.log("*************************"+JSON.stringify(data.data));
      this.product = data.data;
      this.urlreplan = this.plService.listorder(this.product.orderNo);
      this.backUrl = data.data.wbbackUrl;
      this.getProductInfofk();
      console.log(this.getProductInfofk)
      this.titleService.setTitle("编辑订单("+this.product.orderNo+")");
      this.logurl = this.productService.logurl()+this.product.orderNo;
      this.avatarParam1 = { sbtype: '2', filetype:'sb',orderNo: this.product.orderNo};
      this.avatarParam2 = { sbtype: '3', filetype:'sb', orderNo: this.product.orderNo};
      this.avatarParam3 = { sbtype: '6',  filetype:'sb',orderNo: this.product.orderNo};
      this.avatarParam4 = { sbtype: '2', filetype:'sb', orderNo: this.product.orderNo};
      this.avatarParam5 = { sbtype: 'No',  filetype:'mq',orderNo: this.product.orderNo};
      this.avatarParam6 = { sbtype: '10',  filetype:'yhls',orderNo: this.product.orderNo};
      this.avatarParam7 = { sbtype: '20',  filetype:'zxbg',orderNo: this.product.orderNo};
      this.avatarParam8 = { sbtype: '30',  filetype:'wxb',orderNo: this.product.orderNo};
      this.avatarParam9 = { sbtype: '30',  filetype:'jszandxsz',orderNo: this.product.orderNo};
      if (data.data.fxjOrderEmerList.length > 0) {
        this.product.emergencyRelation = data.data.fxjOrderEmerList[0].emergencyRelation;
        this.product.emergencyContact = data.data.fxjOrderEmerList[0].emergencyContact;
        this.product.emergencyMobile = data.data.fxjOrderEmerList[0].emergencyMobile;
        this.product.emergencyOffice = data.data.fxjOrderEmerList[0].emergencyOffice;
        this.product.emergencyResAddr = data.data.fxjOrderEmerList[0].emergencyResAddr;
      }
  console.log("data.data.fxjOrderEmerList[0].emergencyRelation"+data.data.fxjOrderEmerList[0].emergencyRelation);
      if (data.data.fxjOrderLoanList.length > 0) {
        let flag = 0;
        for (const detail of data.data.fxjOrderLoanList) {
          console.log("detail"+detail);

          if(detail.elementNo=='10'){
            this.product.dypname =detail.elementContent;
          }
          if(detail.elementNo=='20'){
            this.product.dypvalue =detail.elementContent;

          }
          if(detail.elementNo=='30'){
            this.product.dypyear =detail.elementContent;

          }
          if(detail.elementNo=='40'){
            this.product.dyptype =detail.elementContent;

          }if(detail.elementNo=='50'){
            this.product.dypclass =detail.elementContent;

          }
          if(detail.elementNo=='60'){
            this.product.dypremark1 =detail.elementContent;

          }if(detail.elementNo=='70'){
            this.product.dypremark2 =detail.elementContent;

          }if(detail.elementNo=='80'){
            this.product.dypremark3 =detail.elementContent;
          }
        }
      };

      if (data.data.fxjOrderAttaList.length > 0) {
        let flag = 0;
        let flag1 = 0;
        let flag2 = 0;
        let flag3 = 0;
        let flag4 = 0;
        let flag5 = 0;
        for (const detail of data.data.fxjOrderAttaList) {
          // this.fileList.push( {
          //   uid: flag,
          //   name: detail.attaName,
          //   status: 'done',
          //   url: detail.attaUrl
          // } )
          // flag++;
          if(detail.attaName == '身份证正面'){
            // if(detail.attaType == 'mq1'){

            this.fileList1.push( {
              uid: flag1,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag1++;
          }
          if(detail.attaName == '身份证反面'){
          // if(detail.attaType == 'mq1'){  
            this.fileList1.push( {
              uid: flag1,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id

            } );
            flag1++;
          }
          if(detail.attaType == '10'){
            this.fileList2.push( {
              uid: flag2,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag2++;
          }
          if(detail.attaType == '20'){
            this.fileList3.push( {
              uid: flag3,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag3++;
          }
          if(detail.attaType == '30'){
            this.fileList4.push( {
              uid: flag4,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag4++;
          }
          if(detail.attaName == '驾驶证'){
          // if(detail.attaType == 'mq1'){  
            this.fileList5.push( {
              uid: flag5,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag5++;
          }
          if(detail.attaName == '行驶证'){
            // if(detail.attaType == 'mq1'){  
              this.fileList5.push( {
                uid: flag5,
                name: detail.attaName,
                status: 'done',
                url: detail.attaUrl,
                id: detail.id
              } );
              flag5++;
            }
        }
      };
    })
  }


  beforeUpload = (file: File) => {
      const isJPG = file.type === 'image/jpeg';
      const isPNG = file.type === 'image/png';
      if (!isJPG && !isPNG) {
          this.msg.warning(`不支持的格式: ${file.type}`);
      }
      const isLt2M = file.size / 1024 / 1024 < 10;
      if (!isLt2M) {
          this.msg.warning('图片不能超过2MB!');
      }
      return (isJPG || isPNG) && isLt2M;
  }

  private getBase64(img: File, callback: (img: any) => void) {
      if (img) {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
      }
  }



  formatterYuan = value => `¥ ${value}`;
  parserYuan = value => value.replace('¥', '');

  formatterDollor = value => `${value}`;
  parserDollor = value => value.replace('$', '');

  back() {
    this.router.navigate([ this.backUrl]);
  }


  fyccount() {
    let sum = 0;
    sum  = Number(sum) + Number(this.productfk.carPinggujia);
    //  let chengshu  =   Number(this.productfk.carChengshu);
    //   if(this.productfk.carRongzie ==''|| this.productfk.carRongzie  ==null){
    //     this.productfk.carRongzie=chengshu*sum;
    //   }
    this.productfk.carRongzie = Number(this.product.orderBy34)
      // if(this.productfk.bxFy ==''|| this.productfk.bxFy  ==null){
      //   // this.productfk.bxFy = sum*0.05;
        this.productfk.bxFy = sum*0;

      // }
      console.log(this.productfk.carRongzie)
        this.productfk.bxFy = 0;
        this.productfk.loanMonth = 0;
    this.fybcount();
    this.fyzhcbcount();
  }

  fybcount() {
    let sum = 0;
    sum  =   Number(this.productfk.carRongzie);
    console.log(sum)
    let jixjine  = Number(sum) + Number(this.productfk.bxFy);

    if(this.productfk.loanSum ==''|| this.productfk.loanSum  ==null){
      this.productfk.loanSum = jixjine;
    }
  }

  fyzhcbcount() {
    let loanPeriod  =  Number(this.productfk.loanPeriod);
    let yuegong = 0;
    yuegong = Math.round((Number(this.productfk.loanSum)/loanPeriod) + ((Number(this.productfk.loanSum)*0.003)));
    this.productfk.conJine =Math.round(Number(yuegong*loanPeriod)+Number(this.product.orderBy33))
    this.productfk.loanSum =Math.round(Number(this.productfk.carRongzie)+Number(this.productfk.carRongzie)*0.003*Number(loanPeriod))
    // let lixi = 0;
    // lixi  = Number(lixi) + Number(this.productfk.loanMonth);
    // let loanPeriod  =  Number(this.productfk.loanPeriod);
    // let benjin  =  Number(this.productfk.loanSum);

    // this.productfk.conJine = Math.round(benjin*(1+lixi*loanPeriod/100));

    // let jxjine = Number(this.productfk.loanSum);

    this.productfk.loanAmount  = Number(this.productfk.conJine) ;

    // this.productfk.lydbFuwfei =  Math.round(benjin*(1+lixi*loanPeriod/100)/1.1034/100)*100 - jxjine;
    // this.productfk.daoshouJine =  Math.round( ((Number(this.productfk.loanSum ) - Number(this.productfk.bxFy) - Number(this.productfk.gpsFy) -  Number(this.productfk.conJine)/loanPeriod))*10 )/10;
  }
  savestc(orderOper,title) {
    if(orderOper=='N'){
      if(this.product.spcomment == '' || this.product.spcomment == null){
        this.msg.warning("请填写审批意见");
        return;
      }
    }
    if(orderOper=='R'){
      if(this.product.spcomment == '' || this.product.spcomment == null){
        this.msg.warning("请填写审批意见");
        return;
      }
    }
    this.confirmServ.confirm({
      nzTitle: '确定'+title+'?',
      nzOnOk: () => {
        this.savest(orderOper);
        },
    });
  }
  savest(orderOper) {
    // 调用接口
    if(this.product.spcomment==''||this.product.spcomment==null){
      this.product.spcomment="同意";
    }
    // if(this.productfk.loanDate===''||this.productfk.loanDate===null){
    //   this.msg.warning("请选择首次还款日期");
    //   return;
    // }
    // if(this.productfk.loanReMethod===''||this.productfk.loanReMethod===null){
    //   this.msg.warning("请选择还款方式");
    //   return;
    // }
    // if(this.productfk.loanAmount===''||this.productfk.loanAmount===null){
    //   this.msg.warning("请输入放款金额");
    //   return;
    // }

    // if(this.productfk.loanMonth===''||this.productfk.loanMonth===null){
    //   this.msg.warning("请输入银行月利息");
    //   return;
    // }
    // if(this.productfk.loanSum===''||this.productfk.loanSum===null){
    //   this.msg.warning("请输入计息金额");
    //   return;
    // }
    // product.spcomment
    let fxjOrderLogList = [];
    if(this.product.spcomment!=''&& this.productfk.loanSum!=null){
      // console.log("this.product.dypname*********"+this.product.dypname);

      let dto ={
        orderNo:this.product.orderNo,
        logUser:this.setting.user.username,
        logContent:this.product.spcomment
      }
      fxjOrderLogList.push(dto);
    }


    let fxjOrderMainPage = {
      updateBy:this.setting.user.username,
      id:this.product.id,
      orderNo:this.product.orderNo,
      orderOper:orderOper,

      bmDate: this.product.bmDate,
      bmTime: this.product.bmTime,
      bxRq: this.product.bxRq,
      carColor: this.product.carColor,
      carDetail: this.product.carDetail,
      carVin: this.product.carVin,
      conJson: this.product.conJson,
      conNo: this.product.conNo,
      conType: this.product.conType,
      conUrl: this.product.conUrl,
      createSysorg: this.product.createSysorg,
      createUser: this.product.createUser,
      cusAge: this.product.cusAge,
      cusBirthday: this.product.cusBirthday,
      cusDeu: this.product.cusDeu,
      cusEthnical: this.product.cusEthnical,
      cusGender: this.product.cusGender,
      cusIdValidfrom: this.product.cusIdValidfrom,
      cusIdValidto: this.product.cusIdValidto,
      cusIncard: this.product.cusIncard,
      cusMaritalStatus: this.product.cusMaritalStatus,
      cusMobile: this.product.cusMobile,
      cusName: this.product.cusName,
      // carPinggujia:this.product.orderBy32,
      // loanSum:this.product.orderBy34,
      cusQqid: this.product.cusQqid,
      cusRemark: this.product.cusRemark,
      cusResAddr: this.product.cusResAddr,
      cusWechatid: this.product.cusWechatid,
      cusrIdtype: this.product.cusrIdtype,
      danganNo: this.product.danganNo,
      diyaStatus: this.product.diyaStatus,
      diyaUser: this.product.diyaUser,
      fangkBank: this.product.fangkBank,
      fangkId: this.product.fangkId,
      fangkName: this.product.fangkName,
      fangkType: this.product.fangkType,
      fkAmount: this.product.fkAmount,
      gpsInsAddr: this.product.gpsInsAddr,
      gpsInsData: this.product.gpsInsData,
      gpsInsMobile: this.product.gpsInsMobile,
      gpsInsUser: this.product.gpsInsUser,
      gpsStatus: this.product.gpsStatus,
      gpsUser: this.product.gpsUser,
      guohuStatus: this.product.guohuStatus,
      guohuUser: this.product.guohuUser,
      luqiaoRq: this.product.luqiaoRq,
      lxMobile1: this.product.lxMobile1,
      lxMobile2: this.product.lxMobile2,
      lxMobile3: this.product.lxMobile3,
      lxName1: this.product.lxName1,
      lxName2: this.product.lxName2,
      lxName3: this.product.lxName3,
      mpName: this.product.mpName,
      mpUrl: this.product.mpUrl,
      mpbackUrl: this.product.mpbackUrl,
      nianjiaRq: this.product.nianjiaRq,
      orderAmount: this.product.orderAmount,
      orderArea: this.product.orderArea,
      orderBy1: this.product.orderBy1,
      orderBy10: this.product.orderBy10,
      orderBy11: this.product.orderBy11,
      orderBy12: this.product.orderBy12,
      orderBy13: this.product.orderBy13,
      orderBy14: this.product.orderBy14,
      orderBy15: this.product.orderBy15,
      orderBy16: this.product.orderBy16,
      orderBy17: this.product.orderBy17,
      orderBy18: this.product.orderBy18,
      orderBy45: this.product.orderBy45,
      orderBy46: this.product.orderBy46,
      orderBy47: this.product.orderBy47,
      orderBy52: this.product.orderBy52,
      orderBy2: this.product.orderBy2,
      orderBy3: this.product.orderBy3,
      orderBy4: this.product.orderBy4,
      orderBy5: this.product.orderBy5,
      orderBy6: this.product.orderBy6,
      orderBy7: this.product.orderBy7,
      orderBy8: this.product.orderBy8,
      orderBy9: this.product.orderBy9,
      orderCarno: this.product.orderCarno,
      orderCartype: this.product.orderCartype,
      orderInfo: this.product.orderInfo,

      orderPeriod: this.product.orderPeriod,
      orderStatus: this.product.orderStatus,
      proClass: this.product.proClass,
      proIsdb: this.product.proIsdb,
      proName: this.product.proName,
      proNo: this.product.proNo,
      proType: this.product.proType,
      proWf: this.product.proWf,
      qiandanUser: this.product.qiandanUser,
      sfAnpaijiafang: this.product.sfAnpaijiafang,
      sfDianshen: this.product.sfDianshen,
      wbName: this.product.wbName,
      wbUrl: this.product.wbUrl,
      wbbackUrl: this.product.wbbackUrl,
      yinhangUser: this.product.yinhangUser,
      fxjOrderLogList:JSON.parse(JSON.stringify(fxjOrderLogList))

    };
    this.savefk();
    this.productService.editPro(fxjOrderMainPage).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      console.log("data.ok"+data.ok);
      if (data.ok === true) {
        if(orderOper!="S"){
          this.back();
        }else{
          this.st.reload();

        }

        this.msg.success('操作成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })
  }

  savefk() {

    let fxjLoanFk = {
      updateBy:this.setting.user.username,
      id:this.productfk.id,
      bpmStatus: this.productfk.bpmStatus,
      bxFy: this.productfk.bxFy,
      bxRq: this.productfk.bxRq,
      carChengshu: this.productfk.carChengshu,
      carPinggujia: this.productfk.carPinggujia,
      carRongzie: this.productfk.carRongzie,
      conJine: this.productfk.conJine,
      createBy: this.productfk.createBy,
      // createDate: this.productfk.createDate,
      createName: this.productfk.createName,
      cusId: this.productfk.cusId,
      cusMobile: this.productfk.cusMobile,
      cusName: this.productfk.cusName,
      // carPinggujia: this.productfk.orderBy32,
      daoshouJine: this.productfk.daoshouJine,
      fkBy1: this.productfk.fkBy1,
      fkBy10: this.productfk.fkBy10,
      fkBy11: this.productfk.fkBy11,
      fkBy12: this.productfk.fkBy12,
      fkBy13: this.productfk.fkBy13,
      fkBy2: this.productfk.fkBy2,
      fkBy3: this.productfk.fkBy3,
      fkBy4: this.productfk.fkBy4,
      fkBy5: this.productfk.fkBy5,
      fkBy6: this.productfk.fkBy6,
      fkBy7: this.productfk.fkBy7,
      fkBy8: this.productfk.fkBy8,
      fkBy9: this.productfk.fkBy9,
      gpsFy: this.productfk.gpsFy,
      liulFy: this.productfk.liulFy,
      loanAmount: this.productfk.loanAmount,
      loanAtta: this.productfk.loanAtta,
      loanDate: this.productfk.loanDate,
      loanDbmonth: this.productfk.loanDbmonth,
      loanMonth: this.productfk.loanMonth,
      loanMonthSum: this.productfk.loanMonthSum,
      loanPeriod: this.productfk.loanPeriod,
      loanReMethod: this.productfk.loanReMethod,
      loanRemark: this.productfk.loanRemark,
      loanSbamount: this.productfk.loanSbamount,
      loanSum: this.productfk.loanSum,
      luqiaoRq: this.productfk.luqiaoRq,
      lydbFuwfei: this.productfk.lydbFuwfei,
      nianjianRq: this.productfk.nianjianRq,
      orderNo: this.productfk.orderNo,
      qitaFy: this.productfk.qitaFy,
      sysCompanyCode: this.productfk.sysCompanyCode,
      sysOrgCode: this.productfk.sysOrgCode,
      // updateDate: this.productfk.updateDate,
      updateName: this.productfk.updateName,
      ygJine: this.productfk.ygJine,
    };

    // 调用接口
    console.log("fxjLoanFk*******"+JSON.stringify(fxjLoanFk));
    this.fkService.editPro(fxjLoanFk).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if (data.ok === true) {
        // this.msg.success('修改成功');
        // this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data['message']);
      }
    })
  }

  changeColor(res) {
    this.product.colorSort = res.color.hex;
  }




  handleChange1(info: { file: UploadFile }) {


    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl1 = img;
        console.log("info1*********"+JSON.stringify(info.file.response[0].piccontent));

        var sfxx=JSON.parse(info.file.response[0].piccontent)

        this.product.cusName=sfxx.result.cardsinfo[0].items[1].content;
        this.product.cusGender=sfxx.result.cardsinfo[0].items[2].content;
        this.product.cusEthnical=sfxx.result.cardsinfo[0].items[3].content;
        this.product.cusBirthday=sfxx.result.cardsinfo[0].items[4].content;
        this.product.cusResAddr=sfxx.result.cardsinfo[0].items[5].content;
        this.product.cusIncard=sfxx.result.cardsinfo[0].items[6].content;
        info.file=null;
      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange2(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }


    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl2 = img;


        var sfxx=JSON.parse(info.file.response[0].piccontent)

        console.log("varfrom*********"+JSON.stringify(sfxx));
        this.product.cusIdValidfrom= sfxx.result.cardsinfo[0].items[3].content;
        this.product.cusIdValidto=sfxx.result.cardsinfo[0].items[4].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange3(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl3 = img;

        var sfxx=JSON.parse(info.file.response[0].piccontent)

        this.product.dyptype = sfxx.result.cardsinfo[0].items[5].content +"/"+  sfxx.result.cardsinfo[0].items[2].content;

        this.product.dypname = sfxx.result.cardsinfo[0].items[1].content;
        this.product.dypvalue = sfxx.result.cardsinfo[0].items[3].content;
        this.product.dypyear =  sfxx.result.cardsinfo[0].items[8].content + "/"+sfxx.result.cardsinfo[0].items[9].content;
        this.product.dypclass =  sfxx.result.cardsinfo[0].items[10].content;

        this.product.dypremark1 = sfxx.result.cardsinfo[0].items[4].content;
        this.product.dypremark2 = sfxx.result.cardsinfo[0].items[6].content;
        this.product.dypremark3   = sfxx.result.cardsinfo[0].items[7].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange4(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl4 = img;
        var sfxx=JSON.parse(info.file.response[0].piccontent)

        console.log("varfrom*********"+JSON.stringify(sfxx));
        this.product.emergencyContact=sfxx.result.cardsinfo[0].items[1].content;

        this.product.emergencyResAddr=sfxx.result.cardsinfo[0].items[5].content;
        this.product.emergencyOffice=sfxx.result.cardsinfo[0].items[6].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange5(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl5 = img;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }

  handlePreview = (file: UploadFile) => {
    console.log("1")
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;

    // this.previewImage1 = file.url || file.thumbUrl;
    // this.previewVisible1 = true;

    // this.previewImage2 = file.url || file.thumbUrl;
    // this.previewVisible2 = true;

    // this.previewImage3 = file.url || file.thumbUrl;
    // this.previewVisible3 = true;

    // this.previewImage4 = file.url || file.thumbUrl;
    // this.previewVisible4 = true;

    // this.previewImage5 = file.url || file.thumbUrl;
    // this.previewVisible5 = true;
  }
  handlePreview1 = (file: UploadFile) => {
    console.log("1");
    console.log(file)
    this.previewImage1 = file.url || file.thumbUrl;
    this.previewVisible1 = true;
    console.log(this.previewImage1);
  }
  handlePreview2 = (file: UploadFile) => {
    console.log("2");
    console.log(file)
    this.previewImage2 = file.url || file.thumbUrl;
    this.previewVisible2 = true;
    console.log(this.previewImage2);
  }
  handlePreview3 = (file: UploadFile) => {
    console.log("3");
    console.log(file)
    this.previewImage3 = file.url || file.thumbUrl;
    this.previewVisible3 = true;
    console.log(this.previewImage3);
  }
  handlePreview4 = (file: UploadFile) => {
    console.log("4");
    console.log(file)
    this.previewImage4 = file.url || file.thumbUrl;
    this.previewVisible4 = true;
    console.log(this.previewImage4);
  }
  handlePreview5 = (file: UploadFile) => {
    console.log("5");
    console.log(file)
    this.previewImage5 = file.url || file.thumbUrl;
    this.previewVisible5 = true;
    console.log(this.previewImage5);
  }


  data = [];
  uploadreplanExcelUrl='';
  mbdownurl='https://zhaodui.oss-cn-shenzhen.aliyuncs.com/doc/hkjh.xls';

  uploadProd(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
    }
    if (info.file.status === 'done') {
      this.loading = false;
      this.st.load();
      this.notice.create('success', '成功', '还款信息导入成功');
    }
    if (info.file.status === 'error') {
      this.loading = false;
      this.notice.create('error', '失败', '还款信息导入失败');
    }
  }
  refresh(){
    this.st.reload();
  }

 // 图片 wuwq
 upLoadChange1(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}

 // 图片 wuwq
 upLoadChange2(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}

 upLoadChange3(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}

upLoadChange4(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}


upLoadChange5(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}

// 删除图片 wuwq
delPic(id) {
  this.productService.delfile(id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if(data.ok){
        // this.productorder = data.data;
        // console.log(this.productorder);
        // this.reload();
      }else{
        console.log("22")
      }
    })
}

}
