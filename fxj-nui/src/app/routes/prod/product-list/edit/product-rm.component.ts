import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { ProductService } from '../../../../service/prod/product.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {SimpleTableButton, SimpleTableColumn, SimpleTableComponent} from "@delon/abc";

@Component({
  selector: 'product-view-mqsp',
  templateUrl: './product-rm.component.html',
  styleUrls: ['./product-rm.component.less']
})
export class ProductrmComponent implements OnInit, OnDestroy {
  product:  any = {
    id:null,
    bmDate: null,
    bmTime: null,
    bxRq: null,
    carColor: null,
    carDetail: null,
    carVin: null,
    conJson: null,
    conNo: null,
    conType: null,
    conUrl: null,
    createSysorg: null,
    createUser: null,
    cusAge: null,
    cusBirthday: null,
    cusDeu: null,
    cusEthnical: null,
    cusGender: null,
    cusIdValidfrom: null,
    cusIdValidto: null,
    cusIncard: null,
    cusMaritalStatus: null,
    cusMobile: null,
    cusName: null,
    cusQqid: null,
    cusRemark: null,
    cusResAddr: null,
    cusWechatid: null,
    cusrIdtype: null,
    danganNo: null,
    diyaStatus: null,
    diyaUser: null,
    fangkBank: null,
    fangkId: null,
    fangkName: null,
    fangkType: null,
    fkAmount: null,
    gpsInsAddr: null,
    gpsInsData: null,
    gpsInsMobile: null,
    gpsInsUser: null,
    gpsStatus: null,
    gpsUser: null,
    guohuStatus: null,
    guohuUser: null,
    luqiaoRq: null,
    lxMobile1: null,
    lxMobile2: null,
    lxMobile3: null,
    lxName1: null,
    lxName2: null,
    lxName3: null,
    mpName: null,
    mpUrl: null,
    mpbackUrl: null,
    nianjiaRq: null,
    orderAmount: null,
    orderArea: null,
    orderBy1: null,
    orderBy10: null,
    orderBy11: null,
    orderBy12: null,
    orderBy13: null,
    orderBy14: null,
    orderBy15: null,
    orderBy16: null,
    orderBy17: null,
    orderBy18: null,
    orderBy2: null,
    orderBy3: null,
    orderBy4: null,
    orderBy5: null,
    orderBy6: null,
    orderBy7: null,
    orderBy8: null,
    orderBy9: null,
    orderCarno: null,
    orderCartype: null,
    orderInfo: null,
    orderNo: null,
    orderOper: null,
    orderPeriod: null,
    orderStatus: null,
    proClass: null,
    proIsdb: null,
    proName: null,
    proNo: null,
    proType: null,
    proWf: null,
    qiandanUser: null,
    sfAnpaijiafang: null,
    sfDianshen: null,
    wbName: null,
    wbUrl: null,
    wbbackUrl: null,
    yinhangUser: null,

    dypname: null,
    dypvalue: null,
    dypyear: null,
    dyptype: null,
    dypclass: null,
    fkname:null,
    dypremark1: null,
    dypremark2: null,
    dypremark3: null,
    emergencyRelation:  null,
    emergencyContact: null,
    emergencyMobile: null,
    emergencyOffice:null,
    emergencyResAddr:null,
    // spcomment:'同意',
    spcomment:null,

  };
  ps = 100;
  // 搜索表单
  q: any = {
    bpmStatus:"",
    cusName: '',
    cusMobile: '',
    cusIncard: '',
    proNo: '',
    orderNo:''
  };

  @ViewChild('st') st: SimpleTableComponent;

  columns: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '操作状态', index: 'bpmStatus' },
    { title: '订单编号', index: 'orderNo' },
    { title: '操作人 ', index: 'logUser'},
    { title: '日志日期 ', index: 'logDate' },
    { title: '日志内容', index: 'logContent' }

  ];
  data = [];
  downurl='';


  @ViewChild('strm') strm: SimpleTableComponent;

  columnsrm: SimpleTableColumn[] = [
    { title: '查询日期 ', index: 'createDate' },
    { title: '查询人 ', index: 'createBy' },
    { title: '风控类型', index: 'rmType' },
    // { title: '查询结果', index: 'rmView' },
    { title: '风控得分 ', index: 'rmScores'},
    { title: '身份属性 ', index: 'rmBy1'},
    { title: '履约能力', index: 'rmBy2' },
    { title: '信用记录 ', index: 'rmBy3'},
    { title: '行为特质', index: 'rmBy4' },
    { title: '社交影响 ', index: 'rmBy5'},
    { title: '分数段', index: 'rmBy6' },
    { title: '结果建议 ', index: 'rmBy7'},
    { title: '消息 ', index: 'rmName'},

    {
      title: '操作',
      buttons: [
        {
          text: '详情',
          type: 'link',
          click: (record: any) => this.opennewwindow(record.rmBy13+record.rmName),
          iif: (item: any, btn: SimpleTableButton, column: SimpleTableColumn) => {
            return item.rmName != null
          }

        }

      ]
    }
  ];



  // 预览图片对象
  previewImage = '';
  previewImage1 = '';
  previewImage2 = '';
  previewImage3 = '';
  previewImage4 = '';
  previewImage5 = '';
  previewImage6 = '';
  // 是否可以预览
  previewVisible = false;
  previewVisible1 = false;
  previewVisible2 = false;
  previewVisible3 = false;
  previewVisible4 = false;
  previewVisible5 = false;
  previewVisible6 = false;
  // 预览图片地址
  propName = "";
  picUrl = '';
  // 图片上传地址

  proNos = [];
  cusrIdtypes = [];
  cusGenders = [];
  cusMaritalStatuss = [];
  cusDeus = [];
  cusEthnicals = [];
  bpmStatus = [];
  dyptypes = [];
  dypclasss = [];
  relTypes=[];
  fknames=[];
  avatarUrl: string;
  avatarUrl1:string;
  avatarUrl2:string;
  avatarUrl3:string;
  logurl:string;
  rmurl:string;
  avatarUrl4:string;
  avatarUrl5:string;
  avatarUrl6:string;

  loading = false;
  uploadUrl = '';
  uploadUr1 = '';
  uploadUr5 = '';
  uploadUr6 = '';
  uploadUr7 = '';
  uploadUr8 = '';
  uploadUr9 = '';
  uploadUr10 = '';
  orbitUrl='';
  avatarParam1 = { sbtype: '2', filetype:'sb',orderNo: '2'};
  avatarParam2 = { sbtype: '3', filetype:'sb', orderNo: '3'};
  avatarParam3 = { sbtype: '6',  filetype:'sb',orderNo: '6'};
  avatarParam4 = { sbtype: '2', filetype:'sb', orderNo: '2'};
  avatarParam5 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam6 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam7 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam8 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam9 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam10 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};


  fileList = [ ];
  fileList1 = [ ];
  fileList2 = [ ];
  fileList3 = [ ];
  fileList4 = [ ];
  fileList5 = [ ];
  fileList6 = [ ];
  backUrl='';
  MAX_COUNT = 60;
  tabIndex = 0;
  saleing = false;
  private sub: any;

  constructor(
    public setting: SettingsService,

    private msg: NzMessageService,
    private modalService: NzModalService,
    private productService: ProductService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService,
    private confirmServ: NzModalService,
  ) {}

  ngOnInit(): void {
    this.loadproNos();
    this.loadcusrIdtypes();
    this.loadcusGenders();
    this.loadcusMaritalStatuss();
    this.loadcusDeus();
    this.loadcusEthnicals();
    this.loadbpmStatus();
    this.loaddyptypes();
    this.loaddypclasss();
    this.loadrelTypes();
    this.loadfknames();
    this.uploadUrl = this.productService.uploadsbUrl();
    this.uploadUr1 = this.productService.uploadUrl();
    this.uploadUr5 = this.productService.uploadUrl();
    this.uploadUr6 = this.productService.uploadUrl();
    this.uploadUr7 = this.productService.uploadUrl();
    this.uploadUr8 = this.productService.uploadUrl();
    this.uploadUr9 = this.productService.uploadUrl();
    this.uploadUr10 = this.productService.uploadUrl();
    this.orbitUrl = "http://120.78.150.43/spmq";
    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"];
      // 获取商品详情
      this.getProductInfo();
    });
    // 隐藏reusetab
    setTimeout(() => {
        this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);

  }

  opennewwindow(url){

    const newWindow = window.open(url, '_blank');
    newWindow.document.title = '同盾报告';

  }
  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  changeTab(index) {
    // 使用异步的方式更新组件, 防止ngIf错误
    Promise.resolve(null).then(() => this.tabIndex = index);
  }
  loadrelTypes() {
    this.relTypes = [];
    this.comm.getType("reltype").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.relTypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
// 加载产品
  loadproNos() {
    this.proNos = [];
    this.comm.getType("proNo").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.proNos.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载证件类型
  loadcusrIdtypes() {
    this.cusrIdtypes = [];
    this.comm.getType("idType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusrIdtypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载性别
  loadcusGenders() {
    this.cusGenders = [];
    this.comm.getType("sex").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusGenders.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载婚姻
  loadcusMaritalStatuss() {
    this.cusMaritalStatuss = [];
    this.comm.getType("marType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusMaritalStatuss.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载学历
  loadcusDeus() {
    this.cusDeus = [];
    this.comm.getType("eduType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusDeus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
//加载民族
  loadcusEthnicals() {
    this.cusEthnicals = [];
    this.comm.getType("ethType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusEthnicals.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载证件类型
  loadbpmStatus() {
    this.bpmStatus = [];
    this.comm.getType("bpm_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.bpmStatus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载证件类型
  loaddyptypes() {
    this.dyptypes = [];
    this.comm.getType("dyptype").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.dyptypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loaddypclasss() {
    this.dypclasss = [];
    this.comm.getType("dypclass").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.dypclasss.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loadfknames() {
    this.fknames = [];
    this.comm.getType("fkname").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.fknames.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  getProductInfo() {
    this.productService.getProduct(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      console.log("*************************"+JSON.stringify(data.data));
      this.product = data.data;
      this.backUrl = data.data.wbbackUrl;

      this.titleService.setTitle("编辑订单("+this.product.orderNo+")");
      this.logurl = this.productService.logurl()+this.product.orderNo;
      this.rmurl = this.productService.rmurl()+this.product.orderNo;

      // this.product.dypname = data.data.fxjOrderLoanList[0].elementContent;
      // this.product.dypvalue = data.data.fxjOrderLoanList[1].elementContent;
      // this.product.dypyear = data.data.fxjOrderLoanList[2].elementContent;
      // this.product.dyptype = data.data.fxjOrderLoanList[3].elementContent;
      // this.product.dypclass = data.data.fxjOrderLoanList[4].elementContent;
      // this.product.dypremark1 = data.data.fxjOrderLoanList[5].elementContent;
      // this.product.dypremark2 = data.data.fxjOrderLoanList[6].elementContent;
      // this.product.dypremark3 = data.data.fxjOrderLoanList[7].elementContent;
      this.avatarParam1 = { sbtype: '2', filetype:'sb',orderNo: this.product.orderNo};
      this.avatarParam2 = { sbtype: '3', filetype:'sb', orderNo: this.product.orderNo};
      this.avatarParam3 = { sbtype: '6',  filetype:'sb',orderNo: this.product.orderNo};
      this.avatarParam4 = { sbtype: '2', filetype:'sb', orderNo: this.product.orderNo};
      this.avatarParam5 = { sbtype: 'No',  filetype:'mq',orderNo: this.product.orderNo};
      this.avatarParam6 = { sbtype: '10',  filetype:'yhls',orderNo: this.product.orderNo};
      this.avatarParam7 = { sbtype: '20',  filetype:'zxbg',orderNo: this.product.orderNo};
      this.avatarParam8 = { sbtype: '30',  filetype:'wxb',orderNo: this.product.orderNo};
      this.avatarParam9 = { sbtype: '30',  filetype:'jszandxsz',orderNo: this.product.orderNo};
      this.avatarParam10 = { sbtype: '30',  filetype:'GPS',orderNo: this.product.orderNo};
      if (data.data.fxjOrderEmerList.length > 0) {
      if (data.data.fxjOrderEmerList.length > 0) {
        this.product.emergencyRelation = data.data.fxjOrderEmerList[0].emergencyRelation;
        this.product.emergencyContact = data.data.fxjOrderEmerList[0].emergencyContact;
        this.product.emergencyMobile = data.data.fxjOrderEmerList[0].emergencyMobile;
        this.product.emergencyOffice = data.data.fxjOrderEmerList[0].emergencyOffice;
        this.product.emergencyResAddr = data.data.fxjOrderEmerList[0].emergencyResAddr;
        this.product.emergencyContact1 = data.data.fxjOrderEmerList[1].emergencyContact;
        this.product.emergencyMobile1 = data.data.fxjOrderEmerList[1].emergencyMobile;
      }
  console.log("data.data.fxjOrderEmerList[0].emergencyRelation"+data.data.fxjOrderEmerList[0].emergencyRelation);
      if (data.data.fxjOrderLoanList.length > 0) {
        let flag = 0;
        for (const detail of data.data.fxjOrderLoanList) {
          console.log("detail"+detail);

          if(detail.elementNo=='10'){
            this.product.dypname =detail.elementContent;
          }
          if(detail.elementNo=='20'){
            this.product.dypvalue =detail.elementContent;

          }
          if(detail.elementNo=='30'){
            this.product.dypyear =detail.elementContent;

          }
          if(detail.elementNo=='40'){
            this.product.dyptype =detail.elementContent;

          }if(detail.elementNo=='50'){
            this.product.dypclass =detail.elementContent;

          }
          if(detail.elementNo=='60'){
            this.product.dypremark1 =detail.elementContent;

          }if(detail.elementNo=='70'){
            this.product.dypremark2 =detail.elementContent;

          }if(detail.elementNo=='80'){
            this.product.dypremark3 =detail.elementContent;

          }if(detail.elementNo=='90'){
            this.product.dyptype1 =detail.elementContent;

          }if(detail.elementNo=='100'){
            this.product.dypyear1 =detail.elementContent;

          }if(detail.elementNo=='110'){
            this.product.dypclass1 =detail.elementContent;

          }if(detail.elementNo=='120'){
            this.product.dypremark11 =detail.elementContent;
          }
        }
      };

      if (data.data.fxjOrderAttaList.length > 0) {
        let flag = 0;
        let flag1 = 0;
        let flag2 = 0;
        let flag3 = 0;
        let flag4 = 0;
        let flag5 = 0;
        let flag6 = 0;
        for (const detail of data.data.fxjOrderAttaList) {
          // this.fileList.push( {
          //   uid: flag,
          //   name: detail.attaName,
          //   status: 'done',
          //   url: detail.attaUrl
          // } )
          // flag++;
          if(detail.attaName == '身份证正面'){
            // if(detail.attaType == 'mq1'){

            this.fileList1.push( {
              uid: flag1,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag1++;
          }
          if(detail.attaName == '身份证反面'){
          // if(detail.attaType == 'mq1'){  
            this.fileList1.push( {
              uid: flag1,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag1++;
          }
          if(detail.attaType == 'sb'){
              this.fileList1.push( {
                uid: flag1,
                name: detail.attaName,
                status: 'done',
                url: detail.attaUrl,
                id: detail.id
              } );
              flag1++;
            }
          if(detail.attaType == '10'){
            this.fileList2.push( {
              uid: flag2,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag2++;
          }
          if(detail.attaType == 'yhls'){
            this.fileList2.push( {
              uid: flag2,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag2++;
          }
         
          if(detail.attaType == '20'){
            this.fileList3.push( {
              uid: flag3,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag3++;
          }
          if(detail.attaType == 'zxbg'){
            this.fileList3.push( {
              uid: flag3,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag3++;
          }
          if(detail.attaType == '30'){
            this.fileList4.push( {
              uid: flag4,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag4++;
          }
          if(detail.attaType == 'wxb'){
            this.fileList4.push( {
              uid: flag4,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag4++;
          }
          if(detail.attaName == '驾驶证'){
          // if(detail.attaType == 'mq1'){  
            this.fileList5.push( {
              uid: flag5,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag5++;
          }
          if(detail.attaType == 'jszandxsz'){
            // if(detail.attaType == 'mq1'){  
              this.fileList5.push( {
                uid: flag5,
                name: detail.attaName,
                status: 'done',
                url: detail.attaUrl,
                id: detail.id
              } );
              flag5++;
            }
          if(detail.attaName == '行驶证'){
            // if(detail.attaType == 'mq1'){  
              this.fileList5.push( {
                uid: flag5,
                name: detail.attaName,
                status: 'done',
                url: detail.attaUrl,
                id: detail.id
              } );
              flag5++;
            }
            if(detail.attaType == 'GPS'){
              // if(detail.attaType == 'mq1'){  
                this.fileList6.push( {
                  uid: flag6,
                  name: detail.attaName,
                  status: 'done',
                  url: detail.attaUrl,
                  id: detail.id
                } );
                flag6++;
              }
        }
      };
    }
    })
  }


  beforeUpload = (file: File) => {
      const isJPG = file.type === 'image/jpeg';
      const isPNG = file.type === 'image/png';
      if (!isJPG && !isPNG) {
          this.msg.warning(`不支持的格式: ${file.type}`);
      }
      const isLt2M = file.size / 1024 / 1024 < 10;
      if (!isLt2M) {
          this.msg.warning('图片不能超过2MB!');
      }
      return (isJPG || isPNG) && isLt2M;
  }

  private getBase64(img: File, callback: (img: any) => void) {
      if (img) {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
      }
  }



  formatterYuan = value => `¥ ${value}`;
  parserYuan = value => value.replace('¥', '');

  formatterDollor = value => `${value}`;
  parserDollor = value => value.replace('$', '');

  back() {
    this.router.navigate([ this.backUrl]);
  }

  savestc(orderOper,title) {
    if(orderOper=='N'){
      if(this.product.spcomment == '' || this.product.spcomment == null){
        this.msg.warning("请填写审批意见");
        return;
      }
    }
    if(orderOper=='R'){
      if(this.product.spcomment == '' || this.product.spcomment == null){
        this.msg.warning("请填写审批意见");
        return;
      }
    }
    this.confirmServ.confirm({
      nzTitle: '确定'+title+'?',
      nzOnOk: () => {
        this.savest(orderOper);
        },
    });
  }

  savest(orderOper) {

    // 调用接口
    if(this.product.spcomment==''||this.product.spcomment==null){
      this.product.spcomment="同意";
    }
    // product.spcomment
    let fxjOrderLogList = [];
    if(this.product.spcomment!=''){
      // console.log("this.product.dypname*********"+this.product.dypname);

      let dto ={
        orderNo:this.product.orderNo,
        logUser:this.setting.user.username,
        logContent:this.product.spcomment
      }
      fxjOrderLogList.push(dto);
    }


    let fxjOrderMainPage = {
      updateBy:this.setting.user.username,
      id:this.product.id,
      orderNo:this.product.orderNo,
      // bpmStatus:stau,
      orderOper:orderOper,
      bmDate: this.product.bmDate,
      bmTime: this.product.bmTime,
      bxRq: this.product.bxRq,
      carColor: this.product.carColor,
      carDetail: this.product.carDetail,
      carVin: this.product.carVin,
      conJson: this.product.conJson,
      conNo: this.product.conNo,
      conType: this.product.conType,
      conUrl: this.product.conUrl,
      createSysorg: this.product.createSysorg,
      createUser: this.product.createUser,
      cusAge: this.product.cusAge,
      cusBirthday: this.product.cusBirthday,
      cusDeu: this.product.cusDeu,
      cusEthnical: this.product.cusEthnical,
      cusGender: this.product.cusGender,
      cusIdValidfrom: this.product.cusIdValidfrom,
      cusIdValidto: this.product.cusIdValidto,
      cusIncard: this.product.cusIncard,
      cusMaritalStatus: this.product.cusMaritalStatus,
      cusMobile: this.product.cusMobile,
      cusName: this.product.cusName,
      cusQqid: this.product.cusQqid,
      cusRemark: this.product.cusRemark,
      cusResAddr: this.product.cusResAddr,
      cusWechatid: this.product.cusWechatid,
      cusrIdtype: this.product.cusrIdtype,
      danganNo: this.product.danganNo,
      diyaStatus: this.product.diyaStatus,
      diyaUser: this.product.diyaUser,
      fangkBank: this.product.fangkBank,
      fangkId: this.product.fangkId,
      fangkName: this.product.fangkName,
      fangkType: this.product.fangkType,
      fkAmount: this.product.fkAmount,
      gpsInsAddr: this.product.gpsInsAddr,
      gpsInsData: this.product.gpsInsData,
      gpsInsMobile: this.product.gpsInsMobile,
      gpsInsUser: this.product.gpsInsUser,
      gpsStatus: this.product.gpsStatus,
      gpsUser: this.product.gpsUser,
      guohuStatus: this.product.guohuStatus,
      guohuUser: this.product.guohuUser,
      luqiaoRq: this.product.luqiaoRq,
      lxMobile1: this.product.lxMobile1,
      lxMobile2: this.product.lxMobile2,
      lxMobile3: this.product.lxMobile3,
      lxName1: this.product.lxName1,
      lxName2: this.product.lxName2,
      lxName3: this.product.lxName3,
      mpName: this.product.mpName,
      mpUrl: this.product.mpUrl,
      mpbackUrl: this.product.mpbackUrl,
      nianjiaRq: this.product.nianjiaRq,
      orderAmount: this.product.orderAmount,
      orderArea: this.product.orderArea,
      orderBy1: this.product.orderBy1,
      orderBy10: this.product.orderBy10,
      orderBy11: this.product.orderBy11,
      orderBy12: this.product.orderBy12,
      orderBy13: this.product.orderBy13,
      orderBy14: this.product.orderBy14,
      orderBy15: this.product.orderBy15,
      orderBy16: this.product.orderBy16,
      orderBy17: this.product.orderBy17,
      orderBy18: this.product.orderBy18,
      orderBy30: this.product.orderBy30,
      orderBy31: this.product.orderBy31,
      orderBy32: this.product.orderBy32,
      orderBy33: this.product.orderBy33,
      orderBy34: this.product.orderBy34,
      orderBy35: this.product.orderBy35,
      orderBy36: this.product.orderBy36,
      orderBy37: this.product.orderBy37,
      orderBy38: this.product.orderBy38,
      orderBy39: this.product.orderBy39,
      orderBy40: this.product.orderBy40,
      orderBy41: this.product.orderBy41,
      orderBy42: this.product.orderBy42,
      orderBy43: this.product.orderBy43,
      orderBy44: this.product.orderBy44,
      orderBy45: this.product.orderBy45,
      orderBy46: this.product.orderBy46,
      orderBy47: this.product.orderBy47,
      orderBy48: this.product.orderBy48,
      orderBy49: this.product.orderBy49,
      orderBy50: this.product.orderBy50,
      orderBy51: this.product.orderBy51,
      orderBy52: this.product.orderBy52,
      orderBy53: this.product.orderBy53,
      orderBy54: this.product.orderBy54,
      orderBy55: this.product.orderBy55,
      orderBy56: this.product.orderBy56,
      orderBy57: this.product.orderBy57,
      orderBy58: this.product.orderBy58,
      orderBy59: this.product.orderBy59,
      orderBy60: this.product.orderBy60,
      orderBy78: this.product.orderBy78,
      orderBy4: this.product.orderBy4,
      orderBy5: this.product.orderBy5,
      orderBy6: this.product.orderBy6,
      orderBy7: this.product.orderBy7,
      orderBy8: this.product.orderBy8,
      orderBy9: this.product.orderBy9,
      orderCarno: this.product.orderCarno,
      orderCartype: this.product.orderCartype,
      orderInfo: this.product.orderInfo,
      orderPeriod: this.product.orderPeriod,
      orderStatus: this.product.orderStatus,
      proClass: this.product.proClass,
      proIsdb: this.product.proIsdb,
      proName: this.product.proName,
      proNo: this.product.proNo,
      proType: this.product.proType,
      proWf: this.product.proWf,
      qiandanUser: this.product.qiandanUser,
      sfAnpaijiafang: this.product.sfAnpaijiafang,
      sfDianshen: this.product.sfDianshen,
      wbName: this.product.wbName,
      wbUrl: this.product.wbUrl,
      wbbackUrl: this.product.wbbackUrl,
      yinhangUser: this.product.yinhangUser,
      fxjOrderLogList:JSON.parse(JSON.stringify(fxjOrderLogList))

    };
    console.log(fxjOrderMainPage)
    this.productService.editPro(fxjOrderMainPage).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      console.log("data.ok"+data.ok);
      if (data.ok === true) {
        this.st.load(1, this.q);
        this.back();
        console.log('1321321')
        this.msg.success('成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
        console.log(data.respMsg)
      }
    })
  }
  rmqueryc(type,title) {
    this.confirmServ.confirm({
      nzTitle: '确定'+title+'?',
      nzOnOk: () => {
        this.rmquery(type);
        },
    });
  }
  rmquery(type) {
    // 调用接口
    if(this.product.fkname==''||this.product.fkname==null ){
      this.msg.warning("请选择风控类别");
      return;
    }
    // 调用接口
    if(this.product.orderNo==''||this.product.orderNo==null){
      this.msg.warning("请输入订单号");
      return;
    }
    let fkinput = {

    };
    if(type=="J"){
      // 调用接口
      if(this.product.cusIncard===''||this.product.cusIncard===null){
        this.msg.warning("请输入身份证");
        return;
      }
      // 调用接口
      if(this.product.cusName===''||this.product.cusName===null){
        this.msg.warning("请输入姓名");
        return;
      }
      // 调用接口
      if(this.product.cusMobile===''||this.product.cusMobile===null){
        this.msg.warning("请输入电话");
        return;
      }
      if(this.product.cusResAddr===''||this.product.cusResAddr===null){
        this.msg.warning("请输入地址");
        return;
      }

      fkinput = {
        createBy:this.setting.user.username,
        orderNo:this.product.orderNo,
        cardNo:this.product.cusIncard,
        realName:this.product.cusName,
        keyword:this.product.cusResAddr,
        mobile:this.product.cusMobile
      };
    }else if(type=="D") {
      // 调用接口
      if(this.product.emergencyContact===''||this.product.emergencyContact===null){
        this.msg.warning("请输入姓名");
        return;
      }
      // 调用接口
      if(this.product.emergencyOffice===''||this.product.emergencyOffice===null){
        this.msg.warning("请输入身份证");
        return;
      }
      // 调用接口
      if(this.product.emergencyMobile===''||this.product.emergencyMobile===null){
        this.msg.warning("请输入电话");
        return;
      }
      fkinput = {
        createBy:this.setting.user.username,
        orderNo:this.product.orderNo,
        cardNo:this.product.emergencyOffice,
        realName:this.product.emergencyContact,
        mobile:this.product.emergencyMobile,
      };
       }else if(type=="C") {
        // 调用接口
        if (this.product.dypvalue === '' || this.product.dypvalue === null) {
          this.msg.warning("请输入车牌号");
          return;
        }
        // 调用接口
        if (this.product.dypremark2 === '' || this.product.dypremark2 === null) {
          this.msg.warning("请输入车架号");
          return;
        }
        // 调用接口
        if (this.product.dypremark3 === '' || this.product.dypremark3 === null) {
          this.msg.warning("请输入发动机号");
          return;
        }
      fkinput = {
        createBy:this.setting.user.username,
        orderNo:this.product.orderNo,
        lsnum:this.product.dypvalue,
        frameno:this.product.dypremark2,
        engineno:this.product.dypremark3,
      };

    }




    this.productService.rmquery(this.product.fkname,fkinput).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      console.log("data.ok"+data.ok);
      if (data.ok === true) {
        this.strm.load(1, this.q);

        this.msg.success('成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })
  }


  changeColor(res) {
    this.product.colorSort = res.color.hex;
  }




  handleChange1(info: { file: UploadFile }) {


    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl1 = img;
        console.log("info1*********"+JSON.stringify(info.file.response[0].piccontent));

        var sfxx=JSON.parse(info.file.response[0].piccontent)

        this.product.cusName=sfxx.result.cardsinfo[0].items[1].content;
        this.product.cusGender=sfxx.result.cardsinfo[0].items[2].content;
        this.product.cusEthnical=sfxx.result.cardsinfo[0].items[3].content;
        this.product.cusBirthday=sfxx.result.cardsinfo[0].items[4].content;
        this.product.cusResAddr=sfxx.result.cardsinfo[0].items[5].content;
        this.product.cusIncard=sfxx.result.cardsinfo[0].items[6].content;
        info.file=null;
      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange2(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }


    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl2 = img;


        var sfxx=JSON.parse(info.file.response[0].piccontent)

        console.log("varfrom*********"+JSON.stringify(sfxx));
        this.product.cusIdValidfrom= sfxx.result.cardsinfo[0].items[3].content;
        this.product.cusIdValidto=sfxx.result.cardsinfo[0].items[4].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange3(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl3 = img;

        var sfxx=JSON.parse(info.file.response[0].piccontent)

        this.product.dyptype = sfxx.result.cardsinfo[0].items[5].content +"/"+  sfxx.result.cardsinfo[0].items[2].content;

        this.product.dypname = sfxx.result.cardsinfo[0].items[1].content;
        this.product.dypvalue = sfxx.result.cardsinfo[0].items[3].content;
        this.product.dypyear =  sfxx.result.cardsinfo[0].items[8].content + "/"+sfxx.result.cardsinfo[0].items[9].content;
        this.product.dypclass =  sfxx.result.cardsinfo[0].items[10].content;

        this.product.dypremark1 = sfxx.result.cardsinfo[0].items[4].content;
        this.product.dypremark2 = sfxx.result.cardsinfo[0].items[6].content;
        this.product.dypremark3   = sfxx.result.cardsinfo[0].items[7].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange4(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl4 = img;
        var sfxx=JSON.parse(info.file.response[0].piccontent)

        console.log("varfrom*********"+JSON.stringify(sfxx));
        this.product.emergencyContact=sfxx.result.cardsinfo[0].items[1].content;

        this.product.emergencyResAddr=sfxx.result.cardsinfo[0].items[5].content;
        this.product.emergencyOffice=sfxx.result.cardsinfo[0].items[6].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange5(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl5 = img;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  packattac() {
    this.confirmServ.confirm({
      nzTitle: '确定打包附件?',
      nzOnOk: () => {
        this.packatta();
        },
    });
  }
  packatta(){
    this.comm.getzip(this.product.orderNo).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.msg.success('打包成功');

      console.log(JSON.stringify("data"+JSON.stringify(data)));
      console.log(data[0].fileUrl)
       this.downurl = data[0].fileUrl;
       console.log(this.downurl)

    })
  }
  handlePreview = (file: UploadFile) => {
    console.log("1")
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;

    // this.previewImage1 = file.url || file.thumbUrl;
    // this.previewVisible1 = true;

    // this.previewImage2 = file.url || file.thumbUrl;
    // this.previewVisible2 = true;

    // this.previewImage3 = file.url || file.thumbUrl;
    // this.previewVisible3 = true;

    // this.previewImage4 = file.url || file.thumbUrl;
    // this.previewVisible4 = true;

    // this.previewImage5 = file.url || file.thumbUrl;
    // this.previewVisible5 = true;
  }
  handlePreview1 = (file: UploadFile) => {
    console.log("1");
    console.log(file)
    this.previewImage1 = file.url || file.thumbUrl;
    this.previewVisible1 = true;
    console.log(this.previewImage1);
  }
  handlePreview2 = (file: UploadFile) => {
    console.log("2");
    console.log(file)
    this.previewImage2 = file.url || file.thumbUrl;
    this.previewVisible2 = true;
    console.log(this.previewImage2);
  }
  handlePreview3 = (file: UploadFile) => {
    console.log("3");
    console.log(file)
    this.previewImage3 = file.url || file.thumbUrl;
    this.previewVisible3 = true;
    console.log(this.previewImage3);
  }
  handlePreview4 = (file: UploadFile) => {
    console.log("4");
    console.log(file)
    this.previewImage4 = file.url || file.thumbUrl;
    this.previewVisible4 = true;
    console.log(this.previewImage4);
  }
  handlePreview5 = (file: UploadFile) => {
    console.log("5");
    console.log(file)
    this.previewImage5 = file.url || file.thumbUrl;
    this.previewVisible5 = true;
    console.log(this.previewImage5);
  }
  handlePreview6 = (file: UploadFile) => {
    console.log("6");
    console.log(file)
    this.previewImage6 = file.url || file.thumbUrl;
    this.previewVisible6 = true;
    console.log(this.previewImage6);
  }
   // 图片 wuwq
 upLoadChange1(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}

 // 图片 wuwq
 upLoadChange2(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}

 upLoadChange3(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}

upLoadChange4(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        // this.confirmServ.confirm({
        //   nzTitle: `您确定要退出系统?`,
        //   nzOkText: '确定',
        //   nzCancelText: '取消',
        //   nzOnOk: () => {
        //     console.log('wwq');
        //   }
        // });
        this.delPic(picId);
      }
  }
}


upLoadChange5(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}
upLoadChange6(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}

// 删除图片 wuwq
delPic(id) {
  this.productService.delfile(id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if(data.ok){
        // this.productorder = data.data;
        // console.log(this.productorder);
        // this.reload();
      }else{
        console.log("22")
      }
    })
}

}

