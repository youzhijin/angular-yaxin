import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { ProductService } from '../../../../service/prod/product.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {SimpleTableColumn, SimpleTableComponent} from "@delon/abc";
import {fkService} from "../../../../service/fk/fk.service";
import {plService} from "../../../../service/pl/pl.service";

@Component({
  selector: 'product-view-fico',
  templateUrl: './product-fico.component.html',
  styleUrls: ['./product-fico.component.less']
})
export class ProductficoComponent implements OnInit, OnDestroy {
  product:  any = {
    id:null,
    bmDate: null,
    bmTime: null,
    bxRq: null,
    carColor: null,
    carDetail: null,
    carVin: null,
    conJson: null,
    conNo: null,
    conType: null,
    conUrl: null,
    createSysorg: null,
    createUser: null,
    cusAge: null,
    cusBirthday: null,
    cusDeu: null,
    cusEthnical: null,
    cusGender: null,
    cusIdValidfrom: null,
    cusIdValidto: null,
    cusIncard: null,
    cusMaritalStatus: null,
    cusMobile: null,
    cusName: null,
    cusQqid: null,
    cusRemark: null,
    cusResAddr: null,
    cusWechatid: null,
    cusrIdtype: null,
    danganNo: null,
    diyaStatus: null,
    diyaUser: null,
    fangkBank: null,
    fangkId: null,
    fangkName: null,
    fangkType: null,
    fkAmount: null,
    gpsInsAddr: null,
    gpsInsData: null,
    gpsInsMobile: null,
    gpsInsUser: null,
    gpsStatus: null,
    gpsUser: null,
    guohuStatus: null,
    guohuUser: null,
    luqiaoRq: null,
    lxMobile1: null,
    lxMobile2: null,
    lxMobile3: null,
    lxName1: null,
    lxName2: null,
    lxName3: null,
    mpName: null,
    mpUrl: null,
    mpbackUrl: null,
    nianjiaRq: null,
    orderAmount: null,
    orderArea: null,
    orderBy1: null,
    orderBy10: null,
    orderBy11: null,
    orderBy12: null,
    orderBy13: null,
    orderBy14: null,
    orderBy15: null,
    orderBy16: null,
    orderBy17: null,
    orderBy18: null,
    orderBy2: null,
    orderBy3: null,
    orderBy4: null,
    orderBy5: null,
    orderBy6: null,
    orderBy7: null,
    orderBy8: null,
    orderBy9: null,
    orderBy27:null,

    orderBy28:null,

    orderBy29:null,

    orderBy30:null,
    orderCarno: null,
    orderCartype: null,
    orderInfo: null,
    orderNo: null,
    orderOper: null,
    orderPeriod: null,
    orderStatus: null,
    proClass: null,
    proIsdb: null,
    proName: null,
    proNo: null,
    proType: null,
    proWf: null,
    qiandanUser: null,
    sfAnpaijiafang: null,
    sfDianshen: null,
    wbName: null,
    wbUrl: null,
    wbbackUrl: null,
    yinhangUser: null,
    dypname: null,
    dypvalue: null,
    dypyear: null,
    dyptype: null,
    dypclass: null,

    dypremark1: null,
    dypremark2: null,
    dypremark3: null,
    emergencyRelation:  null,
    emergencyContact: null,
    emergencyMobile: null,
    emergencyOffice:null,
    emergencyResAddr:null,
    spcomment:'同意',
    bt1:null,
    bt2:null,
    bt3:null,
  };

  productfk:  any = {
    bpmStatus: null,
    bxFy: null,
    bxRq: null,
    carChengshu: null,
    carPinggujia: null,
    carRongzie: null,
    conJine: null,
    createBy: null,
    createDate: null,
    createName: null,
    cusId: null,
    cusMobile: null,
    cusName: null,
    daoshouJine: null,
    fkBy1: null,
    fkBy10: null,
    fkBy11: null,
    fkBy12: null,
    fkBy13: null,
    fkBy2: null,
    fkBy3: null,
    fkBy4: null,
    fkBy5: null,
    fkBy6: null,
    fkBy7: null,
    fkBy8: null,
    fkBy9: null,
    gpsFy: null,
    id: null,
    liulFy: null,
    loanAmount: null,
    loanAtta: null,
    loanDate: null,
    loanDbmonth: null,
    loanMonth: null,
    loanMonthSum: null,
    loanPeriod: null,
    loanReMethod: '',
    loanRemark: null,
    loanSbamount: null,
    loanSum: null,
    luqiaoRq: null,
    lydbFuwfei: null,
    nianjianRq: null,
    orderNo: null,
    qitaFy: null,
    sysCompanyCode: null,
    sysOrgCode: null,
    updateBy: null,
    updateDate: null,
    updateName: null,
    ygJine: null,

  };

  poprice:  any = {
    prType:null,
    prValue: null,
    prCount:"Y",
    prfrcount:"Y"
  };
  fkme:  any = {
    orderNo:"",
    fkMethod:"",
    fkAmount:"",
    fkRemark:"",
    fkDate:"",
    fbStatus:"",
    fbAmount:"",
    fbRemark:"",
    fbDate:""

  }
  proNos = [];
  cusrIdtypes = [];
  cusGenders = [];
  cusMaritalStatuss = [];
  cusDeus = [];
  cusEthnicals = [];
  bpmStatus = [];
  dyptypes = [];
  dypclasss = [];
  relTypes=[];
  pop_type=[];
  pay_meth=[];
  sdp_type=[];
  sf_yn=[];


  // poprice
  columns1: SimpleTableColumn[] = [

    { title: '类型 ', index: 'prType'},
    { title: '价格 ', index: 'prValue' },
    { title: '是否参与成本计算', index: 'prCount' ,render:'sf_yn'},
    { title: '是否参与分润计算', index: 'prFrcount' },
    {
      title: '删除',
      buttons: [

        {
          text: '删除',
          pop: true,
          popTitle: '确认删除?',
          click: (record: any) =>this.enable(record.id,1)
        }
      ]
    }
  ];
  url1 = this.productService.listfxjpoprice(1);

  // poprice
  columns2: SimpleTableColumn[] = [
    { title: '订单编号', index: 'orderNo'},
    { title: '放款方式', index: 'fkMethod'},
    { title: '放款金额', index: 'fkAmount'},
    { title: '放款备注', index: 'fkRemark'},
    { title: '放款日期', index: 'fkDate'},
    { title: '反馈状态', index: 'fbStatus'},
    { title: '收到金额', index: 'fbAmount'},
    { title: '反馈备注', index: 'fbRemark'},
    { title: '反馈日期', index: 'fbDate'},
    {
      title: '删除',
      buttons: [

        {
          text: '删除',
          pop: true,
          popTitle: '确认删除?',
          click: (record: any) =>this.enable(record.id,2)
        }
      ]
    }

  ];
  url2 = this.fkService.listfkme(1);
  ps = 100;
  // 搜索表单
  q: any = {
    bpmStatus:"",
    cusName: '',
    cusMobile: '',
    cusIncard: '',
    proNo: '',
    orderNo:''
  };

  loanReMethods =  [{text: '请选择', value: ''},{text: '等额本金', value: '10'}, {text: '等额本息', value: '20'}, {text: '先息后本', value: '30'}];

  columns: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '操作状态', index: 'bpmStatus' },
    { title: '订单编号', index: 'orderNo' },
    { title: '操作人 ', index: 'logUser'},
    { title: '日志日期 ', index: 'logDate' },
    { title: '日志内容', index: 'logContent' }

  ];



  // 表格组件
  @ViewChild('stre') stre: SimpleTableComponent;
  // 表格数据url
  urlreplan = this.plService.listorder("11");


  columnsreplan: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    // { title: '产品种类', index: 'proClass' },
    // { title: '融租类型', index: 'proType'},
    // { title: '车辆信息', index: 'orderInfo' },
    // { title: '订单编号 ', index: 'orderNo' },
    // { title: '客户姓名' , index: 'cusName' },
    { title: '期数', index: 'refPeriod' },
    { title: '总期数', index: 'refTotalPeriod' },
    { title: '还款日期', index: 'refDate' },
    { title: '期初金额', index: 'refFbenjin' },
    { title: '还款金额', index: 'refAmount' },
    // { title: '归还利息', index: 'refLixi' },
    { title: '剩余本金', index: 'refSybenjin' },
    { title: '备注', index: 'reMark' },
    // { title: '年份', index: 'year' },
    // { title: '广告款', index: 'isAd', render: 'isAd' },
    { title: '状态', index: 'bpmStatus', render: 'bpmStatus' },

  ];
  emenurl="";

  emen:  any = {
    emergencyContact:null,
    emergencyMobile: null,
    emergencyOffice:null
  };

  @ViewChild('stemen') stemen: SimpleTableComponent;

  columnsemen: SimpleTableColumn[] = [
    { title: '姓名 ', index: 'emergencyContact' },
    { title: '电话 ', index: 'emergencyMobile' },
    { title: '身份证', index: 'emergencyOffice' }
  ];
 // 预览图片对象
 previewImage = '';
 previewImage1 = '';
 previewImage2 = '';
 previewImage3 = '';
 previewImage4 = '';
 previewImage5 = '';
 // 是否可以预览
 previewVisible = false;
 previewVisible1 = false;
 previewVisible2 = false;
 previewVisible3 = false;
 previewVisible4 = false;
 previewVisible5 = false;
  // 预览图片地址
  propName = "";
  picUrl = '';
  // 图片上传地址

  data = [];
  downurl='https://zhaodui.oss-cn-shenzhen.aliyuncs.com/doc/hkjh.xls';
  mbdownurl='https://zhaodui.oss-cn-shenzhen.aliyuncs.com/doc/hkjh.xls';
  avatarUrl: string;

  avatarUrl1:string;
  avatarUrl2:string;

  avatarUrl3:string;
  logurl:string;
  avatarUrl4:string;
  avatarUrl5:string;
  loading = false;
  uploadUrl = '';
  uploadUr1 = '';
  uploadUr5 = '';
  uploadUr6 = '';
  uploadUr7 = '';
  uploadUr8 = '';
  uploadUr9 = '';
  orbitUrl='';
  avatarParam1 = { sbtype: '2', filetype:'sb',orderNo: '2'};
  avatarParam2 = { sbtype: '3', filetype:'sb', orderNo: '3'};
  avatarParam3 = { sbtype: '6',  filetype:'sb',orderNo: '6'};
  avatarParam4 = { sbtype: '2', filetype:'sb', orderNo: '2'};
  avatarParam5 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam6 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam7 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam8 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam9 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};

  fileList = [ ];
  fileList1 = [ ];
  fileList2 = [ ];
  fileList3 = [ ];
  fileList4 = [ ];
  fileList5 = [ ];
  backUrl='';
  MAX_COUNT = 60;
  tabIndex = 0;
  saleing = false;
  private sub: any;
  uploadExcelUrl='';
  uploadreplanExcelUrl='';
  constructor(
    public setting: SettingsService,
    private fkService: fkService,
    private plService: plService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private productService: ProductService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService,
    private confirmServ: NzModalService,
    private notice: NzNotificationService,


  ) {}

  ngOnInit(): void {
    this.loadproNos();
    this.loadcusrIdtypes();
    this.loadcusGenders();
    this.loadcusMaritalStatuss();
    this.loadcusDeus();
    this.loadcusEthnicals();
    this.loadbpmStatus();
    this.loaddyptypes();
    this.loaddypclasss();
    this.loadrelTypes();
    this.loadpop_type();
    this.loadsdp_type();
    this.loadpay_meth();
this.loadsf_yn();
    this.uploadUrl = this.productService.uploadsbUrl();
    this.uploadUr5 = this.productService.uploadUrl();
    this.orbitUrl = "http://120.78.150.43/spmq";
    this.uploadreplanExcelUrl = this.plService.uploadreplaninfo();
    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"];
      // 获取商品详情
      this.getProductInfo();
    });
    // 隐藏reusetab
    setTimeout(() => {
        this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);

  }

  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  changeTab(index) {
    // 使用异步的方式更新组件, 防止ngIf错误
    Promise.resolve(null).then(() => this.tabIndex = index);
  }
  loadsf_yn() {
    this.sf_yn = [];
    this.comm.getType("sf_yn").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.sf_yn.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loadsdp_type() {
    this.sdp_type = [];
    this.comm.getType("sdp_type").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.sdp_type.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  loadpop_type() {
    this.pop_type = [];
    this.comm.getType("pop_type").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.pop_type.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  loadpay_meth() {
    this.pay_meth = [];
    this.comm.getType("pay_meth").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.pay_meth.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }


  loadrelTypes() {
    this.relTypes = [];
    this.comm.getType("reltype").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.relTypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
// 加载产品
  loadproNos() {
    this.proNos = [];
    this.comm.getType("proNo").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.proNos.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载证件类型
  loadcusrIdtypes() {
    this.cusrIdtypes = [];
    this.comm.getType("idType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusrIdtypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载性别
  loadcusGenders() {
    this.cusGenders = [];
    this.comm.getType("sex").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusGenders.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载婚姻
  loadcusMaritalStatuss() {
    this.cusMaritalStatuss = [];
    this.comm.getType("marType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusMaritalStatuss.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载学历
  loadcusDeus() {
    this.cusDeus = [];
    this.comm.getType("eduType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusDeus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
//加载民族
  loadcusEthnicals() {
    this.cusEthnicals = [];
    this.comm.getType("ethType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusEthnicals.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载证件类型
  loadbpmStatus() {
    this.bpmStatus = [];
    this.comm.getType("bpm_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.bpmStatus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载证件类型
  loaddyptypes() {
    this.dyptypes = [];
    this.comm.getType("dyptype").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.dyptypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loaddypclasss() {
    this.dypclasss = [];
    this.comm.getType("dypclass").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.dypclasss.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  getProductInfofk() {
    this.fkService.getProduct(this.product.orderNo).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if(data.ok){
        this.productfk = data.data;
        console.log("this.productfk"+JSON.stringify(this.productfk));
      }else{
        this.productfk.loanReMethod='10';
      }

      // this.productfk.loanDate="";
    })
  }

  getProductInfo() {
    this.productService.getProduct(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      console.log("*************************"+JSON.stringify(data.data));
      this.product = data.data;
      this.backUrl = data.data.wbbackUrl;
      this.downurl = data.data.orderBy17;
      this.url1 = this.productService.listfxjpoprice(this.product.orderNo);
      this.url2 = this.fkService.listfkme(this.product.orderNo);
      this.titleService.setTitle("编辑订单("+this.product.orderNo+")");
      this.logurl = this.productService.logurl()+this.product.orderNo;
      this.urlreplan = this.plService.listorder(this.product.orderNo);
      this.emenurl = this.productService.emenurl()+this.product.orderNo;

      this.getProductInfofk();
      // this.product.dypname = data.data.fxjOrderLoanList[0].elementContent;
      // this.product.dypvalue = data.data.fxjOrderLoanList[1].elementContent;
      // this.product.dypyear = data.data.fxjOrderLoanList[2].elementContent;
      // this.product.dyptype = data.data.fxjOrderLoanList[3].elementContent;
      // this.product.dypclass = data.data.fxjOrderLoanList[4].elementContent;
      // this.product.dypremark1 = data.data.fxjOrderLoanList[5].elementContent;
      // this.product.dypremark2 = data.data.fxjOrderLoanList[6].elementContent;
      // this.product.dypremark3 = data.data.fxjOrderLoanList[7].elementContent;
      this.avatarParam1 = { sbtype: '2', filetype:'sb',orderNo: this.product.orderNo};
      this.avatarParam2 = { sbtype: '3', filetype:'sb', orderNo: this.product.orderNo};
      this.avatarParam3 = { sbtype: '6',  filetype:'sb',orderNo: this.product.orderNo};
      this.avatarParam4 = { sbtype: '2', filetype:'sb', orderNo: this.product.orderNo};
      this.avatarParam5 = { sbtype: 'No',  filetype:'mq',orderNo: this.product.orderNo};
      this.avatarParam6 = { sbtype: '10',  filetype:'yhls',orderNo: this.product.orderNo};
      this.avatarParam7 = { sbtype: '20',  filetype:'zxbg',orderNo: this.product.orderNo};
      this.avatarParam8 = { sbtype: '30',  filetype:'wxb',orderNo: this.product.orderNo};
      this.avatarParam9 = { sbtype: '30',  filetype:'jszandxsz',orderNo: this.product.orderNo};
      if (data.data.fxjOrderEmerList.length > 0) {
        this.product.emergencyRelation = data.data.fxjOrderEmerList[0].emergencyRelation;
        this.product.emergencyContact = data.data.fxjOrderEmerList[0].emergencyContact;
        this.product.emergencyMobile = data.data.fxjOrderEmerList[0].emergencyMobile;
        this.product.emergencyOffice = data.data.fxjOrderEmerList[0].emergencyOffice;
        this.product.emergencyResAddr = data.data.fxjOrderEmerList[0].emergencyResAddr;
      }
  console.log("data.data.fxjOrderEmerList[0].emergencyRelation"+data.data.fxjOrderEmerList[0].emergencyRelation);
      if (data.data.fxjOrderLoanList.length > 0) {
        let flag = 0;
        for (const detail of data.data.fxjOrderLoanList) {
          console.log("detail"+detail);

          if(detail.elementNo=='10'){
            this.product.dypname =detail.elementContent;
          }
          if(detail.elementNo=='20'){
            this.product.dypvalue =detail.elementContent;

          }
          if(detail.elementNo=='30'){
            this.product.dypyear =detail.elementContent;

          }
          if(detail.elementNo=='40'){
            this.product.dyptype =detail.elementContent;

          }if(detail.elementNo=='50'){
            this.product.dypclass =detail.elementContent;

          }
          if(detail.elementNo=='60'){
            this.product.dypremark1 =detail.elementContent;

          }if(detail.elementNo=='70'){
            this.product.dypremark2 =detail.elementContent;

          }if(detail.elementNo=='80'){
            this.product.dypremark3 =detail.elementContent;
          }
        }
      };

      if (data.data.fxjOrderAttaList.length > 0) {
        let flag = 0;
        let flag1 = 0;
        let flag2 = 0;
        let flag3 = 0;
        let flag4 = 0;
        let flag5 = 0;
        for (const detail of data.data.fxjOrderAttaList) {
          // this.fileList.push( {
          //   uid: flag,
          //   name: detail.attaName,
          //   status: 'done',
          //   url: detail.attaUrl
          // } )
          // flag++;
          if(detail.attaName == '身份证正面'){
            // if(detail.attaType == 'mq1'){

            this.fileList1.push( {
              uid: flag1,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag1++;
          }
          if(detail.attaName == '身份证反面'){
          // if(detail.attaType == 'mq1'){  
            this.fileList1.push( {
              uid: flag1,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id

            } );
            flag1++;
          }
          if(detail.attaType == '10'){
            this.fileList2.push( {
              uid: flag2,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag2++;
          }
          if(detail.attaType == '20'){
            this.fileList3.push( {
              uid: flag3,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag3++;
          }
          if(detail.attaType == '30'){
            this.fileList4.push( {
              uid: flag4,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag4++;
          }
          if(detail.attaName == '驾驶证'){
          // if(detail.attaType == 'mq1'){  
            this.fileList5.push( {
              uid: flag5,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl,
              id: detail.id
            } );
            flag5++;
          }
          if(detail.attaName == '行驶证'){
            // if(detail.attaType == 'mq1'){  
              this.fileList5.push( {
                uid: flag5,
                name: detail.attaName,
                status: 'done',
                url: detail.attaUrl,
                id: detail.id
              } );
              flag5++;
            }
        }
      };
    })
  }


  beforeUpload = (file: File) => {
      const isJPG = file.type === 'image/jpeg';
      const isPNG = file.type === 'image/png';
      if (!isJPG && !isPNG) {
          this.msg.warning(`不支持的格式: ${file.type}`);
      }
      const isLt2M = file.size / 1024 / 1024 < 10;
      if (!isLt2M) {
          this.msg.warning('图片不能超过2MB!');
      }
      return (isJPG || isPNG) && isLt2M;
  }

  private getBase64(img: File, callback: (img: any) => void) {
      if (img) {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
      }
  }



  formatterYuan = value => `¥ ${value}`;
  parserYuan = value => value.replace('¥', '');

  formatterDollor = value => `${value}`;
  parserDollor = value => value.replace('$', '');

  back() {
    this.router.navigate([ this.backUrl]);
  }

  gencon(){
    let conHead={

    };

    this.productService.gencon(this.product.orderNo,conHead).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      console.log("data.ok"+data.ok);
      if (data.ok === true) {
        this.product.conUrl = data.data.contoUrl;
        this.msg.success('合同生成成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })

  }
  fyccount() {
    let sum = 0;
    sum  = Number(sum) + Number(this.productfk.carPinggujia);
    let chengshu  =   Number(this.productfk.carChengshu);
    if(this.productfk.carRongzie ==''|| this.productfk.carRongzie  ==null){
      this.productfk.carRongzie=chengshu*sum;
    }
    if(this.productfk.bxFy ==''|| this.productfk.bxFy  ==null){
      this.productfk.bxFy = sum*0.05;
    }
    this.fybcount();
    this.fyzhcbcount();
  }

  fybcount() {
    let sum = 0;
    sum  =   Number(this.productfk.carRongzie);
    let jixjine  = Number(sum) + Number(this.productfk.bxFy);

    if(this.productfk.loanSum ==''|| this.productfk.loanSum  ==null){
      this.productfk.loanSum = jixjine;
    }
  }

  fyzhcbcount() {
    let lixi = 0;
    lixi  = Number(lixi) + Number(this.productfk.loanMonth);
    let loanPeriod  =  Number(this.productfk.loanPeriod);
    let benjin  =  Number(this.productfk.loanSum);

    this.productfk.conJine = Math.round(benjin*(1+lixi*loanPeriod/100));

    let jxjine = Number(this.productfk.loanSum);

    this.productfk.loanAmount  = Math.round(benjin*(1+lixi*loanPeriod/100)/1.1034/100)*100;

    this.productfk.lydbFuwfei =  Math.round(benjin*(1+lixi*loanPeriod/100)/1.1034/100)*100 - jxjine;
    this.productfk.daoshouJine =  Math.round( ((Number(this.productfk.loanSum ) - Number(this.productfk.bxFy) - Number(this.productfk.gpsFy) -  Number(this.productfk.conJine)/loanPeriod))*10 )/10;
  }
  savestc(orderOper,title) {
    if(orderOper=='N'){
      if(this.product.spcomment == '' || this.product.spcomment == null){
        this.msg.warning("请填写审批意见");
        return;
      }
    }
    if(orderOper=='R'){
      if(this.product.spcomment == '' || this.product.spcomment == null){
        this.msg.warning("请填写审批意见");
        return;
      }
    }  if(orderOper=='Y'){
      if(this.product.gpsInsData == '' || this.product.gpsInsData == null){
        this.msg.warning("请等待安装GPS");
        return;
      }
    }
    this.confirmServ.confirm({
      nzTitle: '确定'+title+'?',
      nzOnOk: () => {
        this.savest(orderOper);
        },
    });
  }
  savest(orderOper) {
    // 调用接口
    if(orderOper=='Y'){
      if(this.productfk.bt1===''||this.productfk.bt1===null){
        this.msg.warning("请填写登记证信息");
        return;
      }
      if(this.productfk.bt2===''||this.productfk.bt2===null){
        this.msg.warning("请填写行驶证信息");
        return;
      }
      if(this.productfk.bt3===''||this.productfk.bt3===null){
        this.msg.warning("请填写二手车发票信息");
        return;
      }
    }

    if(this.product.spcomment===''||this.product.spcomment===null){
      this.product.spcomment="同意";
    }
    // product.spcomment
    let fxjOrderLogList = [];
    if(this.product.spcomment!=''){
      // console.log("this.product.dypname*********"+this.product.dypname);

      let dto ={
        orderNo:this.product.orderNo,
        logUser:this.setting.user.username,
        logContent:this.product.spcomment
      }
      fxjOrderLogList.push(dto);
    }
    if(orderOper=='B'){
      this.product.mpUrl='/pages/annex/annex';
      this.product.mpName='补充材料';
      orderOper = "S";
    }

    let fxjOrderMainPage = {
      updateBy:this.setting.user.username,
      id:this.product.id,
      orderNo:this.product.orderNo,
      orderOper:orderOper,


      bmDate: this.product.bmDate,
      bmTime: this.product.bmTime,
      bxRq: this.product.bxRq,
      carColor: this.product.carColor,
      carDetail: this.product.carDetail,
      carVin: this.product.carVin,
      conJson: this.product.conJson,
      conNo: this.product.conNo,
      conType: this.product.conType,
      conUrl: this.product.conUrl,
      createSysorg: this.product.createSysorg,
      createUser: this.product.createUser,
      cusAge: this.product.cusAge,
      cusBirthday: this.product.cusBirthday,
      cusDeu: this.product.cusDeu,
      cusEthnical: this.product.cusEthnical,
      cusGender: this.product.cusGender,
      cusIdValidfrom: this.product.cusIdValidfrom,
      cusIdValidto: this.product.cusIdValidto,
      cusIncard: this.product.cusIncard,
      cusMaritalStatus: this.product.cusMaritalStatus,
      cusMobile: this.product.cusMobile,
      cusName: this.product.cusName,
      cusQqid: this.product.cusQqid,
      cusRemark: this.product.cusRemark,
      cusResAddr: this.product.cusResAddr,
      cusWechatid: this.product.cusWechatid,
      cusrIdtype: this.product.cusrIdtype,
      danganNo: this.product.danganNo,
      diyaStatus: this.product.diyaStatus,
      diyaUser: this.product.diyaUser,
      fangkBank: this.product.fangkBank,
      fangkId: this.product.fangkId,
      fangkName: this.product.fangkName,
      fangkType: this.product.fangkType,
      fkAmount: this.product.fkAmount,
      gpsInsAddr: this.product.gpsInsAddr,
      gpsInsData: this.product.gpsInsData,
      gpsInsMobile: this.product.gpsInsMobile,
      gpsInsUser: this.product.gpsInsUser,
      gpsStatus: this.product.gpsStatus,
      gpsUser: this.product.gpsUser,
      guohuStatus: this.product.guohuStatus,
      guohuUser: this.product.guohuUser,
      luqiaoRq: this.product.luqiaoRq,
      lxMobile1: this.product.lxMobile1,
      lxMobile2: this.product.lxMobile2,
      lxMobile3: this.product.lxMobile3,
      lxName1: this.product.lxName1,
      lxName2: this.product.lxName2,
      lxName3: this.product.lxName3,
      mpName: this.product.mpName,
      mpUrl: this.product.mpUrl,
      mpbackUrl: this.product.mpbackUrl,
      nianjiaRq: this.product.nianjiaRq,
      orderAmount: this.product.orderAmount,
      orderArea: this.product.orderArea,
      orderBy1: this.product.orderBy1,
      orderBy10: this.product.orderBy10,
      orderBy11: this.product.orderBy11,
      orderBy12: this.product.orderBy12,
      orderBy13: this.product.orderBy13,
      orderBy14: this.product.orderBy14,
      orderBy15: this.product.orderBy15,
      orderBy16: this.product.orderBy16,
      orderBy17: this.product.orderBy17,
      orderBy18: this.product.orderBy18,
      orderBy2: this.product.orderBy2,
      orderBy3: this.product.orderBy3,
      orderBy4: this.product.orderBy4,
      orderBy5: this.product.orderBy5,
      orderBy6: this.product.orderBy6,
      orderBy7: this.product.orderBy7,
      orderBy8: this.product.orderBy8,
      orderBy9: this.product.orderBy9,
      orderCarno: this.product.orderCarno,
      orderCartype: this.product.orderCartype,
      orderInfo: this.product.orderInfo,

      orderPeriod: this.product.orderPeriod,
      orderStatus: this.product.orderStatus,
      proClass: this.product.proClass,
      proIsdb: this.product.proIsdb,
      proName: this.product.proName,
      proNo: this.product.proNo,
      proType: this.product.proType,
      proWf: this.product.proWf,
      qiandanUser: this.product.qiandanUser,
      sfAnpaijiafang: this.product.sfAnpaijiafang,
      sfDianshen: this.product.sfDianshen,
      wbName: this.product.wbName,
      wbUrl: this.product.wbUrl,
      wbbackUrl: this.product.wbbackUrl,
      yinhangUser: this.product.yinhangUser,
      fxjOrderLogList:JSON.parse(JSON.stringify(fxjOrderLogList))

    };

    this.productService.editPro(fxjOrderMainPage).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      console.log("data.ok"+data.ok);
      if (data.ok === true) {
        this.savefk();
        // this.msg.success('放款成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })
  }

  savefk() {
    // if(this.productfk.loanDate===''||this.productfk.loanDate===''){
    //   this.msg.warning("请选择首次还款日期");
    //   return;
    // }
    // if(this.productfk.loanReMethod===''||this.productfk.loanReMethod===''){
    //   this.msg.warning("请选择还款方式");
    //   return;
    // }
    // if(this.productfk.loanAmount===''||this.productfk.loanAmount===''){
    //   this.msg.warning("请输入放款金额");
    //   return;
    // }
    // if(this.productfk.loanDbmonth===''||this.productfk.loanDbmonth===''){
    //   this.msg.warning("请输入担保月利息");
    //   return;
    // }
    // if(this.productfk.loanMonth===''||this.productfk.loanMonth===''){
    //   this.msg.warning("请输入银行月利息");
    //   return;
    // }
    // if(this.productfk.loanSum===''||this.productfk.loanSum===''){
    //   this.msg.warning("请输入应还总金额");
    //   return;
    // }
    let fxjLoanFk = {
      updateBy:this.setting.user.username,
      id:this.productfk.id,
      bpmStatus: this.productfk.bpmStatus,
      bxFy: this.productfk.bxFy,
      bxRq: this.productfk.bxRq,
      carChengshu: this.productfk.carChengshu,
      carPinggujia: this.productfk.carPinggujia,
      carRongzie: this.productfk.carRongzie,
      conJine: this.productfk.conJine,
      createBy: this.productfk.createBy,
      createDate: this.productfk.createDate,
      createName: this.productfk.createName,
      cusId: this.productfk.cusId,
      cusMobile: this.productfk.cusMobile,
      cusName: this.productfk.cusName,
      daoshouJine: this.productfk.daoshouJine,
      fkBy1: this.productfk.fkBy1,
      fkBy10: this.productfk.fkBy10,
      fkBy11: this.productfk.fkBy11,
      fkBy12: this.productfk.fkBy12,
      fkBy13: this.productfk.fkBy13,
      fkBy2: this.productfk.fkBy2,
      fkBy3: this.productfk.fkBy3,
      fkBy4: this.productfk.fkBy4,
      fkBy5: this.productfk.fkBy5,
      fkBy6: this.productfk.fkBy6,
      fkBy7: this.productfk.fkBy7,
      fkBy8: this.productfk.fkBy8,
      fkBy9: this.productfk.fkBy9,
      gpsFy: this.productfk.gpsFy,
      liulFy: this.productfk.liulFy,
      loanAmount: this.productfk.loanAmount,
      loanAtta: this.productfk.loanAtta,
      loanDate: this.productfk.loanDate,
      loanDbmonth: this.productfk.loanDbmonth,
      loanMonth: this.productfk.loanMonth,
      loanMonthSum: this.productfk.loanMonthSum,
      loanPeriod: this.productfk.loanPeriod,
      loanReMethod: this.productfk.loanReMethod,
      loanRemark: this.productfk.loanRemark,
      loanSbamount: this.productfk.loanSbamount,
      loanSum: this.productfk.loanSum,
      luqiaoRq: this.productfk.luqiaoRq,
      lydbFuwfei: this.productfk.lydbFuwfei,
      nianjianRq: this.productfk.nianjianRq,
      orderNo: this.productfk.orderNo,
      qitaFy: this.productfk.qitaFy,
      // sysCompanyCode: this.productfk.sysCompanyCode,
      // sysOrgCode: this.productfk.sysOrgCode,
      // updateName: this.productfk.updateName,
      ygJine: this.productfk.ygJine,
    };

    // 调用接口
    this.fkService.editPro(fxjLoanFk).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if (data.ok === true) {
        // this.msg.success('修改成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data['message']);
      }
    })
  }
  @ViewChild('st1') st1: SimpleTableComponent;

  saveprice(){
    // if(this.product.carVin==""||this.product.carVin==null){
    //   this.msg.warning("请输入VIN");
    //   return;
    // }
    let SdPoPriceEntity = {

      orderNo: this.product.orderNo,
      prType:this.poprice.prType,
      prValue: this.poprice.prValue,
      prCount:this.poprice.prCount,
      prFrcount:this.poprice.prFrcount
    };
    this.loading = true;
    this.productService.createfxjpoprice(SdPoPriceEntity).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.loading = false;
      if (data.ok === true) {
        this.st1.reload();
        this.msg.success("成功");
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })
  }

  @ViewChild('st2') st2: SimpleTableComponent;

  savefkme(){
    // if(this.product.carVin==""||this.product.carVin==null){
    //   this.msg.warning("请输入VIN");
    //   return;
    // }

    let SdPoPriceEntity = {

      orderNo: this.product.orderNo,

      fkMethod:this.fkme.fkMethod,
      fkAmount:this.fkme.fkAmount,
      fkRemark:this.fkme.fkRemark,
      fkDate:this.fkme.fkDate,
      fbStatus:this.fkme.fbStatus,
      fbAmount:this.fkme.fbAmount,
      fbRemark:this.fkme.fbRemark,
      fbDate:this.fkme.fbDate


    };
    this.loading = true;
    this.fkService.createfkme(SdPoPriceEntity).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.loading = false;
      if (data.ok === true) {
        this.st2.reload();
        this.msg.success("成功");
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })
  }

  changeColor(res) {
    this.product.colorSort = res.color.hex;
  }




  handleChange1(info: { file: UploadFile }) {


    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl1 = img;
        console.log("info1*********"+JSON.stringify(info.file.response[0].piccontent));

        var sfxx=JSON.parse(info.file.response[0].piccontent)

        this.product.cusName=sfxx.result.cardsinfo[0].items[1].content;
        this.product.cusGender=sfxx.result.cardsinfo[0].items[2].content;
        this.product.cusEthnical=sfxx.result.cardsinfo[0].items[3].content;
        this.product.cusBirthday=sfxx.result.cardsinfo[0].items[4].content;
        this.product.cusResAddr=sfxx.result.cardsinfo[0].items[5].content;
        this.product.cusIncard=sfxx.result.cardsinfo[0].items[6].content;
        info.file=null;
      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange2(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }


    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl2 = img;


        var sfxx=JSON.parse(info.file.response[0].piccontent)

        console.log("varfrom*********"+JSON.stringify(sfxx));
        this.product.cusIdValidfrom= sfxx.result.cardsinfo[0].items[3].content;
        this.product.cusIdValidto=sfxx.result.cardsinfo[0].items[4].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange3(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl3 = img;

        var sfxx=JSON.parse(info.file.response[0].piccontent)

        this.product.dyptype = sfxx.result.cardsinfo[0].items[5].content +"/"+  sfxx.result.cardsinfo[0].items[2].content;

        this.product.dypname = sfxx.result.cardsinfo[0].items[1].content;
        this.product.dypvalue = sfxx.result.cardsinfo[0].items[3].content;
        this.product.dypyear =  sfxx.result.cardsinfo[0].items[8].content + "/"+sfxx.result.cardsinfo[0].items[9].content;
        this.product.dypclass =  sfxx.result.cardsinfo[0].items[10].content;

        this.product.dypremark1 = sfxx.result.cardsinfo[0].items[4].content;
        this.product.dypremark2 = sfxx.result.cardsinfo[0].items[6].content;
        this.product.dypremark3   = sfxx.result.cardsinfo[0].items[7].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange4(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl4 = img;
        var sfxx=JSON.parse(info.file.response[0].piccontent)

        console.log("varfrom*********"+JSON.stringify(sfxx));
        this.product.emergencyContact=sfxx.result.cardsinfo[0].items[1].content;

        this.product.emergencyResAddr=sfxx.result.cardsinfo[0].items[5].content;
        this.product.emergencyOffice=sfxx.result.cardsinfo[0].items[6].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange5(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl5 = img;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }

  handlePreview = (file: UploadFile) => {
    console.log("1")
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;

    // this.previewImage1 = file.url || file.thumbUrl;
    // this.previewVisible1 = true;

    // this.previewImage2 = file.url || file.thumbUrl;
    // this.previewVisible2 = true;

    // this.previewImage3 = file.url || file.thumbUrl;
    // this.previewVisible3 = true;

    // this.previewImage4 = file.url || file.thumbUrl;
    // this.previewVisible4 = true;

    // this.previewImage5 = file.url || file.thumbUrl;
    // this.previewVisible5 = true;
  }
  handlePreview1 = (file: UploadFile) => {
    console.log("1");
    console.log(file)
    this.previewImage1 = file.url || file.thumbUrl;
    this.previewVisible1 = true;
    console.log(this.previewImage1);
  }
  handlePreview2 = (file: UploadFile) => {
    console.log("2");
    console.log(file)
    this.previewImage2 = file.url || file.thumbUrl;
    this.previewVisible2 = true;
    console.log(this.previewImage2);
  }
  handlePreview3 = (file: UploadFile) => {
    console.log("3");
    console.log(file)
    this.previewImage3 = file.url || file.thumbUrl;
    this.previewVisible3 = true;
    console.log(this.previewImage3);
  }
  handlePreview4 = (file: UploadFile) => {
    console.log("4");
    console.log(file)
    this.previewImage4 = file.url || file.thumbUrl;
    this.previewVisible4 = true;
    console.log(this.previewImage4);
  }
  handlePreview5 = (file: UploadFile) => {
    console.log("5");
    console.log(file)
    this.previewImage5 = file.url || file.thumbUrl;
    this.previewVisible5 = true;
    console.log(this.previewImage5);
  }

  enable(id,deltype) {
    this.productService.delid(id,deltype).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(ret=>{
      if (ret.ok === true) {



        if(deltype==1){
          this.st1.reload();
        }
        if(deltype==2){
          this.st2.reload();
        }



      } else {
        this.msg.error(ret.message);
      }
    });


  }

  packattac(){
    this.confirmServ.confirm({
      nzTitle: '确定要打包合同?',
      nzOnOk: () => {
        this.packatta();
        },
    });
  }




  packatta(){

    let conmapList = [];

    try{
      let dto1 ={
        conKey:"htbh",
        conValue:this.product.orderNo,
      }
      conmapList.push(dto1);
    }catch (e) {

    }
    try{
      let dto2 ={
        conKey:"xm",
        conValue:this.product.cusName,
      }
      conmapList.push(dto2);
    }catch (e) {

    }
    try{
      let dto3 ={
        conKey:"sfz",
        conValue:this.product.cusIncard,
      }
      conmapList.push(dto3);
    }catch (e) {

    }
    try{
      let dto4 ={
        conKey:"lxdz",
        conValue:this.product.cusResAddr,
      }
      conmapList.push(dto4);
    }catch (e) {

    }
    try{
      let dto5 ={
        conKey:"lxdh",
        conValue:this.product.cusMobile,
      }
      conmapList.push(dto5);
    }catch (e) {

    }
    try{
      let dto6 ={
        conKey:"cphm",
        conValue:this.product.orderBy61,
      }
      conmapList.push(dto6);
    }catch (e) {

    }
    try{
      let dto7 ={
        conKey:"cjhm",
        conValue:this.product.orderBy64,
      }
      conmapList.push(dto7);
    }catch (e) {

    }
    try{
      let dto8 ={
        conKey:"fdjh",
        conValue:this.product.orderBy63,
      }
      conmapList.push(dto8);
    }catch (e) {

    }
    try{
      let dto9 ={
        conKey:"pplx",
        conValue:this.product.orderBy62,
      }
      conmapList.push(dto9);
    }catch (e) {

    }
    try{
      let dto10 ={
        conKey:"cszj",
        conValue:this.product.orderBy48,
      }
      conmapList.push(dto10);
    }catch (e) {

    }
    try{
      let dto11 ={
        conKey:"csys",
        conValue:this.product.orderBy37,
      }
      conmapList.push(dto11);
    }catch (e) {

    }
    try{
      let dto12 ={
        conKey:"sfzj",
        conValue:this.product.orderBy33,
      }
      conmapList.push(dto12);
    }catch (e) {

    }
    try{
      let dto13 ={
        conKey:"dkzj",
        conValue:this.product.orderBy51,
      }
      conmapList.push(dto13);
    }catch (e) {

    }


    let  conhead={
      contempPath:"https://zhaodui.oss-cn-shenzhen.aliyuncs.com/doc/htmb/hxhtmb.docx",
      conmapList:JSON.parse(JSON.stringify(conmapList)),
    }
    console.log(conhead)
    this.comm.gencon(this.product.orderNo,conhead).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.msg.success('生成成功');
        if(data.ok){
          console.log(data);
          this.downurl = data.data.contoUrl;
          // this.downurl = data.data.contempPath;
          // if(this.downurl!=null||this.downurl==null){
          if(this.downurl!=null){
            this.opennewwindow(this.downurl);
          }
        }


    })




  }
  opennewwindow(url){

    const newWindow = window.open(url, '_blank');
    newWindow.document.title = '合同';

  }

  uploadProd(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
    }
    if (info.file.status === 'done') {
      this.loading = false;
      this.stre.load();
      this.notice.create('success', '成功', '还款信息导入成功');
    }
    if (info.file.status === 'error') {
      this.loading = false;
      this.notice.create('error', '失败', '还款信息导入失败');
    }
  }
  saveemen(){

    if(this.emen.emergencyMobile==""||this.emen.emergencyMobile==null){
      this.msg.warning("电话");
      return;
    }

    let emen = {

      emergencyContact: this.emen.emergencyContact,
      emergencyMobile:this.emen.emergencyMobile,
      emergencyOffice: this.emen.emergencyOffice,
      orderNo:this.product.orderNo

    };
    this.loading = true;
    this.productService.savemen(emen).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.loading = false;
      if (data.ok === true) {
        this.stemen.reload();
        this.msg.success("成功");
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })
  }
savejq(){
  let fxjOrderLogList = [];
  if(this.product.spcomment!=''){
    // console.log("this.product.dypname*********"+this.product.dypname);

    let dto ={
      orderNo:this.product.orderNo,
      logUser:this.setting.user.username,
      logContent:'结清'
    }
    fxjOrderLogList.push(dto);
  }
  let fxjOrderMainPage = {
    updateBy:this.setting.user.username,
    id:this.product.id,
    orderNo:this.product.orderNo,
    orderOper:'S',


    bmDate: this.product.bmDate,
    bmTime: this.product.bmTime,
    bxRq: this.product.bxRq,
    carColor: this.product.carColor,
    carDetail: this.product.carDetail,
    carVin: this.product.carVin,
    conJson: this.product.conJson,
    conNo: this.product.conNo,
    conType: this.product.conType,
    conUrl: this.product.conUrl,
    createSysorg: this.product.createSysorg,
    createUser: this.product.createUser,
    cusAge: this.product.cusAge,
    cusBirthday: this.product.cusBirthday,
    cusDeu: this.product.cusDeu,
    cusEthnical: this.product.cusEthnical,
    cusGender: this.product.cusGender,
    cusIdValidfrom: this.product.cusIdValidfrom,
    cusIdValidto: this.product.cusIdValidto,
    cusIncard: this.product.cusIncard,
    cusMaritalStatus: this.product.cusMaritalStatus,
    cusMobile: this.product.cusMobile,
    cusName: this.product.cusName,
    cusQqid: this.product.cusQqid,
    cusRemark: this.product.cusRemark,
    cusResAddr: this.product.cusResAddr,
    cusWechatid: this.product.cusWechatid,
    cusrIdtype: this.product.cusrIdtype,
    danganNo: this.product.danganNo,
    diyaStatus: this.product.diyaStatus,
    diyaUser: this.product.diyaUser,
    fangkBank: this.product.fangkBank,
    fangkId: this.product.fangkId,
    fangkName: this.product.fangkName,
    fangkType: this.product.fangkType,
    fkAmount: this.product.fkAmount,
    gpsInsAddr: this.product.gpsInsAddr,
    gpsInsData: this.product.gpsInsData,
    gpsInsMobile: this.product.gpsInsMobile,
    gpsInsUser: this.product.gpsInsUser,
    gpsStatus: this.product.gpsStatus,
    gpsUser: this.product.gpsUser,
    guohuStatus: this.product.guohuStatus,
    guohuUser: this.product.guohuUser,
    luqiaoRq: this.product.luqiaoRq,
    lxMobile1: this.product.lxMobile1,
    lxMobile2: this.product.lxMobile2,
    lxMobile3: this.product.lxMobile3,
    lxName1: this.product.lxName1,
    lxName2: this.product.lxName2,
    lxName3: this.product.lxName3,
    mpName: this.product.mpName,
    mpUrl: this.product.mpUrl,
    mpbackUrl: this.product.mpbackUrl,
    nianjiaRq: this.product.nianjiaRq,
    orderAmount: this.product.orderAmount,
    orderArea: this.product.orderArea,
    orderBy1: this.product.orderBy1,
    orderBy10: this.product.orderBy10,
    orderBy11: this.product.orderBy11,
    orderBy12: this.product.orderBy12,
    orderBy27: this.product.orderBy27,
    orderBy28: this.product.orderBy28,
    orderBy29: this.product.orderBy29,
    orderBy13: this.product.orderBy13,
    orderBy14: this.product.orderBy14,
    orderBy15: this.product.orderBy15,
    orderBy16: this.product.orderBy16,
    orderBy17: this.product.orderBy17,
    orderBy18: this.product.orderBy18,
    orderBy2: this.product.orderBy2,
    orderBy3: this.product.orderBy3,
    orderBy4: this.product.orderBy4,
    orderBy5: this.product.orderBy5,
    orderBy6: this.product.orderBy6,
    orderBy7: this.product.orderBy7,
    orderBy8: this.product.orderBy8,
    orderBy9: this.product.orderBy9,
    orderCarno: this.product.orderCarno,
    orderCartype: this.product.orderCartype,
    orderInfo: this.product.orderInfo,

    orderPeriod: this.product.orderPeriod,
    orderStatus: this.product.orderStatus,
    proClass: this.product.proClass,
    proIsdb: this.product.proIsdb,
    proName: this.product.proName,
    proNo: this.product.proNo,
    proType: this.product.proType,
    proWf: this.product.proWf,
    qiandanUser: this.product.qiandanUser,
    sfAnpaijiafang: this.product.sfAnpaijiafang,
    sfDianshen: this.product.sfDianshen,
    wbName: this.product.wbName,
    wbUrl: this.product.wbUrl,
    wbbackUrl: this.product.wbbackUrl,
    yinhangUser: this.product.yinhangUser,
    fxjOrderLogList:JSON.parse(JSON.stringify(fxjOrderLogList))

  };

  this.productService.editPro(fxjOrderMainPage).pipe(
    catchError(err => {
      return of(err);
    })
  ).subscribe(data => {
    console.log("data.ok"+data.ok);
    if (data.ok === true) {
      this.savefk();
      this.msg.success('结清成功');
      this.emit.sendMessage("RELOAD_GOODS_LIST");
    } else {
      this.msg.error(data.respMsg);
    }
  })


}

 // 图片 wuwq
 upLoadChange1(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}

 // 图片 wuwq
 upLoadChange2(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}

 upLoadChange3(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}

upLoadChange4(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}


upLoadChange5(event) {
  console.log(event);
  let file = event ? event.file : null;
  console.log(file);
  let datas = file && file.id ? file : file.response;
  console.log(datas);
  if (datas) {
      if (event.type == 'success') {
          console.log('ss');
      } else if (event.type == 'removed' ) {
        console.log('aa');
        let picId = file.id ? file.id : file.response[0].fileId
        this.delPic(picId);
      }
  }
}

// 删除图片 wuwq
delPic(id) {
  this.productService.delfile(id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if(data.ok){
        // this.productorder = data.data;
        // console.log(this.productorder);
        // this.reload();
      }else{
        console.log("22")
      }
    })
}
}
