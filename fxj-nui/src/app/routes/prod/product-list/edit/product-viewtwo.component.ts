import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { ProductService } from '../../../../service/prod/product.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {SimpleTableButton, SimpleTableColumn, SimpleTableComponent} from "@delon/abc";

@Component({
  selector: 'product-viewtwo',
  templateUrl: './product-viewtwo.component.html',
  styleUrls: ['./product-viewtwo.component.less']
})
export class ProductviewTwoComponent implements OnInit, OnDestroy {
  product:  any = {
    id:null,
    bmDate: null,
    bmTime: null,
    bxRq: null,
    carColor: null,
    carDetail: null,
    carVin: null,
    conJson: null,
    conNo: null,
    conType: null,
    conUrl: null,
    createSysorg: null,
    createUser: null,
    cusAge: null,
    cusBirthday: null,
    cusDeu: null,
    cusEthnical: null,
    cusGender: null,
    cusIdValidfrom: null,
    cusIdValidto: null,
    cusIncard: null,
    cusMaritalStatus: null,
    cusMobile: null,
    cusName: null,
    cusQqid: null,
    cusRemark: null,
    cusResAddr: null,
    cusWechatid: null,
    cusrIdtype: null,
    danganNo: null,
    diyaStatus: null,
    diyaUser: null,
    fangkBank: null,
    fangkId: null,
    fangkName: null,
    fangkType: null,
    fkAmount: null,
    gpsInsAddr: null,
    gpsInsData: null,
    gpsInsMobile: null,
    gpsInsUser: null,
    gpsStatus: null,
    gpsUser: null,
    guohuStatus: null,
    guohuUser: null,
    luqiaoRq: null,
    lxMobile1: null,
    lxMobile2: null,
    lxMobile3: null,
    lxName1: null,
    lxName2: null,
    lxName3: null,
    mpName: null,
    mpUrl: null,
    mpbackUrl: null,
    nianjiaRq: null,
    orderAmount: null,
    orderArea: null,
    orderBy1: null,
    orderBy10: null,
    orderBy11: null,
    orderBy12: null,
    orderBy13: null,
    orderBy14: null,
    orderBy15: null,
    orderBy16: null,
    orderBy17: null,
    orderBy18: null,
    orderBy2: null,
    orderBy3: null,
    orderBy4: null,
    orderBy5: null,
    orderBy6: null,
    orderBy7: null,
    orderBy8: null,
    orderBy9: null,
    orderBy60: null,
    orderBy61: null,
    orderBy62: null,
    orderBy63: null,
    orderBy64: null,
    orderBy65: null,
    orderBy66: null,
    orderBy67: null,
    orderBy68: null,
    orderBy69: null,
    orderBy70: null,
    orderBy71: null,
    orderBy72: null,

    orderCarno: null,
    orderCartype: null,
    orderInfo: null,
    orderNo: null,
    orderOper: null,
    orderPeriod: null,
    orderStatus: null,
    proClass: null,
    proIsdb: null,
    proName: null,
    proNo: null,
    proType: null,
    proWf: null,
    qiandanUser: null,
    sfAnpaijiafang: null,
    sfDianshen: null,
    wbName: null,
    wbUrl: null,
    wbbackUrl: null,
    yinhangUser: null,
    dypname: null,
    dypvalue: null,
    dypyear: null,
    dyptype: null,
    dypclass: null,

    dypremark1: null,
    dypremark2: null,
    dypremark3: null,
    emergencyRelation:  null,
    emergencyContact: null,
    emergencyMobile: null,
    emergencyOffice:null,
    emergencyResAddr:null,
    spcomment:'',

  };
  ps = 100;
  // 搜索表单
  q: any = {
    bpmStatus:"",
    cusName: '',
    cusMobile: '',
    cusIncard: '',
    proNo: '',
    orderNo:''
  };
  orderBy86 =  [{text: '履约中', value: '履约中'}, {text: '已结清', value: '已结清'} ,{text: '违约', value: '违约'}, {text: '短租', value: '短租'}, {text: '二次销售中', value: '二次销售中'}, {text: '已二次销售中', value: '已二次销售中'} ];

  emenurl="";


  @ViewChild('stemen') stemen: SimpleTableComponent;

  columnsemen: SimpleTableColumn[] = [
    { title: '姓名 ', index: 'emergencyContact' },
    { title: '电话 ', index: 'emergencyMobile' },
    { title: '联系人身份证', index: 'emergencyOffice' }
  ];

   columns: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '操作状态', index: 'bpmStatus' },
    { title: '订单编号', index: 'orderNo' },
    { title: '操作人 ', index: 'logUser'},
    { title: '日志日期 ', index: 'logDate' },
    { title: '日志内容', index: 'logContent' }

  ];
  // 预览图片对象
  previewImage = '';
  previewImage1 = '';
  previewImage2 = '';
  previewImage3 = '';
  previewImage4 = '';
  previewImage5 = '';
  previewImage6 = '';
  // 是否可以预览
  previewVisible = false;
  previewVisible1 = false;
  previewVisible2 = false;
  previewVisible3 = false;
  previewVisible4 = false;
  previewVisible5 = false;
  previewVisible6 = false;
  // 预览图片地址
  propName = "";
  picUrl = '';
  // 图片上传地址

  proNos = [];
  cusrIdtypes = [];
  cusGenders = [];
  cusMaritalStatuss = [];
  cusDeus = [];
  cusEthnicals = [];
  bpmStatus = [];
  dyptypes = [];
  dypclasss = [];
  relTypes=[];
  qdys=[];

  avatarUrl: string;

  avatarUrl1:string;
  avatarUrl2:string;

  avatarUrl3:string;
  logurl:string;
  avatarUrl4:string;
  avatarUrl5:string;
  avatarUrl6:string;
  loading = false;
  uploadUrl = '';
  uploadUr1 = '';
  uploadUr5 = '';
  uploadUr6 = '';
  uploadUr7 = '';
  uploadUr8 = '';
  uploadUr9 = '';
  uploadUr10 = '';
  orbitUrl='';
  avatarParam1 = { sbtype: '2', filetype:'sb',orderNo: '2'};
  avatarParam2 = { sbtype: '3', filetype:'sb', orderNo: '3'};
  avatarParam3 = { sbtype: '6',  filetype:'sb',orderNo: '6'};
  avatarParam4 = { sbtype: '2', filetype:'sb', orderNo: '2'};
  avatarParam5 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam6 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam7 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam8 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam9 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  avatarParam10 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};


  fileList = [ ];
  fileList1 = [ ];
  fileList2 = [ ];
  fileList3 = [ ];
  fileList4 = [ ];
  fileList5 = [ ];
  fileList6 = [ ];
  backUrl='';
  MAX_COUNT = 60;
  tabIndex = 0;
  saleing = false;
  private sub: any;

  constructor(
    public setting: SettingsService,

    private msg: NzMessageService,
    private modalService: NzModalService,
    private productService: ProductService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService
  ) {}

  ngOnInit(): void {
    this.loadproNos();
    this.loadcusrIdtypes();
    this.loadcusGenders();
    this.loadcusMaritalStatuss();
    this.loadcusDeus();
    this.loadcusEthnicals();
    this.loadbpmStatus();
    this.loaddyptypes();
    this.loaddypclasss();
    this.loadrelTypes();
    this.loadqdys();

    this.uploadUrl = this.productService.uploadsbUrl();
    this.uploadUr1 = this.productService.uploadUrl();
    this.uploadUr5 = this.productService.uploadUrl();
    this.uploadUr6 = this.productService.uploadUrl();
    this.uploadUr7 = this.productService.uploadUrl();
    this.uploadUr8 = this.productService.uploadUrl();
    this.uploadUr9 = this.productService.uploadUrl();
    this.uploadUr10 = this.productService.uploadUrl();
    this.orbitUrl = "http://120.78.150.43/spmq";
    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"];
      // 获取商品详情
      this.getProductInfo();
    });
    // 隐藏reusetab
    setTimeout(() => {
        this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);

  }

  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  changeTab(index) {
    // 使用异步的方式更新组件, 防止ngIf错误
    Promise.resolve(null).then(() => this.tabIndex = index);
  }
  loadrelTypes() {
    this.relTypes = [];
    this.comm.getType("reltype").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.relTypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载产品
  loadproNos() {
    this.proNos = [];
    this.comm.getType("proNo").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.proNos.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载产品
  loadqdys() {
    this.qdys = [];
    this.comm.getType("qdy").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.qdys.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载证件类型
  loadcusrIdtypes() {
    this.cusrIdtypes = [];
    this.comm.getType("idType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusrIdtypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载性别
  loadcusGenders() {
    this.cusGenders = [];
    this.comm.getType("sex").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusGenders.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载婚姻
  loadcusMaritalStatuss() {
    this.cusMaritalStatuss = [];
    this.comm.getType("marType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusMaritalStatuss.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载学历
  loadcusDeus() {
    this.cusDeus = [];
    this.comm.getType("eduType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusDeus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
//加载民族
  loadcusEthnicals() {
    this.cusEthnicals = [];
    this.comm.getType("ethType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusEthnicals.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载证件类型
  loadbpmStatus() {
    this.bpmStatus = [];
    this.comm.getType("bpm_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.bpmStatus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载证件类型
  loaddyptypes() {
    this.dyptypes = [];
    this.comm.getType("dyptype").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.dyptypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loaddypclasss() {
    this.dypclasss = [];
    this.comm.getType("dypclass").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.dypclasss.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  getProductInfo() {
    this.productService.getProduct(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.product = data.data;
      this.backUrl = "/prod/product/5/";

      this.titleService.setTitle("订单("+this.product.orderNo+")");
      this.logurl = this.productService.logurl()+this.product.orderNo;
      this.emenurl = this.productService.emenurl()+this.product.orderNo;

      console.log(this.logurl);
      this.avatarParam1 = { sbtype: '2', filetype:'sb',orderNo: this.product.orderNo};
      this.avatarParam2 = { sbtype: '3', filetype:'sb', orderNo: this.product.orderNo};
      this.avatarParam3 = { sbtype: '6',  filetype:'sb',orderNo: this.product.orderNo};
      this.avatarParam4 = { sbtype: '2', filetype:'sb', orderNo: this.product.orderNo};
      this.avatarParam5 = { sbtype: 'No',  filetype:'mq',orderNo: this.product.orderNo};
      this.avatarParam6 = { sbtype: '10',  filetype:'yhls',orderNo: this.product.orderNo};
      this.avatarParam7 = { sbtype: '20',  filetype:'zxbg',orderNo: this.product.orderNo};
      this.avatarParam8 = { sbtype: '30',  filetype:'wxb',orderNo: this.product.orderNo};
      this.avatarParam9 = { sbtype: '30',  filetype:'jszandxsz',orderNo: this.product.orderNo};
      this.avatarParam10 = { sbtype: '30',  filetype:'GPS',orderNo: this.product.orderNo};
      // if (data.data.fxjOrderEmerList!=null&&data.data.fxjOrderEmerList.length > 0) {
      //   this.product.emergencyRelation = data.data.fxjOrderEmerList[0].emergencyRelation;
      //   this.product.emergencyContact = data.data.fxjOrderEmerList[0].emergencyContact;
      //   this.product.emergencyMobile = data.data.fxjOrderEmerList[0].emergencyMobile;
      //   this.product.emergencyOffice = data.data.fxjOrderEmerList[0].emergencyOffice;
      //   this.product.emergencyResAddr = data.data.fxjOrderEmerList[0].emergencyResAddr;
      //   this.product.emergencyContact1 = data.data.fxjOrderEmerList[1].emergencyContact;
      //   this.product.emergencyMobile1 = data.data.fxjOrderEmerList[1].emergencyMobile;
      // }
  // console.log("data.data.fxjOrderEmerList[0].emergencyRelation"+data.data.fxjOrderEmerList[0].emergencyRelation);
      if (data.data.fxjOrderLoanList.length > 0) {
        let flag = 0;
        for (const detail of data.data.fxjOrderLoanList) {
          console.log("detail"+detail);

          if(detail.elementNo=='10'){
            this.product.dypname =detail.elementContent;
          }
          if(detail.elementNo=='20'){
            this.product.dypvalue =detail.elementContent;

          }
          if(detail.elementNo=='30'){
            this.product.dypyear =detail.elementContent;

          }
          if(detail.elementNo=='40'){
            this.product.dyptype =detail.elementContent;

          }if(detail.elementNo=='50'){
            this.product.dypclass =detail.elementContent;

          }
          if(detail.elementNo=='60'){
            this.product.dypremark1 =detail.elementContent;

          }if(detail.elementNo=='70'){
            this.product.dypremark2 =detail.elementContent;

          }if(detail.elementNo=='80'){
            this.product.dypremark3 =detail.elementContent;
          }if(detail.elementNo=='90'){
            this.product.dyptype1 =detail.elementContent;

          }if(detail.elementNo=='100'){
            this.product.dypyear1 =detail.elementContent;

          }if(detail.elementNo=='110'){
            this.product.dypclass1 =detail.elementContent;

          }if(detail.elementNo=='120'){
            this.product.dypremark11 =detail.elementContent;
          }
        }
      };

      if (data.data.fxjOrderAttaList.length > 0) {
        let flag = 0;
        let flag1 = 0;
        let flag2 = 0;
        let flag3 = 0;
        let flag4 = 0;
        let flag5 = 0;
        let flag6 = 0;
        for (const detail of data.data.fxjOrderAttaList) {
          // this.fileList.push( {
          //   uid: flag,
          //   name: detail.attaName,
          //   status: 'done',
          //   url: detail.attaUrl
          // } )
          // flag++;
          if(detail.attaName == '身份证正面'){
            // if(detail.attaType == 'mq1'){

            this.fileList1.push( {
              uid: flag1,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl
            } );
            flag1++;
          }
          if(detail.attaName == '身份证反面'){
          // if(detail.attaType == 'mq1'){  
            this.fileList1.push( {
              uid: flag1,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl
            } );
            flag1++;
          }
          if(detail.attaType == 'sb'){
              this.fileList1.push( {
                uid: flag1,
                name: detail.attaName,
                status: 'done',
                url: detail.attaUrl
              } );
              flag1++;
            }
          if(detail.attaType == '10'){
            this.fileList2.push( {
              uid: flag2,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl
            } );
            flag2++;
          }
          if(detail.attaType == 'yhls'){
            this.fileList2.push( {
              uid: flag2,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl
            } );
            flag2++;
          }
         
          if(detail.attaType == '20'){
            this.fileList3.push( {
              uid: flag3,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl
            } );
            flag3++;
          }
          if(detail.attaType == 'zxbg'){
            this.fileList3.push( {
              uid: flag3,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl
            } );
            flag3++;
          }
          if(detail.attaType == '30'){
            this.fileList4.push( {
              uid: flag4,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl
            } );
            flag4++;
          }
          if(detail.attaType == 'wxb'){
            this.fileList4.push( {
              uid: flag4,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl
            } );
            flag4++;
          }
          if(detail.attaName == '驾驶证'){
          // if(detail.attaType == 'mq1'){  
            this.fileList5.push( {
              uid: flag5,
              name: detail.attaName,
              status: 'done',
              url: detail.attaUrl
            } );
            flag5++;
          }
          if(detail.attaType == 'jszandxsz'){
            // if(detail.attaType == 'mq1'){  
              this.fileList5.push( {
                uid: flag5,
                name: detail.attaName,
                status: 'done',
                url: detail.attaUrl
              } );
              flag5++;
            }
          if(detail.attaName == '行驶证'){
            // if(detail.attaType == 'mq1'){  
              this.fileList5.push( {
                uid: flag5,
                name: detail.attaName,
                status: 'done',
                url: detail.attaUrl
              } );
              flag5++;
            }
            if(detail.attaType == 'GPS'){
              // if(detail.attaType == 'mq1'){  
                this.fileList6.push( {
                  uid: flag6,
                  name: detail.attaName,
                  status: 'done',
                  url: detail.attaUrl
                } );
                flag6++;
              }
        }
      };
    })
  }


  beforeUpload = (file: File) => {
      const isJPG = file.type === 'image/jpeg';
      const isPNG = file.type === 'image/png';
      if (!isJPG && !isPNG) {
          this.msg.warning(`不支持的格式: ${file.type}`);
      }
      const isLt2M = file.size / 1024 / 1024 < 10;
      if (!isLt2M) {
          this.msg.warning('图片不能超过2MB!');
      }
      return (isJPG || isPNG) && isLt2M;
  }

  private getBase64(img: File, callback: (img: any) => void) {
      if (img) {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
      }
  }



  formatterYuan = value => `¥ ${value}`;
  parserYuan = value => value.replace('¥', '');

  formatterDollor = value => `${value}`;
  parserDollor = value => value.replace('$', '');

  back() {
    this.router.navigate([ 
      "/prod/product/5/"
    ]);
  }




  savest(orderOper) {
    // 调用接口
    if(this.product.spcomment===''||this.product.spcomment===null){
      this.msg.warning("请输入意见");
      return;
    }
    // product.spcomment
    let fxjOrderLogList = [];
    if(this.product.spcomment!=''){
      // console.log("this.product.dypname*********"+this.product.dypname);

      let dto ={
        orderNo:this.product.orderNo,
        logUser:this.setting.user.username,
        logContent:this.product.spcomment
      }
      fxjOrderLogList.push(dto);
    }
    console.log('112')

    let fxjOrderMainPage = {
      updateBy:this.setting.user.username,
      id:this.product.id,
      orderNo:this.product.orderNo,
      orderOper:'S',
      bmDate: this.product.bmDate,
      bmTime: this.product.bmTime,
      bxRq: this.product.bxRq,
      carColor: this.product.carColor,
      carDetail: this.product.carDetail,
      carVin: this.product.carVin,
      conJson: this.product.conJson,
      conNo: this.product.conNo,
      conType: this.product.conType,
      conUrl: this.product.conUrl,
      createSysorg: this.product.createSysorg,
      createUser: this.product.createUser,
      cusAge: this.product.cusAge,
      cusBirthday: this.product.cusBirthday,
      cusDeu: this.product.cusDeu,
      cusEthnical: this.product.cusEthnical,
      cusGender: this.product.cusGender,
      cusIdValidfrom: this.product.cusIdValidfrom,
      cusIdValidto: this.product.cusIdValidto,
      cusIncard: this.product.cusIncard,
      cusMaritalStatus: this.product.cusMaritalStatus,
      cusMobile: this.product.cusMobile,
      cusName: this.product.cusName,
      cusQqid: this.product.cusQqid,
      orderBy18:this.product.orderBy18,
      orderBy60:this.product.orderBy60,
      orderBy61:this.product.orderBy61,
      orderBy62:this.product.orderBy62,
      orderBy63:this.product.orderBy63,
      orderBy64:this.product.orderBy64,
      orderBy65:this.product.orderBy65,
      orderBy66:this.product.orderBy66,
      orderBy67:this.product.orderBy67,
      orderBy68:this.product.orderBy68,
      orderBy69:this.product.orderBy69,
      orderBy70:this.product.orderBy70,
      orderBy71:this.product.orderBy71,
      orderBy72:this.product.orderBy72,
      orderBy73:this.product.orderBy73,
      orderBy86:this.product.orderBy86,

      fxjOrderLogList:JSON.parse(JSON.stringify(fxjOrderLogList))

    };
    console.log(fxjOrderMainPage)
    this.productService.editPro(fxjOrderMainPage).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      console.log("data.ok"+data.ok);
      if (data.ok === true) {
        this.back();
        this.msg.success('保存成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error('保存失败');
      }
    })
  }



  changeColor(res) {
    this.product.colorSort = res.color.hex;
  }




  handleChange1(info: { file: UploadFile }) {


    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl1 = img;
        console.log("info1*********"+JSON.stringify(info.file.response[0].piccontent));

        var sfxx=JSON.parse(info.file.response[0].piccontent)

        this.product.cusName=sfxx.result.cardsinfo[0].items[1].content;
        this.product.cusGender=sfxx.result.cardsinfo[0].items[2].content;
        this.product.cusEthnical=sfxx.result.cardsinfo[0].items[3].content;
        this.product.cusBirthday=sfxx.result.cardsinfo[0].items[4].content;
        this.product.cusResAddr=sfxx.result.cardsinfo[0].items[5].content;
        this.product.cusIncard=sfxx.result.cardsinfo[0].items[6].content;
        info.file=null;
      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange2(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }


    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl2 = img;


        var sfxx=JSON.parse(info.file.response[0].piccontent)

        console.log("varfrom*********"+JSON.stringify(sfxx));
        this.product.cusIdValidfrom= sfxx.result.cardsinfo[0].items[3].content;
        this.product.cusIdValidto=sfxx.result.cardsinfo[0].items[4].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange3(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl3 = img;

        var sfxx=JSON.parse(info.file.response[0].piccontent)

        this.product.dyptype = sfxx.result.cardsinfo[0].items[5].content +"/"+  sfxx.result.cardsinfo[0].items[2].content;

        this.product.dypname = sfxx.result.cardsinfo[0].items[1].content;
        this.product.dypvalue = sfxx.result.cardsinfo[0].items[3].content;
        this.product.dypyear =  sfxx.result.cardsinfo[0].items[8].content + "/"+sfxx.result.cardsinfo[0].items[9].content;
        this.product.dypclass =  sfxx.result.cardsinfo[0].items[10].content;

        this.product.dypremark1 = sfxx.result.cardsinfo[0].items[4].content;
        this.product.dypremark2 = sfxx.result.cardsinfo[0].items[6].content;
        this.product.dypremark3   = sfxx.result.cardsinfo[0].items[7].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange4(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl4 = img;
        var sfxx=JSON.parse(info.file.response[0].piccontent)

        console.log("varfrom*********"+JSON.stringify(sfxx));
        this.product.emergencyContact=sfxx.result.cardsinfo[0].items[1].content;

        this.product.emergencyResAddr=sfxx.result.cardsinfo[0].items[5].content;
        this.product.emergencyOffice=sfxx.result.cardsinfo[0].items[6].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange5(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl5 = img;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }

  handlePreview = (file: UploadFile) => {
    console.log("1")
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;

    // this.previewImage1 = file.url || file.thumbUrl;
    // this.previewVisible1 = true;

    // this.previewImage2 = file.url || file.thumbUrl;
    // this.previewVisible2 = true;

    // this.previewImage3 = file.url || file.thumbUrl;
    // this.previewVisible3 = true;

    // this.previewImage4 = file.url || file.thumbUrl;
    // this.previewVisible4 = true;

    // this.previewImage5 = file.url || file.thumbUrl;
    // this.previewVisible5 = true;
  }
  handlePreview1 = (file: UploadFile) => {
    console.log("1");
    console.log(file)
    this.previewImage1 = file.url || file.thumbUrl;
    this.previewVisible1 = true;
    console.log(this.previewImage1);
  }
  handlePreview2 = (file: UploadFile) => {
    console.log("2");
    console.log(file)
    this.previewImage2 = file.url || file.thumbUrl;
    this.previewVisible2 = true;
    console.log(this.previewImage2);
  }
  handlePreview3 = (file: UploadFile) => {
    console.log("3");
    console.log(file)
    this.previewImage3 = file.url || file.thumbUrl;
    this.previewVisible3 = true;
    console.log(this.previewImage3);
  }
  handlePreview4 = (file: UploadFile) => {
    console.log("4");
    console.log(file)
    this.previewImage4 = file.url || file.thumbUrl;
    this.previewVisible4 = true;
    console.log(this.previewImage4);
  }
  handlePreview5 = (file: UploadFile) => {
    console.log("5");
    console.log(file)
    this.previewImage5 = file.url || file.thumbUrl;
    this.previewVisible5 = true;
    console.log(this.previewImage5);
  }
  handlePreview6 = (file: UploadFile) => {
    console.log("6");
    console.log(file)
    this.previewImage6 = file.url || file.thumbUrl;
    this.previewVisible6 = true;
    console.log(this.previewImage6);
  }
}
