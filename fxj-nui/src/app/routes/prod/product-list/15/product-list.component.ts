import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import {SimpleTableButton, SimpleTableColumn, SimpleTableComponent} from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { ProductService } from '../../../../service/prod/product.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';


@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.less']
})
export class ProdProductList15Component implements OnInit, OnDestroy {
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    fxjOut01:"",
    fxjOut14:"",
    fxjOut07:"",
    // cusName: '',
    // cusMobile: '',
    // cusIncard: '',
    // proNo: '',
    // orderNo:'',
    // orderBy35:''
  };

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.productService.listyq();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';
  proNos = [];

  years = [];
  userGroups = [];
  brands = [];
  isAds = [{ text: '广告款', value: this.cnt.BoolUtil.TRUE}, { text: '普通款', value: this.cnt.BoolUtil.FALSE }];
  isCms = [{ text: '是', value: this.cnt.BoolUtil.TRUE}, { text: '否', value: this.cnt.BoolUtil.FALSE }];
  seasons = [];
  checkedList = [];
  data = [];
  bpmStatus = [];


  uploadExcelUrl = "";
  uploadExcelUrlfk = "";


  columns: SimpleTableColumn[] = [

    {
      title: '操作',
      buttons: [
        // {
        //   text: '审批',
        //   type: 'link',
        //   click: (record: any) => this.router.navigate([record.wbUrl+record.id]),
        //   iif: (item: any, btn: SimpleTableButton, column: SimpleTableColumn) => {
        //     return item.bpmStatus === '1'
        //   }
        // },
        {
          text: '查看',
          type: 'link',
          click: (record: any) => this.router.navigate(['/prod/product/yqview/'+record.id])
        }                                                                                                                                                                                                                                                                                                                          
      ]
    } ,
    // { title: '', index: 'key', type: 'checkbox',exported:false },
    { title: '借款人', index: 'fxjOut01',width:500 },
    { title: '贷款本金', index: 'fxjOut02' },
    { title: '利率', index: 'fxjOut03' },
    { title: '逾期金额', index: 'fxjOut04' },
    { title: '本金余额 ', index: 'fxjOut05'},
    { title: '逾期天数 ', index: 'fxjOut06' },
    { title: '渠道', index: 'fxjOut07' },
    { title: '是否已回收 ', index: 'fxjOut08' },
    { title: '车辆销售出租情况', index: 'fxjOut09' },
    { title: '二次租售客户名字', index: 'fxjOut10' },
    { title: '二次租售客户缴费情况', index: 'fxjOut11' },
    { title: '60天内代偿金额', index: 'fxjOut12' },
    { title: '30天内代偿金额 ', index: 'fxjOut13' },
    { title: '更新人员 ', index: 'updateBy' },
  ];

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private productService: ProductService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  inter
  ngOnInit() {
    this.uploadExcelUrl = this.productService.uploadyq();
    this.uploadExcelUrlfk = this.productService.uploadfk();

    this.imgSuffix = this.comm.getGoodsUrl();
    this.loadproNos();
    this.loadbpmStatus();

    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })

  }

  // 加载证件类型
  loadbpmStatus() {
    this.bpmStatus = [];
    this.comm.getType("bpm_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.bpmStatus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
// 加载产品
  loadproNos() {
    this.proNos = [];
    this.comm.getType("proNo").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.proNos.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }



  change(args: NzTabChangeEvent) {
        switch (args.index) {
          case 0:
            this.q.bpmStatus = "";
            break;
          case 1:
            this.q.bpmStatus = 1;
            break;
          case 2:
            this.q.bpmStatus = 10;
            break;
          case 3:
            this.q.bpmStatus = 20;
            break;

          case 4:
            this.q.bpmStatus = 30;
            break;
          case 5:
            this.q.bpmStatus = 40;
            break;
          case 6:
            this.q.bpmStatus = 50;
            break;
          case 7:
            this.q.bpmStatus = 60;
            break;
          case 8:
            this.q.bpmStatus = 70;
            break;
          case 9:
            this.q.bpmStatus = 80;
            break;
          case 10:
            this.q.bpmStatus = 90;
            break;
          case 11:
            this.q.bpmStatus = 99;
            break;
          case 12:
            this.q.bpmStatus = 100;
            break;
          default:
            break;
        }
      this.st.load(1, this.q);
  }

  showImage(src) {
    this.previewImage = src;
    this.previewVisible = true;
  }

  checkboxChange(list: any[]) {
    this.checkedList = list;
  }

  gotoAdd() {
    setTimeout(() => {
      this.router.navigate(['/prod/product/add']);
    }, 500);
  }

  gotoImg() {
    this.router.navigate(['/prod/product/upload']);
  }



  selectProd: {[propName: string]: any;} = {}
  selectUrl = null;

  view(record) {
    this.productService.getProduct(record.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.selectProd = data.data;
      // 拼接图片地址
      this.selectUrl = this.comm.getGoodsUrl() + this.selectProd.img;
      this.visible = true;
    })

  }

  close(): void {
    this.visible = false;
  }



  cancel() {
    this.repdate = null;
  }
  repdate = null;



  uploadProd(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
        this.loading = true;
    }
    if (info.file.status === 'done') {
        this.loading = false;
        this.notice.create('success', 'Success', info.file.response.data['successTip']);
        if (undefined !== info.file.response.data['failTip']) {
          this.notice.create('info', 'Info', info.file.response.data['failTip']);
        }
        this.st.load();
    }
    if (info.file.status === 'error') {
        this.loading = false;
      this.notice.create('error', 'Error', info.file.response.msg);
    }
  }


  exportPop() {
    if (this.checkedList.length > 0) {
      console.log("1*******");
      this.st.export(this.checkedList, { filename: '订单.xlsx' });
    } else {
      let param = `?pageNumber=1&pageSize=99999`;
      console.log("this.url + param*******"+this.url + param);

      // if (this.st.extraParams.sku) {
      //   param += '&sku=' + this.st.extraParams.sku;
      // }
      // if (this.st.extraParams.sap) {
      //   param += '&sap=' + this.st.extraParams.sap;
      // }
      // if (this.st.extraParams.brand) {
      //   param += '&brand=' + this.st.extraParams.brand;
      // }
      this.st.export(this.url + param, { filename: '订单.xlsx' });
    }
  }


}
