import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import {SimpleTableButton, SimpleTableColumn, SimpleTableComponent} from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { ProductService } from '../../../../service/prod/product.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.less']
})
export class ProdProductList5Component implements OnInit, OnDestroy {
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    bpmStatus:"",
    cusName: '',
    cusMobile: '',
    cusIncard: '',
    proNo: '',
    orderNo:'',
    orderBy86:''

  };
  orderBy35 =  [{text: '厦门分公司', value: '厦门'}, {text: '泉州分公司', value: '泉州'} ,{text: '江西分公司', value: '江西'}, {text: '福州分公司', value: '福州'}, {text: '南昌分公司', value: '南昌'}, {text: '合肥分公司', value: '合肥'} , {text: '广州分公司', value: '广州'}, {text: '武汉分公司', value: '武汉'} ];

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.productService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';
  proNos = [];

  years = [];
  userGroups = [];
  brands = [];
  isAds = [{ text: '广告款', value: this.cnt.BoolUtil.TRUE}, { text: '普通款', value: this.cnt.BoolUtil.FALSE }];
  isCms = [{ text: '是', value: this.cnt.BoolUtil.TRUE}, { text: '否', value: this.cnt.BoolUtil.FALSE }];
  seasons = [];
  checkedList = [];
  data = [];
  bpmStatus = [];


  uploadExcelUrl = "";


  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox',exported:false },
    {
      title: '操作',
      buttons: [
        // {
        //   text: '审批',
        //   type: 'link',
        //   click: (record: any) => this.router.navigate(['/prod/product/5/'+record.id]),
        //   iif: (item: any, btn: SimpleTableButton, column: SimpleTableColumn) => {
        //     return item.bpmStatus === '1'
        //   }
        // },
        {
          text: '查看',
          type: 'link',
          click: (record: any) => this.router.navigate(['/prod/product/viewtwo/'+record.id])
        },
        {
          text: '档案',
          type: 'link',
          click: (record: any) => this.router.navigate(['/prod/product/viewone/'+record.id])
        },
        {
          text: '合同生成',
          type: 'link',
          click: (record: any) => this.router.navigate(['/prod/product/fico/'+record.id])
        }
      ]
    } ,
    { title: '订单编号', index: 'orderNo',width:500 },
    { title: '合同编号', index: 'conNo' },
    { title: '履约情况', index: 'orderBy86' },
    { title: '档案编号', index: 'orderBy85' },
    { title: '订单状态', index: 'orderStatus' },
    { title: '产品', index: 'proClass' },
    { title: '客户姓名 ', index: 'cusName'},
    { title: '身份证号 ', index: 'cusIncard' },
    { title: '联系电话', index: 'cusMobile' },
    { title: '联系地址 ', index: 'cusResAddr' },
    { title: '车身颜色', index: 'orderBy37' },
    { title: '合同金额 ', index: 'orderBy48' },
    { title: '车牌 ', index: 'orderCarno' },
    { title: '品牌型号 ', index: 'orderCartype' },
    { title: '发动机号 ', index: 'orderBy63' },
    { title: '车架号 ', index: 'carVin' },
    { title: '合同起止时间 ', index: 'orderBy65' },
    { title: '车辆初次登记日期 ', index: 'orderBy66' },
    { title: '年检期 ', index: 'orderBy67' },
    { title: '续保月份 ', index: 'orderBy68' },
    { title: '交强险起止日期 ', index: 'orderBy69' },
    { title: '商业险起止日期 ', index: 'orderBy70' },
    { title: '申请期数 ', index: 'orderPeriod' },
    { title: '录入业务员  ' , index: 'createBy' },
    { title: '订单提交时间', index: 'createDate' },
    { title: '审批人员', index: 'updateBy' }
  ];

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private productService: ProductService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  inter
  ngOnInit() {
    this.uploadExcelUrl = this.productService.upload();

    this.imgSuffix = this.comm.getGoodsUrl();
    this.loadproNos();
    this.loadbpmStatus();

    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })

  }

  // 加载证件类型
  loadbpmStatus() {
    this.bpmStatus = [];
    this.comm.getType("bpm_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.bpmStatus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
// 加载产品
  loadproNos() {
    this.proNos = [];
    this.comm.getType("proNo").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.proNos.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }



  change(args: NzTabChangeEvent) {
        switch (args.index) {
          case 0:
            this.q.bpmStatus = "";
            break;
          case 1:
            this.q.bpmStatus = 1;
            break;
          case 2:
            this.q.bpmStatus = 10;
            break;
          case 3:
            this.q.bpmStatus = 20;
            break;

          case 4:
            this.q.bpmStatus = 30;
            break;
          case 5:
            this.q.bpmStatus = 40;
            break;
          case 6:
            this.q.bpmStatus = 50;
            break;
          case 7:
            this.q.bpmStatus = 60;
            break;
          case 8:
            this.q.bpmStatus = 70;
            break;
          case 9:
            this.q.bpmStatus = 80;
            break;
          case 10:
            this.q.bpmStatus = 90;
            break;
          case 11:
            this.q.bpmStatus = 99;
            break;
          case 12:
            this.q.bpmStatus = 100;
            break;
          default:
            break;
        }
      this.st.load(1, this.q);
  }

  showImage(src) {
    this.previewImage = src;
    this.previewVisible = true;
  }

  checkboxChange(list: any[]) {
    this.checkedList = list;
  }

  gotoAdd() {
    setTimeout(() => {
      this.router.navigate(['/prod/product/add']);
    }, 500);
  }

  gotoImg() {
    this.router.navigate(['/prod/product/upload']);
  }



  selectProd: {[propName: string]: any;} = {}
  selectUrl = null;

  view(record) {
    this.productService.getProduct(record.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.selectProd = data.data;
      // 拼接图片地址
      this.selectUrl = this.comm.getGoodsUrl() + this.selectProd.img;
      this.visible = true;
    })

  }

  close(): void {
    this.visible = false;
  }



  cancel() {
    this.repdate = null;
  }
  repdate = null;



  uploadProd(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
        this.loading = true;
    }
    if (info.file.status === 'done') {
        this.loading = false;
        this.notice.create('success', 'Success', info.file.response.data['successTip']);
        if (undefined !== info.file.response.data['failTip']) {
          this.notice.create('info', 'Info', info.file.response.data['failTip']);
        }
        this.st.load();
    }
    if (info.file.status === 'error') {
        this.loading = false;
      this.notice.create('error', 'Error', info.file.response.msg);
    }
  }


  exportPop() {
    if (this.checkedList.length > 0) {
      console.log("1*******");
      this.st.export(this.checkedList, { filename: '订单.xlsx' });
    } else {
      let param = `?pageNumber=1&pageSize=99999`;
      console.log("this.url + param*******"+this.url + param);

      // if (this.st.extraParams.sku) {
      //   param += '&sku=' + this.st.extraParams.sku;
      // }
      // if (this.st.extraParams.sap) {
      //   param += '&sap=' + this.st.extraParams.sap;
      // }
      // if (this.st.extraParams.brand) {
      //   param += '&brand=' + this.st.extraParams.brand;
      // }
      this.st.export(this.url + param, { filename: '订单.xlsx' });
    }
  }


}
