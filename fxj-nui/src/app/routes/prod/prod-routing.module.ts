import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProdProductListComponent } from './product-list/product-list.component';
import { ProdProductList5Component } from './product-list/5/product-list.component';
import { ProdProductList10Component } from './product-list/10/product-list.component';
import { ProdProductList15Component } from './product-list/15/product-list.component';

import { AuthGuardService } from '@core/auth/auth-guard.service';
import { ProductAddComponent } from 'app/routes/prod/product-list/edit/product-add.component';
import { ProductViewEditComponent } from 'app/routes/prod/product-list/edit/product-edit.component';
import { GoodsImgUploadComponent } from './goods-upload/goods-img-upload.component';
import {ProductmqComponent} from "./product-list/edit/product-mq.component";
import {ProductmqspComponent} from "./product-list/edit/product-mqsp.component";
import {ProductrmComponent} from "./product-list/edit/product-rm.component";
import {Productrm1Component} from "./product-list/edit/product-rm1.component";
import {ProdProductList20Component} from "./product-list/20/product-list.component";
import {ProdProductList25Component} from "./product-list/25/product-list.component";
import {ProdProductList30Component} from "./product-list/30/product-list.component";
import {ProdProductList40Component} from "./product-list/40/product-list.component";
import {ProdProductList50Component} from "./product-list/50/product-list.component";
import {ProdProductList60Component} from "./product-list/60/product-list.component";
import {ProdProductList70Component} from "./product-list/70/product-list.component";
import {ProdProductList80Component} from "./product-list/80/product-list.component";
import {ProdProductList90Component} from "./product-list/90/product-list.component";
import {ProdProductList99Component} from "./product-list/99/product-list.component";
import {ProdProductList100Component} from "./product-list/100/product-list.component";
import {ProdProductList35Component} from "./product-list/35/product-list.component";
import {ProductfkComponent} from "./product-list/edit/product-fk.component";
import {ProdProductListwfpComponent} from "./product-list/wfp/product-list.component";
import {ProdProductListyfpComponent} from "./product-list/yfp/product-list.component";
import {ProductfpComponent} from "./product-list/edit/product-fp.component";
import {ProductficoComponent} from "./product-list/edit/product-fico.component";  
import {ProdProductList1Component} from "./product-list/1/product-list.component";
import {ProductviewComponent} from "./product-list/edit/product-view.component";

import {ProductyqviewComponent} from "./product-list/edit/product-yqview.component";
import {ProductviewOneComponent} from "./product-list/edit/product-viewone.component";

import {ProductviewTwoComponent} from "./product-list/edit/product-viewtwo.component";
import {ProductcwcheckComponent} from "./product-list/edit/product-cwcheck.component";

import {ProdProductList85Component} from "./product-list/85/product-list.component";
import {ProductyhmqComponent} from "./product-list/edit/product-yhmq.component";
import {Productfk2Component} from "./product-list/edit/product-fk2.component";
import {ProductnrmComponent} from "./product-list/edit/product-nrm.component";
import {Productfk3Component} from "./product-list/edit/product-fk3.component";
import {Productfk4Component} from "./product-list/edit/product-fk4.component";
import {Productfk5Component} from "./product-list/edit/product-fk5.component";
import {ProductreplanComponent} from "./product-list/edit/product-replan.component";
import {ProductfkhxComponent} from "./product-list/edit/product-fkhx.component";
import {ProductrmviewComponent} from "./product-list/edit/product-rmview.component";
import {ProductdhviewComponent} from "./product-list/edit/product-dhview.component";

const routes: Routes = [
  {
    path: 'product',
    children: [
      { path: '', component: ProdProductListComponent },
      { path: '5', component: ProdProductList5Component },
      { path: '1', component: ProdProductList1Component },
      { path: '10', component: ProdProductList10Component },
      { path: '15', component: ProdProductList15Component },

      { path: '20', component: ProdProductList20Component },
      { path: '25', component: ProdProductList25Component },
      { path: 'wfp', component: ProdProductListwfpComponent },

      { path: 'yfp', component: ProdProductListyfpComponent },

      { path: '30', component: ProdProductList30Component },
      { path: '35', component: ProdProductList35Component },

      { path: '40', component: ProdProductList40Component },
      { path: '50', component: ProdProductList50Component },
      { path: '60', component: ProdProductList60Component },
      { path: '70', component: ProdProductList70Component },
      { path: '80', component: ProdProductList80Component },
      { path: '85', component: ProdProductList85Component },

      { path: '90', component: ProdProductList90Component },
      { path: '99', component: ProdProductList99Component },
      { path: '100', component: ProdProductList100Component },

      { path: 'add', component: ProductAddComponent },

      { path: 'mqsp/:id', component: ProductmqspComponent },
      { path: 'rm/:id', component: ProductrmComponent },
      { path: 'rm1/:id', component: Productrm1Component },
      { path: 'nrm/:id', component: ProductnrmComponent },

      { path: 'fk/:id', component: ProductfkComponent },
      { path: 'fkhx/:id', component: ProductfkhxComponent },

      { path: 'replan/:id', component: ProductreplanComponent },

      { path: 'fk2/:id', component: Productfk2Component },
      { path: 'fk3/:id', component: Productfk3Component },
      { path: 'fk4/:id', component: Productfk4Component },
      { path: 'fk5/:id', component: Productfk5Component },


      { path: 'fp/:id', component: ProductfpComponent },
      { path: 'fico/:id', component: ProductficoComponent },

      { path: 'mq/:id', component: ProductmqComponent },
      { path: 'yhmq/:id', component: ProductyhmqComponent },

      { path: 'edit/:id', component: ProductViewEditComponent },
      { path: 'view/:id', component: ProductviewComponent },
      { path: 'viewone/:id', component: ProductviewOneComponent },
      { path: 'yqview/:id', component: ProductyqviewComponent },

      { path: 'viewtwo/:id', component: ProductviewTwoComponent },
      { path: 'cwcheck/:id', component: ProductcwcheckComponent },

      { path: 'rmview/:id', component: ProductrmviewComponent },
      { path: 'dhview/:id', component: ProductdhviewComponent },

      { path: 'upload', component: GoodsImgUploadComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProdRoutingModule { }
