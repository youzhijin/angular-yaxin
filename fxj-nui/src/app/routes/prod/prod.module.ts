import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { ProdRoutingModule } from './prod-routing.module';
import { ProdProductListComponent } from './product-list/product-list.component';
import { ProdProductList5Component } from './product-list/5/product-list.component';
import { ProdProductList15Component } from './product-list/15/product-list.component';

import { ProductAddComponent } from './product-list/edit/product-add.component';
import { ProductViewEditComponent } from './product-list/edit/product-edit.component';
import { GoodsImgUploadComponent } from './goods-upload/goods-img-upload.component';
import {ProductmqComponent} from "./product-list/edit/product-mq.component";
import {ProdProductList10Component} from "./product-list/10/product-list.component";
import {ProductmqspComponent} from "./product-list/edit/product-mqsp.component";
import {ProductrmComponent} from "./product-list/edit/product-rm.component";
import {Productrm1Component} from "./product-list/edit/product-rm1.component";
import {ProdProductList20Component} from "./product-list/20/product-list.component";
import {ProdProductList25Component} from "./product-list/25/product-list.component";
import {ProdProductList30Component} from "./product-list/30/product-list.component";
import {ProdProductList40Component} from "./product-list/40/product-list.component";
import {ProdProductList50Component} from "./product-list/50/product-list.component";
import {ProdProductList60Component} from "./product-list/60/product-list.component";
import {ProdProductList70Component} from "./product-list/70/product-list.component";
import {ProdProductList80Component} from "./product-list/80/product-list.component";
import {ProdProductList90Component} from "./product-list/90/product-list.component";
import {ProdProductList99Component} from "./product-list/99/product-list.component";
import {ProdProductList100Component} from "./product-list/100/product-list.component";
import {ProdProductList35Component} from "./product-list/35/product-list.component";
import {ProductfkComponent} from "./product-list/edit/product-fk.component";
import {ProdProductListwfpComponent} from "./product-list/wfp/product-list.component";
import {ProdProductListyfpComponent} from "./product-list/yfp/product-list.component";
import {ProductfpComponent} from "./product-list/edit/product-fp.component";
import {ProductficoComponent} from "./product-list/edit/product-fico.component";
import {ProdProductList1Component} from "./product-list/1/product-list.component";
import {ProductviewComponent} from "./product-list/edit/product-view.component";
import {ProductviewOneComponent} from "./product-list/edit/product-viewone.component";
import {ProductyqviewComponent} from "./product-list/edit/product-yqview.component";

import {ProductviewTwoComponent} from "./product-list/edit/product-viewtwo.component";
import {ProductcwcheckComponent} from "./product-list/edit/product-cwcheck.component";

import {ProdProductList85Component} from "./product-list/85/product-list.component";
import {ProductyhmqComponent} from "./product-list/edit/product-yhmq.component";
import {Productfk2Component} from "./product-list/edit/product-fk2.component";
import {ProductnrmComponent} from "./product-list/edit/product-nrm.component";
import {Productfk3Component} from "./product-list/edit/product-fk3.component";
import {Productfk4Component} from "./product-list/edit/product-fk4.component";
import {Productfk5Component} from "./product-list/edit/product-fk5.component";
import {ProductreplanComponent} from "./product-list/edit/product-replan.component";
import {ProductfkhxComponent} from "./product-list/edit/product-fkhx.component";
import {ProductrmviewComponent} from "./product-list/edit/product-rmview.component";
import {ProductdhviewComponent} from "./product-list/edit/product-dhview.component";
// import {OrderManComponent} from "../orderMan/list/orderMan";

const COMPONENTS = [
  ProdProductList1Component,
  ProdProductList10Component,
  ProdProductList20Component,
  ProdProductList25Component,
  ProdProductListwfpComponent,
  ProdProductListyfpComponent,

  ProdProductList30Component,
  ProdProductList35Component,
  ProdProductList40Component,
  ProdProductList50Component,
  ProdProductList60Component,
  ProdProductList70Component,
  ProdProductList80Component,
  ProdProductList85Component,
  ProdProductList90Component,
  ProdProductList99Component,
  ProdProductList100Component,
  ProdProductListComponent,
  ProdProductList5Component,
  ProdProductList15Component,

  GoodsImgUploadComponent
];
const COMPONENTS_NOROUNT = [
  ProductAddComponent,
  ProductmqComponent,
  ProductmqspComponent,
  ProductyhmqComponent,

  ProductrmComponent,
  Productrm1Component,
  ProductnrmComponent,
  ProductfkComponent,
  ProductfkhxComponent,
  ProductreplanComponent,
  Productfk2Component,
  Productfk3Component,
  Productfk4Component,
  Productfk5Component,
  ProductfpComponent,
  ProductficoComponent,
  ProductviewComponent,
  ProductviewOneComponent,
  ProductyqviewComponent,

  ProductviewTwoComponent,
  ProductcwcheckComponent,

  ProductrmviewComponent,
  ProductdhviewComponent,
  ProductViewEditComponent
];

@NgModule({
  imports: [
    SharedModule,
    ProdRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class ProdModule { }
