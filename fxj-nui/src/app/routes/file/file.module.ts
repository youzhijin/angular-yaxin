import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import {fileRoutingModule} from "./file-routing.module";
import {fileAddComponent} from "./file-list/edit/file-add.component";
import {fileViewEditComponent} from "./file-list/edit/file-edit.component";
import {fileListComponent} from "./file-list/file-list.component";
import {fileListOneComponent} from "./file-list/file-listone.component";
import {fileViewEdittoComponent} from "./file-list/edit/file-editto.component";

const COMPONENTS = [
  fileListComponent,
  fileListOneComponent
];
const COMPONENTS_NOROUNT = [
  fileAddComponent,
  fileViewEditComponent,
  fileViewEdittoComponent
];

@NgModule({
  imports: [
    SharedModule,
    fileRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class fileModule {

}
