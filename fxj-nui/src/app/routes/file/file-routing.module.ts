import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import {fileListComponent} from "./file-list/file-list.component";
import {fileListOneComponent} from "./file-list/file-listone.component";
import {fileViewEditComponent} from "./file-list/edit/file-edit.component";
import {fileAddComponent} from "./file-list/edit/file-add.component";
import {fileViewEdittoComponent} from "./file-list/edit/file-editto.component";

const routes: Routes = [
  {
    path: 'file',
    children: [
      { path: '', component: fileListComponent },
      { path: 'add', component: fileAddComponent },
      { path: 'edit/:id', component: fileViewEditComponent },
      { path: 'editto/:id', component: fileViewEdittoComponent }
    ]
  },{
    path: 'fileone',
    children: [
      { path: '', component: fileListOneComponent },
      // { path: 'add', component: fileAddComponent },
      // { path: 'edit/:id', component: fileViewEditComponent },
      // { path: 'editto/:id', component: fileViewEdittoComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class fileRoutingModule { }
