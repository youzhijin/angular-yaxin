import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { maService } from '../../../service/ma/ma.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';
import {fileService} from "../../../service/file/file.service";




@Component({
  selector: 'file-listone',
  templateUrl: './file-listone.component.html',
  styleUrls: ['./file-listone.component.less']
})
export class fileListOneComponent implements OnInit, OnDestroy {
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    bpmStatus:"",

    fileName: '',
    fileRemark: ''
  };

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.fileService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  seasons = [];
  checkedList = [];
  data = [];

  uploadUrl = this.fileService.uploadUrl();




  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '文件编号', index: 'fileNo' },
    { title: '文件名', index: 'fileName'},
    { title: '状态', index: 'fileStatus' },
    { title: '上传者', index: 'fileOwner' },
    { title: '当前拥有者' , index: 'fileNowOwner' },
    { title: '转出人', index: 'fileFrom' },
    // { title: '附件', index: 'fiAtta' },
    { title: '接收人', index: 'fileTo' },
    { title: '备注', index: 'fileRemark' },
    {
        title: '操作',
        buttons: [
            {
              text: '发出',
              type: 'link',
              click: (record: any) => this.router.navigate(['/file/file/edit/'+record.id])
            },
          {
            text: '接收',
            type: 'link',
            click: (record: any) => this.router.navigate(['/file/file/editto/'+record.id])
          }
        ]
    }
  ];

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private fileService: fileService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  inter
  ngOnInit() {
    this.imgSuffix = this.comm.getGoodsUrl();

    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })


  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }



  change(args: NzTabChangeEvent) {
        switch (args.index) {
          case 0:
            this.q.bpmStatus = "";
            break;
          case 1:
            this.q.bpmStatus = 10;
            break;
          case 2:
            this.q.bpmStatus = 20;
            break;

          default:
            break;
        }
      this.st.load(1, this.q);
  }



  checkboxChange(list: any[]) {
    this.checkedList = list;
  }
  gotoAdd() {
    setTimeout(() => {
      this.router.navigate(['/file/file/add']);
    }, 500);
  }

  close(): void {
    this.visible = false;
  }



  cancel() {
    this.repdate = null;
  }
  repdate = null;



}
