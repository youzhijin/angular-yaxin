import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { xsjkService } from '../../../service/xsjk/xsjk.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';

@Component({
  selector: 'xsjk-list',
  templateUrl: './xsjk-list.component.html',
  styleUrls: ['./xsjk-list.component.less']
})
export class xsjkListComponent implements OnInit, OnDestroy {
  // region 公共字段

  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    start: null,
    end: null,
    bpmStatus:"",
    // fiName: '',
    // cusMobile: '',
    // cusIncard: '',
    orderNo: ''
  };
  percent = 55;

  listOfData = [
    {
      provinces: '福建厦门',
      volume: '66',
      loan: '32',
      conversionRate: '80%',
      onYear:'上升1%'
    },
    {
      provinces: '福建厦门',
      volume: '66',
      loan: '32',
      conversionRate: '80%',
      onYear:'上升1%'
    },
    {
      provinces: '福建厦门',
      volume: '66',
      loan: '32',
      conversionRate: '80%',
      onYear:'上升1%'
    },
    {
      provinces: '福建厦门',
      volume: '66',
      loan: '32',
      conversionRate: '80%',
      onYear:'上升1%'
    },
    {
      provinces: '福建厦门',
      volume: '66',
      loan: '32',
      conversionRate: '80%',
      onYear:'上升1%'
    }
  ];

  loading = true;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.xsjkService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  data = [];

  // uploadUrl = this.xsjkService.uploadProds();
  uploadDateUrl = this.xsjkService.batchRepdate();

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private xsjkService: xsjkService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  inter
  ngOnInit() {
    this.imgSuffix = this.comm.getGoodsUrl();

    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })


  }


  activeTime$: any;

  activeData: any[];

  activeStat = {
    max: 1.23,
    min: 2.54,
    t1: 1.11,
    t2: 0.23,
  };

  refData() {
    const activeData = [];
    for (let i = 0; i < 24; i += 1) {
      activeData.push({
        x: `${i.toString().padStart(2, '0')}:00`,
        y: i * 50 + Math.floor(Math.random() * 200),
      });
    }
    this.activeData = activeData;
    // stat
    this.activeStat.max = [...activeData].sort()[activeData.length - 1].y + 200;
    this.activeStat.min = [...activeData].sort()[Math.floor(activeData.length / 2)].y;
    this.activeStat.t1 = activeData[Math.floor(activeData.length / 2)].x;
    this.activeStat.t2 = activeData[activeData.length - 1].x;
    // percent
    this.percent = Math.floor(Math.random() * 100);
    // this.cdr.detectChanges();
  }

  couponFormat(val: any) {
    switch (parseInt(val, 10)) {
      case 20:
        return '差';
      case 40:
        return '中';
      case 60:
        return '良';
      case 80:
        return '优';
      default:
        return '';
    }
  }
  


  


  



  



}
