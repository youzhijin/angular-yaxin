import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { xsjkRoutingModule } from './xsjk-routing.module';
import { xsjkListComponent } from './xsjk-list/xsjk-list.component';
import { xsjkViewEditComponent } from 'app/routes/xsjk/xsjk-list/edit/xsjk-edit.component';

const COMPONENTS = [
  xsjkListComponent
];
const COMPONENTS_NOROUNT = [
  xsjkViewEditComponent
];

@NgModule({
  imports: [
    SharedModule,
    xsjkRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class xsjkModule { }
