import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { xsjkListComponent } from './xsjk-list/xsjk-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { xsjkViewEditComponent } from 'app/routes/xsjk/xsjk-list/edit/xsjk-edit.component';

const routes: Routes = [
  {
    path: 'xsjk',
    children: [
      { path: '', component: xsjkListComponent },
      { path: 'edit/:id', component: xsjkViewEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class xsjkRoutingModule { }
