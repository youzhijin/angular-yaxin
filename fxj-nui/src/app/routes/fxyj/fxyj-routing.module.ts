import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { fxyjListComponent } from './fxyj-list/fxyj-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { fxyjViewEditComponent } from 'app/routes/fxyj/fxyj-list/edit/fxyj-edit.component';

const routes: Routes = [
  {
    path: 'fxyj',
    children: [
      { path: '', component: fxyjListComponent },
      { path: 'edit/:id', component: fxyjViewEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class fxyjRoutingModule { }
