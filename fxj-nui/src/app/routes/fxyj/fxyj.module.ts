import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { fxyjRoutingModule } from './fxyj-routing.module';
import { fxyjListComponent } from './fxyj-list/fxyj-list.component';
import { fxyjViewEditComponent } from 'app/routes/fxyj/fxyj-list/edit/fxyj-edit.component';

const COMPONENTS = [
  fxyjListComponent
];
const COMPONENTS_NOROUNT = [
  fxyjViewEditComponent
];

@NgModule({
  imports: [
    SharedModule,
    fxyjRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class fxyjModule { }
