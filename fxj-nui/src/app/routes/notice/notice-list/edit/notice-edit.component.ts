import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { noticeService } from '../../../../service/notice/notice.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';

@Component({
  selector: 'notice-view-edit',
  templateUrl: './notice-edit.component.html',
  styleUrls: ['./notice-edit.component.less']
})
export class noticeViewEditComponent implements OnInit, OnDestroy {
  product:  any = {
    bpmStatus: null,
    fxjOut01: null,
    fxjOut02: null,
    fxjOut03: null,
    fxjOut04: null,
    fxjOut05: null,
    fxjOut06: null,
    fxjOut07: null,
    fxjOut08: null,
    fxjOut09: null,
    fxjOut10: null,
    id: null,
    ntBegindate:null,
    ntContent: null,
    ntFreq: null,
    ntHead: null,
    ntMobile: null,
    ntName: null,
    ntNo: null,
    ntObjId: null,
    ntOpenid: null,
    ntStatus: null,
    ntType: null,
    ntUser: null,
    orderNo: null,

  };

  bpmStatus =  [{text: '未放款', value: '1'}, {text: '已放款', value: '10'}];
  loanReMethods =  [{text: '等额本金', value: '10'}, {text: '等额本息', value: '20'}, {text: '先息后本', value: '30'}];
  MAX_COUNT = 60;
  tabIndex = 0;
  saleing = false;


  uploadUr5 = '';
  avatarParam5 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  fileList = [ ];
  // 预览图片对象
  previewImage = '';
  // 是否可以预览
  previewVisible = false;

  private sub: any;

  constructor(
    public setting: SettingsService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private noticeService: noticeService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService
  ) {}

  ngOnInit(): void {

    // this.loadbpmStatus();


    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"]
      console.log("this.product.id****************"+this.product.id);
      // 获取商品详情
      this.getProductInfo();
    });
    // 隐藏reusetab
    setTimeout(() => {
        this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);
    this.titleService.setTitle("编辑产品");

    this.uploadUr5 = this.noticeService.uploadUrl();

  }
  // 加载证件类型
  loadbpmStatus() {
    this.bpmStatus = [];
    this.comm.getType("bpm_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.bpmStatus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  changeTab(index) {
    // 使用异步的方式更新组件, 防止ngIf错误
    Promise.resolve(null).then(() => this.tabIndex = index);
  }



  getProductInfo() {
    this.noticeService.getProduct(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.product = data.data;
      this.product.loanDate="";
      this.avatarParam5 = { sbtype: 'No',  filetype:'mq',orderNo: this.product.orderNo};
      this.product.ntStatus="已处理";
    })
  }




  back() {
    this.router.navigate(['/notice/notice']);
  }


  save() {

    let fxjLoanNotice = {
      // updateBy:this.setting.user.username,
      // id:this.product.id,
      // bpmStatus:80,

      bpmStatus: null,
      fxjOut01: this.product.fxjOut01,
      fxjOut02: this.product.fxjOut02,
      fxjOut03: this.product.fxjOut03,
      fxjOut04: this.product.fxjOut04,
      fxjOut05: this.product.fxjOut05,
      fxjOut06: this.product.fxjOut06,
      fxjOut07: this.product.fxjOut07,
      fxjOut08: this.product.fxjOut08,
      fxjOut09: this.product.fxjOut09,
      fxjOut10: this.product.fxjOut10,
      id: this.product.id,
      ntBegindate:this.product.ntBegindate,
      ntContent: this.product.ntContent,
      ntFreq: this.product.ntFreq,
      ntHead: this.product.ntHead,
      ntMobile: this.product.ntMobile,
      ntName: this.product.ntName,
      ntNo: this.product.ntNo,
      ntObjId: this.product.ntObjId,
      ntOpenid: this.product.ntOpenid,
      ntStatus: this.product.ntStatus,
      ntType: this.product.ntType,
      ntUser: this.product.ntUser,
      orderNo: this.product.orderNo,


    };

    // 调用接口
    this.noticeService.editPro(fxjLoanNotice).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if (data.ok === true) {
        this.msg.success('修改成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
        this.back();
      } else {
        this.msg.error(data['message']);
      }
    })
  }

  changeColor(res) {
    this.product.colorSort = res.color.hex;
  }

  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  }
}
