import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import {ActivatedRoute, Router} from '@angular/router';
import { noticeService } from '../../../service/notice/notice.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';
import {formatDate} from "@angular/common";




@Component({
  selector: 'notice-list',
  templateUrl: './notice-list.component.html',
  styleUrls: ['./notice-list.component.less']
})
export class noticeListComponent implements OnInit, OnDestroy {
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    ntStatus:"1",
    ntType:"",
    orderNo: '',
    ntName: '',
    dateRange: null,
    startDate:'',
    endDate:''

    // cusIncard: '',
    // orderNo: ''
  };

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.noticeService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  seasons = [];
  checkedList = [];
  data = [];

  // uploadUrl = this.maService.uploadProds();
  uploadDateUrl = this.noticeService.batchRepdate();




  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '订单编号', index: 'orderNo' },
    { title: '提醒类型', index: 'ntType'},
    { title: '名称', index: 'ntName'},
    { title: '电话', index: 'ntMobile'},
    { title: '提醒编号', index: 'ntNo'},
    { title: '提醒标题', index: 'ntHead'},
    { title: '提醒内容', index: 'ntContent'},
    { title: '开始时间', index: 'ntBegindate'},
    { title: '频率', index: 'ntFreq'},
    { title: '提醒状态', index: 'ntStatus' },
    { title: '计划处理人', index: 'ntUser' },
    {
        title: '操作',
        buttons: [
            {
              text: '处理',
              type: 'link',
              click: (record: any) => this.router.navigate(['/notice/notice/edit/'+record.id])
            }
        ]
    }
  ];

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private noticeService: noticeService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
    private route: ActivatedRoute,

  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }
  private sub: any;

  inter
  ngOnInit() {
    this.imgSuffix = this.comm.getGoodsUrl();

    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      let varinpur = params["id"];
      if(varinpur){
        this.q.orderNo = params["id"];
      }
    });

    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })

    // 隐藏reusetab
    setTimeout(() => {
      this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);
  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }

  back() {
    this.router.navigate(['/dh/dh10']);
  }
  change(args: NzTabChangeEvent) {
    switch (args.index) {
      case 0:
        this.q.ntType = "";
        break;
      case 1:
        this.q.ntType = "路桥";
        break;
      case 2:
        this.q.ntType = "保险";
        break;
      case 3:
        this.q.ntType = "保养";
        break;
      case 4:
        this.q.ntType = "还款";
        break;

      default:
        break;
    }
  this.st.load(1, this.q);
}




  checkboxChange(list: any[]) {
    this.checkedList = list;
  }


  close(): void {
    this.visible = false;
  }



  cancel() {
    this.repdate = null;
  }
  repdate = null;

  dateChange(result: Date[]): void {
    if (result.length !== 0) {
      this.q.startDate = formatDate(result[0], 'yyyy-MM-dd', 'en-US');
      this.q.endDate = formatDate(result[1], 'yyyy-MM-dd', 'en-US');
    } else {
      this.q.startDate = '';
      this.q.endDate = '';
    }
  }

}
