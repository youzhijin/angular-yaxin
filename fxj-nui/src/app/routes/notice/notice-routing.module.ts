import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { noticeListComponent } from './notice-list/notice-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { noticeViewEditComponent } from 'app/routes/notice/notice-list/edit/notice-edit.component';

const routes: Routes = [
  {
    path: 'notice',
    children: [
      { path: '', component: noticeListComponent },
      { path: 'edit/:id', component: noticeViewEditComponent },
      { path: 'query/:id', component: noticeListComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class noticeRoutingModule { }
