import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { noticeRoutingModule } from './notice-routing.module';
import { noticeListComponent } from './notice-list/notice-list.component';
import { noticeViewEditComponent } from 'app/routes/notice/notice-list/edit/notice-edit.component';

const COMPONENTS = [
  noticeListComponent
];
const COMPONENTS_NOROUNT = [
  noticeViewEditComponent
];

@NgModule({
  imports: [
    SharedModule,
    noticeRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class noticeModule { }
