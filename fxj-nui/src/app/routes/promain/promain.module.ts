import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { promainRoutingModule } from './promain-routing.module';
import { promainListComponent } from './promain-list/promain-list.component';

const COMPONENTS = [
  promainListComponent
];
const COMPONENTS_NOROUNT = [
];

@NgModule({
  imports: [
    SharedModule,
    promainRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class promainModule { }
