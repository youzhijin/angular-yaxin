import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import {promainListComponent} from "./promain-list/promain-list.component";

const routes: Routes = [
  {
    path: 'promain',
    children: [
      { path: '', component: promainListComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class promainRoutingModule { }
