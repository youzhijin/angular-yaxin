import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import {posListComponent} from "./pos-list/pos-list.component";
import {posheadViewEditComponent} from "./pos-list/edit/pos-head.component";
import {posdetailViewEditComponent} from "./pos-list/edit/pos-detail.component";
import {posxjdetailViewEditComponent} from "./pos-list/edit/posxj-detail.component";
import {posallheadViewEditComponent} from "./pos-list/edit/posall-head.component";
import {posallxjdetailViewEditComponent} from "./pos-list/edit/posallxj-detail.component";

const routes: Routes = [
  {
    path: 'pos',
    children: [
      { path: '', component: posListComponent },
      { path: 'poshead/:id', component: posheadViewEditComponent },
      { path: 'posallhead', component: posallheadViewEditComponent },

      { path: 'posdetail/:id', component: posdetailViewEditComponent } ,
      { path: 'posxjdetail/:id', component: posxjdetailViewEditComponent } ,
      { path: 'posallxjdetail/:id', component: posallxjdetailViewEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class posRoutingModule { }
