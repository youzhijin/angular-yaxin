import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { posRoutingModule } from './pos-routing.module';
import { posListComponent } from './pos-list/pos-list.component';
import {posheadViewEditComponent} from "./pos-list/edit/pos-head.component";
import {posdetailViewEditComponent} from "./pos-list/edit/pos-detail.component";
import {posxjdetailViewEditComponent} from "./pos-list/edit/posxj-detail.component";
import {posallheadViewEditComponent} from "./pos-list/edit/posall-head.component";
import {posallxjdetailViewEditComponent} from "./pos-list/edit/posallxj-detail.component";


const COMPONENTS = [
  posListComponent,
  posallheadViewEditComponent
];
const COMPONENTS_NOROUNT = [
  posheadViewEditComponent,
  posdetailViewEditComponent,
  posxjdetailViewEditComponent,
  posallxjdetailViewEditComponent

];

@NgModule({
  imports: [
    SharedModule,
    posRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class posModule { }
