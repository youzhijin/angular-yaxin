import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { maService } from '../../../service/ma/ma.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';




@Component({
  selector: 'pos-list',
  templateUrl: './pos-list.component.html',
  styleUrls: ['./pos-list.component.less']
})
export class posListComponent implements OnInit, OnDestroy {
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    posNo:"",
    posName: '',
    cusMobile: '',
    cusIncard: '',
    orderNo: ''
  };

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.maService.listposdetail();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  seasons = [];
  checkedList = [];
  data = [];
  downurl='https://zhaodui.oss-cn-shenzhen.aliyuncs.com/doc/posdetail.xls';

  uploadExcelUrl = "";

  uploadDateUrl = this.maService.batchRepdate();

  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    // { title: '订单编号', index: 'orderNo' },
    { title: '商户号', index: 'posNo'},
    { title: '商户名', index: 'posName' },
    { title: '终端号', index: 'posId' },
    { title: '商户地址', index: 'posAddr' },
    { title: '联系人' , index: 'posConcract' },
    // { title: '附件', index: 'fiAtta' },
    { title: '联系电话', index: 'posMobile' },
    { title: 'POS类型', index: 'posType' },
    { title: '备注', index: 'posRemark' },
    {
        title: '操作',
        buttons: [
            {
              text: '任务详情',
              type: 'link',
              click: (record: any) => this.router.navigate(['/pos/pos/poshead/'+record.posNo])
            },
          {
            text: 'POS详情',
            type: 'link',
                click: (record: any) => this.router.navigate(['/pos/pos/posdetail/'+record.posNo])
          }
        ]
    }
  ];

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private maService: maService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  inter
  ngOnInit() {
    this.uploadExcelUrl = this.maService.uploadposdetail();
    this.imgSuffix = this.comm.getGoodsUrl();

    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })


  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }

  gotoone(){
    this.router.navigate(['/dhcar/dhcar/queryone']);

  }
  exportPop(){


    if(this.checkedList.length>1){
      this.notice.create('error', '失败', '只能选择一批进行查询');
return;
    }

    if(this.checkedList.length<1){
      this.notice.create('error', '失败', '请选择一批进行查询');
      return;
    }
    this.loading = true;
    console.log("this.checkedList[0].id"+this.checkedList[0].id);

    /////

    this.maService.getwzqueryg(this.checkedList[0].id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {

      if (data.ok === true) {

        this.msg.success("数据已经提交后台，请点击查询按钮刷新");
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })


    /////


    this.st.load();

    this.loading = false;

  }
  uploadProd(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
    }
    if (info.file.status === 'done') {
      this.loading = false;
      this.st.load();
      this.notice.create('success', '成功', '导入成功');
    }
    if (info.file.status === 'error') {
      this.loading = false;
      this.notice.create('error', '失败', '导入失败');
    }
  }
  checkboxChange(list: any[]) {
    this.checkedList = list;
  }


  close(): void {
    this.visible = false;
  }



  cancel() {
    this.repdate = null;
  }
  repdate = null;



}
