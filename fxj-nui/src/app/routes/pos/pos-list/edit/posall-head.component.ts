import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { maService } from '../../../../service/ma/ma.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {insService} from "../../../../service/ins/ins.service";
import {ProductService} from "../../../../service/prod/product.service";
import {posListComponent} from "../pos-list.component";
import {SimpleTableColumn, SimpleTableComponent} from "@delon/abc";

@Component({
  selector: 'posall-head-edit',
  templateUrl: './posall-head.component.html',
  styleUrls: ['./posall-head.component.less']
})
export class posallheadViewEditComponent implements OnInit, OnDestroy {
  product:  any = {
    id:null,
    carNo: null,
    carVin:null
  };
  ps = 10;
  checkedList = [];
  data = [];
  downurl='https://zhaodui.oss-cn-shenzhen.aliyuncs.com/doc/postask.xls';

  uploadExcelUrl = "";

  @ViewChild('st') st: SimpleTableComponent;
  column: SimpleTableColumn[] = [
    // { title: '订单编号', index: 'orderNo' },
    { title: '', index: 'key', type: 'checkbox',exported:false },


    // { title: '订单编号', index: 'orderNo' },
    { title: 'POS编号', index: 'posNo'},
    { title: '商户名称', index: 'posName' },
    { title: '商户地址', index: 'posAddr' },
    { title: '联系人' , index: 'posConcract' },
    // { title: '附件', index: 'fiAtta' },
    { title: '联系电话', index: 'posMobile' },
    { title: 'POS类型', index: 'posType' },
    { title: '巡检类型 ', index: 'posXjType'},


    { title: '巡检备注', index: 'posRemark' },

    { title: '巡检状态 ', index: 'bpmStatus' },

    // { title: '位置状态 ', index: 'wzChayi' },

    {
      title: '操作',
      buttons: [

        {
          text: 'POS巡检详情',
          type: 'link',
          click: (record: any) => this.router.navigate(['/pos/pos/posallxjdetail/'+record.id])
        }
      ]
    }

  ];
  // column: SimpleTableColumn[] = [
  //   // { title: '订单编号', index: 'orderNo' },
  //   { title: '违章时间 ', index: 'wzsj'},
  //   { title: '违章地点 ', index: 'wzdd' },
  //   { title: '违章行为', index: 'wzxw' },
  //   { title: '违章扣分 ', index: 'wzjf'},
  //   { title: '违章罚款 ', index: 'wzfk' },
  //   { title: '违章城市', index: 'csmc' },
  //   { title: '城市代码 ', index: 'csdm' },
  //   { title: '违章代码', index: 'wfdm' }
  //
  // ];
  loading = false;
  constructor(
    private productService: ProductService,

    public setting: SettingsService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private maService: maService,
    private emit: EmitterService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private notice: NzNotificationService,

    private comm: CommService
  ) {}
  q: any = {
    posNo: '',
    posName:''
  };
  private sub: any;
url="";
  ngOnInit(): void {
    // 获取参数
    this.uploadExcelUrl = this.maService.uploadpostask();

    this.sub = this.route.params.subscribe(params => {
      // 获取商品详情
              this.url=this.maService.getpostasklistall();
    });
    // 隐藏reusetab
    setTimeout(() => {
      this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);
    this.titleService.setTitle("汇总显示")
  }

  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }
  back() {
    this.router.navigate(['/pos/pos']);
  }
  checkboxChange(list: any[]) {
    this.checkedList = list;
  }
  exportPop() {
    if (this.checkedList.length > 0) {
      console.log("1*******");
      this.st.export(this.checkedList, { filename: '查询汇总.xlsx' });
    } else {
      this.notice.create('error', '失败', '请选择进行导出');

    }
  }
  uploadProd(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
    }
    if (info.file.status === 'done') {
      this.loading = false;
      this.st.load();
      this.notice.create('success', '成功', '导入成功');
    }
    if (info.file.status === 'error') {
      this.loading = false;
      this.notice.create('error', '失败', '导入失败');
    }
  }
}
