import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { maService } from '../../../../service/ma/ma.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {insService} from "../../../../service/ins/ins.service";
import {ProductService} from "../../../../service/prod/product.service";
import {posListComponent} from "../pos-list.component";
import {SimpleTableColumn, SimpleTableComponent} from "@delon/abc";

@Component({
  selector: 'posallxj-detail-edit',
  templateUrl: './posallxj-detail.component.html',
  styleUrls: ['./posallxj-detail.component.less']
})
export class posallxjdetailViewEditComponent implements OnInit, OnDestroy {
  product:  any = {
    id:null,
    posNo:null,
    posName:null,
    posAddr:null,
    posConcract:null,
    posMobile:null,
    posType:null,
    posRemark:null,
  };
  fileList = [ ];

  checkedList = [];

  loading = false;
  constructor(
    private productService: ProductService,

    public setting: SettingsService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private maService: maService,
    private emit: EmitterService,
    private notice: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService
  ) {}
  // 预览图片对象
  previewImage = '';
  // 是否可以预览
  previewVisible = false;
  // 预览图片地址
  propName = "";
  picUrl = '';
  // 图片上传地址
  uploadUr5 = '';

  avatarParam5 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};
  private sub: any;
  url="";
  ngOnInit(): void {
    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"];
      this.getProductInfo();

      // 获取商品详情
      //         this.url=this.maService.getposdetail(this.product.id);
    });
    // 隐藏reusetab
    setTimeout(() => {
      this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);
    this.titleService.setTitle("详情显示")

  }
  getProductInfo() {
    this.maService.getpostaskdetail(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.product = data.data;

      this.getfilelist();

    })
  }


  getfilelist() {
    this.maService.getposfilelist(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if (data.data.length > 0) {
        let flag = 0;
        for (const detail of data.data) {
          this.fileList.push( {
            uid: detail.id,
            name: detail.posXjfjname,
            status: 'done',
            url: detail.posXjfjurl
          } )
          flag++;
        }
      };


    })
  }
  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }
  back() {
    this.router.navigate(['/pos/pos/posallhead']);
  }
  checkboxChange(list: any[]) {
    this.checkedList = list;
  }
  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  }
}
