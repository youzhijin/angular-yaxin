import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { insService } from '../../../../service/ins/ins.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {fkService} from "../../../../service/fk/fk.service";

@Component({
  selector: 'ins-view-edit',
  templateUrl: './ins-edit.component.html',
  styleUrls: ['./ins-edit.component.less']
})
export class insViewEditComponent implements OnInit, OnDestroy {
  product:  any = {
    id:null,
    proNo: null,
    bpmStatus:null,
    cusName: null,
    cusMobile: null,
    orderNo: null,
    insName: null,
    sendContent: null /*No show*/,
    sendUrl: null /*No show*/,
    sendDate: null /*No show*/,
    recContent: null,
    recUrl: null,
    recDate: null
  };
  bpmStatus =  [{text: '未分配', value: '1'}, {text: '已分配', value: '10'}];
  insNos = [];

  MAX_COUNT = 60;
  tabIndex = 0;
  saleing = false;
  private sub: any;

  constructor(
    public setting: SettingsService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private insService: insService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService
  ) {}

  ngOnInit(): void {

    // this.loadbpmStatus();
    this.loadinss();


    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"]
      console.log("this.product.id****************"+this.product.id);
      // 获取商品详情
      this.getProductInfo();
    });
    // 隐藏reusetab
    setTimeout(() => {
      this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);
    this.titleService.setTitle("分配保险公司")
  }
  loadbpmStatus() {
    this.bpmStatus = [];
    this.comm.getType("bpm_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.bpmStatus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  loadinss() {
    this.insNos = [];
    this.comm.getentityType("ins").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.insNos.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  changeTab(index) {
    // 使用异步的方式更新组件, 防止ngIf错误
    Promise.resolve(null).then(() => this.tabIndex = index);
  }



  getProductInfo() {
    this.insService.getProduct(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.product = data.data;

    })
  }




  back() {
    this.router.navigate(['/ins/ins']);
  }


  save() {

    // { title: '|订单编号', index: 'orderNo' },
    // { title: '客户姓名 ', index: 'cusName'},
    // { title: '联系电话', index: 'cusMobile' },
    // { title: '保险公司 ', index: 'insName' },
    // { title: '发送内容' , index: 'sendContent' },
    // { title: '发送材料', index: 'sendUrl' },
    // { title: '发送时间', index: 'sendDate' },
    // { title: '回复内容', index: 'recContent' },
    // { title: '回复材料', index: 'recUrl' },
    // { title: '回复时间', index: 'recDate' },


    let fxjLoanins = {
      updateBy:this.setting.user.username,
      id:this.product.id,
      bpmStatus:this.product.bpmStatus,
      // orderNo: this.product.orderNo,
      cusName: this.product.cusName,
      cusMobile: this.product.cusMobile,
      insName: this.product.insName,
      sendContent: this.product.sendContent /*No show*/,
      sendUrl: this.product.sendUrl /*No show*/,
      sendDate: this.product.sendDate /*No show*/,
      recContent: this.product.recContent,
      recUrl: this.product.recUrl,
      recDate: this.product.recDate

    };

    // 调用接口
    this.insService.editPro(fxjLoanins).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if (data.ok === true) {
        this.msg.success('修改成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data['message']);
      }
    })
  }

  changeColor(res) {
    this.product.colorSort = res.color.hex;
  }
}

