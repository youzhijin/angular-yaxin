import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { insListComponent } from './ins-list/ins-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { insViewEditComponent } from 'app/routes/ins/ins-list/edit/ins-edit.component';
import { GoodsImgUploadComponent } from './goods-upload/goods-img-upload.component';

const routes: Routes = [
  {
    path: 'ins',
    children: [
      { path: '', component: insListComponent },
      { path: 'edit/:id', component: insViewEditComponent },
      { path: 'upload', component: GoodsImgUploadComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class insRoutingModule { }
