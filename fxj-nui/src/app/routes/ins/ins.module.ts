import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { insRoutingModule } from './ins-routing.module';
import { insListComponent } from './ins-list/ins-list.component';
import { insViewEditComponent } from 'app/routes/ins/ins-list/edit/ins-edit.component';
import { GoodsImgUploadComponent } from './goods-upload/goods-img-upload.component';

const COMPONENTS = [
  insListComponent,
  GoodsImgUploadComponent
];
const COMPONENTS_NOROUNT = [
  insViewEditComponent,

];

@NgModule({
  imports: [
    SharedModule,
    insRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class insModule { }
