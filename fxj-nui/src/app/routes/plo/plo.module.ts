import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { ploRoutingModule } from './plo-routing.module';
import { ploListComponent } from './plo-list/plo-list.component';
import { ploAddComponent } from 'app/routes/plo/plo-list/edit/plo-add.component';
import { ploViewEditComponent } from 'app/routes/plo/plo-list/edit/plo-edit.component';
import { GoodsImgUploadComponent } from '../plop/goods-upload/goods-img-upload.component';

const COMPONENTS = [
  ploListComponent,
  GoodsImgUploadComponent
];
const COMPONENTS_NOROUNT = [
  ploAddComponent,
  ploViewEditComponent,
];

@NgModule({
  imports: [
    SharedModule,
    ploRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class ploModule { }
