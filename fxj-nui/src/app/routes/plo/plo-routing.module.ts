import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ploListComponent } from './plo-list/plo-list.component';
import { AuthGuardService } from '../../core/auth/auth-guard.service';
import { ploAddComponent } from 'app/routes/plo/plo-list/edit/plo-add.component';
import { ploViewEditComponent } from 'app/routes/plo/plo-list/edit/plo-edit.component';
import { GoodsImgUploadComponent } from '../plop/goods-upload/goods-img-upload.component';

const routes: Routes = [
  {
    path: 'plo',
    children: [
      { path: '', component: ploListComponent },
      { path: 'query/:id', component: ploListComponent },
      { path: 'add/:id', component: ploAddComponent },
      { path: 'edit/:id', component: ploViewEditComponent },
      { path: 'upload', component: GoodsImgUploadComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ploRoutingModule { }
