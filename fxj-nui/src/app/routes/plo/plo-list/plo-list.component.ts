import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '../../../core/constant';
import { EmitterService } from 'app/service/emit.service';
import {ActivatedRoute, Router} from '@angular/router';
import { ploService } from '../../../service/plo/plo.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '../../../shared/descriptor/timeout';

@Component({
  selector: 'plo-list',
  templateUrl: './plo-list.component.html',
  styleUrls: ['./plo-list.component.less']
})
export class ploListComponent implements OnInit, OnDestroy {
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    bpmStatus:"",

    cusName: '',
    cusMobile: '',
    orderNo: '',
    proNo: ''
  };

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.ploService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  isAds = [{ text: '广告款', value: this.cnt.BoolUtil.TRUE}, { text: '普通款', value: this.cnt.BoolUtil.FALSE }];
  isCms = [{ text: '是', value: this.cnt.BoolUtil.TRUE}, { text: '否', value: this.cnt.BoolUtil.FALSE }];
  seasons = [];
  checkedList = [];
  data = [];
  bpmStatus =  [{text: 'M1', value: 'M1'}, {text: 'M2', value: 'M2'}, {text: 'M3', value: 'M3'}, {text: 'M4', value: 'M4'}, {text: 'M5', value: 'M5'}];

  uploadUrl = this.ploService.uploadProds();
  uploadDateUrl = this.ploService.batchRepdate();


  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '订单编号', index: 'orderNo' },
    { title: '客户姓名', index: 'cusName'},
    { title: '联系电话', index: 'cusMobile' },
    { title: '逾期期数 ', index: 'overduePeriod' },
    { title: '逾期金额' , index: 'overdueAmount' },
    { title: '违约金', index: 'penaltyAmount' },
    // { title: '年份', index: 'year' },
    { title: '状态', index: 'bpmStatus', render: 'bpmStatus' },
    // { title: '定制', index: 'isCm', render: 'isCm' },

    {
        title: '操作',
        buttons: [
            {
              text: '处理',
              type: 'link',
              click: (record: any) => this.router.navigate(['/plo/plo/edit/'+record.id])
            }
        ]
    }
  ];

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private ploService: ploService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
    private route: ActivatedRoute,

  ) {}
  syncing = false;
  lastDate = null;
  private sub: any;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  inter
  ngOnInit() {
    this.imgSuffix = this.comm.getGoodsUrl();
    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      let varinpur = params["id"];
      if(varinpur){
        this.q.orderNo = params["id"];
      }

    });
    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })

    // // 每隔5s获取同步状态
    // this.inter = setInterval(() => {
    //   this.productService.getSyncLog().pipe(catchError(err => {
    //     return of(err)
    //   })).subscribe(data => {
    //     if (data.status === 200) {
    //       if (data.data) {
    //         if (data.data.last_date) {
    //           this.lastDate = data.data.last_date;
    //         }
    //         if (data.data.status === 1) {
    //           this.syncing = true;
    //         } else {
    //           this.syncing = false;
    //         }
    //       } else {
    //         this.syncing = false;
    //       }
    //     } else {
    //       this.syncing = false;
    //     }
    //   })
    // }, 5000);
  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }
  back() {
    this.router.navigate(['/prod/product/80']);
  }

  change(args: NzTabChangeEvent) {
        switch (args.index) {
          case 0:
            this.q.bpmStatus = "";
            break;
          case 1:
            this.q.bpmStatus = 1;
            break;
          case 2:
            this.q.bpmStatus = 10;
            break;
          case 3:
            this.q.bpmStatus = 20;
            break;

          case 4:
            this.q.bpmStatus = 30;
            break;
          case 5:
            this.q.bpmStatus = 40;
            break;
          case 6:
            this.q.bpmStatus = 50;
            break;
          case 7:
            this.q.bpmStatus = 60;
            break;
          case 8:
            this.q.bpmStatus = 70;
            break;
          case 9:
            this.q.bpmStatus = 80;
            break;
          case 10:
            this.q.bpmStatus = 90;
            break;
          case 11:
            this.q.bpmStatus = 99;
            break;
          case 12:
            this.q.bpmStatus = 100;
            break;
          default:
            break;
        }
      this.st.load(1, this.q);
  }

  showImage(src) {
    this.previewImage = src;
    this.previewVisible = true;
  }

  checkboxChange(list: any[]) {
    this.checkedList = list;
  }

  gotoAdd() {
    setTimeout(() => {
      this.router.navigate(['/plo/plo/add/'+this.q.orderNo]);
    }, 500);
  }



  close(): void {
    this.visible = false;
  }


  cancel() {
    this.repdate = null;
  }
  repdate = null;

}
