import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { ProductService } from '../../../../service/prod/product.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import { ServiceResult, Constant } from '../../../../core/constant';
import { ProductSkuEditV2Component } from '../../../../shared/component/product-sku-edit-v2/product-sku-edit-v2.component';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ploService} from "../../../../service/plo/plo.service";

@Component({
  selector: 'plo-add',
  templateUrl: './plo-add.component.html',
  styleUrls: ['./plo-add.component.less']
})
export class ploAddComponent implements OnInit, OnDestroy {
  productorder:  any = {
    id:null,
    bmDate: null,
    bmTime: null,
    bxRq: null,
    carColor: null,
    carDetail: null,
    carVin: null,
    conJson: null,
    conNo: null,
    conType: null,
    conUrl: null,
    createSysorg: null,
    createUser: null,
    cusAge: null,
    cusBirthday: null,
    cusDeu: null,
    cusEthnical: null,
    cusGender: null,
    cusIdValidfrom: null,
    cusIdValidto: null,
    cusIncard: null,
    cusMaritalStatus: null,
    cusMobile: null,
    cusName: null,
    cusQqid: null,
    cusRemark: null,
    cusResAddr: null,
    cusWechatid: null,
    cusrIdtype: null,
    danganNo: null,
    diyaStatus: null,
    diyaUser: null,
    fangkBank: null,
    fangkId: null,
    fangkName: null,
    fangkType: null,
    fkAmount: null,
    gpsInsAddr: null,
    gpsInsData: null,
    gpsInsMobile: null,
    gpsInsUser: null,
    gpsStatus: null,
    gpsUser: null,
    guohuStatus: null,
    guohuUser: null,
    luqiaoRq: null,
    lxMobile1: null,
    lxMobile2: null,
    lxMobile3: null,
    lxName1: null,
    lxName2: null,
    lxName3: null,
    mpName: null,
    mpUrl: null,
    mpbackUrl: null,
    nianjiaRq: null,
    orderAmount: null,
    orderArea: null,
    orderBy1: null,
    orderBy10: null,
    orderBy11: null,
    orderBy12: null,
    orderBy13: null,
    orderBy14: null,
    orderBy15: null,
    orderBy16: null,
    orderBy17: null,
    orderBy18: null,
    orderBy2: null,
    orderBy3: null,
    orderBy4: null,
    orderBy5: null,
    orderBy6: null,
    orderBy7: null,
    orderBy8: null,
    orderBy9: null,
    orderCarno: null,
    orderCartype: null,
    orderInfo: null,
    orderNo: null,
    orderOper: null,
    orderPeriod: null,
    orderStatus: null,
    proClass: null,
    proIsdb: null,
    proName: null,
    proNo: null,
    proType: null,
    proWf: null,
    qiandanUser: null,
    sfAnpaijiafang: null,
    sfDianshen: null,
    wbName: null,
    wbUrl: null,
    wbbackUrl: null,
    yinhangUser: null,
    dypname: null,
    dypvalue: null,
    dypyear: null,
    dyptype: null,
    dypclass: null,

    dypremark1: null,
    dypremark2: null,
    dypremark3: null,
    emergencyRelation:  null,
    emergencyContact: null,
    emergencyMobile: null,
    emergencyOffice:null,
    emergencyResAddr:null,
    spcomment:'',

  };
  product:  any = {
    id:null,
    bpmStatus:null,
    orderNo:null,
    cusName:null,
    cusMobile:null,
    overduePeriod:null,
    overdueAmount:null,
    penaltyAmount:null

  };
  bpmStatus =  [{text: 'M1', value: 'M1'}, {text: 'M2', value: 'M2'}, {text: 'M3', value: 'M3'}, {text: 'M4', value: 'M4'}, {text: 'M5', value: 'M5'}];

  // 预览图片对象
  previewImage = '';
  // 是否可以预览
  previewVisible = false;
  profileForm: FormGroup;

  saleing = false;
  proNos = [];

  @ViewChild("skuEdit2") skuEdit2: ProductSkuEditV2Component;
  private sub: any;


  avatarUrl: string;
  loading = false;
  uploadUrl = '';
  avatarParam = { type: 'Avatar', userId: ''};


  constructor(
    private ploService: ploService,
    public setting: SettingsService,

    private msg: NzMessageService,
    private modalService: NzModalService,
    private productService: ProductService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private router: Router,
    private cnt: Constant,
    private comm: CommService
  ) {
    this.titleService.setTitle("添加");

  }

  ngOnInit(): void {


    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.orderNo = params["id"]
      console.log("this.product.id****************"+this.product.id);
      // 获取商品详情
      this.getProductInfo();
    });
    setTimeout(() => {
        this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);

  }

  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  getProductInfo() {
    this.productService.getProduct(this.product.orderNo).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.product = data.data;




    })
  }


  reset(i) {
    // 基本信息重置

  }

  save() {



    let fxjPlOver = {
      createBy:this.setting.user.username,
      sysOrgCode:this.setting.user.sysorgCode,
      id:this.product.id,
      bpmStatus:this.product.bpmStatus,
      orderNo: this.product.orderNo,
      cusName: this.product.cusName,
      cusMobile: this.product.cusMobile,
      // cusrIdtype: this.product.cusrIdtype,
      // cusIncard: this.product.cusIncard /*No show*/,
      overduePeriod: this.product.overduePeriod /*No show*/,
      // cusIdValidto: this.product.cusIdValidto /*No show*/,
      overdueAmount: this.product.overdueAmount,
      penaltyAmount: this.product.penaltyAmount

    };

    // 调用接口
    this.loading = true;
    this.ploService.addPro(fxjPlOver).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.loading = false;
      if (data.ok === true) {
        this.msg.success('添加成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data['message']);
      }
    })
  }

  back() {
    this.router.navigate(['/prod/product/80']);
  }

  beforeUpload = (file: File) => {
    const isJPG = file.type === 'image/jpeg';
    const isPNG = file.type === 'image/png';
    if (!isJPG && !isPNG) {
      this.msg.warning(`No support: ${file.type}`);
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      this.msg.warning('The pic file not more than 2MB!');
    }
    return (isJPG || isPNG) && isLt2M;
  }

  private getBase64(img: File, callback: (img: any) => void) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  handleChange(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl = img;
      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }

}
