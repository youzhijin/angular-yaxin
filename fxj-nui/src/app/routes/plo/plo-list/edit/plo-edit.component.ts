import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '../../../../core/constant';
import { ploService } from '../../../../service/plo/plo.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {insService} from "../../../../service/ins/ins.service";

@Component({
  selector: 'plo-view-edit',
  templateUrl: './plo-edit.component.html',
  styleUrls: ['./plo-edit.component.less']
})
export class ploViewEditComponent implements OnInit, OnDestroy {
  product:  any = {
    id:null,
    bpmStatus:null,
    orderNo:null,
    cusName:null,
    cusMobile:null,
    overduePeriod:null,
    overdueAmount:null,
    penaltyAmount:null

  };
  bpmStatus =  [{text: 'M1', value: 'M1'}, {text: 'M2', value: 'M2'}, {text: 'M3', value: 'M3'}, {text: 'M4', value: 'M4'}, {text: 'M5', value: 'M5'}];

  MAX_COUNT = 60;
  tabIndex = 0;
  saleing = false;
  private sub: any;

  constructor(
    public setting: SettingsService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private ploService: ploService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService
  ) {}

  ngOnInit(): void {

    // this.loadbpmStatus();


    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"]
      console.log("this.product.id****************"+this.product.id);
      // 获取商品详情
      this.getProductInfo();
    });
    // 隐藏reusetab
    setTimeout(() => {
      this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);
    this.titleService.setTitle("编辑")
  }
  loadbpmStatus() {
    this.bpmStatus = [];
    this.comm.getType("bpm_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.bpmStatus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  changeTab(index) {
    // 使用异步的方式更新组件, 防止ngIf错误
    Promise.resolve(null).then(() => this.tabIndex = index);
  }



  getProductInfo() {
    this.ploService.getProduct(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.product = data.data;

    })
  }




  back() {
    this.router.navigate(['/prod/product/80']);
  }


  save() {

    let fxjPlOver = {
      updateBy:this.setting.user.username,
      id:this.product.id,
      bpmStatus:this.product.bpmStatus,
      orderNo: this.product.orderNo,
      cusName: this.product.cusName,
      cusMobile: this.product.cusMobile,
      // cusrIdtype: this.product.cusrIdtype,
      // cusIncard: this.product.cusIncard /*No show*/,
      overduePeriod: this.product.overduePeriod /*No show*/,
      // cusIdValidto: this.product.cusIdValidto /*No show*/,
      overdueAmount: this.product.overdueAmount,
      penaltyAmount: this.product.penaltyAmount

    };

    // 调用接口
    this.ploService.editPro(fxjPlOver).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if (data.ok === true) {
        this.msg.success('修改成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data['message']);
      }
    })
  }

  changeColor(res) {
    this.product.colorSort = res.color.hex;
  }
}
