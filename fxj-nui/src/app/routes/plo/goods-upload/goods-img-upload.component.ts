import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { TitleService } from '@delon/theme';
import { EmitterService } from 'app/service/emit.service';
import { ServiceResult, Constant } from '@core/constant';
import { ProductService } from '../../../service/prod/product.service';
import { CommService } from '../../../service/comm/comm.service';

@Component({
  selector: 'goods-img-upload',
  templateUrl: './goods-img-upload.component.html',
  styleUrls: ['./goods-img-upload.component.less']
})
export class GoodsImgUploadComponent implements OnInit, OnDestroy {
  url = this.productService.uploadUrl();
  extraParam = { save: '1' };
  constructor(
    private msg: NzMessageService,
    private productService: ProductService,
    private emit: EmitterService,
    private titleService: TitleService,
    private router: Router
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
        this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);
    this.titleService.setTitle("批量上传商品图片");
  }

  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  back() {
    this.emit.sendMessage("RELOAD_GOODS_LIST");
    this.router.navigate(['/prod/product']);
  }

  handleChange({ file,  }): void {
    const status = file.status;

    if (status === 'done') {
      this.msg.success(`${file.name} file uploaded successfully.`);
    } else if (status === 'error') {
      this.msg.error(`${file.name} file upload failed.`);
    }
  }
}
