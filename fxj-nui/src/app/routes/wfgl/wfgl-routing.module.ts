import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { wfglListComponent } from './wfgl-list/wfgl-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { wfglViewEditComponent } from 'app/routes/wfgl/wfgl-list/edit/wfgl-edit.component';

const routes: Routes = [
  {
    path: 'wfgl',
    children: [
      { path: '', component: wfglListComponent },
      { path: 'edit/:id', component: wfglViewEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class wfglRoutingModule { }
