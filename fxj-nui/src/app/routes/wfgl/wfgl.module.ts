import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { wfglRoutingModule } from './wfgl-routing.module';
import { wfglListComponent } from './wfgl-list/wfgl-list.component';
import { wfglViewEditComponent } from 'app/routes/wfgl/wfgl-list/edit/wfgl-edit.component';

const COMPONENTS = [
  wfglListComponent
];
const COMPONENTS_NOROUNT = [
  wfglViewEditComponent
];

@NgModule({
  imports: [
    SharedModule,
    wfglRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class wfglModule { }
