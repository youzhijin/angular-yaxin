import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';

import { _HttpClient, ModalHelper, SettingsService, TitleService} from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
// import { Router } from '@angular/router';
import { ActivatedRoute, Router } from '@angular/router';
import { workbenchService } from '../../../../service/workbench/workbench.service';
import { ProductService } from '../../../../service/prod/product.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';





@Component({
  selector: 'workbench-rm',
  templateUrl: './workbench-rm.component.html',
  styleUrls: ['./workbench-rm.component.less']
})
export class workbenchrmComponent implements OnInit, OnDestroy {
  onShow = 0;
  selectedIndex = 0;
  isfullscreen = true; //是否全屏显示
  fullscreenpadding = '20px';
  hideTitle = true; //全屏是否显示标题
  autoBreadcrumb = true;
  breadcrumb='';
  // region 公共字段
  workbench:any={
    bi1:"15",
    bi2:"5",
    bi3:"3",
    bi4:"20",
    bi5:"12"
  };
  percent = '48'
  color = "#ff1600"
  

  visible = false;
  // pageSize
  ps = 10;

  // 搜索表单
  q: any = {
    bpmStatus:"",
    cusName: '',
    cusMobile: '',
    cusIncard: '',
    proNo: '',
    orderNo:''
  };

  @ViewChild('st') st: SimpleTableComponent;
  @ViewChild('strm') strm: SimpleTableComponent;

  loanInfoShow = true;//借款人信息展开
  commonBorrowerShow = true;//共借人信息展开
  guarantorInfoShow = true;//担保人信息展开
  loanMaterialsShow = true;//贷款材料展开
  bodyPhotosShow = true;//车身照片

  product:  any = {
    id:null,
    bmDate: null,
    bmTime: null,
    bxRq: null,
    carColor: null,
    carDetail: null,
    carVin: null,
    conJson: null,
    conNo: null,
    conType: null,
    conUrl: null,
    createSysorg: null,
    createUser: null,
    cusAge: null,
    cusBirthday: null,
    cusDeu: null,
    cusEthnical: null,
    cusGender: null,
    cusIdValidfrom: null,
    cusIdValidto: null,
    cusIncard: null,
    cusMaritalStatus: null,
    cusMobile: null,
    cusName: null,
    cusQqid: null,
    cusRemark: null,
    cusResAddr: null,
    cusWechatid: null,
    cusrIdtype: null,
    danganNo: null,
    diyaStatus: null,
    diyaUser: null,
    fangkBank: null,
    fangkId: null,
    fangkName: null,
    fangkType: null,
    fkAmount: null,
    gpsInsAddr: null,
    gpsInsData: null,
    gpsInsMobile: null,
    gpsInsUser: null,
    gpsStatus: null,
    gpsUser: null,
    guohuStatus: null,
    guohuUser: null,
    luqiaoRq: null,
    lxMobile1: null,
    lxMobile2: null,
    lxMobile3: null,
    lxName1: null,
    lxName2: null,
    lxName3: null,
    mpName: null,
    mpUrl: null,
    mpbackUrl: null,
    nianjiaRq: null,
    orderAmount: null,
    orderArea: null,
    orderBy1: null,
    orderBy10: null,
    orderBy11: null,
    orderBy12: null,
    orderBy13: null,
    orderBy14: null,
    orderBy15: null,
    orderBy16: null,
    orderBy17: null,
    orderBy18: null,
    orderBy2: null,
    orderBy3: null,
    orderBy4: null,
    orderBy5: null,
    orderBy6: null,
    orderBy7: null,
    orderBy8: null,
    orderBy9: null,
    orderCarno: null,
    orderCartype: null,
    orderInfo: null,
    orderNo: null,
    orderOper: null,
    orderPeriod: null,
    orderStatus: null,
    proClass: null,
    proIsdb: null,
    proName: null,
    proNo: null,
    proType: null,
    proWf: null,
    qiandanUser: null,
    sfAnpaijiafang: null,
    sfDianshen: null,
    wbName: null,
    wbUrl: null,
    wbbackUrl: null,
    yinhangUser: null,
    dypname: null,
    dypvalue: null,
    dypyear: null,
    dyptype: null,
    dypclass: null,
    fkname:null,
    dypremark1: null,
    dypremark2: null,
    dypremark3: null,
    emergencyRelation:  null,
    emergencyContact: null,
    emergencyMobile: null,
    emergencyOffice:null,
    emergencyResAddr:null,
    spcomment: null,

  };

  // zhengxin
  zx: any = {
    fxjOut26:null,
    fxjOut27:null,
    fxjOut28:null,
    fxjOut29:null,
    fxjOut30:null,
    fxjOut31:null,
    fxjOut32:null,
    fxjOut33:null,
    fxjOut34:null,
    fxjOut35:null,
    fxjOut36:null,
    fxjOut37:null,
    fxjOut38:null,
    fxjOut39:null,
    fxjOut40:null,
    fxjOut41:null,
    fxjOut42:null,
    fxjOut43:null,
    fxjOut44:null,
    fxjOut45:null,
    fxjOut46:null,
    fxjOut47:null,
    fxjOut48:null,
    fxjOut49:null,
    fxjOut50:null,
    fxjOut51:null,
    fxjOut52:null,
    fxjOut53:null,
    fxjOut54:null,
    fxjOut55:null,
    fxjOut56:null,
    fxjOut57:null,
    fxjOut58:null,
    fxjOut59:null,
    fxjOut60:null,
    fxjOut61:null,
    fxjOut62:null,
    fxjOut63:null,
    fxjOut64:null,
    fxjOut65:null,
    fxjOut66:null,
    fxjOut67:null,
    fxjOut68:null,
    fxjOut69:null,
    fxjOut70:null,
    fxjOut71:null,
  }

  // 机器审批
  jiqi: any = {
    fxjOut11:'彭于晏',
    fxjOut12:'通过',
    fxjOut13:'通过',
    fxjOut14:'501分',
    fxjOut15:'58%<150%',
    fxjOut16:'无',
    fxjOut17:'建议通过',
    fxjOut71:{
      orderBy01:'1',
      orderBy02:'工资流水',
      orderBy03:null,
      orderBy04:null,
      orderBy05:null,
      orderBy06:null,
      orderBy07:null,
      orderBy08:null,
      orderBy09:null,
      orderBy10:null,
      orderBy11:null,
      orderBy12:null,
      orderBy13:null,
      orderBy14:null,
      orderBy15:null,
    },
    fxjOut72:{
      orderBy01:'2',
      orderBy02:'常规流水(结息)',
      orderBy03:null,
      orderBy04:null,
      orderBy05:null,
      orderBy06:null,
      orderBy07:null,
      orderBy08:null,
      orderBy09:null,
      orderBy10:null,
      orderBy11:null,
      orderBy12:null,
      orderBy13:null,
      orderBy14:null,
      orderBy15:null,
    },
    fxjOut73:{
      orderBy01:'3',
      orderBy02:'常规流水(营业收入)',
      orderBy03:null,
      orderBy04:null,
      orderBy05:null,
      orderBy06:null,
      orderBy07:null,
      orderBy08:null,
      orderBy09:null,
      orderBy10:null,
    },
    fxjOut74:{
      orderBy01:'1',
      orderBy02:'房按揭',
      orderBy03:null,
      orderBy04:null,
      orderBy05:null,
      orderBy06:null,
      orderBy07:null,
      orderBy08:null,
      orderBy09:null,
      orderBy10:null,
    },
    fxjOut75:{
      orderBy01:'2',
      orderBy02:'房抵贷',
      orderBy03:null,
      orderBy04:null,
      orderBy05:null,
      orderBy06:null,
      orderBy07:null,
      orderBy08:null,
      orderBy09:null,
      orderBy10:null,
    },
    fxjOut76:{
      orderBy01:'3',
      orderBy02:'全款房',
      orderBy03:null,
      orderBy04:null,
      orderBy05:null,
      orderBy06:null,
      orderBy07:null,
      orderBy08:null,
      orderBy09:null,
      orderBy10:null,
    },
    fxjOut77:{
      orderBy01:'4',
      orderBy02:'车按揭',
      orderBy03:null,
      orderBy04:null,
      orderBy05:null,
      orderBy06:null,
      orderBy07:null,
      orderBy08:null,
      orderBy09:null,
      orderBy10:null,
    },
    fxjOut78:{
      orderBy01:'5',
      orderBy02:'寿险',
      orderBy03:null,
      orderBy04:null,
      orderBy05:null,
      orderBy06:null,
      orderBy07:null,
      orderBy08:null,
      orderBy09:null,
      orderBy10:null,
    },
    fxjOut79:{
      orderBy01:'6',
      orderBy02:'车险',
      orderBy03:null,
      orderBy04:null,
      orderBy05:null,
      orderBy06:null,
      orderBy07:null,
      orderBy08:null,
      orderBy09:null,
      orderBy10:null,
    },
    fxjOut80:{
      orderBy01:'7',
      orderBy02:'公积金',
      orderBy03:null,
      orderBy04:null,
      orderBy05:null,
      orderBy06:null,
      orderBy07:null,
      orderBy08:null,
      orderBy09:null,
      orderBy10:null,
    },
  }

  // 人工审批
  rgsp: any = {
    fxjOut11: null,
    fxjOut12: null,
    fxjOut13: null,
    fxjOut14: null,
    fxjOut15: null,
    fxjOut16: null,
    fxjOut17: null,
    fxjOut18: null,
    fxjOut19: null,
    fxjOut20: null,
    fxjOut21: null,
    fxjOut22: null,
    fxjOut23: null,
    fxjOut24: null,
    fxjOut25: null,
  }

  loading = false;
  // 表格组件
  // @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  // url = this.workbenchService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  seasons = [];
  checkedList = [];
  data = [];


  columns: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '操作状态', index: 'bpmStatus' },
    { title: '订单编号', index: 'orderNo' },
    { title: '操作人 ', index: 'logUser'},
    { title: '日志日期 ', index: 'logDate' },
    { title: '日志内容', index: 'logContent' }

  ];

  // 返回
  back() {
    this.router.navigate([ ]);
  }

  proNos = [];
  cusrIdtypes = [];
  cusGenders = [];
  cusMaritalStatuss = [];
  cusDeus = [];
  cusEthnicals = [];
  bpmStatus = [];
  dyptypes = [];
  dypclasss = [];
  relTypes=[];
  fknames=[];
  qdys=[];

  avatarUrl: string;

  avatarUrl1:string;
  avatarUrl2:string;

  avatarUrl3:string;
  logurl:string;
  avatarUrl4:string;
  avatarUrl5:string;
  // loading = false;
  uploadUrl = '';
  uploadUr5 = '';
  orbitUrl='';
  avatarParam1 = { sbtype: '2', filetype:'sb',orderNo: '2'};
  avatarParam2 = { sbtype: '3', filetype:'sb', orderNo: '3'};
  avatarParam3 = { sbtype: '6',  filetype:'sb',orderNo: '6'};
  avatarParam4 = { sbtype: '2', filetype:'sb', orderNo: '2'};
  avatarParam5 = { sbtype: 'No',  filetype:'jj',orderNo: 'no'};

  fileList = [ ];
  backUrl='';
  MAX_COUNT = 60;
  tabIndex = 0;
  saleing = false;
  
  private sub: any;
  constructor(
    public setting: SettingsService,
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private workbenchService: workbenchService,
    private productService: ProductService,
    private emit: EmitterService,
    private router: Router,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private modalService: NzModalService,
    private notice: NzNotificationService,
    private confirmServ: NzModalService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  private getBase64(img: File, callback: (img: any) => void) {
    if (img) {
      const reader = new FileReader();
      reader.addEventListener('load', () => callback(reader.result));
      reader.readAsDataURL(img);
    }
}

  inter
  ngOnInit() {
    // this.imgSuffix = this.comm.getGoodsUrl();

    // this.emit.getMessage().subscribe(data => {
    //   if (data.text === 'RELOAD_GOODS_LIST') {
    //     this.st.load(1, this.q);
    //   }
    // })
    this.loadproNos();
    this.loadcusrIdtypes();
    this.loadcusGenders();
    this.loadcusMaritalStatuss();
    this.loadcusDeus();
    this.loadcusEthnicals();
    this.loadbpmStatus();
    this.loaddyptypes();
    this.loaddypclasss();
    this.loadrelTypes();
    this.loadqdys();
    this.loadfknames();

    this.uploadUrl = this.workbenchService.uploadsbUrl();
    this.uploadUr5 = this.workbenchService.uploadUrl();
    this.orbitUrl = "http://120.78.150.43/spmq";
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"];
      // 获取商品详情
      this.getProductInfo();
      // 获取征信
      this.getZxInfo();
    });
    // this.gaugeColor(this.zx.fxjOut27);//percent
    this.gaugeColor(this.percent);


  }
  

  loadrelTypes() {
    this.relTypes = [];
    this.comm.getType("reltype").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.relTypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载产品
  loadproNos() {
    this.proNos = [];
    this.comm.getType("proNo").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.proNos.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载产品
  loadqdys() {
    this.qdys = [];
    this.comm.getType("qdy").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.qdys.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载证件类型
  loadcusrIdtypes() {
    this.cusrIdtypes = [];
    this.comm.getType("idType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusrIdtypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载性别
  loadcusGenders() {
    this.cusGenders = [];
    this.comm.getType("sex").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusGenders.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载婚姻
  loadcusMaritalStatuss() {
    this.cusMaritalStatuss = [];
    this.comm.getType("marType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusMaritalStatuss.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }

  // 加载学历
  loadcusDeus() {
    this.cusDeus = [];
    this.comm.getType("eduType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusDeus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
//加载民族
  loadcusEthnicals() {
    this.cusEthnicals = [];
    this.comm.getType("ethType").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cusEthnicals.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载证件类型
  loadbpmStatus() {
    this.bpmStatus = [];
    this.comm.getType("bpm_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.bpmStatus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载证件类型
  loaddyptypes() {
    this.dyptypes = [];
    this.comm.getType("dyptype").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.dyptypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loaddypclasss() {
    this.dypclasss = [];
    this.comm.getType("dypclass").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.dypclasss.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 加载风控类型
  loadfknames() {
    this.fknames = [];
    this.comm.getType("fkname").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.fknames.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  gaugeColor(percent) {
    console.log(percent)
    if(percent > 0 && percent <=20) {
      this.color = "#ff1600";
    }
    if(percent > 20 && percent <=40) {
      this.color = "#ff8c00";
    }
    if(percent > 40 && percent <=60) {
      this.color = "#f8cd00";
    }
    if(percent > 60 && percent <=80) {
      this.color = "#adce00";
    }
    if(percent > 80 && percent <=100) {
      this.color = "#8ac500";
    }
  }



  // 获取商品详情
  getProductInfo() {
    this.workbenchService.getProduct(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      console.log(data)
      this.product = data.data;
      this.titleService.setTitle("订单("+this.product.orderNo+")");
      this.logurl = this.workbenchService.logurl()+this.product.orderNo;
      this.avatarParam1 = { sbtype: '2', filetype:'sb',orderNo: this.product.orderNo};
      this.avatarParam2 = { sbtype: '3', filetype:'sb', orderNo: this.product.orderNo};
      this.avatarParam3 = { sbtype: '6',  filetype:'sb',orderNo: this.product.orderNo};
      this.avatarParam4 = { sbtype: '2', filetype:'sb', orderNo: this.product.orderNo};
      this.avatarParam5 = { sbtype: 'No',  filetype:'mq',orderNo: this.product.orderNo};
      if (data.data.fxjOrderEmerList!=null&&data.data.fxjOrderEmerList.length > 0) {
        this.product.emergencyRelation = data.data.fxjOrderEmerList[0].emergencyRelation;
        this.product.emergencyContact = data.data.fxjOrderEmerList[0].emergencyContact;
        this.product.emergencyMobile = data.data.fxjOrderEmerList[0].emergencyMobile;
        this.product.emergencyOffice = data.data.fxjOrderEmerList[0].emergencyOffice;
        this.product.emergencyResAddr = data.data.fxjOrderEmerList[0].emergencyResAddr;
      }

      if (data.data.fxjOrderLoanList.length > 0) {
        let flag = 0;
        for (const detail of data.data.fxjOrderLoanList) {
          console.log("detail"+detail);

          if(detail.elementNo=='10'){
            this.product.dypname =detail.elementContent;
          }
          if(detail.elementNo=='20'){
            this.product.dypvalue =detail.elementContent;

          }
          if(detail.elementNo=='30'){
            this.product.dypyear =detail.elementContent;

          }
          if(detail.elementNo=='40'){
            this.product.dyptype =detail.elementContent;

          }if(detail.elementNo=='50'){
            this.product.dypclass =detail.elementContent;

          }
          if(detail.elementNo=='60'){
            this.product.dypremark1 =detail.elementContent;

          }if(detail.elementNo=='70'){
            this.product.dypremark2 =detail.elementContent;

          }if(detail.elementNo=='80'){
            this.product.dypremark3 =detail.elementContent;
          }
        }
      };

      if (data.data.fxjOrderAttaList.length > 0) {
        let flag = 0;
        for (const detail of data.data.fxjOrderAttaList) {
          this.fileList.push( {
            uid: flag,
            name: detail.attaName,
            status: 'done',
            url: detail.attaUrl
          } )
          flag++;
        }
      };

    });
  }
  // 获取征信报告详情
  getZxInfo() {
    // console.log(this.workbenchService.getZx())
    this.workbenchService.getZx().pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      console.log(data);
      this.zx = data.data[0];
      console.log(this.zx);
      this.titleService.setTitle("征信报告("+this.product.orderNo+")");
    });
  }
  // 获取机器审批DTI表详情
  getJqInfo() {
    this.workbenchService.getJq().pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.zx = data.data[0];
      this.titleService.setTitle("机器审批("+this.product.orderNo+")");
    });
  }
  // 获取人工审批详情 
  getRgInfo() {
    this.workbenchService.getRg().pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.rgsp = data.data[0];
      this.titleService.setTitle("机器审批("+this.product.orderNo+")");
    });
  }

  savestc(orderOper,title) {
    if(orderOper=='N'){
      if(this.product.spcomment == '' || this.product.spcomment == null){
        this.msg.warning("请填写审批意见");
        return;
      }
    }
    if(orderOper=='R'){
      if(this.product.spcomment == '' || this.product.spcomment == null){
        this.msg.warning("请填写审批意见");
        return;
      }
    }
    this.confirmServ.confirm({
      nzTitle: '确定'+title+'?',
      nzOnOk: () => {
        this.savest(orderOper);
        },
    });
  }
  savest(orderOper) {

    // 调用接口
    if(this.product.spcomment==''||this.product.spcomment==null){
      this.product.spcomment="同意";
    }
    // product.spcomment
    let fxjOrderLogList = [];
    if(this.product.spcomment!=''){
      // console.log("this.product.dypname*********"+this.product.dypname);

      let dto ={
        orderNo:this.product.orderNo,
        logUser:this.setting.user.username,
        logContent:this.product.spcomment
      }
      fxjOrderLogList.push(dto);
    }


    let fxjOrderMainPage = {
      updateBy:this.setting.user.username,
      id:this.product.id,
      orderNo:this.product.orderNo,
      // bpmStatus:stau,
      orderOper:orderOper,

      bmDate: this.product.bmDate,
      bmTime: this.product.bmTime,
      bxRq: this.product.bxRq,
      carColor: this.product.carColor,
      carDetail: this.product.carDetail,
      carVin: this.product.carVin,
      conJson: this.product.conJson,
      conNo: this.product.conNo,
      conType: this.product.conType,
      conUrl: this.product.conUrl,
      createSysorg: this.product.createSysorg,
      createUser: this.product.createUser,
      cusAge: this.product.cusAge,
      cusBirthday: this.product.cusBirthday,
      cusDeu: this.product.cusDeu,
      cusEthnical: this.product.cusEthnical,
      cusGender: this.product.cusGender,
      cusIdValidfrom: this.product.cusIdValidfrom,
      cusIdValidto: this.product.cusIdValidto,
      cusIncard: this.product.cusIncard,
      cusMaritalStatus: this.product.cusMaritalStatus,
      cusMobile: this.product.cusMobile,
      cusName: this.product.cusName,
      cusQqid: this.product.cusQqid,
      cusRemark: this.product.cusRemark,
      cusResAddr: this.product.cusResAddr,
      cusWechatid: this.product.cusWechatid,
      cusrIdtype: this.product.cusrIdtype,
      danganNo: this.product.danganNo,
      diyaStatus: this.product.diyaStatus,
      diyaUser: this.product.diyaUser,
      fangkBank: this.product.fangkBank,
      fangkId: this.product.fangkId,
      fangkName: this.product.fangkName,
      fangkType: this.product.fangkType,
      fkAmount: this.product.fkAmount,
      gpsInsAddr: this.product.gpsInsAddr,
      gpsInsData: this.product.gpsInsData,
      gpsInsMobile: this.product.gpsInsMobile,
      gpsInsUser: this.product.gpsInsUser,
      gpsStatus: this.product.gpsStatus,
      gpsUser: this.product.gpsUser,
      guohuStatus: this.product.guohuStatus,
      guohuUser: this.product.guohuUser,
      luqiaoRq: this.product.luqiaoRq,
      lxMobile1: this.product.lxMobile1,
      lxMobile2: this.product.lxMobile2,
      lxMobile3: this.product.lxMobile3,
      lxName1: this.product.lxName1,
      lxName2: this.product.lxName2,
      lxName3: this.product.lxName3,
      mpName: this.product.mpName,
      mpUrl: this.product.mpUrl,
      mpbackUrl: this.product.mpbackUrl,
      nianjiaRq: this.product.nianjiaRq,
      orderAmount: this.product.orderAmount,
      orderArea: this.product.orderArea,
      orderBy1: this.product.orderBy1,
      orderBy10: this.product.orderBy10,
      orderBy11: this.product.orderBy11,
      orderBy12: this.product.orderBy12,
      orderBy13: this.product.orderBy13,
      orderBy14: this.product.orderBy14,
      orderBy15: this.product.orderBy15,
      orderBy16: this.product.orderBy16,
      orderBy17: this.product.orderBy17,
      orderBy18: this.product.orderBy18,
      orderBy2: this.product.orderBy2,
      orderBy3: this.product.orderBy3,
      orderBy4: this.product.orderBy4,
      orderBy5: this.product.orderBy5,
      orderBy6: this.product.orderBy6,
      orderBy7: this.product.orderBy7,
      orderBy8: this.product.orderBy8,
      orderBy9: this.product.orderBy9,
      orderCarno: this.product.orderCarno,
      orderCartype: this.product.orderCartype,
      orderInfo: this.product.orderInfo,

      orderPeriod: this.product.orderPeriod,
      orderStatus: this.product.orderStatus,
      proClass: this.product.proClass,
      proIsdb: this.product.proIsdb,
      proName: this.product.proName,
      proNo: this.product.proNo,
      proType: this.product.proType,
      proWf: this.product.proWf,
      qiandanUser: this.product.qiandanUser,
      sfAnpaijiafang: this.product.sfAnpaijiafang,
      sfDianshen: this.product.sfDianshen,
      wbName: this.product.wbName,
      wbUrl: this.product.wbUrl,
      wbbackUrl: this.product.wbbackUrl,
      yinhangUser: this.product.yinhangUser,
      fxjOrderLogList:JSON.parse(JSON.stringify(fxjOrderLogList))

    };

    this.productService.editPro(fxjOrderMainPage).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      console.log("data.ok"+data.ok);
      if (data.ok === true) {
        this.st.load(1, this.q);
        this.back();

        this.msg.success('成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })
  }

  rmqueryc(type,title) {
    this.confirmServ.confirm({
      nzTitle: '确定'+title+'?',
      nzOnOk: () => {
        this.rmquery(type);
        },
    });
  }
  rmquery(type) {
    // 调用接口
    if(this.product.fkname==''||this.product.fkname==null ){
      this.msg.warning("请选择风控类别");
      return;
    }
    // 调用接口
    if(this.product.orderNo==''||this.product.orderNo==null){
      this.msg.warning("请输入订单号");
      return;
    }
    let fkinput = {

    };
    if(type=="J"){
      // 调用接口
      if(this.product.cusIncard===''||this.product.cusIncard===null){
        this.msg.warning("请输入身份证");
        return;
      }
      // 调用接口
      if(this.product.cusName===''||this.product.cusName===null){
        this.msg.warning("请输入姓名");
        return;
      }
      // 调用接口
      if(this.product.cusMobile===''||this.product.cusMobile===null){
        this.msg.warning("请输入电话");
        return;
      }
      if(this.product.cusResAddr===''||this.product.cusResAddr===null){
        this.msg.warning("请输入地址");
        return;
      }

      fkinput = {
        createBy:this.setting.user.username,
        orderNo:this.product.orderNo,
        cardNo:this.product.cusIncard,
        realName:this.product.cusName,
        keyword:this.product.cusResAddr,
        mobile:this.product.cusMobile
      };
    }else if(type=="D") {
      // 调用接口
      if(this.product.emergencyContact===''||this.product.emergencyContact===null){
        this.msg.warning("请输入姓名");
        return;
      }
      // 调用接口
      if(this.product.emergencyOffice===''||this.product.emergencyOffice===null){
        this.msg.warning("请输入身份证");
        return;
      }
      // 调用接口
      if(this.product.emergencyMobile===''||this.product.emergencyMobile===null){
        this.msg.warning("请输入电话");
        return;
      }
      fkinput = {
        createBy:this.setting.user.username,
        orderNo:this.product.orderNo,
        cardNo:this.product.emergencyOffice,
        realName:this.product.emergencyContact,

        mobile:this.product.emergencyMobile,
      };
       }else if(type=="C") {
        // 调用接口
        if (this.product.dypvalue === '' || this.product.dypvalue === null) {
          this.msg.warning("请输入车牌号");
          return;
        }
        // 调用接口
        if (this.product.dypremark2 === '' || this.product.dypremark2 === null) {
          this.msg.warning("请输入车架号");
          return;
        }
        // 调用接口
        if (this.product.dypremark3 === '' || this.product.dypremark3 === null) {
          this.msg.warning("请输入发动机号");
          return;
        }
      fkinput = {
        createBy:this.setting.user.username,
        orderNo:this.product.orderNo,
        lsnum:this.product.dypvalue,
        frameno:this.product.dypremark2,
        engineno:this.product.dypremark3,
      };

    }




    this.productService.rmquery(this.product.fkname,fkinput).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      console.log("data.ok"+data.ok);
      if (data.ok === true) {
        this.strm.load(1, this.q);

        this.msg.success('成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })
  }


  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }

  checkboxChange(list: any[]) {
    this.checkedList = list;
  }
  close(): void {
    this.visible = false;
  }
  cancel() {
    this.repdate = null;
  }
  repdate = null;
  change(args) {
    this.selectedIndex = args.index;
  }
  changea(args) {
    // this.onShow = args.index;
    if(args.index == 3) {
      this.router.navigate(['/prod/product/mq/'+this.product.id]);
    }else{
      this.onShow = args.index;
    }
  }

  // 查看征信报告
  viewZx() {
    this.onShow = 0;
    this.selectedIndex = 1;
  }
  // 查看DTI
  viewDTI() {

  }
  // 查看大数据评分
  viewBigData() {
    this.onShow = 0;
    this.selectedIndex = 2;
  }
  // 查看车辆评估
  viewCarDet() {
    this.onShow = 0;
    this.selectedIndex = 3;
  }
  // 查看通话详单 
  viewTel() {
    this.onShow = 0;
    this.selectedIndex = 4;
  }
  // 征信保存
  save() {
    let fxjZx = {
      updateBy: this.setting.user.username,
      id: this.product.id,
      bpmStatus: this.product.bpmStatus,
      fxjOut26:this.zx.fxjOut26,
      fxjOut27:this.zx.fxjOut27,
      fxjOut28:this.zx.fxjOut28,
      fxjOut29:this.zx.fxjOut29,
      fxjOut30:this.zx.fxjOut30,
      fxjOut31:this.zx.fxjOut31,
      fxjOut32:this.zx.fxjOut32,
      fxjOut33:this.zx.fxjOut33,
      fxjOut34:this.zx.fxjOut34,
      fxjOut35:this.zx.fxjOut35,
      fxjOut36:this.zx.fxjOut36,
      fxjOut37:this.zx.fxjOut37,
      fxjOut38:this.zx.fxjOut38,
      fxjOut39:this.zx.fxjOut39,
      fxjOut40:this.zx.fxjOut40,
      fxjOut41:this.zx.fxjOut41,
      fxjOut42:this.zx.fxjOut42,
      fxjOut43:this.zx.fxjOut43,
      fxjOut44:this.zx.fxjOut44,
      fxjOut45:this.zx.fxjOut45,
      fxjOut46:this.zx.fxjOut46,
      fxjOut47:this.zx.fxjOut47,
      fxjOut48:this.zx.fxjOut48,
      fxjOut49:this.zx.fxjOut49,
      fxjOut50:this.zx.fxjOut50,
      fxjOut51:this.zx.fxjOut51,
      fxjOut52:this.zx.fxjOut52,
      fxjOut53:this.zx.fxjOut53,
      fxjOut54:this.zx.fxjOut54,
      fxjOut55:this.zx.fxjOut55,
      fxjOut56:this.zx.fxjOut56,
      fxjOut57:this.zx.fxjOut57,
      fxjOut58:this.zx.fxjOut58,
      fxjOut59:this.zx.fxjOut59,
      fxjOut60:this.zx.fxjOut60,
      fxjOut61:this.zx.fxjOut61,
      fxjOut62:this.zx.fxjOut62,
      fxjOut63:this.zx.fxjOut63,
      fxjOut64:this.zx.fxjOut64,
      fxjOut65:this.zx.fxjOut65,
      fxjOut66:this.zx.fxjOut66,
      fxjOut67:this.zx.fxjOut67,
      fxjOut68:this.zx.fxjOut68,
      fxjOut69:this.zx.fxjOut69,
      fxjOut70:this.zx.fxjOut70,
      fxjOut71:this.zx.fxjOut71,
    }
    // console.log(fxjZx);
    this.workbenchService.zhengxin(fxjZx).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if (data.ok === true) {
        this.msg.success('保存成功');
        // this.back();
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data['message']);
      }
    })
  }
  // 机器审批保存
  saveJq() {
    let fxjJiqi = {
      updateBy: this.setting.user.username,
      id: this.product.id,
      bpmStatus: this.product.bpmStatus,
      fxjOut11:'彭于晏',
      fxjOut12:'通过',
      fxjOut13:'通过',
      fxjOut14:'501分',
      fxjOut15:'58%<150%',
      fxjOut16:'无',
      fxjOut17:'建议通过',
      fxjOut71:{
        orderBy01:'1',
        orderBy02:'工资流水',
        orderBy03:this.jiqi.fxjOut71.orderBy03,
        orderBy04:this.jiqi.fxjOut71.orderBy04,
        orderBy05:this.jiqi.fxjOut71.orderBy05,
        orderBy06:this.jiqi.fxjOut71.orderBy06,
        orderBy07:this.jiqi.fxjOut71.orderBy07,
        orderBy08:this.jiqi.fxjOut71.orderBy08,
        orderBy09:this.jiqi.fxjOut71.orderBy09,
        orderBy10:this.jiqi.fxjOut71.orderBy10,
        orderBy11:this.jiqi.fxjOut71.orderBy11,
        orderBy12:this.jiqi.fxjOut71.orderBy12,
        orderBy13:this.jiqi.fxjOut71.orderBy13,
        orderBy14:this.jiqi.fxjOut71.orderBy14,
        orderBy15:this.jiqi.fxjOut71.orderBy15,
      },
      fxjOut72:{
        orderBy01:'2',
        orderBy02:'常规流水(结息)',
        orderBy03:this.jiqi.fxjOut72.orderBy03,
        orderBy04:this.jiqi.fxjOut72.orderBy04,
        orderBy05:this.jiqi.fxjOut72.orderBy05,
        orderBy06:this.jiqi.fxjOut72.orderBy06,
        orderBy07:this.jiqi.fxjOut72.orderBy07,
        orderBy08:this.jiqi.fxjOut72.orderBy08,
        orderBy09:this.jiqi.fxjOut72.orderBy09,
        orderBy10:this.jiqi.fxjOut72.orderBy10,
        orderBy11:this.jiqi.fxjOut71.orderBy11,
        orderBy12:this.jiqi.fxjOut71.orderBy12,
        orderBy13:this.jiqi.fxjOut71.orderBy13,
        orderBy14:this.jiqi.fxjOut71.orderBy14,
        orderBy15:this.jiqi.fxjOut71.orderBy15,
      },
      fxjOut73:{
        orderBy01:'3',
        orderBy02:'常规流水(营业收入)',
        orderBy03:this.jiqi.fxjOut73.orderBy03,
        orderBy04:this.jiqi.fxjOut73.orderBy04,
        orderBy05:this.jiqi.fxjOut73.orderBy05,
        orderBy06:this.jiqi.fxjOut73.orderBy06,
        orderBy07:this.jiqi.fxjOut73.orderBy07,
        orderBy08:this.jiqi.fxjOut73.orderBy08,
        orderBy09:this.jiqi.fxjOut73.orderBy09,
        orderBy10:this.jiqi.fxjOut73.orderBy10,
      },
      fxjOut74:{
        orderBy01:'1',
        orderBy02:'房按揭',
        orderBy03:this.jiqi.fxjOut74.orderBy03,
        orderBy04:this.jiqi.fxjOut74.orderBy04,
        orderBy05:this.jiqi.fxjOut74.orderBy05,
        orderBy06:this.jiqi.fxjOut74.orderBy06,
        orderBy07:this.jiqi.fxjOut74.orderBy07,
        orderBy08:this.jiqi.fxjOut74.orderBy08,
        orderBy09:this.jiqi.fxjOut74.orderBy09,
        orderBy10:this.jiqi.fxjOut74.orderBy10,
      },
      fxjOut75:{
        orderBy01:'2',
        orderBy02:'房抵贷',
        orderBy03:this.jiqi.fxjOut75.orderBy03,
        orderBy04:this.jiqi.fxjOut75.orderBy04,
        orderBy05:this.jiqi.fxjOut75.orderBy05,
        orderBy06:this.jiqi.fxjOut75.orderBy06,
        orderBy07:this.jiqi.fxjOut75.orderBy07,
        orderBy08:this.jiqi.fxjOut75.orderBy08,
        orderBy09:this.jiqi.fxjOut75.orderBy09,
        orderBy10:this.jiqi.fxjOut75.orderBy10,
      },
      fxjOut76:{
        orderBy01:'3',
        orderBy02:'全款房',
        orderBy03:this.jiqi.fxjOut76.orderBy03,
        orderBy04:this.jiqi.fxjOut76.orderBy04,
        orderBy05:this.jiqi.fxjOut76.orderBy05,
        orderBy06:this.jiqi.fxjOut76.orderBy06,
        orderBy07:this.jiqi.fxjOut76.orderBy07,
        orderBy08:this.jiqi.fxjOut76.orderBy08,
        orderBy09:this.jiqi.fxjOut76.orderBy09,
        orderBy10:this.jiqi.fxjOut76.orderBy10,
      },
      fxjOut77:{
        orderBy01:'4',
        orderBy02:'车按揭',
        orderBy03:this.jiqi.fxjOut77.orderBy03,
        orderBy04:this.jiqi.fxjOut77.orderBy04,
        orderBy05:this.jiqi.fxjOut77.orderBy05,
        orderBy06:this.jiqi.fxjOut77.orderBy06,
        orderBy07:this.jiqi.fxjOut77.orderBy07,
        orderBy08:this.jiqi.fxjOut77.orderBy08,
        orderBy09:this.jiqi.fxjOut77.orderBy09,
        orderBy10:this.jiqi.fxjOut77.orderBy10,
      },
      fxjOut78:{
        orderBy01:'5',
        orderBy02:'寿险',
        orderBy03:this.jiqi.fxjOut78.orderBy03,
        orderBy04:this.jiqi.fxjOut78.orderBy04,
        orderBy05:this.jiqi.fxjOut78.orderBy05,
        orderBy06:this.jiqi.fxjOut78.orderBy06,
        orderBy07:this.jiqi.fxjOut78.orderBy07,
        orderBy08:this.jiqi.fxjOut78.orderBy08,
        orderBy09:this.jiqi.fxjOut78.orderBy09,
        orderBy10:this.jiqi.fxjOut78.orderBy10,
      },
      fxjOut79:{
        orderBy01:'6',
        orderBy02:'车险',
        orderBy03:this.jiqi.fxjOut79.orderBy03,
        orderBy04:this.jiqi.fxjOut79.orderBy04,
        orderBy05:this.jiqi.fxjOut79.orderBy05,
        orderBy06:this.jiqi.fxjOut79.orderBy06,
        orderBy07:this.jiqi.fxjOut79.orderBy07,
        orderBy08:this.jiqi.fxjOut79.orderBy08,
        orderBy09:this.jiqi.fxjOut79.orderBy09,
        orderBy10:this.jiqi.fxjOut79.orderBy10,
      },
      fxjOut80:{
        orderBy01:'7',
        orderBy02:'公积金',
        orderBy03:this.jiqi.fxjOut80.orderBy03,
        orderBy04:this.jiqi.fxjOut80.orderBy04,
        orderBy05:this.jiqi.fxjOut80.orderBy05,
        orderBy06:this.jiqi.fxjOut80.orderBy06,
        orderBy07:this.jiqi.fxjOut80.orderBy07,
        orderBy08:this.jiqi.fxjOut80.orderBy08,
        orderBy09:this.jiqi.fxjOut80.orderBy09,
        orderBy10:this.jiqi.fxjOut80.orderBy10,
      },
    }
    this.workbenchService.jiqisp(fxjJiqi).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if (data.ok === true) {
        this.msg.success('保存成功');
        // this.back();
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data['message']);
      }
    })
  }
  // 人工审批保存
  saveRg() {
    let fxjRg = {
      updateBy: this.setting.user.username,
      id: this.product.id,
      bpmStatus: this.product.bpmStatus,
      fxjOut11: this.rgsp.fxjOut11,
      fxjOut12: this.rgsp.fxjOut12,
      fxjOut13: this.rgsp.fxjOut13,
      fxjOut14: this.rgsp.fxjOut14,
      fxjOut15: this.rgsp.fxjOut15,
      fxjOut16: this.rgsp.fxjOut16,
      fxjOut17: this.rgsp.fxjOut17,
      fxjOut18: this.rgsp.fxjOut18,
      fxjOut19: this.rgsp.fxjOut19,
      fxjOut20: this.rgsp.fxjOut20,
      fxjOut21: this.rgsp.fxjOut21,
      fxjOut22: this.rgsp.fxjOut22,
      fxjOut23: this.rgsp.fxjOut23,
      fxjOut24: this.rgsp.fxjOut24,
      fxjOut25: this.rgsp.fxjOut25,
    }
    this.workbenchService.rengsp(fxjRg).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if (data.ok === true) {
        this.msg.success('保存成功');
        // this.back();
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data['message']);
      }
    })
  }
  // 人工审批取消
  savesQx() {
    this.rgsp = {
      fxjOut11: null,
      fxjOut12: null,
      fxjOut13: null,
      fxjOut14: null,
      fxjOut15: null,
      fxjOut16: null,
      fxjOut17: null,
      fxjOut18: null,
      fxjOut19: null,
      fxjOut20: null,
      fxjOut21: null,
      fxjOut22: null,
      fxjOut23: null,
      fxjOut24: null,
      fxjOut25: null,
    }
  }



  handleChange1(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl1 = img;
        console.log("info1*********"+JSON.stringify(info.file.response[0].piccontent));

        var sfxx=JSON.parse(info.file.response[0].piccontent)

        this.product.cusName=sfxx.result.cardsinfo[0].items[1].content;
        this.product.cusGender=sfxx.result.cardsinfo[0].items[2].content;
        this.product.cusEthnical=sfxx.result.cardsinfo[0].items[3].content;
        this.product.cusBirthday=sfxx.result.cardsinfo[0].items[4].content;
        this.product.cusResAddr=sfxx.result.cardsinfo[0].items[5].content;
        this.product.cusIncard=sfxx.result.cardsinfo[0].items[6].content;
        info.file=null;
      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange2(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }


    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl2 = img;


        var sfxx=JSON.parse(info.file.response[0].piccontent)

        console.log("varfrom*********"+JSON.stringify(sfxx));
        this.product.cusIdValidfrom= sfxx.result.cardsinfo[0].items[3].content;
        this.product.cusIdValidto=sfxx.result.cardsinfo[0].items[4].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange3(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl3 = img;

        var sfxx=JSON.parse(info.file.response[0].piccontent)

        this.product.dyptype = sfxx.result.cardsinfo[0].items[5].content +"/"+  sfxx.result.cardsinfo[0].items[2].content;

        this.product.dypname = sfxx.result.cardsinfo[0].items[1].content;
        this.product.dypvalue = sfxx.result.cardsinfo[0].items[3].content;
        this.product.dypyear =  sfxx.result.cardsinfo[0].items[8].content + "/"+sfxx.result.cardsinfo[0].items[9].content;
        this.product.dypclass =  sfxx.result.cardsinfo[0].items[10].content;

        this.product.dypremark1 = sfxx.result.cardsinfo[0].items[4].content;
        this.product.dypremark2 = sfxx.result.cardsinfo[0].items[6].content;
        this.product.dypremark3   = sfxx.result.cardsinfo[0].items[7].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange4(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl4 = img;
        var sfxx=JSON.parse(info.file.response[0].piccontent)

        console.log("varfrom*********"+JSON.stringify(sfxx));
        this.product.emergencyContact=sfxx.result.cardsinfo[0].items[1].content;

        this.product.emergencyResAddr=sfxx.result.cardsinfo[0].items[5].content;
        this.product.emergencyOffice=sfxx.result.cardsinfo[0].items[6].content;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handleChange5(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl5 = img;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }
  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  }

}
