import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import {SimpleTableButton, SimpleTableColumn, SimpleTableComponent} from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { workbenchService } from '../../../service/workbench/workbench.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';
import {formatDate} from "@angular/common";




@Component({
  selector: 'workbench-list',
  templateUrl: './workbench-list.component.html',
  styleUrls: ['./workbench-list.component.less']
})
export class workbenchListComponent implements OnInit, OnDestroy {
  selectedIndex = 0;
  onSw = 0;
  // full = true;
  // region 公共字段
  workbench:any={
    fxjOut11: "0",
    fxjOut12: "0",
    fxjOut13: "0",
    fxjOut14: "0",
    fxjOut15: "0",
    fxjOut16: "0",
    fxjOut17: "0",
  };

  list = [];

  loading = false;
  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  // 搜索表单
  q: any = {
    fxjOut01: "",
    fxjOut02: "",
    fxjOut03: "",
    fxjOut04: "",
    fxjOut05: "",
    fxjOut06: "",
    fxjOut07: "",
    fxjOut08: null,//日期范围
    fxjOut09:'',//开始时间
    fxjOut10:''//结束时间
  };

  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.workbenchService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  seasons = [];
  checkedList = [];
  data = [];

  columns: SimpleTableColumn[] = [

    // {
    //   title: '操作',
    //   buttons: [
    //     {
    //       text: '审批',
    //       type: 'link',
    //       click: (record: any) => this.router.navigate([record.wbUrl+record.id]),
    //       iif: (item: any, btn: SimpleTableButton, column: SimpleTableColumn) => {
    //         return item.bpmStatus === '1'
    //       }
    //     },
    //     {
    //       text: '查看',
    //       type: 'link',
    //       click: (record: any) => this.router.navigate(['/prod/product/view/'+record.id])
    //     }                                                                                                                                                                                                                                                                                                                          
    //   ]
    // } ,
     // { title: '', index: 'key', type: 'checkbox',exported:false },
     { title: '订单编号', index: 'orderNo',width:500 },
     { title: '合同编号', index: 'conNo' },
    { title: '履约情况', index: 'orderBy86' },
    { title: '档案编号', index: 'orderBy85' },
     { title: '订单状态', index: 'orderStatus' },
     { title: '产品', index: 'proClass' },
     { title: '代理模式', index: 'orderBy1' },
     { title: '客户姓名 ', index: 'cusName'},
     { title: '身份证号 ', index: 'cusIncard' },
     { title: '联系电话', index: 'cusMobile' },
     { title: '联系地址 ', index: 'cusResAddr' },
     { title: '车牌', index: 'orderCarno' },
     { title: '品牌型号', index: 'orderCartype' },
     { title: '车身颜色', index: 'orderBy37' },
     { title: '所属公司', index: 'orderBy35' },
     { title: '合同金额 ', index: 'orderBy48' },
     { title: '申请期数 ', index: 'orderPeriod' },
     { title: '录入业务员  ' , index: 'qiandanUser' },
     { title: '订单提交时间', index: 'createDate' },
     { title: '审批人员', index: 'updateBy' }
   ];

  // uploadUrl = this.workbenchService.uploadProds();
  uploadDateUrl = this.workbenchService.batchRepdate();

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private workbenchService: workbenchService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  inter
  ngOnInit() {
    this.imgSuffix = this.comm.getGoodsUrl();

    // this.emit.getMessage().subscribe(data => {
    //   if (data.text === 'RELOAD_GOODS_LIST') {
    //     this.st.load(1, this.q);
    //   }
    // });
    this.geiFkBi();
  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }


  geiFkBi(){
    console.log('a');
    this.workbenchService.getYxFkBiUrl(this.q.fxjOut09,this.q.fxjOut10).pipe(catchError(err => {
      return of(err)
    })).subscribe(data => {
      if(data.ok){
        console.log(data);
        this.loading = false;
        if(data.data.length){
          this.list = data.data;
          // this.workbench.fxjOut11 = data.data[0].fxjOut11;
          // this.workbench.fxjOut12=data.data[0].fxjOut12;
          // this.workbench.fxjOut13=data.data[0].fxjOut13;
          // this.workbench.fxjOut14=data.data[0].fxjOut14;
          // this.workbench.fxjOut15=data.data[0].fxjOut15;
          // this.workbench.fxjOut16=data.data[0].fxjOut16;
          // this.workbench.fxjOut17=data.data[0].fxjOut17;
          // this.workbench.fxjOut18=data.data[0].fxjOut18;
          // this.workbench.fxjOut19=data.data[0].fxjOut19;
        }
      }
    });
  }

  reloadBi(){
    this.loading = true;
    this.geiFkBi();
    this.st.load(1, this.q);
  }




  checkboxChange(list: any[]) {
    this.checkedList = list;
  }


  close(): void {
    this.visible = false;
  }



  cancel() {
    this.repdate = null;
  }
  repdate = null;

  change(args) {
    this.selectedIndex = args.index;
  }

  
  dateChange(result: Date[]): void {
    if (result.length !== 0) {
      this.q.fxjOut09 = formatDate(result[0], 'yyyy-MM-dd', 'en-US');
      this.q.fxjOut10 = formatDate(result[1], 'yyyy-MM-dd', 'en-US');
    } else {
      this.q.fxjOut09 = '';
      this.q.fxjOut10 = '';
    }
  }

}
