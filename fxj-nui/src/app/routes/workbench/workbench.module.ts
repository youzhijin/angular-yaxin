import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { workbenchRoutingModule } from './workbench-routing.module';
import { workbenchListComponent } from './workbench-list/workbench-list.component';
import { workbenchEditComponent } from './workbench-list/edit/workbench-edit.component';
import {workbenchrmComponent} from './workbench-list/edit/workbench-rm.component';

const COMPONENTS = [
  workbenchListComponent
];
const COMPONENTS_NOROUNT = [
  workbenchEditComponent,
  workbenchrmComponent
];

@NgModule({
  imports: [
    SharedModule,
    workbenchRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class workbenchModule { }
