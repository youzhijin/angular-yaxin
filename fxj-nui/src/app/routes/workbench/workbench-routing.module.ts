import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { workbenchListComponent } from './workbench-list/workbench-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import {workbenchEditComponent} from './workbench-list/edit/workbench-edit.component';
import {workbenchrmComponent} from './workbench-list/edit/workbench-rm.component';

const routes: Routes = [
  {
    path: 'workbench',
    children: [
      { path: '', component: workbenchListComponent },
      { path: 'edit/:id', component: workbenchEditComponent },
      { path: 'rm/:id', component: workbenchrmComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class workbenchRoutingModule { }
