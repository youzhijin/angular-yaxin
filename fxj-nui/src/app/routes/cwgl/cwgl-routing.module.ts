import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { cwglListComponent } from './cwgl-list/cwgl-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
//import { cwglViewEditComponent } from 'app/routes/cwgl/cwgl-list/edit/cwgl-edit.component';

const routes: Routes = [
  {
    path: 'cwgl',
    children: [
      { path: '', component: cwglListComponent },
      //{ path: 'edit/:id', component: cwglViewEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class cwglRoutingModule { }
