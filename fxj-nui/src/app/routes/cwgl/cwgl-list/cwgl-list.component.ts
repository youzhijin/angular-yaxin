import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { cwglService } from '../../../service/cwgl/cwgl.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';




@Component({
  selector: 'cwgl-list',
  templateUrl: './cwgl-list.component.html',
  styleUrls: ['./cwgl-list.component.less']
})
export class cwglListComponent implements OnInit, OnDestroy {
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    bpmStatus:"",

    fiName: '',
    cusMobile: '',
    cusIncard: '',
    orderNo: ''
  };
  wzbShow = true;
  wzOfData = [
    {
      id:'1',
      cxsj:'2018-01-01 12:01',
      khxm:'彭于晏',
      khdh:'13805920000',
      khcs:'XIAMEN',
      khcp:'闽D88888',
      khzfk:'533元',
      khzf:'25分',
      khtxfs:''
    },{
      id:'2',
      cxsj:'2018-01-02 12:01',
      khxm:'彭于晏a',
      khdh:'13805920000',
      khcs:'XIAMEN',
      khcp:'闽D88888',
      khzfk:'533元',
      khzf:'25分',
      khtxfs:''
    },
  ]

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.cwglService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  seasons = [];
  checkedList = [];
  data = [];

  // uploadUrl = this.maService.uploadProds();
  uploadDateUrl = this.cwglService.batchRepdate();



  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private cwglService: cwglService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private cwgl: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  inter
  ngOnInit() {
    this.imgSuffix = this.comm.getGoodsUrl();

    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })


  }
  // 违章查询记录下载
  wzDownLoad(){
    console.log('违章查询记录下载');
  }
  // 违章查询记录发送
  wzSend(){
    console.log('违章查询记录发送');
  }

  // 短信提醒
  dxWarn(e){
    console.log('短信提醒'+e);
  }
  // 微信提醒
  wxWarn(e){
    console.log('微信提醒'+e);
  }
  // 电话提醒
  dhWarn(e){
    console.log('电话提醒'+e);
  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }



  change(args: NzTabChangeEvent) {
        switch (args.index) {
          case 0:
            this.q.bpmStatus = "";
            break;
          case 1:
            this.q.bpmStatus = 1;
            break;
          case 2:
            this.q.bpmStatus = 10;
            break;
          case 3:
            this.q.bpmStatus = 20;
            break;

          case 4:
            this.q.bpmStatus = 30;
            break;
          case 5:
            this.q.bpmStatus = 40;
            break;
          case 6:
            this.q.bpmStatus = 50;
            break;
          case 7:
            this.q.bpmStatus = 60;
            break;
          case 8:
            this.q.bpmStatus = 70;
            break;
          case 9:
            this.q.bpmStatus = 80;
            break;
          case 10:
            this.q.bpmStatus = 90;
            break;
          case 11:
            this.q.bpmStatus = 99;
            break;
          case 12:
            this.q.bpmStatus = 100;
            break;
          default:
            break;
        }
      this.st.load(1, this.q);
  }



  checkboxChange(list: any[]) {
    this.checkedList = list;
  }


  close(): void {
    this.visible = false;
  }



  cancel() {
    this.repdate = null;
  }
  repdate = null;



}
