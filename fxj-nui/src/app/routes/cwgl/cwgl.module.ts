import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { cwglRoutingModule } from './cwgl-routing.module';
import { cwglListComponent } from './cwgl-list/cwgl-list.component';
//import { cwglViewEditComponent } from 'app/routes/cwgl/cwgl-list/edit/cwgl-edit.component';

const COMPONENTS = [
  cwglListComponent
];
const COMPONENTS_NOROUNT = [
  //cwglViewEditComponent
];

@NgModule({
  imports: [
    SharedModule,
    cwglRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class cwglModule { }
