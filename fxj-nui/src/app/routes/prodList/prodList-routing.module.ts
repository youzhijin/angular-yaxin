import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { prodListComponent } from './prodList-list/prodList-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { prodListEditComponent } from 'app/routes/prodList/prodList-list/edit/prodList-edit.component';
import {ProductAddoneComponent} from "./prodList-list/edit/product-addone.component";

const routes: Routes = [
  {
    path: 'prodList',
    children: [
      { path: '', component: prodListComponent },
      { path: 'addone/:id', component: ProductAddoneComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class prodListRoutingModule { }
