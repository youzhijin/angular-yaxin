import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { prodListRoutingModule } from './prodList-routing.module';
import { prodListComponent } from './prodList-list/prodList-list.component';
import { prodListEditComponent } from 'app/routes/prodList/prodList-list/edit/prodList-edit.component';
import { ProductAddoneComponent } from "./prodList-list/edit/product-addone.component";

const COMPONENTS = [
  prodListComponent
];
const COMPONENTS_NOROUNT = [
  prodListEditComponent,
  ProductAddoneComponent
];

@NgModule({
  imports: [
    SharedModule,
    prodListRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class prodListModule { }
