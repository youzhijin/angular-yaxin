import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { maService } from '../../../service/ma/ma.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';
import {prodListService} from "../../../service/prodList/prodList.service";




@Component({
  selector: 'prodList-list',
  templateUrl: './prodList-list.component.html',
  styleUrls: ['./prodList-list.component.less']
})
export class prodListComponent implements OnInit, OnDestroy {
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    bpmStatus:"",

    fiName: '',
    cusMobile: '',
    cusIncard: '',
    orderNo: ''
  };

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.prodListService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  seasons = [];
  checkedList = [];
  data = [];
  // uploadUrl = this.maService.uploadProds();
  uploadDateUrl = this.maService.batchRepdate();
  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '产品编号', index: 'proNo' },
    { title: '产品名称', index: 'proName'},
    { title: '产品种类', index: 'proClass' },
    { title: '融租类型', index: 'proType' },
    { title: '最低期数' , index: 'proMinPeriod' },
    { title: '最高期数', index: 'proMaxPeriod' },
    // { title: '附件', index: 'fiAtta' },
    { title: '最低贷款比例', index: 'proMinLoanratio' },
    {
        title: '操作',
        buttons: [
            {
              text: '立即下单',
              type: 'link',
              click: (record: any) => this.router.navigate(['/prodList/prodList/addone/'+record.proNo])
            }
        ]
    }
  ];

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private maService: maService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private prodListService: prodListService,
    private notice: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }
  inter
  ngOnInit() {
    this.imgSuffix = this.comm.getGoodsUrl();
    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })
  }
  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }
  change(args: NzTabChangeEvent) {
        switch (args.index) {
          case 0:
            this.q.bpmStatus = "";
            break;
          case 1:
            this.q.bpmStatus = 1;
            break;
          case 2:
            this.q.bpmStatus = 10;
            break;
          case 3:
            this.q.bpmStatus = 20;
            break;
          case 4:
            this.q.bpmStatus = 30;
            break;
          case 5:
            this.q.bpmStatus = 40;
            break;
          case 6:
            this.q.bpmStatus = 50;
            break;
          case 7:
            this.q.bpmStatus = 60;
            break;
          case 8:
            this.q.bpmStatus = 70;
            break;
          case 9:
            this.q.bpmStatus = 80;
            break;
          case 10:
            this.q.bpmStatus = 90;
            break;
          case 11:
            this.q.bpmStatus = 99;
            break;
          case 12:
            this.q.bpmStatus = 100;
            break;
          default:
            break;
        }
      this.st.load(1, this.q);
  }
  checkboxChange(list: any[]) {
    this.checkedList = list;
  }
  close(): void {
    this.visible = false;
  }
  cancel() {
    this.repdate = null;
  }
  repdate = null;
}
