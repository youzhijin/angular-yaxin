import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '../../../core/constant';
import { EmitterService } from 'app/service/emit.service';
import {ActivatedRoute, Router} from '@angular/router';
import { plopService } from '../../../service/plop/plop.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '../../../shared/descriptor/timeout';
@Component({
  selector: 'plop-list',
  templateUrl: './plop-list.component.html',
  styleUrls: ['./plop-list.component.less']
})
export class plopListComponent implements OnInit, OnDestroy {
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    bpmStatus:"",

    cusName: '',
    cusMobile: '',
    cusIncard: '',
    orderNo: ''
  };
  bpmStatus =  [{text: '未处理', value: '1'}, {text: '已处理', value: '10'}];

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.plopService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';


  data = [];

  uploadUrl = this.plopService.uploadProds();
  uploadDateUrl = this.plopService.batchRepdate();


  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '订单编号', index: 'orderNo' },
    { title: '客户姓名', index: 'cusName'},
    { title: '联系电话', index: 'cusMobile' },
    { title: '逾期期数 ', index: 'overduePeriod' },
    { title: '逾期金额' , index: 'overdueAmount' },
    { title: '违约金', index: 'penaltyAmount' },
    { title: '操作人', index: 'overdueUser' },
    { title: '处理方法', index: 'overdueContent' },
    { title: '处理内容', index: 'overdueRemark' },

    { title: '操作日期', index: 'overdueDate' },
    // { title: '年份', index: 'year' },
    { title: '状态', index: 'bpmStatus', render: 'bpmStatus' },
    // { title: '定制', index: 'isCm', render: 'isCm' },
    // { title: '状态', index: 'status', type: 'badge', badge: {
    //     '1': { text: '上架', color: 'success' },
    //     '0': { text: '下架', color: 'error' }
    //   }
    // },
    {
        title: '操作',
        buttons: [
            {
              text: '编辑',
              type: 'link',
              click: (record: any) => this.router.navigate(['/plop/plop/edit/'+record.id])
            }
        ]
    }
  ];

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private plopService: plopService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
    private route: ActivatedRoute,

  ) {}
  syncing = false;
  lastDate = null;
  private sub: any;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  inter
  ngOnInit() {
    this.imgSuffix = this.comm.getGoodsUrl();
    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      let varinpur = params["id"];
      if(varinpur){
        this.q.orderNo = params["id"];
      }
    });
    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })

  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }


  back() {
    this.router.navigate(['/prod/product/80']);
  }
  change(args: NzTabChangeEvent) {
        switch (args.index) {
          case 0:
            this.q.bpmStatus = "";
            break;
          case 1:
            this.q.bpmStatus = 1;
            break;
          case 2:
            this.q.bpmStatus = 10;
            break;
          case 3:
            this.q.bpmStatus = 20;
            break;

          case 4:
            this.q.bpmStatus = 30;
            break;
           default:
            break;
        }
      this.st.load(1, this.q);
  }
  showImage(src) {
    this.previewImage = src;
    this.previewVisible = true;
  }

  gotoAdd() {
    setTimeout(() => {
      this.router.navigate(['/plop/plop/add/'+this.q.orderNo]);
    }, 500);
  }

  close(): void {
    this.visible = false;
  }

  cancel() {
    this.repdate = null;
  }
  repdate = null;


}
