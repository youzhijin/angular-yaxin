import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { plopListComponent } from './plop-list/plop-list.component';
import { AuthGuardService } from '../../core/auth/auth-guard.service';
import { plopAddComponent } from 'app/routes/plop/plop-list/edit/plop-add.component';
import { plopViewEditComponent } from 'app/routes/plop/plop-list/edit/plop-edit.component';
import { GoodsImgUploadComponent } from '../plo/goods-upload/goods-img-upload.component';

const routes: Routes = [
  {
    path: 'plop',
    children: [
      { path: '', component: plopListComponent },
      { path: 'query/:id', component: plopListComponent },

      { path: 'add/:id', component: plopAddComponent },
      { path: 'edit/:id', component: plopViewEditComponent },
      { path: 'upload', component: GoodsImgUploadComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class plopRoutingModule { }
