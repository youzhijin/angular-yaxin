import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { plopRoutingModule } from './plop-routing.module';
import { plopListComponent } from './plop-list/plop-list.component';
import { plopAddComponent } from 'app/routes/plop/plop-list/edit/plop-add.component';
import { plopViewEditComponent } from 'app/routes/plop/plop-list/edit/plop-edit.component';
import { GoodsImgUploadComponent } from '../plo/goods-upload/goods-img-upload.component';

const COMPONENTS = [
  plopListComponent,
  GoodsImgUploadComponent
];
const COMPONENTS_NOROUNT = [
  plopAddComponent,
  plopViewEditComponent,
];

@NgModule({
  imports: [
    SharedModule,
    plopRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class plopModule { }
