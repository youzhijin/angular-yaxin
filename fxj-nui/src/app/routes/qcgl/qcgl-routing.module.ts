import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { qcglListComponent } from './qcgl-list/qcgl-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { qcglViewEditComponent } from 'app/routes/qcgl/qcgl-list/edit/qcgl-edit.component';

const routes: Routes = [
  {
    path: 'qcgl',
    children: [
      { path: '', component: qcglListComponent },
      { path: 'edit/:id', component: qcglViewEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class qcglRoutingModule { }
