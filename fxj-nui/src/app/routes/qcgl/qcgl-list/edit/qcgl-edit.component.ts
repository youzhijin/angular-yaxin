import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { qcglService } from '../../../../service/qcgl/qcgl.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {insService} from "../../../../service/ins/ins.service";
import {ProductService} from "../../../../service/prod/product.service";

@Component({
  selector: 'qcgl-view-edit',
  templateUrl: './qcgl-edit.component.html',
  styleUrls: ['./qcgl-edit.component.less']
})
export class qcglViewEditComponent implements OnInit, OnDestroy {
  product:  any = {
    id:null,
    orderNo: null,
    bpmStatus:null,
    fiName: null,
    fiOperView: null,
    fiDueDate: null,
    fiOperUser: null /*No show*/,
    fiOperDate: null
  };
  bpmStatus =  [{text: '未抵押', value: '1'}, {text: '已抵押', value: '10'}];

  MAX_COUNT = 60;
  tabIndex = 0;
  saleing = false;
  private sub: any;

  // 预览图片对象
  previewImage = '';
  // 是否可以预览
  previewVisible = false;

  uploadUrl = '';
  avatarParam = { sbtype: 'No',  filetype:'ma',orderNo: 'no'};
  fileList = [ ];
  avatarUrl:string;
  loading = false;


  constructor(
    private productService: ProductService,

    public setting: SettingsService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private qcglService: qcglService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService
  ) {}

  ngOnInit(): void {

    // this.loadbpmStatus();

    this.uploadUrl = this.productService.uploadUrl();

    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"]
      console.log("this.product.id****************"+this.product.id);
      // 获取商品详情
      this.getProductInfo();
      // this.getfileList(this.product.id);

    });
    // 隐藏reusetab
    setTimeout(() => {
      this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 1000);
  }
  loadbpmStatus() {
    this.bpmStatus = [];
    this.comm.getType("bpm_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.bpmStatus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  changeTab(index) {
    // 使用异步的方式更新组件, 防止ngIf错误
    Promise.resolve(null).then(() => this.tabIndex = index);
  }



  getProductInfo() {
    this.qcglService.getProduct(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.product = data.data;
      this.avatarParam = { sbtype: 'No',  filetype:'ma',orderNo: this.product.orderNo};
      this.titleService.setTitle("编辑产品");
      this.getfileList(this.product.orderNo);
    })
  }
  getfileList(id) {
    this.productService.getfileList(id,"ma").pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {

console.log("qcgl*****************"+JSON.stringify(data));

      if (data.data.length > 0) {
        let flag = 0;
        for (const detail of data.data) {
          this.fileList.push( {
            uid: flag,
            name: detail.attaName,
            status: 'done',
            url: detail.attaUrl
          } )
          flag++;
        }
      };
    })
  }




  back() {
    this.router.navigate(['/qcgl/qcgl']);
  }


  save() {


    let fxjLoanins = {
      updateBy:this.setting.user.username,
      id:this.product.id,
      bpmStatus:this.product.bpmStatus,
      fiName:this.product.fiName,
      // orderNo: this.product.orderNo,
      fiOperView: this.product.fiOperView,
      fiDueDate: this.product.fiDueDate,
      fiOperUser: this.setting.user.username
      // fiOperDate: this.product.fiOperDate /*No show*/
    };

    // 调用接口
    this.qcglService.editPro(fxjLoanins).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if (data.ok === true) {
        this.msg.success('修改成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data['message']);
      }
    })
  }

  changeColor(res) {
    this.product.colorSort = res.color.hex;
  }

  beforeUpload = (file: File) => {
    const isJPG = file.type === 'image/jpeg';
    const isPNG = file.type === 'image/png';
    if (!isJPG && !isPNG) {
      this.msg.warning(`不支持的格式: ${file.type}`);
    }
    const isLt2M = file.size / 1024 / 1024 < 10;
    if (!isLt2M) {
      this.msg.warning('图片不能超过2MB!');
    }
    return (isJPG || isPNG) && isLt2M;
  }

  private getBase64(img: File, callback: (img: any) => void) {
    if (img) {
      const reader = new FileReader();
      reader.addEventListener('load', () => callback(reader.result));
      reader.readAsDataURL(img);
    }
  }

  handleChange(info: { file: UploadFile }) {
    if (info.file.status === 'uploading') {
      this.loading = true;
      return;
    }
    if (info.file.status === 'done') {
      this.getBase64(info.file.originFileObj, (img: any) => {
        this.loading = false;
        this.avatarUrl = img;

      });
    }
    if (info.file.status === 'error') {
      this.msg.error(info.file.error.error);
    }
  }

  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  }
}
