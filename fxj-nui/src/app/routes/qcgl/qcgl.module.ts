import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { qcglRoutingModule } from './qcgl-routing.module';
import { qcglListComponent } from './qcgl-list/qcgl-list.component';
import { qcglViewEditComponent } from 'app/routes/qcgl/qcgl-list/edit/qcgl-edit.component';

const COMPONENTS = [
  qcglListComponent
];
const COMPONENTS_NOROUNT = [
  qcglViewEditComponent
];

@NgModule({
  imports: [
    SharedModule,
    qcglRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class qcglModule { }
