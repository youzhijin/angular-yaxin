import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { repayListComponent } from './repay-list/repay-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { repayViewEditComponent } from 'app/routes/repay/repay-list/edit/repay-edit.component';

const routes: Routes = [
  {
    path: 'repay',
    children: [
      { path: '', component: repayListComponent },
      { path: 'edit/:id', component: repayViewEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class repayRoutingModule { }
