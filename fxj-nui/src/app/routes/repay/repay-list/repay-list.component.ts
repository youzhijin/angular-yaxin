import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { repayService } from '../../../service/repay/repay.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';




@Component({
  selector: 'repay-list',
  templateUrl: './repay-list.component.html',
  styleUrls: ['./repay-list.component.less']
})
export class repayListComponent implements OnInit, OnDestroy {
  sjtj = 123; //事件统计
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    bpmStatus:"",
    orderNo:""
  };

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.repayService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  seasons = [];
  checkedList = [];
  data = [];

  // uploadUrl = this.repayService.uploadProds();
  uploadDateUrl = this.repayService.batchRepdate();




  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '订单编号', index: 'orderNo' },
    { title: '客户姓名', index: 'orderNo' },
    { title: '订单类型', index: 'orderNo' },
    { title: '联系电话', index: 'orderNo' },
    { title: '车辆信息', index: 'orderNo' },
    { title: '评估价值', index: 'orderNo' },
    { title: 'GPS状态', index: 'orderNo' },
    { title: '贷款期数', index: 'orderNo' },
    { title: '融资金额', index: 'orderNo' },
    { title: '融资利息', index: 'orderNo' },
    { title: '已还期数', index: 'orderNo' },
    { title: '已还金额', index: 'orderNo' },
    { title: '未还金额', index: 'orderNo' },
    { title: '录入单位', index: 'orderNo' },
    { title: '负责人', index: 'orderNo' },
    { title: '联系电话', index: 'orderNo' },
    { title: '放款时间', index: 'orderNo' },
    { title: '到期还款日', index: 'orderNo' },
    { title: 'MOB状态', index: 'orderNo' },
    {
        title: '操作',
        buttons: [
            {
              text: '查看',
              type: 'link',
              click: (record: any) => this.router.navigate(['/repay/repay/edit/'+record.id])
            },{
              text: '提醒',
              type: 'link',
              click: (record: any) => this.router.navigate(['/repay/repay/edit/'+record.id])
            }
        ]
    }
  ];
  columnsa: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '订单编号', index: 'orderNo' },
    { title: '客户姓名', index: 'orderNo' },
    { title: '订单类型', index: 'orderNo' },
    { title: '联系电话', index: 'orderNo' },
    { title: '车辆信息', index: 'orderNo' },
    { title: '评估价值', index: 'orderNo' },
    { title: 'GPS状态', index: 'orderNo' },
    { title: '贷款期数', index: 'orderNo' },
    { title: '融资金额', index: 'orderNo' },
    { title: '融资利息', index: 'orderNo' },
    { title: '已还期数', index: 'orderNo' },
    { title: '已还金额', index: 'orderNo' },
    { title: '未还金额', index: 'orderNo' },
    { title: '录入单位', index: 'orderNo' },
    { title: '负责人', index: 'orderNo' },
    { title: '联系电话', index: 'orderNo' },
    { title: '放款时间', index: 'orderNo' },
    { title: '到期还款日', index: 'orderNo' },
    { title: 'MOB状态', index: 'orderNo' },
    {
        title: '操作',
        buttons: [
            {
              text: '查看',
              type: 'link',
              click: (record: any) => this.router.navigate(['/repay/repay/edit/'+record.id])
            },{
              text: '提醒',
              type: 'link',
              click: (record: any) => this.router.navigate(['/repay/repay/edit/'+record.id])
            }
        ]
    }
  ];
  columnsb: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '订单编号', index: 'orderNo' },
    { title: '客户姓名', index: 'orderNo' },
    { title: '订单类型', index: 'orderNo' },
    { title: '联系电话', index: 'orderNo' },
    { title: '车辆信息', index: 'orderNo' },
    { title: '评估价值', index: 'orderNo' },
    { title: 'GPS状态', index: 'orderNo' },
    { title: '贷款期数', index: 'orderNo' },
    { title: '融资金额', index: 'orderNo' },
    { title: '融资利息', index: 'orderNo' },
    { title: '已还期数', index: 'orderNo' },
    { title: '已还金额', index: 'orderNo' },
    { title: '未还金额', index: 'orderNo' },
    { title: '录入单位', index: 'orderNo' },
    { title: '负责人', index: 'orderNo' },
    { title: '联系电话', index: 'orderNo' },
    { title: '放款时间', index: 'orderNo' },
    { title: '到期还款日', index: 'orderNo' },
    { title: 'MOB状态', index: 'orderNo' },
    {
        title: '操作',
        buttons: [
            {
              text: '查看',
              type: 'link',
              click: (record: any) => this.router.navigate(['/repay/repay/edit/'+record.id])
            },{
              text: '催收',
              type: 'link',
              click: (record: any) => this.router.navigate(['/repay/repay/edit/'+record.id])
            }
        ]
    }
  ];

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private repayService: repayService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  inter
  ngOnInit() {
    this.imgSuffix = this.comm.getGoodsUrl();

    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })


  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }



  change(args: NzTabChangeEvent) {
        switch (args.index) {
          case 0:
            this.q.bpmStatus = "";
            this.q.fxjOut01 = "待还款订单";
            this.columns = this.columnsa;
            break;
          case 1:
            // this.q.bpmStatus = 1;
            this.q.fxjOut01 = "逾期订单";
            this.columns = this.columnsb;
            break;
          case 2:
            // this.q.bpmStatus = 10;
            this.q.fxjOut01 = "提前还款申请";
            this.columns = this.columnsa;
            break;
          case 3:
            // this.q.bpmStatus = 20;
            this.q.fxjOut01 = "展期还款申请";
            this.columns = this.columnsa;
            break;

          case 4:
            // this.q.bpmStatus = 30;
            this.q.fxjOut01 = "代偿订单申请";
            this.columns = this.columnsa;
            break;
          default:
            break;
        }
      this.st.load(1, this.q);
  }



  checkboxChange(list: any[]) {
    this.checkedList = list;
  }


  close(): void {
    this.visible = false;
  }



  cancel() {
    this.repdate = null;
  }
  repdate = null;



}
