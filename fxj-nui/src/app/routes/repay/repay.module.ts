import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { repayRoutingModule } from './repay-routing.module';
import { repayListComponent } from './repay-list/repay-list.component';
import { repayViewEditComponent } from 'app/routes/repay/repay-list/edit/repay-edit.component';

const COMPONENTS = [
  repayListComponent
];
const COMPONENTS_NOROUNT = [
  repayViewEditComponent
];

@NgModule({
  imports: [
    SharedModule,
    repayRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class repayModule { }
