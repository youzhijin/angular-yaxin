import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { sdRoutingModule } from './sd-routing.module';
import { sdListComponent } from './sd-list/sd-list.component';
import {sdHeadComponent} from "./sd-list/edit/sd-head.component";
import {sdviewComponent} from "./sd-list/edit/sdview.component";
import {sdpoListComponent} from "./sd-list/sdpo-list.component";
import {sdsdListComponent} from "./sd-list/sdsd-list.component";
import {sdficoListComponent} from "./sd-list/sdfico-list.component";
import {sdcsListComponent} from "./sd-list/sdcs-list.component";
import {sdpoviewComponent} from "./sd-list/edit/sdpoview.component";
import {sdsdviewComponent} from "./sd-list/edit/sdsdview.component";
import {sdcsviewComponent} from "./sd-list/edit/sdcsview.component";
import {sdficoviewComponent} from "./sd-list/edit/sdficoview.component";

const COMPONENTS = [
  sdpoListComponent,
  sdsdListComponent,
  sdficoListComponent,
  sdcsListComponent,

  sdListComponent
];
const COMPONENTS_NOROUNT = [
  sdHeadComponent,
  sdpoviewComponent,
  sdsdviewComponent,
  sdficoviewComponent,
  sdcsviewComponent,
  sdviewComponent

];

@NgModule({
  imports: [
    SharedModule,
    sdRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class sdModule { }
