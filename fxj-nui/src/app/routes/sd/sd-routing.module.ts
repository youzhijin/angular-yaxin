import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import {sdListComponent} from "./sd-list/sd-list.component";
import {sdHeadComponent} from "./sd-list/edit/sd-head.component";
import {sdviewComponent} from "./sd-list/edit/sdview.component";
import {sdpoListComponent} from "./sd-list/sdpo-list.component";
import {sdsdListComponent} from "./sd-list/sdsd-list.component";
import {sdficoListComponent} from "./sd-list/sdfico-list.component";
import {sdcsListComponent} from "./sd-list/sdcs-list.component";
import {sdpoviewComponent} from "./sd-list/edit/sdpoview.component";
import {sdsdviewComponent} from "./sd-list/edit/sdsdview.component";
import {sdcsviewComponent} from "./sd-list/edit/sdcsview.component";
import {sdficoviewComponent} from "./sd-list/edit/sdficoview.component";

const routes: Routes = [
  {
    path: 'sd',
    children: [
      { path: 'list', component: sdListComponent },
      { path: 'polist', component: sdpoListComponent },
      { path: 'sdlist', component: sdsdListComponent },
      { path: 'ficolist', component: sdficoListComponent },
      { path: 'sdcslist', component: sdcsListComponent },

      { path: 'sdhead/:id', component: sdHeadComponent },
      { path: 'sdview/:id', component: sdviewComponent },
      { path: 'sdpoview/:id', component: sdpoviewComponent },
      { path: 'sdsdview/:id', component: sdsdviewComponent },
      { path: 'sdcsview/:id', component: sdcsviewComponent  },
      { path: 'sdficoview/:id', component: sdficoviewComponent  }

      // { path: 'querydetail/:id', component: dhcardetailViewEditComponent },

      // { path: 'queryone', component: dhcarViewEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class sdRoutingModule { }
