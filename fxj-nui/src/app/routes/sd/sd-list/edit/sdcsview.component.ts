import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { ProductService } from '../../../../service/prod/product.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {SimpleTableColumn, SimpleTableComponent} from "@delon/abc";
import {sdService} from "../../../../service/sd/sd.service";

@Component({
  selector: 'sdcsview',
  templateUrl: './sdcsview.component.html',
  styleUrls: ['./sdcsview.component.less']
})
export class sdcsviewComponent implements OnInit, OnDestroy {

  sdcsplan:  any = {
    csType:null,
    csNo: null,
    csDate:null,
    csContent:null,
    csUser: null
  }

  sdcs:  any = {
    csaType:null,
    csaNo: null,
    orderNo:null,
    csaContent:null,
    csaUser: null
  }


  product:  any = {
    id:null,
    carType: null,
    carYear:null,
    carNo:null,
    carName: null,
    carInfo: null,
    carVin: null,
    carEnno: null /*No show*/,
    carLsnumo: null /*No show*/,
    carLsnumn1: null /*No show*/,
    carLsnumn2: null,
    carPoPrice: null,
    carSdPrice: null /*No show*/,
    carSdzdPrice: null /*No show*/,
    carSdsjPrice: null,
    carXslrPrice: null,
    carYszk: null,
    carSk: null /*No show*/,
    carYfzk: null,
    carFk: null,
    carRkData: null,
    carCkData: null,
    carZkData: null,
    carPoUser: null,
    carSdUser:null,
    carStatus: null,
    carStore: null,
    carSdDis: null,
    poType:null,
    sdType:null,
    carChezhu1:null,
    carChezhu2:null,
    carChezhu3:null,
    jqxRq:null,
    bxRq:null,
    lqRq:null,
    carBy1:null,
    carBy2:null,
    carBy3:null
  };
  ps = 100;
  // 搜索表单
  q: any = {
    bpmStatus:"",
    cusName: '',
    cusMobile: '',
    cusIncard: '',
    proNo: '',
    orderNo:''
  };



  tabIndex = 0;

  backUrl='/sd/sd/cslist';

  // 预览图片对象
  previewImage = '';
  // 是否可以预览
  previewVisible = false;
  // 预览图片地址
  propName = "";
  picUrl = '';
  url = this.sdService.list();

  url0 = this.sdService.list0(0);

  url1 = this.sdService.list1(1);

  url2 = this.sdService.list2(2);
  url3 = this.sdService.list3(3);

  url4 = this.sdService.list4(4);

  url5 = this.sdService.list5(5);

  url6 = this.sdService.list6(6);

  url7 = this.sdService.list7(7);

  url8 = this.sdService.list8(8);

  url9 = this.sdService.list9(9);



  columns0: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '供应商编码', index: 'vendorCode' },
    { title: '供应商名称', index: 'vendorName' },
    { title: '备注 ', index: 'vendorRemark'}

  ];
  // poprice
  columns1: SimpleTableColumn[] = [

    { title: '类型 ', index: 'prType'},
    { title: '价格 ', index: 'prValue' },
    { title: '是否参与合计', index: 'prCount' }

  ];
  columns2: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '图片URL', index: 'picUrl',render: 'img' },
    { title: '图片名称', index: 'picName' },
  ];

  columns3: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '付款方式', index: 'fkMethod' },
    { title: '付款金额', index: 'fkAmount' },
    { title: '付款日期 ', index: 'fkData'},
    { title: '付款备注 ', index: 'fkRemark' }

  ];

  columns4: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '客户编码', index: 'cusCode' },
    { title: '客户名称', index: 'cusName' },
    { title: '客户电话 ', index: 'cusTel'},
    { title: '备注 ', index: 'cusRemark' }

  ];


  columns5: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '活动类型', index: 'acType' },
    { title: '活动内容', index: 'acContent' },
    { title: '客户编码 ', index: 'cusCode'},
    { title: '客户名称 ', index: 'cusName' },
    { title: '客户电话', index: 'cusMobile' },
    { title: '客户回复', index: 'acReplay' }

  ];

  columns6: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '价格类型', index: 'prType' },
    { title: '价格', index: 'prPrice' },
    { title: '是否参与计算 ', index: 'prCiunt'}

  ];
  columns7: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '收款方式', index: 'skMethod' },
    { title: '收款金额', index: 'skAmount' },
    { title: '收款日期 ', index: 'skData'},
    { title: '收款备注 ', index: 'skRemark' }

  ];



  columns8: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '售后类型', index: 'csType' },
    { title: '售后编号', index: 'csNo' },
    { title: '计划日期 ', index: 'csDate'},
    { title: '售后内容 ', index: 'csContent' },
    { title: '执行人', index: 'csUser' }

  ];


  columns9: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '售后类型', index: 'csaType' },
    { title: '售后服务编号', index: 'csaNo' },
    { title: '编号 ', index: 'orderNo'},
    { title: '内容 ', index: 'csaContent' },
    { title: '执行人', index: 'csaUser' }

  ];
  loading=false;
  pay_meth=[];
  cs_type=[];
  pop_type=[];
  ac_type=[];
  sdp_type=[];
  car_status=[];
  car_store=[];
  car_sddis=[];
  sf_yn=[];
  cgys=[];
  xsys=[];
  sdtypes=[];
  potypes=[];
  // 图片上传地址
  private sub: any;
  constructor(
    public setting: SettingsService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private sdService: sdService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService
  ) {}

  ngOnInit(): void {
    this.loadpay_meth();
this.loadcar_sddis();
this.loadcar_status();
this.loadcar_store();
    this.loadac_type();

this.loadcs_type();
this.loadpop_type();
this.loadsdp_type()
    this.loadsf_yn();
this.loadxsy();
this.loadcgy();
this.loadsd_type();
this.loadpo_type()
    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"];
      // 获取商品详情
      this.getProductInfo();
    });
    // 隐藏reusetab
    setTimeout(() => {
        this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);

  }
  loadsf_yn() {
    this.sf_yn = [];
    this.comm.getType("sf_yn").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.sf_yn.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loadcar_sddis() {
    this.car_sddis = [];
    this.comm.getType("car_sddis").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.car_sddis.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loadcar_store() {
    this.car_store = [];
    this.comm.getType("car_store").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.car_store.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loadcar_status() {
    this.car_status = [];
    this.comm.getType("car_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.car_status.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loadsdp_type() {
    this.sdp_type = [];
    this.comm.getType("sdp_type").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.sdp_type.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loadac_type() {
    this.ac_type = [];
    this.comm.getType("ac_type").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.ac_type.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loadpop_type() {
    this.pop_type = [];
    this.comm.getType("pop_type").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.pop_type.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loadcs_type() {
    this.cs_type = [];
    this.comm.getType("cs_type").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cs_type.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loadpay_meth() {
    this.pay_meth = [];
    this.comm.getType("pay_meth").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.pay_meth.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  loadxsy() {
    this.xsys = [];
    this.comm.getType("xsy").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.xsys.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }  loadcgy() {
    this.cgys = [];
    this.comm.getType("cgy").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.cgys.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }  loadpo_type() {
    this.potypes = [];
    this.comm.getType("po_type").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.potypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }  loadsd_type() {
    this.sdtypes = [];
    this.comm.getType("sd_type").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.sdtypes.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  changeTab(index) {
    // 使用异步的方式更新组件, 防止ngIf错误
    Promise.resolve(null).then(() => this.tabIndex = index);
  }
  getProductInfo() {
    this.sdService.getProduct(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if (data.ok === true) {
        this.product = data.data;
        this.url0 = this.sdService.list0(data.data.carVin);

        this.url1 = this.sdService.list1(data.data.carVin);

        this.url2 = this.sdService.list2(data.data.carVin);
        this.url3 = this.sdService.list3(data.data.carVin);

        this.url4 = this.sdService.list4(data.data.carVin);

        this.url5 = this.sdService.list5(data.data.carVin);

        this.url6 = this.sdService.list6(data.data.carVin);

        this.url7 = this.sdService.list7(data.data.carVin);

        this.url8 = this.sdService.list8(data.data.carVin);

        this.url9 = this.sdService.list9(data.data.carVin);
      }else{
        this.product.id="";
      }

    })
  }
  beforeUpload = (file: File) => {
      const isJPG = file.type === 'image/jpeg';
      const isPNG = file.type === 'image/png';
      if (!isJPG && !isPNG) {
          this.msg.warning(`不支持的格式: ${file.type}`);
      }
      const isLt2M = file.size / 1024 / 1024 < 10;
      if (!isLt2M) {
          this.msg.warning('图片不能超过2MB!');
      }
      return (isJPG || isPNG) && isLt2M;
  }
  private getBase64(img: File, callback: (img: any) => void) {
      if (img) {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
      }
  }
  back() {
    this.router.navigate([ this.backUrl]);
  }
  save() {
    if(this.product.carVin==""||this.product.carVin==null){
      this.msg.warning("请输入VIN");
      return;
    }
    let sdCarinfoPage = {
      createBy:this.setting.user.username,
      sysOrgCode:this.setting.user.sysorgCode,
      id:this.product.id,
      carType: this.product.carType,
      carYear:this.product.carYear,
      carNo:this.product.carNo,
      carName: this.product.carName,
      carInfo: this.product.carInfo,
      carVin: this.product.carVin,
      carEnno: this.product.carEnno /*No show*/,
      carLsnumo: this.product.carLsnumo /*No show*/,
      carLsnumn1: this.product.carLsnumn1 /*No show*/,
      carLsnumn2: this.product.carLsnumn2,
      carPoPrice: this.product.carPoPrice,
      carSdPrice: this.product.carSdPrice /*No show*/,
      carSdzdPrice: this.product.carSdzdPrice /*No show*/,
      carSdsjPrice: this.product.carSdsjPrice,
      carXslrPrice: this.product.carXslrPrice,
      carYszk: this.product.carYszk,
      carSk: this.product.carSk /*No show*/,
      carYfzk: this.product.carYfzk,
      carFk: this.product.carFk,
      carRkData: this.product.carRkData,
      carCkData: this.product.carCkData,
      carZkData: this.product.carZkData,
      carPoUser: this.product.carPoUser,
      carSdUser:this.product.carSdUser,
      carStatus: this.product.carStatus,
      carStore: this.product.carStore,
      carSdDis: this.product.carSdDis,
      poType:this.product.poType,
      sdType:this.product.sdType,
      carChezhu1:this.product.carChezhu1,
      carChezhu2:this.product.carChezhu2,
      carChezhu3:this.product.carChezhu3,
      jqxRq:this.product.jqxRq,
      bxRq:this.product.bxRq,
      lqRq:this.product.lqRq,
      carBy1:this.product.carBy1,
      carBy2:this.product.carBy2,
      carBy3:this.product.carBy3

    };
    this.loading = true;
    this.sdService.addPro(sdCarinfoPage).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.loading = false;
      if (data.ok === true) {

        this.back();
        this.msg.success("成功");
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })
  }




  @ViewChild('st8') st8: SimpleTableComponent;

  savecsplan() {

    if(this.product.carVin==""||this.product.carVin==null){
      this.msg.warning("请输入VIN");
      return;
    }
    let SdCsPlanEntity = {

      carVin: this.product.carVin,
      csType:this.sdcsplan.csType,
      csNo: this.sdcsplan.csNo,
      csDate:this.sdcsplan.csDate,
      csContent:this.sdcsplan.csContent,
      csUser: this.sdcsplan.csUser

    };
    this.loading = true;
    this.sdService.cre(8,SdCsPlanEntity).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.loading = false;
      if (data.ok === true) {
        this.st8.reload();
        this.msg.success("成功");
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })
  }

  @ViewChild('st9') st9: SimpleTableComponent;

  savecs() {

    if(this.product.carVin==""||this.product.carVin==null){
      this.msg.warning("请输入VIN");
      return;
    }
    let SdCsActityEntity = {

      carVin: this.product.carVin,

      csaType:this.sdcs.csaType,
        csaNo: this.sdcs.csaNo,
      orderNo:this.sdcs.orderNo,
        csaContent:this.sdcs.csaContent,
        csaUser: this.sdcs.csaUser



    };
    this.loading = true;
    this.sdService.cre(9,SdCsActityEntity).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.loading = false;
      if (data.ok === true) {
        this.st9.reload();
        this.msg.success("成功");
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data.respMsg);
      }
    })
  }
  changeColor(res) {
    this.product.colorSort = res.color.hex;
  }

}
