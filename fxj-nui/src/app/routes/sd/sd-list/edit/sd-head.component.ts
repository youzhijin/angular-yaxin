import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { maService } from '../../../../service/ma/ma.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {insService} from "../../../../service/ins/ins.service";
import {ProductService} from "../../../../service/prod/product.service";
import {sdListComponent} from "../sd-list.component";
import {SimpleTableColumn, SimpleTableComponent} from "@delon/abc";

@Component({
  selector: 'sd-head-edit',
  templateUrl: './sd-head.component.html',
  styleUrls: ['./sd-head.component.less']
})
export class sdHeadComponent implements OnInit, OnDestroy {
  product:  any = {
    id:null,
    carNo: null,
    carVin:null
  };
  checkedList = [];
  @ViewChild('st') st: SimpleTableComponent;
  column: SimpleTableColumn[] = [
    // { title: '订单编号', index: 'orderNo' },
    { title: '', index: 'key', type: 'checkbox',exported:false },
    { title: '车牌号 ', index: 'carNo'},
    { title: '车架号 ', index: 'carVin' },
    { title: '发动机号', index: 'carFreno' },
    { title: '违章总扣分 ', index: 'carWzscore'},
    { title: '违章总金额 ', index: 'carEzjine' },
    { title: '违章次数 ', index: 'carRunFrq' },
    { title: '姓名 ', index: 'userName'},
    { title: '电话 ', index: 'userMobile' },
    { title: '地址', index: 'userAddr' },
    { title: '备注1 ', index: 'userBy1'},
    { title: '备注2 ', index: 'userBy2' },
    { title: '备注3 ', index: 'userBy3' },
    { title: '备注4', index: 'userBy4' },
    { title: '查询批号', index: 'carGroup' },
    { title: '查询状态', index: 'carStatus' }
  ];
  // column: SimpleTableColumn[] = [
  //   // { title: '订单编号', index: 'orderNo' },
  //   { title: '违章时间 ', index: 'wzsj'},
  //   { title: '违章地点 ', index: 'wzdd' },
  //   { title: '违章行为', index: 'wzxw' },
  //   { title: '违章扣分 ', index: 'wzjf'},
  //   { title: '违章罚款 ', index: 'wzfk' },
  //   { title: '违章城市', index: 'csmc' },
  //   { title: '城市代码 ', index: 'csdm' },
  //   { title: '违章代码', index: 'wfdm' }
  //
  // ];
  loading = false;
  constructor(
    private productService: ProductService,

    public setting: SettingsService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private maService: maService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private notice: NzNotificationService,

    private comm: CommService
  ) {}
  ps=1000;
  q: any = {
    carNo: '',
    carVin:''
  };
  private sub: any;
url="";
  ngOnInit(): void {
    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"];
      // 获取商品详情
              this.url=this.maService.getwzheadurl(this.product.id);
    });
    // 隐藏reusetab
    setTimeout(() => {
      this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);
    this.titleService.setTitle("汇总显示")
  }

  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }
  back() {
    this.router.navigate(['/dhcar/dhcar']);
  }
  checkboxChange(list: any[]) {
    this.checkedList = list;
  }

}
