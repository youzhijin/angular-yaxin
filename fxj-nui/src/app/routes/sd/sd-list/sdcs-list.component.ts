import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import {_HttpClient, ModalHelper, TitleService} from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { maService } from '../../../service/ma/ma.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';
import {sdService} from "../../../service/sd/sd.service";
@Component({
  selector: 'sdcs-list',
  templateUrl: './sdcs-list.component.html',
  styleUrls: ['./sdcs-list.component.less']
})
export class sdcsListComponent implements OnInit, OnDestroy {
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    carStatus:"30",
    carYear:"",
    carType: '',
    carNo: '',
    carName: '',
    carVin: '',
    carChezhu1: '',
    carChezhu2: '',
    carLsnumo: ''
  };

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.sdService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';
  checkedList = [];

  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '车辆状态', index: 'carStatus' },
    { title: '车型', index: 'carType'},
    { title: '注册日期', index: 'carYear' },
    { title: '车辆编码', index: 'carNo' },
    { title: '车辆名称' , index: 'carName' },
    { title: '车辆信息', index: 'carInfo' },
    { title: 'VIN', index: 'carVin' },
    { title: '发动机号', index: 'carEnno' },
    { title: '原始车牌', index: 'carLsnumo' },
    { title: '状态', index: 'carStatus' },
    { title: '库存地点', index: 'carStore' },
    {
        title: '操作',
        buttons: [
            {
              text: '详情',
              type: 'link',
              click: (record: any) => this.router.navigate(['/sd/sd/sdcsview/'+record.id])
            }
        ]
    }
  ];

  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private sdService: sdService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private titleService: TitleService,
    private notice: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  inter
  ngOnInit() {
    {
      this.titleService.setTitle("销售综合查询");

    }
    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })

    // 隐藏reusetab
    setTimeout(() => {
      this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);

  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }

  checkboxChange(list: any[]) {
    this.checkedList = list;
  }
  close(): void {
    this.visible = false;
  }

}
