import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { jcbgRoutingModule } from './jcbg-routing.module';
import { jcbgListComponent } from './jcbg-list/jcbg-list.component';
import { jcbgaListComponent } from './jcbg-list/jcbga-list.component';
import { jcbgbListComponent } from './jcbg-list/jcbgb-list.component';
import { jcbgViewEditComponent } from 'app/routes/jcbg/jcbg-list/edit/jcbg-edit.component';

const COMPONENTS = [
  jcbgListComponent
];
const COMPONENTS_NOROUNT = [
  jcbgViewEditComponent,
  jcbgaListComponent,
  jcbgbListComponent
];

@NgModule({
  imports: [
    SharedModule,
    jcbgRoutingModule,
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class jcbgModule { }
