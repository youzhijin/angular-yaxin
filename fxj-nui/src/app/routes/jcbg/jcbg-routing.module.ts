import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { jcbgListComponent } from './jcbg-list/jcbg-list.component';
import { jcbgaListComponent } from './jcbg-list/jcbga-list.component';
import { jcbgbListComponent } from './jcbg-list/jcbgb-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { jcbgViewEditComponent } from 'app/routes/jcbg/jcbg-list/edit/jcbg-edit.component';

const routes: Routes = [
  {
    path: 'jcbg',
    children: [
      { path: '', component: jcbgListComponent },
      { path: 'edit/:id', component: jcbgViewEditComponent },
      { path: 'a', component: jcbgaListComponent },
      { path: 'b', component: jcbgbListComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class jcbgRoutingModule { }
