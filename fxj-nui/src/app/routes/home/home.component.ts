import { NzMessageService } from 'ng-zorro-antd';
import {Component, OnInit, ChangeDetectorRef, Injector, ViewChild} from '@angular/core';
import {_HttpClient, SettingsService, TitleService} from '@delon/theme';
import { LoginService } from '../../service/login.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import {ReuseTabService, SimpleTableColumn, SimpleTableComponent} from '@delon/abc';
import { getTimeDistance, yuan } from '@delon/util';
import {ProductService} from "../../service/prod/product.service";
import {fkService} from "../../service/fk/fk.service";
import {Constant} from "@core/constant";
import {CommService} from "../../service/comm/comm.service";

@Component({
    selector: 'pos-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  inter;
home:any={
  bi1:"24563",
  bi2:"11",
  bi3:"122",
  bi4:"78%",
  bi5:"78"
};
  checkedList = [];
  @ViewChild('st') st: SimpleTableComponent;
  // urlpro =this.productService.list();
  urlpro = this.comm.getbiUrl();

  // @ViewChild('st2') st2: SimpleTableComponent;
  // urlloan=this.fkService.list();
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    bitype: 'orderdetail',

    bpmStatus:"",
    cusName: "",
    cusMobile: "",
    cusIncard: "",
    proNo: ""
  };

  data: any = {
    salesData: [],
    offlineData: [],
  };
  loading = true;
  date_range: Date[] = [];
  rankingListData: any[] = Array(7)
    .fill({})
    .map((item, i) => {
      return {
        title: '3',
        total: 323234,
      };
    });
  titleMap = {
    y1: '1',
    y2: '2',
  };


  columns: SimpleTableColumn[] = [
    // { title: '', index: 'key', type: 'checkbox' },
    { title: '订单编号', index: 'fxjx1' },
    { title: '客户姓名', index: 'fxjx2' },
    { title: '订单类型', index: 'fxjx3' },
    { title: '联系电话', index: 'fxjx4' },
    { title: '车辆信息', index: 'fxjx5' },
    { title: '期数', index: 'fxjx6' },
    { title: '评估价', index: 'fxjx7' },
    { title: '还款方式', index: 'fxjx8' },
    { title: '录入业务员', index: 'fxjx9' },
    { title: '产品种类', index: 'fxjx10' },
    { title: '录入组织', index: 'fxjx11' },
    { title: '订单提交时间', index: 'fxjx12' },
    { title: '订单状态', index: 'fxjx13' }
    // { title: '录入业务员  ' , index: 'createBy' },
    // { title: '订单提交时间', index: 'createDate' }

  ];

  // columns1: SimpleTableColumn[] = [
  //   // { title: '', index: 'key', type: 'checkbox' },
  //   { title: '订单编号', index: 'orderNo' },
  //   { title: '客户姓名', index: 'cusName'},
  //   { title: '联系电话', index: 'cusMobile' },
  //   { title: '放款金额', index: 'loanAmount' },
  //   { title: '放款日期' , index: 'loanDate' }
  // ];
  constructor(
    private comm: CommService,

    private http: _HttpClient,
    public msg: NzMessageService,
    private cd: ChangeDetectorRef,
    private productService: ProductService,
    private fkService: fkService,
    private title: TitleService,
    public setting: SettingsService,
  ) {}
      ngOnInit() {

        this.inter = setInterval(() => {
          this.productService.getbihome("bihome").pipe(catchError(err => {
            return of(err)
          })).subscribe(data => {


            this.home.bi1=data.data[0].fxjx1;
            this.home.bi2=data.data[0].fxjx2;

            this.home.bi3=data.data[0].fxjx3;

            this.home.bi4=data.data[0].fxjx4;

            this.home.bi5=data.data[0].fxjx5;
          })

        }, 10000);
        this.inter = setInterval(() => {
            // this.st.load(1, this.q);
            // this.st2.load(1, this.q);
        }, 10000);
        this.title.setTitle("首页");

      }

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }



  _activeTab = 0;
  _tabChange(value: any) {}
}
