import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { fkRoutingModule } from './fk-routing.module';
import { fkListComponent } from './fk-list/fk-list.component';
import { fkViewEditComponent } from 'app/routes/fk/fk-list/edit/fk-edit.component';
import { GoodsImgUploadComponent } from './goods-upload/goods-img-upload.component';

const COMPONENTS = [
  fkListComponent,
  GoodsImgUploadComponent
];
const COMPONENTS_NOROUNT = [
  fkViewEditComponent,
];

@NgModule({
  imports: [
    SharedModule,
    fkRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class fkModule { }
