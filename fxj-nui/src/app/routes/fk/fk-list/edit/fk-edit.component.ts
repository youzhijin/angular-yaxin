import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { fkService } from '../../../../service/fk/fk.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';

@Component({
  selector: 'fk-view-edit',
  templateUrl: './fk-edit.component.html',
  styleUrls: ['./fk-edit.component.less']
})
export class fkViewEditComponent implements OnInit, OnDestroy {
  product:  any = {
    id:null,
    orderNo: null,
    bpmStatus:null,
    cusName: null,
    cusMobile: null,
    loanDate: null /*No show*/,
    loanAmount: null,
    loanAtta: null,
    loanPeriod: null,
    loanSum: null,
    loanSbamount: null,
    loanDbmonth: null,
    loanMonth: null,
    loanRemark: null,
    loanReMethod:null,
    loanMonthSum:null,
    fkBy1:null,
    fkBy2:null,
    fkBy3:null

  };

  bpmStatus =  [{text: '未放款', value: '1'}, {text: '已放款', value: '10'}];
  loanReMethods =  [{text: '等额本金', value: '10'}, {text: '等额本息', value: '20'}, {text: '先息后本', value: '30'}];
  MAX_COUNT = 60;
  tabIndex = 0;
  saleing = false;
  private sub: any;

  constructor(
    public setting: SettingsService,
    private msg: NzMessageService,
    private modalService: NzModalService,
    private fkService: fkService,
    private emit: EmitterService,
    private notification: NzNotificationService,
    private route: ActivatedRoute,
    private titleService: TitleService,
    private cnt: Constant,
    private router: Router,
    private comm: CommService
  ) {}

  ngOnInit(): void {

    // this.loadbpmStatus();


    // 获取参数
    this.sub = this.route.params.subscribe(params => {
      this.product.id = params["id"]
      console.log("this.product.id****************"+this.product.id);
      // 获取商品详情
      this.getProductInfo();
    });
    // 隐藏reusetab
    setTimeout(() => {
        this.emit.sendMessage("HIDE_REUSE_TAB");
    }, 100);
    this.titleService.setTitle("编辑产品")
  }
  // 加载证件类型
  loadbpmStatus() {
    this.bpmStatus = [];
    this.comm.getType("bpm_status").subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.ok ) {
        for (const row of ret['data']) {
          this.bpmStatus.push({ text: row.typename, value: row.typecode });
        }
      }
    })
  }
  // 显示reusetab
  ngOnDestroy() {
    this.emit.sendMessage("SHOW_REUSE_TAB");
  }

  changeTab(index) {
    // 使用异步的方式更新组件, 防止ngIf错误
    Promise.resolve(null).then(() => this.tabIndex = index);
  }



  getProductInfo() {
    this.fkService.getProduct(this.product.id).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      this.product = data.data;
      this.product.loanDate="";
    })
  }




  back() {
    this.router.navigate(['/fk/fk']);
  }


  save() {
    if(this.product.loanDate===''||this.product.loanDate===''){
      this.msg.warning("请选择首次还款日期");
      return;
    }
    if(this.product.loanReMethod===''||this.product.loanReMethod===''){
      this.msg.warning("请选择还款方式");
      return;
    }
    if(this.product.loanAmount===''||this.product.loanAmount===''){
      this.msg.warning("请输入放款金额");
      return;
    }
    if(this.product.loanDbmonth===''||this.product.loanDbmonth===''){
      this.msg.warning("请输入担保月利息");
      return;
    }
    if(this.product.loanMonth===''||this.product.loanMonth===''){
      this.msg.warning("请输入银行月利息");
      return;
    }
    if(this.product.loanSum===''||this.product.loanSum===''){
      this.msg.warning("请输入应还总金额");
      return;
    }
    let fxjLoanFk = {
      updateBy:this.setting.user.username,
      id:this.product.id,
      bpmStatus:80,
      // orderNo: this.product.orderNo,
      cusName: this.product.cusName,
      cusMobile: this.product.cusMobile,
      // cusrIdtype: this.product.cusrIdtype,
      // cusIncard: this.product.cusIncard /*No show*/,
      loanDate: this.product.loanDate /*No show*/,
      // cusIdValidto: this.product.cusIdValidto /*No show*/,
      loanAmount: this.product.loanAmount,
      loanAtta: this.product.loanAtta,
      loanReMethod:this.product.loanReMethod,
      loanPeriod: this.product.loanPeriod,
      loanSum: this.product.loanSum,
      loanSbamount: this.product.loanSbamount,
      loanDbmonth: this.product.loanDbmonth,
      loanMonth: this.product.loanMonth,
      loanRemark: this.product.loanRemark,
      loanMonthSum:this.product.loanMonthSum,
      fkBy1:this.product.fkBy1,
      fkBy2:this.product.fkBy2,
    fkBy3:this.product.fkBy3

    };

    // 调用接口
    this.fkService.editPro(fxjLoanFk).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {
      if (data.ok === true) {
        this.msg.success('修改成功');
        this.emit.sendMessage("RELOAD_GOODS_LIST");
      } else {
        this.msg.error(data['message']);
      }
    })
  }

  changeColor(res) {
    this.product.colorSort = res.color.hex;
  }
}
