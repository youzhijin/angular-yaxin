import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { fkListComponent } from './fk-list/fk-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { fkViewEditComponent } from 'app/routes/fk/fk-list/edit/fk-edit.component';
import { GoodsImgUploadComponent } from './goods-upload/goods-img-upload.component';

const routes: Routes = [
  {
    path: 'fk',
    children: [
      { path: '', component: fkListComponent },
      { path: 'edit/:id', component: fkViewEditComponent },
      { path: 'upload', component: GoodsImgUploadComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class fkRoutingModule { }
