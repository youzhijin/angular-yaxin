// import { Component, OnInit, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, UploadFile, NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {SettingsService, TitleService} from '@delon/theme';
import { Constant, ServiceResult } from '@core/constant';
import { maService } from '../../../../service/ma/ma.service';
import { EmitterService } from 'app/service/emit.service';
import { CommService } from '../../../../service/comm/comm.service';
import {insService} from "../../../../service/ins/ins.service";
import {ProductService} from "../../../../service/prod/product.service";

import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';

@Component({
  selector: 'spList-view-edit',
  templateUrl: './spList-edit.component.html',
  styleUrls: ['./spList-edit.component.less']
})
export class spListEditComponent implements OnInit, OnDestroy {
  changeIndex = 0;
  checked = true;

  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    bpmStatus:"",

    fiName: '',
    cusMobile: '',
    cusIncard: '',
    orderNo: ''
  };

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  // url = this.spListService.list();
  // region end
  columns: SimpleTableColumn[] = [
    { title: '订单编号', index: 'orderNum' },
    { title: '客户姓名', index: 'name' },
    { title: '订单类型', index: 'orderType' },
    { title: '联系电话', index: 'telephone'},
    { title: '车辆信息', index: 'vehicleInfo'},
    { title: '评估金额', index: 'valuationAmount'},
    { title: '期数', index: 'periodsNum'},
    { title: '融资额', index: 'financingAmount'},
    { title: '融资比例', index: 'financingRatio'},
    { title: '录入业务员', index: 'salesman'},
    { title: '录入单位', index: 'entryUnit'},
    { title: '佣金比例', index: 'commissionRate'},
    { title: '放款时间', index: 'time'},
    { title: '逾期次数', index: 'overdueFrequency'},
    { title: '还款状态', index: 'orderStatus'},
    { title: '操作',width:'120px',fixed:'right',
      buttons: [
        {
          text: '查看'
        },
        {
          text: '认领'
        }
      ]
    }
  ];

  listOfData = [
    {
      orderNum:'GM01001',
      name: '刘德华',
      orderType:'售后回租',
      telephone: '136600000',
      vehicleInfo:'宝马5系列',
      valuationAmount:'1512454元',
      periodsNum:'36期限',
      financingAmount:'135455元',
      financingRatio:'70%',
      salesman:'张学友',
      entryUnit:'平安银行',
      commissionRate:'70%',
      time:'2019-03-28',
      overdueFrequency:'1',
      orderStatus:'M1'
    },{
      orderNum:'GM01002',
      name: '张学友',
      orderType:'售后回租',
      telephone: '136600002',
      vehicleInfo:'宝马5系列',
      valuationAmount:'151245333元',
      periodsNum:'36期限',
      financingAmount:'13545511元',
      financingRatio:'78%',
      salesman:'刘德华',
      entryUnit:'平安银行',
      commissionRate:'78%',
      time:'2019-03-01',
      overdueFrequency:'1',
      orderStatus:'M1'
    }
  ];

  change(args) {
    console.log(args.index);
    this.changeIndex = args.index
  }
  checkboxchange(args){
    console.log(args);
  }


  ngOnInit(): void {

  }

  ngOnDestroy() {
    // this.emit.sendMessage("SHOW_REUSE_TAB");
  }










  back() {
//     this.router.navigate(['/spList/spList']);
  }




}
