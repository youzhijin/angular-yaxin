import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { spListComponent } from './spList-list/spList-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { spListEditComponent } from 'app/routes/spList/spList-list/edit/spList-edit.component';
import { spListAddoneComponent } from 'app/routes/spList/spList-list/edit/spList-addone.component';

const routes: Routes = [
  {
    path: 'spList',
    children: [
      { path: 'list', component: spListComponent },
      { path: 'edit/:id', component: spListEditComponent },
      { path: 'add', component: spListAddoneComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class spListRoutingModule { }
