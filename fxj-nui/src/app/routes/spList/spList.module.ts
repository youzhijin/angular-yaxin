import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { spListRoutingModule } from './spList-routing.module';
import { spListComponent } from './spList-list/spList-list.component';
import { spListEditComponent } from 'app/routes/spList/spList-list/edit/spList-edit.component';
import { spListAddoneComponent } from 'app/routes/spList/spList-list/edit/spList-addone.component';


const COMPONENTS = [
  spListComponent
];
const COMPONENTS_NOROUNT = [
  spListEditComponent,
  spListAddoneComponent
];

@NgModule({
  imports: [
    SharedModule,
    spListRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class spListModule { }
