import { Component, OnInit, Injector } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, NzModalRef,UploadFile, NzModalService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { ServiceResult, Constant } from '@core/constant';
import { UserService, User } from '../../../../service/user/user.service';
import { BaseController } from '@shared/base-controller';
import { deepCopy } from '@delon/util';
import { DepartsModalComponent } from '@shared/component/departs/departs-modal.component';
import { SysUserService } from 'app/service/sys/sys-user.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-modal.component.html',
  styleUrls: []
})
export class UserModalComponent extends BaseController implements OnInit {    
    loading = false;    
    user = new User();
    sexs = [];
    sex = null;
    departId = null;
    departName = null;
    isSuper = false;

    constructor(
        private modal: NzModalRef,
        public http: _HttpClient, 
        public msg: NzMessageService, 
        private cnt: Constant,
        public userServie: SysUserService,
        private modalSrv: ModalHelper,  
        private modalService: NzModalService,      
        fb: FormBuilder,
        public injector: Injector
    ) {
        super(injector);    
    }

    ngOnInit() {        
        // this.loadSex().subscribe(data => {
        //     const ret: ServiceResult = <ServiceResult> data;
        //     if (ret.status === 200) {
        //         for (const row of ret['data']['rows']) {
        //             this.sexs.push({ text: row.labelDefault, value: row.value });
        //         }     
        //         if (this.user.id) {
        //             this.sex = this.sexs.find((v, i) => { return v.text == this.user.sex; }).value;
        //         }                        
        //     }
        // })
        if (this.user.id) {
            this.userServie.info(this.user.id).pipe(
                catchError(err => {                             
                    return of(err);
                })
            ).subscribe(ret=>{
                if (ret.status === 200) {
                    const data = ret.data;
                    this.user = data;

                    this.isSuper = true;
                    if (this.user.departId) {
                        const depart = JSON.parse(this.user.departId);
                        if (typeof depart === 'object' && depart) {
                            this.departId = depart.id;
                            this.departName = depart.name;
                        } else {
                            this.departId = this.user.departId;
                            this.departName = this.user.departId;
                        }
                    }
                } else {
                    this.msg.error(ret.message);
                }
            });
        } else {
            this.sex = 'man';
        }
    }

    save() {
        let userForm: User = deepCopy(this.user);        
        userForm.sex = this.sex;
        userForm.departId = this.departId;       

        if (!userForm.name) {
            this.msg.warning('请输入姓名');
            return;
        }
        
        if (!userForm.username) {
            this.msg.warning('请输入账户');
            return;
        }

        if ("admin" === userForm.username) {
            this.msg.warning('不能以"admin"作为账户名');
            return;
        }

        if (!userForm.id && !userForm.password) {
            this.msg.warning('密码不能空');
            return;
        }
        if (!userForm.id && (userForm.password.length < 4 || userForm.password.length > 16)) {
            this.msg.warning('密码长度需4-16个字符');
            return;
        }
        
        if (userForm.id) {
            userForm.password = null;
            this.userServie.modify(userForm).pipe(
                catchError(err => {
                    return of(err)
                })
            ).subscribe(ret => {
                if (ret.status == 200) {
                    this.msg.success('操作成功');
                    this.close();
                } else {
                    this.msg.error(ret.message);
                }
            })
        } else {
            userForm.isAppManager = this.cnt.BoolUtil.FALSE;
            this.userServie.add(userForm).pipe(
                catchError(err => {
                    return of(err);
                })
            ).subscribe(ret => {
                if (ret.status == 200) {
                    this.msg.success('操作成功');
                    this.close();
                } else {
                    this.msg.error(ret.message);
                }
            })
        }

    }

    close() {
        this.modal.close(true);
        this.modal.destroy();
    }

    openDepartDialog() {        
        const modal = this.modalService.create({
            nzContent: DepartsModalComponent,
            nzFooter: null
        }); 
        modal.afterOpen.subscribe(() => {
            modal.getContentComponent().getSubject().subscribe(data => {
                this.departId = data["key"];
                this.departName = data["title"];
            })
        })
    }
}
