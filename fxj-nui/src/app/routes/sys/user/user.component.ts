import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { SimpleTableComponent, SimpleTableColumn, SimpleTableButton } from '@delon/abc';
import { NzMessageService } from 'ng-zorro-antd';
import { ModalHelper } from '@delon/theme';
import { Constant } from '@core/constant';
import { CommService } from '../../../service/comm/comm.service';
import { BaseController } from '@shared/base-controller';
import { UserService, User } from '../../../service/user/user.service';
import { UserModalComponent } from './modal/user-modal.component';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { SysUserService } from 'app/service/sys/sys-user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.less']
})
export class UserComponent extends BaseController implements OnInit {
  ps = 10;
  q: any = {  
    app: 'sms_cloud',
    isAppManager: '0',                   
    name: ''    
  };
  url = this.user.list();
  data = [];
  @ViewChild('st') st: SimpleTableComponent;
  


  columns: SimpleTableColumn[] = [
      { title: '', index: 'key', type: 'checkbox' },
      { title: '姓名', index: 'name' },        
      { title: '账户', index: 'username'},            
      { title: '性别', index: 'sex' },
      { title: '身份', index: 'isSuperAdmin', render: 'isSuperAdmin' },
      { title: '备注', index: 'description' },
      { 
          title: '操作',
          buttons: [
              { 
                  text: '编辑',
                  type: 'modal',
                  component: UserModalComponent,
                  paramName: 'user',
                  size: 600,                  
                  click: (record: any, modal: any) => this.st.reload()
              },
              {
                  text: '删除',
                  type: 'del',                  
                  click: (record: any) =>this.del(record.id)
              }
          ]
      }        
  ];

  constructor(
      public msg: NzMessageService, 
      private modalSrv: ModalHelper,
      private cnt: Constant,
      private user: SysUserService,    
      public injector: Injector
  ) {
      super(injector);    
  }

  ngOnInit() {
      
  }

  

  add() {
      this.modalSrv.static(UserModalComponent, null, 600).subscribe((e) => {
          this.st.load(1, this.q);            
      });
  }


  del(id: string) {
      this.user.delete(id).pipe(
          catchError(err => {                             
              return of(err);
          })
      ).subscribe(ret=>{
          if (ret.status === 200) {
              this.st.load(1, this.q);         
          } else {
              this.msg.error(ret.message);
          }
      });
  }

 
}
