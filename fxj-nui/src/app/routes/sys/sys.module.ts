import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { SharedModule } from '@shared/shared.module';
import { UserComponent } from 'app/routes/sys/user/user.component';
import { UserModalComponent } from 'app/routes/sys/user/modal/user-modal.component';
import { MenuComponent } from 'app/routes/sys/menu/menu.component';
import { ElementModalComponent } from 'app/routes/sys/menu/modal/element-modal.component';
import { DictComponent } from 'app/routes/sys/dict/dict.component';
import { DictModalComponent } from 'app/routes/sys/dict/dict-modal/dict-modal.component';

const COMPONENT = [
  UserComponent,
  MenuComponent,
  DictComponent
];

const COMPONENT_NOROUNT = [
  UserModalComponent,
  ElementModalComponent,
  DictModalComponent
];

const routes: Routes = [    
  { 
    path: 'user',
    component: UserComponent
  },
  {
    path: 'menu',
    component: MenuComponent
  },
  {
    path: 'dict',
    component: DictComponent
  }
];

@NgModule({
  imports: [
    SharedModule,        
    RouterModule.forChild(routes)
  ],
  declarations: [
    ...COMPONENT,
    ...COMPONENT_NOROUNT  
  ],
  exports: [
    RouterModule
  ],
  entryComponents: COMPONENT_NOROUNT
})
export class SysModule { }
