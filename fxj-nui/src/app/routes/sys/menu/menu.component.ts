import { Component, OnInit, TemplateRef, Injector, ViewChild } from '@angular/core';
import { NzTreeNode, NzModalRef, NzModalService, NzMessageService, NzFormatEmitEvent, NzDropdownContextComponent, NzDropdownService } from 'ng-zorro-antd';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ModalHelper } from '@delon/theme';
import { MenuService } from '../../../service/menu/menu.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { BaseController } from '@shared/base-controller';
import { ServiceResult, Constant } from '@core/constant';
import { SimpleTableComponent, SimpleTableColumn, SimpleTableButton } from '@delon/abc';
import { ElementModalComponent } from './modal/element-modal.component';


@Component({
  selector: 'app-menu-list',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.less']
})
export class MenuComponent extends BaseController implements OnInit {

  loading = false;
  // 分类节点集合
  nodes = [];    
  // 当前被选中的节点
  currentNode: NzTreeNode = null;
  // 表单
  menuForm: FormGroup;
  // 编辑状态
  editStatus = false;
  // 添加节点
  tplModal: NzModalRef;
  modal = { suffix: '', code: '', name: ''}
  tplModalButtonLoading = false;
  hasErr = '';
  confirmModal: NzModalRef; 
  searchValue
  types = [];
  formStatus = 'update';

  addBtn = false;
  editBtn = false;
  delBtn = false;
  addElementBtn = false;
  editElementBtn = false;
  delElementBtn = false;
  dropdown: NzDropdownContextComponent;

  ps = 10;
  q: any = {
    menuId: '-1',                     
    name: ''    
  };
  url = this.menu.elements();
  data = [];
  @ViewChild('st') st: SimpleTableComponent;

  columns: SimpleTableColumn[] = [      
      { title: '资源编码', index: 'code' },        
      { title: '资源类型', index: 'type'},            
      { title: '资源名称', index: 'name' },
      { title: '资源地址', index: 'uri' },
      { title: '资源请求类型', index: 'method' },
      { title: '描述', index: 'description' },
      { 
          title: '操作',
          buttons: [
              { 
                  text: '编辑',
                  type: 'modal',
                  component: ElementModalComponent,
                  paramName: 'element',
                  size: 600,
                  iif: (item: any, btn: SimpleTableButton, column: SimpleTableColumn) => {
                    return this.editElementBtn;
                  },
                  click: (record: any, modal: any) => this.st.reload()
              },
              {
                  text: '删除',
                  type: 'del',
                  iif: (item: any, btn: SimpleTableButton, column: SimpleTableColumn) => {
                    return this.delElementBtn;
                  },
                  click: (record: any) =>this.delElement(record.id)
              }
          ],
          fixed: 'right',
          width: '100px'
      }        
  ];

  constructor(        
      private modalService: NzModalService,
      public msg: NzMessageService, 
      private modalSrv: ModalHelper,        
      private fb: FormBuilder,
      private menu: MenuService,
      public injector: Injector ,
      private cnt: Constant,
      private nzDropdownService: NzDropdownService
  ) {
      super(injector);  
  }

  ngOnInit() {
      this.addBtn = this.getAuths(this.cnt.authKey.menu.ADD_BTN);
      this.editBtn = this.getAuths(this.cnt.authKey.menu.EDIT_BTN);
      this.delBtn = this.getAuths(this.cnt.authKey.menu.DEL_BTN);
      this.addElementBtn = this.getAuths(this.cnt.authKey.element.ADD_BTN);
      this.editElementBtn = this.getAuths(this.cnt.authKey.element.EDIT_BTN);
      this.delElementBtn = this.getAuths(this.cnt.authKey.element.DEL_BTN);  
      
      this.loadTypes();
      // 加载菜单树
      this.loadMenuTree();
      // 初始化表单
      this.menuForm = new FormGroup({
          code     : new FormControl({value: null, disabled: !this.editStatus}, Validators.required),
          title     : new FormControl({value: null, disabled: !this.editStatus}, Validators.required),
          parentId     : new FormControl({value: -1, disabled: !this.editStatus}, Validators.required),
          icon         : new FormControl({value: null, disabled: !this.editStatus}),
          href          : new FormControl({value: null, disabled: !this.editStatus}),
          type         : new FormControl({value: null, disabled: !this.editStatus}),       
          orderNum     : new FormControl({value: 0, disabled: !this.editStatus}),
          description : new FormControl({value: null, disabled: !this.editStatus}),
          attr2         : new FormControl({value: null, disabled: !this.editStatus}),
          id           : new FormControl({value: null, disabled: !this.editStatus}),
          path         : new FormControl({value: null, disabled: !this.editStatus})
      });
  }

  loadTypes() {
    this.loadMenuType().subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      if (ret.status === 200) {
        this.types = ret.data.rows;
      }
    })
  }

  /** 
   * 加载分类树
   */
  loadMenuTree() {
      this.menu.menuTree().pipe(
          catchError(err => {                             
              return of(err);
          })
      ).subscribe(list => {            
          this.nodes = [];
          let o = this.replaceKeysDeep(list, this.mapShortToLong);                                 
          for (let index = 0; index < o.length; index++) {
            if (o[index].title.indexOf('sms') > -1) {
              this.nodes.push(
                  new NzTreeNode(o[index])
              );
            }
          }   
      })
  }

  mapShortToLong: any = {
      "id": "key"
  };

  

  /**
   * 节点单击处理事件
   * @param name 
   * @param e 
   */
  mouseAction(name: string, e: NzFormatEmitEvent): void {           
      if (name === 'click') {            
        if (e.node.isSelected) {    
          // 如果当前是编辑状态, 则选中一个节点,按钮应该确保是更新按钮
          if (this.editStatus) {
              this.formStatus = 'update';
          }                
          this.currentNode = e.node;   
          // 获取菜单资源
          this.q.menuId = e.node.key;
          this.st.load(1, this.q);
          // 获取菜单详情       
          this.menu.getMenu(e.node.key).pipe(
            catchError(err => {
              return of(err);
            })
          ).subscribe(data => {            
            if (data.status === 200) {
              this.setFormValue(data.data);                
            } else {
              this.msg.error(data.message);
            }
          })
            
        } else {
            this.resetNode();  
            this.cancel();   
            this.q.menuId = "-1"
            this.st.load(1, this.q);           
        }
      } 
  }

  

  /**
   * 添加
   */
  add(): void {        
    if (this.formStatus == 'update' && this.editStatus) {
        this.msg.warning('有未提交的表单, 请确定是否保存');
        return;
    } 
    this.resetForm();
    this.enableForm();
    this.formStatus = 'create';
    this.editStatus = true;
    if (this.currentNode) {
      this.menuForm.get('parentId').setValue(this.currentNode.key);
    } else {
      this.menuForm.get('parentId').setValue('-1');
    }
    
  }

  doAdd() {
    for (const i in this.menuForm.controls) {
      this.menuForm.controls[ i ].markAsDirty();
      this.menuForm.controls[ i ].updateValueAndValidity();
    }        
    if (this.menuForm.get('code').dirty && this.menuForm.get('code').errors) {
        this.msg.warning('请填写路径编码');
        return;
    }
    if (this.menuForm.get('title').dirty && this.menuForm.get('title').errors) {
        this.msg.warning('请填写标题');
        return;
    }     
  }

  edit() {
      if (!this.currentNode) {
          this.msg.warning('请选择一个菜单');
          return;
      }
      if (this.formStatus == 'create' && this.editStatus) {
        this.msg.warning('有未提交的表单, 请确定是否保存');
        return;
      } 
      this.editStatus = true;
      this.enableForm();
      this.formStatus = 'update';
      if (!this.menuForm.get('id').value) {
          // 获取菜单详情       
          this.menu.getMenu(this.currentNode.key).pipe(
            catchError(err => {
              return of(err);
            })
          ).subscribe(data => {            
            if (data.status === 200) {
              this.setFormValue(data.data);                
            } else {
              this.msg.error(data.message);
            }
          })
      }
  }


  doEdit(): void {
      for (const i in this.menuForm.controls) {
        this.menuForm.controls[ i ].markAsDirty();
        this.menuForm.controls[ i ].updateValueAndValidity();
      }        
      if (this.menuForm.get('code').dirty && this.menuForm.get('code').errors) {
          this.msg.warning('请填写路径编码');
          return;
      }
      if (this.menuForm.get('title').dirty && this.menuForm.get('title').errors) {
          this.msg.warning('请填写标题');
          return;
      }      
      if (this.menuForm.valid) {
          this.loading = true;
          let v = this.menuForm.value;
          
          this.callEdit(v);
      }
  }

  callEdit(v) {
    if (this.formStatus === 'update') {
      this.menu.modify(v).pipe(
          catchError(err => {                             
              return of(err);
          })
      ).subscribe(data => {
          this.loading = false;
          if (data['status'] && data['status'] === 200) {                
              this.loadMenuTree();              
              this.msg.success('操作成功');
              this.cancel();
          } else {
              this.msg.error('操作失败' + data['message']);
          }
      })
    } else {
      this.menu.add(v).pipe(
          catchError(err => {                             
              return of(err);
          })
      ).subscribe(data => {
          this.loading = false;
          if (data['status'] && data['status'] === 200) {                
              this.loadMenuTree();              
              this.msg.success('操作成功');
              this.cancel();
          } else {
              this.msg.error('操作失败' + data['message']);
          }
      })
    }
  }

  /**
   * 取消编辑
   */
  cancel() {
      this.editStatus = false;
      this.disableForm();
      if (this.formStatus === 'create') {
        this.resetForm();
      }
  }

  del() {
      if (this.currentNode) {
          this.confirmModal = this.modalService.confirm({
              nzTitle: `删除提醒`,
              nzContent: `你确定删除菜单 ${this.currentNode.title} ?`,
              nzOnOk: () => {
                  this.doDel();
              }
          });
          
      } else {
          this.msg.error('请选择要删除的菜单');
      } 
  }

  doDel() {
      this.menu.delete(this.currentNode.key).pipe(
          catchError(err => {                             
              return of(err);
          })
      ).subscribe(ret => {
          if (ret.status === 200) {
              this.resetNode();
              this.loadMenuTree();     
          } else {
              this.msg.error(ret.message);
          }
      });
  }

  resetNode() {
    this.currentNode = null;    
    this.resetForm();
  }

  resetForm() {
    for (const i in this.menuForm.controls) {
      this.menuForm.controls[ i ].reset();  
    }
    this.menuForm.get('parentId').setValue("-1");
    this.menuForm.get('orderNum').setValue(0);
  }

  setFormValue(node) {
    this.resetForm();
    this.menuForm.patchValue(node);
  }  

  enableForm() {
    for (const i in this.menuForm.controls) {
      this.menuForm.controls[ i ].enable();  
    }
  }

  disableForm() {    
    for (const i in this.menuForm.controls) {
      this.menuForm.controls[ i ].disable();  
    }
  }

  delElement(id: string) {
    this.menu.deleteElement(id).pipe(
        catchError(err => {                             
            return of(err);
        })
    ).subscribe(ret=>{
        if (ret.status === 200) {
            this.st.load(1, this.q);         
        } else {
            this.msg.error(ret.message);
        }
    });
  }
  addElement() {
    if (this.currentNode) {
        this.modalSrv.static(ElementModalComponent, { menuId: this.currentNode.key }, 600).subscribe((e) => {
            this.st.load(1, this.q);            
        });
    } else {
        this.msg.warning('请先选择一个菜单');
    }
  }

  contextMenu($event, template: TemplateRef<void>): void {
    if (this.currentNode) {
        this.currentNode.isSelected = false;
    }
    $event.node.isSelected = true;
    this.currentNode = $event.node;
    // 如果当前是编辑状态, 则选中一个节点,按钮应该确保是更新按钮
    if (this.editStatus) {
        this.formStatus = 'update';
    }                
   
    // 获取菜单资源
    this.q.menuId = this.currentNode.key;
    this.st.load(1, this.q);
    // 获取菜单详情       
    this.menu.getMenu(this.currentNode.key).pipe(
      catchError(err => {
        return of(err);
      })
    ).subscribe(data => {            
      if (data.status === 200) {
        this.setFormValue(data.data);                
      } else {
        this.msg.error(data.message);
      }
    })
    this.dropdown = this.nzDropdownService.create($event.event, template);
  }
}
