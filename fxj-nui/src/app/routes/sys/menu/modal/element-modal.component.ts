import { Component, OnInit, Injector } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzMessageService, NzModalRef,UploadFile, NzModalService } from 'ng-zorro-antd';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { ServiceResult, Constant } from '@core/constant';
import { UserService, User } from '../../../../service/user/user.service';
import { BaseController } from '@shared/base-controller';
import { deepCopy } from '@delon/util';
import { DepartsModalComponent } from '@shared/component/departs/departs-modal.component';
import { MenuService, Element } from '../../../../service/menu/menu.service';

@Component({
  selector: 'app-element-modal',
  templateUrl: './element-modal.component.html',
  styleUrls: []
})
export class ElementModalComponent extends BaseController implements OnInit {    
    loading = false;    
    element = new Element();
    menuId = null;
    elementTypes = [];      
    methods = [{text: 'GET', value: 'GET'}, {text: 'POST', value: 'POST'}, {text: 'PUT', value: 'PUT'}, {text: 'DELETE', value: 'DELETE'}]    

    constructor(
        private modal: NzModalRef,
        public http: _HttpClient, 
        public msg: NzMessageService, 
        private cnt: Constant,
        public menuService: MenuService,
        private modalSrv: ModalHelper,  
        private modalService: NzModalService,      
        fb: FormBuilder,
        public injector: Injector
    ) {
        super(injector);    
    }

    ngOnInit() {                    
        this.loadElementType().subscribe(data => {
            const ret: ServiceResult = <ServiceResult> data;
            if (ret.status === 200) {
                for (const row of ret['data']['rows']) {
                    this.elementTypes.push({ text: row.labelDefault, value: row.value });
                }                     
            }
        })
        
        if (this.element.id) {
            this.menuService.getElement(this.element.id).pipe(
                catchError(err => {                             
                    return of(err);
                })
            ).subscribe(ret=>{
                if (ret.status === 200) {
                    const data = ret.data;
                    this.element = data;                                        
                } else {
                    this.msg.error(ret.message);
                }
            });
        } 
    }

    save() {        
        if (!this.element.code) {
            this.msg.warning('请输入资源编码');
            return;
        }
        if (!this.element.type) {
            this.msg.warning('请选择资源类型');
            return;
        }        
        if (!this.element.name) {
            this.msg.warning('请输入资源名称');
            return;
        }
        if (!this.element.uri) {
            this.msg.warning('请输入资源地址');
            return;
        }
        if (!this.element.method) {
            this.msg.warning('请选择资源请求类型');
            return;
        }
        
        if (this.element.id) {            
            this.menuService.modifyElement(this.element).pipe(
                catchError(err => {
                    return of(err)
                })
            ).subscribe(ret => {
                if (ret.status == 200) {
                    this.msg.success('操作成功');
                    this.close();
                } else {
                    this.msg.error(ret.message);
                }
            })
        } else {
            if (!this.menuId) {
                this.msg.warning('请选择菜单');
                return;
            }
            this.element.menuId = this.menuId;
            this.menuService.addElement(this.element).pipe(
                catchError(err => {
                    return of(err);
                })
            ).subscribe(ret => {
                if (ret.status == 200) {
                    this.msg.success('操作成功');
                    this.close();
                } else {
                    this.msg.error(ret.message);
                }
            })
        }

    }

    close() {
        this.modal.close(true);
        this.modal.destroy();
    }
}
