import { Component, OnInit, Injector } from '@angular/core';
import { BaseController } from '@shared/base-controller';
import { DictValue, DictService } from '../../../../service/dict/dict.service';
import { NzModalRef, NzMessageService, NzModalService } from 'ng-zorro-antd';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { Constant } from '@core/constant';
import { FormBuilder } from '@angular/forms';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'dict-modal',
  templateUrl: './dict-modal.component.html',
  styleUrls: ['./dict-modal.component.less']
})
export class DictModalComponent extends BaseController implements OnInit {

  loading = false;    
  dictValue = new DictValue();
  typeId = null;
  code = null;
  params

  constructor(
      private modal: NzModalRef,
      public http: _HttpClient, 
      public msg: NzMessageService, 
      private cnt: Constant,
      public dict: DictService,
      private modalSrv: ModalHelper,  
      private modalService: NzModalService,      
      fb: FormBuilder,
      public injector: Injector
  ) {
      super(injector);    
  }

  ngOnInit() {         
      if (this.dictValue.id) {
          this.dict.getDictValue(this.dictValue.id).pipe(
              catchError(err => {                             
                  return of(err);
              })
          ).subscribe(ret=>{
              if (ret.status === 200) {
                  const data = ret.data;
                  this.dictValue = data;  
                  if (this.dictValue.code.indexOf(this.code) > -1) {
                    this.dictValue.code = this.dictValue.code.substring(this.code.length + 1, this.dictValue.code.length)
                  }                                      
              } else {
                  this.msg.error(ret.message);
              }
          });
      } 
  }

  save() {        
      if (!this.dictValue.code) {
          this.msg.warning('请输入编码');
          return;
      }
      if (!this.dictValue.labelDefault) {
          this.msg.warning('请输入默认显示');
          return;
      }        
      if (!this.dictValue.labelEnUs) {
          this.msg.warning('请输入英文显示');
          return;
      }
      if (!this.dictValue.labelZhCh) {
          this.msg.warning('请输入中文显示');
          return;
      }
      this.dictValue.value = this.dictValue.code;
      this.dictValue.code = this.code + '_' + this.dictValue.code;
      if (this.dictValue.id) {            
          this.dict.modifyDictValue(this.dictValue).pipe(
              catchError(err => {
                  return of(err)
              })
          ).subscribe(ret => {
              if (ret.status == 200) {
                  this.msg.success('操作成功');
                  this.close();
              } else {
                  this.msg.error(ret.message);
              }
          })
      } else {
          if (!this.typeId) {
              this.msg.warning('请选择字典类型');
              return;
          }
          this.dictValue.typeId = this.typeId;
          this.dict.addDictValue(this.dictValue).pipe(
              catchError(err => {
                  return of(err);
              })
          ).subscribe(ret => {
              if (ret.status == 200) {
                  this.msg.success('操作成功');
                  this.close();
              } else {
                  this.msg.error(ret.message);
              }
          })
      }

  }

  close() {
      this.modal.close(true);
      this.modal.destroy();
  }

}
