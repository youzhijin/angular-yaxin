import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { gpsListComponent } from './gps-list/gps-list.component';
import { AuthGuardService } from '@core/auth/auth-guard.service';
import { gpsViewEditComponent } from 'app/routes/gps/gps-list/edit/gps-edit.component';

const routes: Routes = [
  {
    path: 'gps',
    children: [
      { path: '', component: gpsListComponent },
      { path: 'edit/:id', component: gpsViewEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class gpsRoutingModule { }
