import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { SimpleTableColumn, SimpleTableComponent } from '@delon/abc';
import { NzMessageService, NzModalService, NzTabChangeEvent, UploadFile, NzNotificationService } from 'ng-zorro-antd';
import { CommService } from 'app/service/comm/comm.service';
import { Constant, ServiceResult } from '@core/constant';
import { EmitterService } from 'app/service/emit.service';
import { Router } from '@angular/router';
import { gpsService } from '../../../service/gps/gps.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Timeout } from '@shared/descriptor/timeout';




@Component({
  selector: 'gps-list',
  templateUrl: './gps-list.component.html',
  styleUrls: ['./gps-list.component.less']
})
export class gpsListComponent implements OnInit, OnDestroy {
  sjtj = 123; //事件统计
  // region 公共字段

  visible = false;
  // pageSize
  ps = 10;
  // 搜索表单
  q: any = {
    bpmStatus:"",

    // fiName: '',
    // cusMobile: '',
    // cusIncard: '',
    orderNo: ''
  };

  loading = false;
  // 表格组件
  @ViewChild('st') st: SimpleTableComponent;
  // 表格数据url
  url = this.gpsService.list();
  // region end

  // 搜索区域显示/隐藏标记
  // 显示图片
  previewVisible = false;
  // 大图地址
  previewImage = '';
  imgSuffix = '';

  years = [];
  userGroups = [];
  brands = [];
  seasons = [];
  checkedList = [];
  data = [];

  // uploadUrl = this.gpsService.uploadProds();
  uploadDateUrl = this.gpsService.batchRepdate();




  columns: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '客户姓名', index: 'orderNo' },
    { title: '车牌号', index: 'orderNo' },
    { title: '手机号', index: 'orderNo' },
    { title: '有线1', index: 'orderNo' },
    { title: '有线2', index: 'orderNo' },
    { title: '有线3', index: 'orderNo' },
    { title: '无线1', index: 'orderNo' },
    { title: '无线2', index: 'orderNo' },
    { title: '无线3', index: 'orderNo' },
    { title: '报警类型', index: 'orderNo' },
    {
        title: '操作',
        buttons: [
            {
              text: '查看',
              type: 'link',
              click: (record: any) => this.router.navigate(['/gps/gps/edit/'+record.id])
            },{
              text: '发送任务',
              type: 'link',
              click: (record: any) => this.router.navigate(['/gps/gps/edit/'+record.id])
            }
        ]
    }
  ];
  columnsa: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '客户姓名', index: 'orderNo' },
    { title: '车牌号', index: 'orderNo' },
    { title: '手机号', index: 'orderNo' },
    { title: '有线1', index: 'orderNo' },
    { title: '有线2', index: 'orderNo' },
    { title: '有线3', index: 'orderNo' },
    { title: '无线1', index: 'orderNo' },
    { title: '无线2', index: 'orderNo' },
    { title: '无线3', index: 'orderNo' },
    { title: '报警类型', index: 'orderNo' },
    {
        title: '操作',
        buttons: [
            {
              text: '查看',
              type: 'link',
              click: (record: any) => this.router.navigate(['/gps/gps/edit/'+record.id])
            },{
              text: '发送任务',
              type: 'link',
              click: (record: any) => this.router.navigate(['/gps/gps/edit/'+record.id])
            }
        ]
    }
  ];
  columnsb: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '客户姓名', index: 'orderNo' },
    { title: '车牌号', index: 'orderNo' },
    { title: '手机号', index: 'orderNo' },
    { title: '有线1', index: 'orderNo' },
    { title: '有线2', index: 'orderNo' },
    { title: '有线3', index: 'orderNo' },
    { title: '无线1', index: 'orderNo' },
    { title: '无线2', index: 'orderNo' },
    { title: '无线3', index: 'orderNo' },
    { title: '报警类型', index: 'orderNo' },
    {
        title: '操作',
        buttons: [
            {
              text: '查看',
              type: 'link',
              click: (record: any) => this.router.navigate(['/gps/gps/edit/'+record.id])
            },{
              text: '待维护',
              type: 'link',
              click: (record: any) => this.router.navigate(['/gps/gps/edit/'+record.id])
            },{
              text: '发提醒',
              type: 'link',
              click: (record: any) => this.router.navigate(['/gps/gps/edit/'+record.id])
            }
        ]
    }
  ];
  columnsc: SimpleTableColumn[] = [
    { title: '', index: 'key', type: 'checkbox' },
    { title: '客户姓名', index: 'orderNo' },
    { title: '车牌号', index: 'orderNo' },
    { title: '手机号', index: 'orderNo' },
    { title: '有线1', index: 'orderNo' },
    { title: '有线2', index: 'orderNo' },
    { title: '有线3', index: 'orderNo' },
    { title: '无线1', index: 'orderNo' },
    { title: '无线2', index: 'orderNo' },
    { title: '无线3', index: 'orderNo' },
    { title: '报警类型', index: 'orderNo' },
    {
        title: '操作',
        buttons: [
            {
              text: '查看',
              type: 'link',
              click: (record: any) => this.router.navigate(['/gps/gps/edit/'+record.id])
            },{
              text: '待核销',
              type: 'link',
              click: (record: any) => this.router.navigate(['/gps/gps/edit/'+record.id])
            }
        ]
    }
  ];
  constructor(
    private trans: TranslateService,
    public http: _HttpClient,
    public msg: NzMessageService,
    private modalSrv: ModalHelper,
    private comm: CommService,
    private cnt: Constant,
    private gpsService: gpsService,
    private emit: EmitterService,
    private router: Router,
    private modalService: NzModalService,
    private notice: NzNotificationService,
  ) {}
  syncing = false;
  lastDate = null;

  ngOnDestroy() {
    if (this.inter) {
      clearInterval(this.inter);
    }
  }

  inter
  ngOnInit() {
    this.imgSuffix = this.comm.getGoodsUrl();

    this.emit.getMessage().subscribe(data => {
      if (data.text === 'RELOAD_GOODS_LIST') {
        this.st.load(1, this.q);
      }
    })


  }

  fullChange(val: boolean) {
    // this.scroll = val ? { y: '350px' } : { y: '230px' };
  }



  change(args: NzTabChangeEvent) {
        switch (args.index) {
          case 0:
            this.q.bpmStatus = "";
            this.columns = this.columnsa;
            break;
          case 1:
            this.q.bpmStatus = 1;
            this.columns = this.columnsb;
            break;
          case 2:
            this.q.bpmStatus = 10;
            this.columns = this.columnsc;
            break;
          // case 3:
          //   this.q.bpmStatus = 20;
          //   this.columns = this.columnsa;
          //   break;

          // case 4:
          //   this.q.bpmStatus = 30;
          //   this.columns = this.columnsa;
          //   break;
          default:
            break;
        }
      this.st.load(1, this.q);
  }



  checkboxChange(list: any[]) {
    this.checkedList = list;
  }


  close(): void {
    this.visible = false;
  }



  cancel() {
    this.repdate = null;
  }
  repdate = null;



}
