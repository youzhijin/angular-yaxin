import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { gpsRoutingModule } from './gps-routing.module';
import { gpsListComponent } from './gps-list/gps-list.component';
import { gpsViewEditComponent } from 'app/routes/gps/gps-list/edit/gps-edit.component';

const COMPONENTS = [
  gpsListComponent
];
const COMPONENTS_NOROUNT = [
  gpsViewEditComponent
];

@NgModule({
  imports: [
    SharedModule,
    gpsRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class gpsModule { }
