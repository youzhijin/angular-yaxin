import { NgModule, Optional, SkipSelf } from '@angular/core';
import { throwIfAlreadyLoaded } from '@core/module-import-guard';
import { I18NService } from '@core/i18n/i18n.service';
import { Constant } from '@core/constant';
import { AuthGuardService } from '@core/auth/auth-guard.service';

@NgModule({
    providers: [      
        I18NService,
        AuthGuardService,
        Constant
    ]
})
export class CoreModule {
  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
