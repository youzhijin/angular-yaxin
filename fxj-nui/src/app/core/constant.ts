import { Injectable } from "@angular/core";

@Injectable()
export class Constant {
s
    // 基础url
    // public host: string = "https://localhost:8080/fxj380";
    // public host: string = "https://www.mintaidanbao.com/fxj380";
    // public host: string = "https://www.zhaodui.com.cn/fxj380";
    // public host: string = "https://www.zhaodui.com.cn/fxjyx";
    public host: string = "https://www.xmlidatong.com/fxjyx";

    // public host: string = "https://www.zhaodui.com.cn/fxjldt";

    // public host: string = "https://www.ebaokeji.cn/fxj380";
    // public host: string = "https://www.5abh.com/fxj380";

    // public host: string = "http://120.78.150.43/fxj380";
    //
    public baseUrl:string =  `${this.host}`; //101.132.180.129//192.168.254.63:8765
    public lockFlag: string = 'LOCK_FLAG';
    public YES: string = 'YES';
    public BoolUtil = {
        TRUE: '1',
        FALSE: '0'
    };

  public bpmStatusUtil = {
    TRUE: '1',
    FALSE: '0'
  };
  public replanUtil = {
    TRUE: '1',
    FALSE: '2'
  };
    public PopStatusUtil = {
        NORMAL: 'normal',
        DISABLED: 'disabled',
    };


    authKey = {
        user: {
            ADD_BTN: 'userManager:btn_add',
            EDIT_BTN: 'userManager:btn_edit',
            DEL_BTN: 'userManager:btn_del',
            APP_BTN: 'userManager:btn_app'
        },
        menu: {
            ADD_BTN: 'menuManager:btn_add',
            EDIT_BTN: 'menuManager:btn_edit',
            DEL_BTN: 'menuManager:btn_del'
        },
        element: {
            ADD_BTN: 'menuManager:btn_element_add',
            EDIT_BTN: 'menuManager:btn_element_edit',
            DEL_BTN: 'menuManager:btn_element_del'
        },
        group: {
            ADD_BTN: 'groupManager:btn_add',
            EDIT_BTN: 'groupManager:btn_edit',
            DEL_BTN: 'groupManager:btn_del',
            USER_BTN: 'groupManager:btn_userManager',
            AUTH_BTN: 'groupManager:btn_resourceManager',
            AUTH_MENU_BTN: 'groupManager:authorize_menu',
            AUTHORIZE_BTN: 'groupManager:btn_authorizeManager',
            AUTHORIZE_ELEMENT: 'groupManager:authorize_element'
        },
        groupType: {
            ADD_BTN: 'groupTypeManager:btn_add',
            EDIT_BTN: 'groupTypeManager:btn_edit',
            DEL_BTN: 'groupTypeManager:btn_del'
        },
        dictType: {
            ADD_BTN: 'dictTypeManager:btn_add',
            EDIT_BTN: 'dictTypeManager:btn_edit',
            DEL_BTN: 'dictTypeManager:btn_del'
        },
        dictValue: {
            ADD_BTN: 'dictValueManager:btn_add',
            EDIT_BTN: 'dictValueManager:btn_edit',
            DEL_BTN: 'dictValueManager:btn_del'
        },
        tenant: {
            ADD_BTN: 'tenantManager:btn_add',
            EDIT_BTN: 'tenantManager:btn_edit',
            DEL_BTN: 'tenantManager:btn_del',
            USER_BTN: 'tenantManager:btn_user'
        },
        depart: {
            ADD_BTN: 'departManager:btn_add',
            EDIT_BTN: 'departManager:btn_edit',
            DEL_BTN: 'departManager:btn_del',
            DEL_MEMBER_BTN: 'departManager:btn_user_del',
            ADD_MEMBER_BTN: 'departManager:btn_user_add'
        },
        position: {
            DEPART_BTN: 'positionManager:btn_depart',
            MEMBER_BTN: 'positionManager:btn_user',
            DEL_BTN: 'positionManager:btn_del',
            ADD_BTN: 'positionManager:btn_add',
            EDIT_BTN: 'positionManager:btn_edit'
        },
        client: {
            ADD_BTN: 'serviceManager:btn_add',
            EDIT_BTN: 'serviceManager:btn_edit',
            DEL_BTN: 'serviceManager:btn_del',
            AUTH_BTN: 'serviceManager:btn_clientManager'
        },
        gate: {
            ADD_BTN: 'gate:add_btn',
            EDIT_BTN: 'gate:edit_btn',
            DEL_BTN: 'gate:del_btn',
            REF_BTN: 'gate:refresh_btn'
        },
        cart: {
            ADD_BTN: 'sms_prodlist_editbtn',
            CART_BTN: 'sms_prodlist_cartbtn'
        }
    }
}


export interface ServiceResult {
    status: number;
    message: string;
    [propName: string]: any;
}
