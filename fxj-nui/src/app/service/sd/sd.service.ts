import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import {_HttpClient, SettingsService} from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class sdService {
  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient,
    public setting: SettingsService,

  ) {}

  /**
   * 分页获取
   */
  list(): string {
    return `${this.cnt.baseUrl}/rest/sdCarinfoController/list/`+this.setting.user.username;
  }
  list0(carVin): string {
    return `${this.cnt.baseUrl}/rest/sdCarinfoController/listdetail0/`+carVin;
  }
  list1(carVin): string {
    return `${this.cnt.baseUrl}/rest/sdCarinfoController/listdetail1/`+carVin;
  }
  list2(carVin): string {
    return `${this.cnt.baseUrl}/rest/sdCarinfoController/listdetail2/`+carVin;
  }
  list3(carVin): string {
    return `${this.cnt.baseUrl}/rest/sdCarinfoController/listdetail3/`+carVin;
  }
  list4(carVin): string {
    return `${this.cnt.baseUrl}/rest/sdCarinfoController/listdetail4/`+carVin;
  }
  list5(carVin): string {
    return `${this.cnt.baseUrl}/rest/sdCarinfoController/listdetail5/`+carVin;
  }
  list6(carVin): string {
    return `${this.cnt.baseUrl}/rest/sdCarinfoController/listdetail6/`+carVin;
  }
  list7(carVin): string {
    return `${this.cnt.baseUrl}/rest/sdCarinfoController/listdetail7/`+carVin;
  }
  list8(carVin): string {
    return `${this.cnt.baseUrl}/rest/sdCarinfoController/listdetail8/`+carVin;
  }
  list9(carVin): string {
    return `${this.cnt.baseUrl}/rest/sdCarinfoController/listdetail9/`+carVin;
  }

//新增
  cre(cindex,prods) {
    return this.http.post(
      `${this.cnt.baseUrl}/rest/sdCarinfoController/cre/`+cindex,
      prods,
    );
  }
  // 上传
  uploadcarpicurl(): string {
    return `${this.cnt.baseUrl}/rest/sdCarinfoController/uploadcarpic`;
  }
  //删除
  delid(id,deltype) {
    return this.http.get(`${this.cnt.baseUrl}/rest/sdCarinfoController/del/`+id+"?deltype="+deltype);
  }
    getProduct(productId) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/sdCarinfoController/${productId}`,
    );
  }

  /**
   * 添加商品
   * @param prods
   */
  addPro(sdCarinfoPage) {
    return this.http.post(
      `${this.cnt.baseUrl}/rest/sdCarinfoController/`+this.setting.user.username,
      sdCarinfoPage,
    );
  }

}
