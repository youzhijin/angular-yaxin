import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import { _HttpClient } from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class StockoutService {

  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient
  ) { }

  /**
   * 执行缺货SKU报表
   */
  doStockoutsku() {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/order/stockout/doreport`);
  }

  /**
   * 获取缺货SKU报表计算状态
   */
  getStockoutSkuStatus() {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/order/stockout/reportstatus`);
  }

  list() {
    return `${this.cnt.baseUrl}/api/sms/order/stockout/page`;
  }

  page(pageNumber, pageSize) {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/order/stockout/page?pageNumber=${pageNumber}&pageSize=${pageSize}`);
  }
  
}