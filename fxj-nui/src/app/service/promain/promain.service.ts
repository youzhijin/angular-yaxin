import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import {_HttpClient, SettingsService} from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class promainService {
  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient,
    public setting: SettingsService,

  ) {}

  /**
   * 分页获取商品
   */
  list(): string {
    return `${this.cnt.baseUrl}/rest/fxjProMainController/list/`+this.setting.user.username;
  }
  /**
   * 分页获取商品
   */
  listdhcar(): string {
    return `${this.cnt.baseUrl}/rest/fxjDhImpController/list/`+this.setting.user.username;
  }
  list2(): string {
    return `${this.cnt.baseUrl}/api/sms/prod/product/page2`;
  }
  /**
   * 上传图片
   */
  uploadUrl() {
    return `${this.cnt.baseUrl}/zuul/api/sms/prod/product/proImg`;
  }

  /**
   * 添加商品
   * @param prods
   */
  addPro(prods) {
    return this.http.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/addPro`,
      prods,
    );
  }

  /**
   * 修改商品
   * @param product
   */
  editPro(product) {
    return this.http.put(
      `${this.cnt.baseUrl}/rest/fxjLoanMaController/${product.id}`,
      product,
    );
  }

  getProduct(productId) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjLoanMaController/${productId}`,
    );
  }

  batchUnder(ids) {
    return this.myHttp.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/batchDown`,
      { ids: ids },
    );
  }

  batchSale(ids) {
    return this.myHttp.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/batchUp`,
      { ids: ids },
    );
  }

  batchRepdate(): string {
    return `${this.cnt.baseUrl}/zuul/api/sms/prod/product/batchRepdate`;
  }

  getProductByIds(ids) {
    return this.myHttp.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/getProducts`,
      { ids: ids },
    );
  }

  batchPrice(list) {
    return this.http.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/batchPrice`,
      list,
    );
  }

  configUpload(): string {
    return `${this.cnt.baseUrl}/zuul/api/sms/prod/product/priceConfig`;
  }

  uploadcarinfo(): string {
    console.log("this.setting.user.username &&&&&&"+this.setting.user.username);
    return `${this.cnt.baseUrl}/rest/fxjDhImpController/uploadexcel?username=`+this.setting.user.username;
  }
  getwzqueryg(id) {
    return this.http.get(`${this.cnt.baseUrl}/rest/fxjDhImpController/querywz/`+id);
  }
  getwzheadurl(id) {
    return `${this.cnt.baseUrl}/rest/fxjDhCarInfoController/list/`+id;
  }
  getwzdetailurl(id) {
    return `${this.cnt.baseUrl}/rest/fxjDhCarWzdetailController/list/`+id;
  }
  getSyncLog() {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/prod/product/syncstatus`);
  }

  doSync() {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/prod/product/dosync`);
  }
}
