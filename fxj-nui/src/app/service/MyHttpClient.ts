import { Injectable, Injector } from "@angular/core";
import { _HttpClient } from "@delon/theme";
import { Observable } from "rxjs/Observable";
import { HttpHeaders, HttpClient } from "@angular/common/http";

@Injectable()
export class MyHttpClient {
    constructor(private injector: Injector, 
        private http: _HttpClient
    ) {  
    }

    post(url, param) {
        return Observable.create(observer => {
            let head = new HttpHeaders({
                'Content-Type':'application/x-www-form-urlencoded;charset=utf-8'
            });            
            this.http.post(url, null, param,
             {headers: head}).subscribe(data => {                
                 observer.next(data)
             }, (err) => {                         
                 observer.next(err)
             },() => {                
                 observer.complete();
             })
        });  
    }

    postJson(url, param) {
        return Observable.create(observer => {            
            this.http.post(url, null, param).subscribe(data => {                
                 observer.next(data)
             }, (err) => {                                                    
                 observer.next(err)
             },() => {                
                 observer.complete();
             })
        });  
    }
}