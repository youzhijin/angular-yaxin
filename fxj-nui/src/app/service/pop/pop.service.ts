import { Injectable, Injector } from '@angular/core';

import { Constant, ServiceResult } from '@core/constant';
import { _HttpClient } from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class PopService {
  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient,
  ) {}

  list() {
    return `${this.cnt.baseUrl}/api/sms/pop/pop/page`;
  }

  productList(q) {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/pop/pop/productList`, q);
  }
  productList2(): string {
    return `${this.cnt.baseUrl}/api/sms/pop/pop/productList`;
  }

  save(pop) {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/pop/pop/save`, pop);
  }

  popMaintenanceList() {
    return `${this.cnt.baseUrl}/api/sms/pop/pop/popMaintenanceList`;
  }

  modifyCart(id, brand, qty) {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/pop/cart/add`, {
      id: id,
      brand: brand,
      qty: qty,
      isAppend: '0',
    });
  }

  cartList() {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/pop/cart/cartList`);
  }

  delByPopId(popId) {
    return this.http.delete(`${this.cnt.baseUrl}/api/sms/pop/cart/delByPopId`, {
      popId: popId,
    });
  }

  disable(id) {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/pop/pop/disable`, {
      id: id,
    });
  }
  enable(id) {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/pop/pop/enable`, {
      id: id,
    });
  }
  delByBrand(brand) {
    return this.http.delete(`${this.cnt.baseUrl}/api/sms/pop/cart/delByBrand`, {
      brand: brand,
    });
  }
  creatOrder(brand) {
    return this.myHttp.post(`${this.cnt.baseUrl}/api/sms/pop/cart/creatOrder`, {
      brand: brand,
    });
  }
  creatOrderNoBrand() {
    return this.myHttp.post(
      `${this.cnt.baseUrl}/api/sms/pop/cart/creatOrderNoBrand`,
      {},
    );
  }

  detail(billId) {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/pop/pop/info/${billId}`);
  }

  sureOrder(id, pzDate = null, jhDate = null) {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/pop/pop/sure/${id}`, {
      jhDate: jhDate,
      pzDate: pzDate,
    });
  }

  cancelOrder(id) {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/pop/pop/cancel/${id}`);
  }

  listPopById(popId) {
    return this.http.get(
      `${this.cnt.baseUrl}/api/sms/pop/pop/listPopById/${popId}`,
    );
  }

  batchUp(ids: string) {
    return this.myHttp.post(`${this.cnt.baseUrl}/api/sms/pop/pop/batchUp`, {
      ids: ids,
    });
  }

  batchDown(ids: string) {
    return this.myHttp.post(`${this.cnt.baseUrl}/api/sms/pop/pop/batchDown`, {
      ids: ids,
    });
  }

  uploadPops(): string {
    return `${this.cnt.baseUrl}/zuul/api/sms/pop/pop/upload/pop`;
  }

  listPopItems(q) {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/pop/pop/listPopItems`, q);
  }
  // 与SimpleTable配合的连接
  listPopItems2() {
    return `${this.cnt.baseUrl}/api/sms/pop/pop/listPopItems`;
  }
  listPopItemById(itemId) {
    // return this.http.post(`${this.cnt.baseUrl}/api/sms/pop/pop/selectPopItemById/${id}`);
    return this.http.get(
      `${this.cnt.baseUrl}/api/sms/pop/pop/selectPopItemById/${itemId}`,
    );
  }
  savePopItem(popItem) {
    return this.http.post(
      `${this.cnt.baseUrl}/api/sms/pop/pop/savePopItem`,
      popItem,
    );
  }
  batchUpPopItems(ids: string) {
    return this.myHttp.post(
      `${this.cnt.baseUrl}/api/sms/pop/pop/batchUpPopItems`,
      {
        ids: ids,
      },
    );
  }

  batchDownPopItems(ids: string) {
    return this.myHttp.post(
      `${this.cnt.baseUrl}/api/sms/pop/pop/batchDownPopItems`,
      {
        ids: ids,
      },
    );
  }
}
