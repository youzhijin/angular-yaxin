import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import {_HttpClient, SettingsService} from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class ProductService {
  get cnt(): Constant {
    return this.injector.get(Constant);
  }
  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient,
    public setting: SettingsService,

  ) {}

  /**
   * 分页获取商品
   */
  list(): string {
    return `${this.cnt.baseUrl}/rest/fxjOrderMainController/list/`+this.setting.user.username;
  }

  listyq(): string {
    return `${this.cnt.baseUrl}/rest/dailyLateController/list/`+this.setting.user.username;
  }
  list0(): string {
    return `${this.cnt.baseUrl}/rest/yxCwController/list/`+this.setting.user.username ;
  }
  cwbilist(): string {
    return `${this.cnt.baseUrl}/rest/yxCwController/listtongji/`+this.setting.user.username ;
  }
  upload(): string {
    return `${this.cnt.baseUrl}/rest/fxjOrderMainController/uploadexcelordermain?username=`+this.setting.user.username;;
  }
  uploadyq(): string {
    return `${this.cnt.baseUrl}/rest/dailyLateController/uploadexcel?username=`+this.setting.user.username;;
  }
  uploadfk(): string {
    return `${this.cnt.baseUrl}/rest/fxjOrderMainController/uploadexcelfk?username=`+this.setting.user.username;;
  }



  listfxjpoprice(orderNo): string {
    return `${this.cnt.baseUrl}/rest/fxjOrderMainController/listdetail1/`+orderNo;
  }

  createfxjpoprice(prods) {
    return this.http.post(
      `${this.cnt.baseUrl}/rest/fxjOrderMainController/cre/1`,
      prods,
    );
  }
  //删除
  delid(id,deltype) {
    return this.http.get(`${this.cnt.baseUrl}/rest/fxjOrderMainController/del/`+id+"?deltype="+deltype);
  }

  //删除
  getrmbyid(id) {
    return this.http.get(`${this.cnt.baseUrl}/rest/fxjOrderMainController/getrm/`+id );
  }
  /**
   * 分页获取商品
   */
  logurl(): string {
    return `${this.cnt.baseUrl}/rest/fxjOrderMainController/loglist/`;
  }

  rmurl(): string {
    return `${this.cnt.baseUrl}/rest/fxjOrderMainController/rmlist/`;
  }
  emenurl(): string {
    return `${this.cnt.baseUrl}/rest/fxjOrderMainController/emenlist/`;
  }
  list2(): string {
    return `${this.cnt.baseUrl}/api/sms/prod/product/page2`;
  }
  /**
   * 上传图片
   */
  uploadUrl() {
    return `${this.cnt.baseUrl}/rest/base/upload`;
  }

  /**
   * 文件管理
   */
  uploadfileUrl() {
    return `${this.cnt.baseUrl}/rest/base/uploadfile`;
  }
  /**
   * 上传图片
   */
  uploadsbUrl() {
    return `${this.cnt.baseUrl}/rest/base/uploadsb`;
  }
  /**
   * 添加商品
   * @param prods
   */
  addPro(fxjOrderMainPage) {
    return this.http.post(
      `${this.cnt.baseUrl}/rest/fxjOrderMainController/`+this.setting.user.username,
      fxjOrderMainPage,
    );
  }
  savemen(fxjOrderMainPage) {
    return this.http.post(
      `${this.cnt.baseUrl}/rest/fxjOrderMainController/savemen`+this.setting.user.username,
      fxjOrderMainPage,
    );
  }
  gencon(orderNo,conHead) {
    return this.http.post(
      `${this.cnt.baseUrl}/rest/base/getcon/`+orderNo,
      conHead,
    );
  }


  /**
   * 修改商品
   * @param product
   */
  editPro(product) {
    return this.http.put(
      `${this.cnt.baseUrl}/rest/fxjOrderMainController/${product.id}`,
      product,
    );
  }
  editPro1(product) {
    return this.http.put(
      `${this.cnt.baseUrl}/rest/yxCwController/${product.id}`,
      product,
    );
  }
  editProyq(product) {
    return this.http.put(
      `${this.cnt.baseUrl}/rest/dailyLateController/${product.id}`,
      product,
    );
  }
  /**
   * 修改商品
   * @param product
   */
  stopmq(mqlog) {
    return this.http.put(
      `${this.cnt.baseUrl}/rest/fxjMqLogController/stop`,
      mqlog,
    );
  }

  getmqsp(orderNo) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjMqLogController/${orderNo}`,
    );
  }
  /**
   * 修改商品
   * @param product
   */
  rmquery(fkname,fkinput) {
    return this.http.post(
      `${this.cnt.baseUrl}/rest/fk/${fkname}`,
      fkinput,
    );
  }
  delfile(productId) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/base/delfile/${productId}`,
    );
  }
  getProduct(productId) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjOrderMainController/${productId}`,
    );
  }
  getProductyq(productId) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/dailyLateController/${productId}`,
    );
  }
  getCw(productId) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/yxCwController/${productId}`,
    );
  }

  getfileList(productId,attaType) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjOrderMainController/filelist/${productId}`+"?attaType="+attaType,
    );
  }
  getbihome(bitype) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/basebi/getbi/`+this.setting.user.username+"?pageNumber=1&pageSize=9999&bitype="+bitype,
    );
  }
  batchRepdate(): string {
    return `${this.cnt.baseUrl}/zuul/api/sms/prod/product/batchRepdate`;
  }
  
  // fxjbiData10Controller/list/admin?pageNumber=1&pageSize=10
  getbiCwUrl(begindate,enddate) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/basebi/getbificobi/`+this.setting.user.username+"?begindate="+begindate+'&enddate='+enddate,
    );
  }

  getCwUrl(fxjOut01,fxjOut02,fxjOut03,fxjOut48,fxjOut49) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/yxCwController/list/`+this.setting.user.username+"?pageNumber=1&pageSize=10"+"&fxjOut01="+fxjOut01+'&fxjOut02='+fxjOut02 +"&fxjOut03="+fxjOut03+'&fxjOut48='+fxjOut48 +"&fxjOut49="+fxjOut49,
    );
  }

  getCwbi(fxjOut01,fxjOut02,fxjOut03) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/yxCwController/listtongji/`+this.setting.user.username+"?pageNumber=1&pageSize=10"+"&fxjOut01="+fxjOut01+'&fxjOut02='+fxjOut02 +"&fxjOut03="+fxjOut03,
    );
  }
  getbi(bitype) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/basebi/getbi/`+this.setting.user.username+"?bitype="+bitype,
    );
  }

}
