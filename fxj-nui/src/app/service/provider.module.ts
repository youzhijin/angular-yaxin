import { NgModule } from '@angular/core';
import { LoginService } from './login.service';
import { MyHttpClient } from './MyHttpClient';
import { EmitterService } from './emit.service';
import { CommService } from './comm/comm.service';
import { UserService } from './user/user.service';
import { DepartService } from './depart/depart.service';
import { MenuService } from './menu/menu.service';
import { GroupService } from './group/group.service';
import { DictService } from './dict/dict.service';
import { TenantService } from './tenant/tenant.service';
import { AuthService } from './auth/auth.service';
import { plService }  from 'app/service/pl/pl.service';
import { ploService }  from 'app/service/plo/plo.service';
import { plopService }  from 'app/service/plop/plop.service';
import { fkService }  from 'app/service/fk/fk.service';
import { insService }  from 'app/service/ins/ins.service';
import { maService }  from 'app/service/ma/ma.service';
import { sdService }  from 'app/service/sd/sd.service';

import { syspjService }  from 'app/service/syspj/syspj.service';
import {qylbService} from "./qylb/qylb.service";

import { ProductService } from 'app/service/prod/product.service';
import { CartService } from 'app/service/cart/cart.service';
import { OrderService } from 'app/service/order/order.service';
import { PopService } from './pop/pop.service';
import { SysUserService } from 'app/service/sys/sys-user.service';
import { SmsService } from 'app/service/sms/sms.service';
import { StockoutService } from 'app/service/order/stockout.service';
import {fileService} from "./file/file.service";
import {promainService} from "./promain/promain.service";
import {prodListService} from "./prodList/prodList.service";
import {spListService} from "./spList/spList.service";
import {csListService} from "./csList/csList.service";

import {noticeService} from "./notice/notice.service";

import {workbenchService} from "./workbench/workbench.service";

import {repayService} from "./repay/repay.service";

import {gpsService} from "./gps/gps.service";
import {wfglService} from "./wfgl/wfgl.service";
import {qcglService} from "./qcgl/qcgl.service";
import {zcglService} from "./zcgl/zcgl.service";
import {htjqService} from "./htjq/htjq.service";

import {xsjkService} from "./xsjk/xsjk.service";

import {fxyjService} from "./fxyj/fxyj.service";

import {cwglService} from "./cwgl/cwgl.service";

import {dashboardService} from "./dashboard/dashboard.service";
import {jcbgService} from "./jcbg/jcbg.service";
import {dh10Service} from "./dh10/dh10.service";

@NgModule({
    imports: [

    ],
    providers: [
        LoginService,
        MyHttpClient,
        EmitterService,
        CommService,
        UserService,
        DepartService,
        MenuService,
        GroupService,
        DictService,
        TenantService,
        AuthService,
        plService,
        ploService,
        promainService,
        sdService,
        prodListService,
        plopService,
        fkService,
        insService,
        maService,
        fileService,
        ProductService,
        CartService,
        OrderService,
        PopService,
        SysUserService,
        SmsService,
        syspjService,
        StockoutService,
        spListService,
        csListService,
        workbenchService,

        noticeService,
     dh10Service,

        repayService,
        gpsService,
        wfglService,
        qcglService,
        zcglService,
        htjqService,

        xsjkService,

        fxyjService,

        cwglService,
        qylbService,

        dashboardService,
		jcbgService
    ],
    declarations: [
    ],
    exports: [
    ]
})
export class ProvidersModule { }
