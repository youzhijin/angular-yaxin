import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import {_HttpClient, SettingsService} from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class syspjService {
  get cnt(): Constant {
    return this.injector.get(Constant);
  }
  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient,
    public setting: SettingsService,

  ) {}

  /**
   * 分页获取商品
   */
  list(): string {
    return `${this.cnt.baseUrl}/rest/sysPingjiaController/list/`+this.setting.user.username;
  }
  /**
   * 分页获取商品
   */
  logurl(): string {
    return `${this.cnt.baseUrl}/rest/fxjOrderMainController/loglist/`;
  }

  rmurl(): string {
    return `${this.cnt.baseUrl}/rest/fxjOrderMainController/rmlist/`;
  }

  list2(): string {
    return `${this.cnt.baseUrl}/api/sms/prod/product/page2`;
  }
  /**
   * 上传图片
   */
  uploadUrl() {
    return `${this.cnt.baseUrl}/rest/base/upload`;
  }

  /**
   * 文件管理
   */
  uploadfileUrl() {
    return `${this.cnt.baseUrl}/rest/base/uploadfile`;
  }
  /**
   * 上传图片
   */
  uploadsbUrl() {
    return `${this.cnt.baseUrl}/rest/base/uploadsb`;
  }
  /**
   * 添加商品
   * @param prods
   */
  addPro(fxjOrderMainPage) {
    return this.http.post(
      `${this.cnt.baseUrl}/rest/fxjOrderMainController/`+this.setting.user.username,
      fxjOrderMainPage,
    );
  }

  /**
   * 修改商品
   * @param product
   */
  editPro(product) {
    return this.http.put(
      `${this.cnt.baseUrl}/rest/sysPingjiaController/${product.id}`,
      product,
    );
  }


  /**
   * 修改商品
   * @param product
   */
  batcheditPro(product) {
    return this.http.put(
      `${this.cnt.baseUrl}/rest/sysPingjiaController/BATCH`,
      product,
    );
  }
  /**
   * 修改商品
   * @param product
   */
  rmquery(fkname,fkinput) {
    return this.http.post(
      `${this.cnt.baseUrl}/rest/fk/${fkname}`,
      fkinput,
    );
  }

  getlist(pjType) {
    return this.http.get(
       this.list()+'?pjtype='+pjType+'&pageNumber=1&pageSize=2000',
    );
  }
  getProduct(productId) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjOrderMainController/${productId}`,
    );
  }

  getfileList(productId,attaType) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjOrderMainController/filelist/${productId}`+"?attaType="+attaType,
    );
  }

  getbi(bitype) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/basebi/getbi/`+this.setting.user.username+"?bitype="+bitype,
    );
  }

}
