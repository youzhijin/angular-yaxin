import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import {_HttpClient, SettingsService} from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class fileService {
  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient,
    public setting: SettingsService,

  ) {}

  /**
   * 分页获取商品
   */
  list(): string {
    return `${this.cnt.baseUrl}/rest/fxjFileMainController/list/`+this.setting.user.username;
  }
  /**
   * 上传图片
   */
  uploadUrl() {
    return `${this.cnt.baseUrl}/rest/base/uploadfile`;
  }
  /**
   * 添加商品
   * @param prods
   */
  addfile(fileobj) {
    return this.http.post(
      `${this.cnt.baseUrl}/rest/fxjFileMainController`,
      fileobj,
    );
  }

  /**
   * 修改商品
   * @param product
   */
  editfile(fileobj) {
    return this.http.put(
      `${this.cnt.baseUrl}/rest/fxjFileMainController/${fileobj.id}`,
      fileobj,
    );
  }

  getfile(fileobjid) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjFileMainController/${fileobjid}`,
    );
  }

}
