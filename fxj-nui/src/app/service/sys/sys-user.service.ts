import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import { _HttpClient } from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class SysUserService {

  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient
  ) { }

  /**
   * 获取用户列表
   */
  list(): string {        
    return `${this.cnt.baseUrl}/api/admin/user/page`;
  }

  /**
   * 获取用户信息
   * @param userId 
   */
  info(userId) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/user/${userId}`);
  }

  /**
   * 更新用户
   * @param user 
   */
  modify(user: User) {
    return this.http.put(`${this.cnt.baseUrl}/api/admin/user/${user.id}`, user);
  }

  add(user: User) {
    return this.http.post(`${this.cnt.baseUrl}/api/admin/user`, user);
  }

  delete(id) {
    return this.http.delete(`${this.cnt.baseUrl}/api/admin/user/${id}`);
  }

  /**
   * 密码修改
   * @param userName 
   * @param oldPassword 
   * @param newPassword 
   */
  modifyPassword(userName, oldPassword, newPassword) {
      return this.myHttp.post(`${this.cnt.baseUrl}/api/admin/user/modifyPassword`, {
          username: userName,
          oldPwd: oldPassword,
          newPwd: newPassword
      });
  }

  /**
   * 返回用户头像上传路径
   */
  uploadUrl() {
    return `${this.cnt.baseUrl}/zuul/api/admin/user/avatar`;
  }
}



export class User {
    id?: string;
    username?: string;
    name?: string;
    password?: string;
    email?: string;
    mobilePhone?: string;
    birthday?: string;
    sex?: string;
    description?: string;
    isDeleted?: string;
    isDisabled?: string;
    departId?: string;
    isSuperAdmin?: string;
    isAppManager?: string;
    [propName: string]: any;
}