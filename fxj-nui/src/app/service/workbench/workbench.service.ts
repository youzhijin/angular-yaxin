import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import {_HttpClient, SettingsService} from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class workbenchService {
  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient,
    public setting: SettingsService,

  ) {}
  // 风控报表 
  geiFkBiUrl(fxjOut01,fxjOut09,fxjOut10) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjbiData09Controller/list/`+this.setting.user.username+"?pageNumber=1&pageSize=9999&fxjOut01="+fxjOut01+'&fxjOut09='+fxjOut09+'&fxjOut10='+fxjOut10,
    );
  }
  // 亚信贷前
  getYxFkBiUrl(begindate,enddate) {
    return this.http.get(///rest/basebi/getbidqbi/YX001
      `${this.cnt.baseUrl}/rest/basebi/getbidqbi/`+this.setting.user.username+"?begindate="+begindate+"&enddate="+enddate,
    );
  }
  // 业务报表
  geiYwBiUrl(fxjOut01,fxjOut09,fxjOut10) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjbiData08Controller/list/`+this.setting.user.username+"?pageNumber=1&pageSize=9999&fxjOut01="+fxjOut01+'&fxjOut09='+fxjOut09+'&fxjOut10='+fxjOut10,
    );
  }

  /**
   * 分页获取商品
   */
  list(): string {
    // return `${this.cnt.baseUrl}/rest/fxjLoanMaController/list/`+this.setting.user.username;
    return `${this.cnt.baseUrl}/rest/fxjOrderMainController/list/`+this.setting.user.username;
  }
  /**
   * 分页获取商品
   */
  listdhcar(): string {
    return `${this.cnt.baseUrl}/rest/fxjDhImpController/list/`+this.setting.user.username;
  }
  listposdetail(): string {
    return `${this.cnt.baseUrl}/rest/posDetailController/list/`+this.setting.user.username;
  }
  list2(): string {
    return `${this.cnt.baseUrl}/api/sms/prod/product/page2`;
  }
    /**
   * 上传图片
   */
  uploadsbUrl() {
    return `${this.cnt.baseUrl}/rest/base/uploadsb`;
  }
  uploadUrl() {
    return `${this.cnt.baseUrl}/rest/base/upload`;
  }
  // /**
  //  * 上传图片
  //  */
  // uploadUrl() {
  //   return `${this.cnt.baseUrl}/zuul/api/sms/prod/product/proImg`;
  // }

  /**
   * 添加商品
   * @param prods
   */
  addPro(prods) {
    return this.http.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/addPro`,
      prods,
    );
  }
  // 征信保存
  zhengxin(product) {
    return this.http.post(
      `${this.cnt.baseUrl}/rest/fxjdqData01Controller`,
      product,
    );
  }
  // 机器审批
  jiqisp(product) {
    return this.http.post(
      `${this.cnt.baseUrl}/rest/fxjdqData02Controller`,
      product,
    );
  }
  // 人工审批
  rengsp(product) {
    return this.http.post(
      `${this.cnt.baseUrl}/rest/fxjdqData03Controller`,
      product,
    );
  }

  /**
   * 修改商品
   * @param product
   */
  editPro(product) {
    return this.http.put(
      `${this.cnt.baseUrl}/rest/fxjLoanMaController/${product.id}`,
      product,
    );
  }
  getProduct(productId) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjOrderMainController/${productId}`,
    );
  }
  // 获取征信报告
  getZx() {
   return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjdqData01Controller/list/`+this.setting.user.username+'?pageNumber=1&pageSize=10',
    );
  }
  // 获取机器审批
  getJq() {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjdqData02Controller/list/`+this.setting.user.username+'?pageNumber=1&pageSize=10',
    );
  }
  // 获取人工审批
  getRg() {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjdqData03Controller/list/`+this.setting.user.username+'?pageNumber=1&pageSize=10',
    );
  }
  logurl(): string {
    return `${this.cnt.baseUrl}/rest/fxjOrderMainController/loglist/`;
  }

  batchUnder(ids) {
    return this.myHttp.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/batchDown`,
      { ids: ids },
    );
  }

  batchSale(ids) {
    return this.myHttp.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/batchUp`,
      { ids: ids },
    );
  }

  batchRepdate(): string {
    return `${this.cnt.baseUrl}/zuul/api/sms/prod/product/batchRepdate`;
  }

  getProductByIds(ids) {
    return this.myHttp.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/getProducts`,
      { ids: ids },
    );
  }

  batchPrice(list) {
    return this.http.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/batchPrice`,
      list,
    );
  }

  configUpload(): string {
    return `${this.cnt.baseUrl}/zuul/api/sms/prod/product/priceConfig`;
  }

  uploadcarinfo(): string {
    return `${this.cnt.baseUrl}/rest/fxjDhImpController/uploadexcel?username=`+this.setting.user.username;
  }

  uploadposdetail(): string {
    return `${this.cnt.baseUrl}/rest/posDetailController/uploadexcel?username=`+this.setting.user.username;
  }


  uploadpostask(): string {
    return `${this.cnt.baseUrl}/rest/posXjtaskController/uploadexcel?username=`+this.setting.user.username;
  }
  getpostasklistall() {

      return `${this.cnt.baseUrl}/rest/posXjtaskController/list/`+this.setting.user.username;


  }
  getpostasklist(id) {

      return `${this.cnt.baseUrl}/rest/posXjtaskController/listbyposno/`+id;


  }

  getposdetail(id) {
    return this.http.get(`${this.cnt.baseUrl}/rest/posDetailController/`+id);
  }
  getposfilelist(id) {
    return this.http.get(`${this.cnt.baseUrl}/rest/posXjtaskController/filelist/`+id);
  }
  getpostaskdetail(id) {
    return this.http.get(`${this.cnt.baseUrl}/rest/posXjtaskController/`+id);
  }

  getwzqueryg(id) {
    return this.http.get(`${this.cnt.baseUrl}/rest/fxjDhImpController/querywz/`+id);
  }
  getwzheadurl(id) {
    return `${this.cnt.baseUrl}/rest/fxjDhCarInfoController/list/`+id;
  }
  getwzdetailurl(id) {
    return `${this.cnt.baseUrl}/rest/fxjDhCarWzdetailController/list/`+id;
  }
  getSyncLog() {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/prod/product/syncstatus`);
  }

  doSync() {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/prod/product/dosync`);
  }
}
