import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import {_HttpClient, SettingsService} from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class plService {
  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private http: _HttpClient,
    private injector: Injector,
    private myHttp: MyHttpClient,
    public setting: SettingsService,

  ) {}

  /**
   * 分页获取商品
   */
  list(): string {
    return `${this.cnt.baseUrl}/rest/fxjPlReplanController/list/`+this.setting.user.username;
  }
  listorder(orderNo): string {
    return `${this.cnt.baseUrl}/rest/fxjPlReplanController/listbyorderNo/`+orderNo;
  }
  list2(): string {
    return `${this.cnt.baseUrl}/api/sms/prod/product/page2`;
  }
  /**
   * 上传图片
   */
  uploadUrl() {
    return `${this.cnt.baseUrl}/zuul/api/sms/prod/product/proImg`;
  }

  /**
   * 添加商品
   * @param prods
   */
  addPro(prods) {
    return this.http.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/addPro`,
      prods,
    );
  }

  /**
   * 修改商品
   * @param product
   */
  editPro(product) {
    return this.http.put(
      `${this.cnt.baseUrl}/rest/fxjPlReplanController/${product.id}`,
      product,
    );
  }
  /**
   * 修改商品
   * @param product
   */
  yuqiPro(product) {
    // return this.http.post(
    //   `${this.cnt.baseUrl}/rest/fxjdhData01Controller/${product.id}`,
    //   product,
    // );
    return this.http.post(
      `${this.cnt.baseUrl}/rest/`,
      product,
    );
  }


  uploadreplaninfo(): string {
    return `${this.cnt.baseUrl}/rest/fxjPlReplanController/uploadexcel?username=`+this.setting.user.username;
  }

  getProduct(productId) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjPlReplanController/${productId}`,
    );
  }

  batchUnder(ids) {
    return this.myHttp.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/batchDown`,
      { ids: ids },
    );
  }

  batchSale(ids) {
    return this.myHttp.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/batchUp`,
      { ids: ids },
    );
  }

  batchRepdate(): string {
    return `${this.cnt.baseUrl}/zuul/api/sms/prod/product/batchRepdate`;
  }

  getProductByIds(ids) {
    return this.myHttp.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/getProducts`,
      { ids: ids },
    );
  }

  batchPrice(list) {
    return this.http.post(
      `${this.cnt.baseUrl}/api/sms/prod/product/batchPrice`,
      list,
    );
  }

  configUpload(): string {
    return `${this.cnt.baseUrl}/zuul/api/sms/prod/product/priceConfig`;
  }

  uploadProds(): string {
    return `${this.cnt.baseUrl}/zuul/api/sms/prod/product/upload/prod`;
  }

  getSyncLog() {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/prod/product/syncstatus`);
  }

  doSync() {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/prod/product/dosync`);
  }
}
