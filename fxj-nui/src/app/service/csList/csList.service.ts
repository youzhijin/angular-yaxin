import {Injectable, Injector} from '@angular/core';
import {Constant} from '@core/constant';
import {_HttpClient, SettingsService} from '@delon/theme';
import {MyHttpClient} from '../MyHttpClient';

@Injectable()
export class csListService {
  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient,
    public setting: SettingsService,
  ) {
  }

  /**
   * 分页获取商品
   */
  list(): string {
    return `${this.cnt.baseUrl}/rest/fxjdhData01Controller/list/` + this.setting.user.username;
  }
  getList() {
    return this.http.get(`${this.cnt.baseUrl}/rest/fxjdhData01Controller/list/` + this.setting.user.username);
  }

  /**
   * 获取产品
   * @param id
   */
  getprodetail(productId) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjOrderMainController/${productId}`,
      // `${this.cnt.baseUrl}/rest/fxjPlReplanController/${id}`,
    );
  }

  // getProduct(productId) {
  //   return this.http.get(
  //     `${this.cnt.baseUrl}/rest/fxjOrderMainController/${productId}`,
  //   );
  // }


  /**
   * 获取贷款信息
   * @param id
   */
  getLoanInfo(id) {
    return this.http.get(
      `${this.cnt.baseUrl}/rest/fxjLoanFkController/` + id,
    );
  }

  /**
   * 修改产品
   * @param product
   */
  editPro(product) {
    return this.http.put(
      `${this.cnt.baseUrl}/rest/fxjPaMainController/${product.id}`,
      product,
    );
  }


  uploadcarinfo(): string {
    return `${this.cnt.baseUrl}/rest/fxjDhImpController/uploadexcel?username=` + this.setting.user.username;
  }


  getpostasklistall() {

    return `${this.cnt.baseUrl}/rest/posXjtaskController/list/` + this.setting.user.username;


  }

  getposfilelist(id) {
    return this.http.get(`${this.cnt.baseUrl}/rest/posXjtaskController/filelist/` + id);
  }


}