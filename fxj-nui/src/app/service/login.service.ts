import { Injectable, Injector } from '@angular/core';

import { Router } from "@angular/router";
import { Constant, ServiceResult } from '@core/constant';
import {_HttpClient, SettingsService} from '@delon/theme';
import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { mergeMap, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { MyHttpClient } from './MyHttpClient';

@Injectable()
export class LoginService {
    get cnt(): Constant {
        return this.injector.get(Constant);
    }

    constructor(
        private injector: Injector,
        private http: _HttpClient,
        private myHttp: MyHttpClient,
        private setting: SettingsService

) {

    }

    login(username, password) {
        return this.http.post<ServiceResult>(`${this.cnt.baseUrl}/rest/tokens`, {
            username: username,
            password: password,
            appCode: 'sms_cloud'
        }, {
          username: username,
          password: password,
          appCode: 'sms_cloud'
        });
    }

    menu(username) {
        // username = this.setting.user.username;
      if(!username){
        username = this.setting.user.username;
      }
      if(username===""){
        username =  this.setting.user.username;
      }
      console.log("menuthis.setting.user.username"+this.setting.user.username);
        return this.http.get<ServiceResult>(`${this.cnt.baseUrl}/rest/base/getmenu/`+username);
    }

    info(username) {
      if(!username){
        username = this.setting.user.username;
      }
      if(username==""){
        username =  this.setting.user.username;
      }
      // console.log("infohis.setting.user.username"+this.setting.user.username);
        return this.http.get<ServiceResult>(`${this.cnt.baseUrl}/rest/base/getuser/`+username);
    }

    getno(username) {
      return this.http.get<ServiceResult>(`${this.cnt.baseUrl}/rest/base/getno/`+username);
    }
}
