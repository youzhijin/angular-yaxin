import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import { _HttpClient } from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class GroupService {

  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient
  ) { }

  groupType(typeId) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/groupType/${typeId}`)
  }

  addGroupType(obj) {
    return this.http.post(`${this.cnt.baseUrl}/api/admin/groupType`, obj);
  }

  modifyGroupType(obj) {
    return this.http.put(`${this.cnt.baseUrl}/api/admin/groupType/${obj.id}`, obj);
  }

  deleteGroupType(id) {
    return this.http.delete(`${this.cnt.baseUrl}/api/admin/groupType/${id}`);
  }

  getGroupType() {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/groupType/all`);
  }

  listForGroupType() {
    return `${this.cnt.baseUrl}/api/admin/groupType/page`;
  }

  getGroupMemberUrl() {
    return `${this.cnt.baseUrl}/api/admin/group/member/page`;
  }
  getGroupMember(groupId) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/group/${groupId}/user`);
  }

  getGroupMember2(groupId) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/group/${groupId}/user2`);
  }

  putGroupMembers(groupId, members) {
    return this.http.put(`${this.cnt.baseUrl}/api/admin/group/${groupId}/user?members=${members}`);
  }

  getGroups(code) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/group/tree?groupType=${code}`)
  }

  getDetail(groupId) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/group/${groupId}`);
  }

  modifyGroup(v) {
    return this.http.put(`${this.cnt.baseUrl}/api/admin/group/${v.id}`, v);
  }

  addGroup(v) {
    return this.http.post(`${this.cnt.baseUrl}/api/admin/group`, v);
  }

  delGroup(id) {
    return this.http.delete(`${this.cnt.baseUrl}/api/admin/group/${id}`);
  }

  /**
   * 获取当前用户可以权限下放的菜单
   */
  getManageMenus() {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/group/menu/authorize/list`);
  }

  /**
   * 返回角色对应的菜单
   * @param groupId 
   */
  getAuthorityMenu(groupId) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/group/${groupId}/authority/menu`);
  }

  /**
   * 获取角色被授权的菜单
   * @param groupId 
   */
  getAuthorizeMenu(groupId) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/group/${groupId}/authorize/menu`);
  }

  /**
   * 返回角色对应的元素
   * @param groupId 
   */
  getAuthorityElement(groupId) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/group/${groupId}/authority/element`)
  }

  /**
   * 获取角色被授权的元素
   * @param groupId 
   */
  getAuthorizeElement(groupId) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/group/${groupId}/authorize/element`)
  }

  /**
   * 给角色赋元素权限
   * @param groupId 
   * @param menuId 
   * @param elementId 
   */
  addGroupElement(groupId, menuId, elementId) {
    return this.http.post(`${this.cnt.baseUrl}/api/admin/group/${groupId}/authority/element/add?menuId=${menuId}&elementId=${elementId}`);
  }

  addGroupAuthorizeElement(groupId, menuId, elementId) {
    return this.http.post(`${this.cnt.baseUrl}/api/admin/group/${groupId}/authorize/element/add?menuId=${menuId}&elementId=${elementId}`);
  }

  /**
   * 移除角色元素权限
   * @param groupId 
   * @param menuId 
   * @param elementId 
   */
  removeGroupElement(groupId, menuId, elementId) {
    return this.http.post(`${this.cnt.baseUrl}/api/admin/group/${groupId}/authority/element/remove?menuId=${menuId}&elementId=${elementId}`);
  }

  removeGroupAuthorizeElement(groupId, menuId, elementId) {
    return this.http.post(`${this.cnt.baseUrl}/api/admin/group/${groupId}/authorize/element/remove?menuId=${menuId}&elementId=${elementId}`);
  }

  /**
   * 添加角色关联的菜单
   * @param groupId 
   * @param menus 菜单id, 用`,`分隔
   */
  addGroupMenu(groupId, menus) {    
    return this.http.post(`${this.cnt.baseUrl}/api/admin/group/${groupId}/authority/menu?menuTrees=${menus}`)
  }

  addGroupAuthorizeMenu(groupId, menus) {    
    return this.http.post(`${this.cnt.baseUrl}/api/admin/group/${groupId}/authorize/menu?menuTrees=${menus}`)
  }

  addGroupMember(groupId, userId) {
    return this.http.post(`${this.cnt.baseUrl}/api/admin/group/${groupId}/user/add?members=${userId}`)
  }
  
  delGroupMember(groupId, userId) {
    return this.http.post(`${this.cnt.baseUrl}/api/admin/group/${groupId}/user/del?members=${userId}`)
  }
}


export class GroupType {
  id?: string;
  name?: string;
  code?: string;
  [propName: string]: any;
}