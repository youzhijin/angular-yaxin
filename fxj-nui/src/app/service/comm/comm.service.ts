import { Injectable, Injector } from '@angular/core';

import { Router } from "@angular/router";
import { Constant, ServiceResult } from '@core/constant';
import {_HttpClient, SettingsService} from '@delon/theme';
import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { mergeMap, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { MyHttpClient } from '../MyHttpClient';
import {of} from "rxjs/index";


@Injectable()
export class CommService {
    get cnt(): Constant {
        return this.injector.get(Constant);
    }

    constructor(
      public setting: SettingsService,

      private injector: Injector,
        private http: _HttpClient,
        private myHttp: MyHttpClient
    ) {

    }


    getGoodsUrl(): string {
        return `${this.cnt.baseUrl}/api/file/goods/`;
    }

    getbiUrl(): string {
      return `${this.cnt.baseUrl}/rest/basebi/getbi/`+this.setting.user.username;
    }

    getDepartType() {
        return this.getDictValue('org_depart');
    }

    getElementType() {
        return this.getDictValue('authority_element');
    }

    /**
     * 获取菜单类型
     */
    getMenuType() {
        return this.getDictValue('authority_menu');
    }

    /**
     * 获取性别
     */
    getSex() {
        return this.getDictValue('comm_sex');
    }

  /**
   * 获取编号
   */
    getnectno(ordertype){
    this.getno(ordertype).pipe().subscribe(data => {
      const ret: ServiceResult = <ServiceResult> data;
      console.log("1111111111111111111111"+ret['data']);
      return ret['data'];
    })

    }
    getYear() {
        return this.getDictValue('sms_year');
    }

    getSeries() {
        return this.getDictValue('sms_series');
    }

    getProType() {
        return this.getDictValue('sms_pro_type');
    }

    getUserGroup() {
        return this.getDictValue('sms_usergroup');
    }

    getBrand() {
        return this.getDictValue('sms_brand');
    }

    getBrandLine() {
        return this.getDictValue('sms_line');
    }

    getSeasons() {
        return this.getDictValue('sms_prod_season');
    }

    getCategory() {
        return this.getDictValue('sms_category');
    }

    getFrameType() {
        return this.getDictValue('sms_frame_type');
    }

  getno(type: string) {
    return this.http.get(`${this.cnt.baseUrl}/rest/base/getno/${type}`);
  }
  getzip(filename: string) {
    return this.http.get(`${this.cnt.baseUrl}/rest/base/getzipfile?fileName=${filename}`);
  }


  gencon(orderno,conhead) {
    return this.http.post(
      `${this.cnt.baseUrl}/rest/base/getcon/${orderno}`,
      conhead,
    );
  }

  getfileno(type: string) {
    return this.http.get(`${this.cnt.baseUrl}/rest/base/getfileno/${type}`);
  }
    getDictValue(type: string) {
        return this.http.get(`${this.cnt.baseUrl}/rest/base/getDictValue/${type}`);
    }
  getDictentityValue(type: string) {
    return this.http.get(`${this.cnt.baseUrl}/rest/base/getDictentityValue/${type}`);
  }

  getsysusers(syscode: string) {
    return this.http.get(`${this.cnt.baseUrl}/rest/base/getsysuser/${syscode}`);
  }


    getType(type: string) {
    return this.getDictValue(type);
  }
  getentityType(type: string) {
    return this.getDictentityValue(type);
  }
    getCurrencys() {
        return this.getDictValue('sms_user_currency');
    }

    getSiteCodes() {
        return this.getDictValue('sms_user_sitecode');
    }

    getLabels() {
        return this.getDictValue('sms_user_label');
    }
}
