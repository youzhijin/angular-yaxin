import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import { _HttpClient } from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class MenuService {

  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient
  ) { }

  menuTree() {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/menu/tree`);
  }
  
  getMenu(id) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/menu/${id}`);
  }

  modify(menu) {
    return this.http.put(`${this.cnt.baseUrl}/api/admin/menu/${menu.id}`, menu);
  }

  add(menu) {
    return this.http.post(`${this.cnt.baseUrl}/api/admin/menu`, menu);
  }

  delete(id) {
    return this.http.delete(`${this.cnt.baseUrl}/api/admin/menu/${id}`);
  }

  elements() {
    return `${this.cnt.baseUrl}/api/admin/element/list`;
  }

  getElement(id) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/element/${id}`);
  }

  modifyElement(element) {
    return this.http.put(`${this.cnt.baseUrl}/api/admin/element/${element.id}`, element);
  }

  addElement(element) {
    return this.http.post(`${this.cnt.baseUrl}/api/admin/element`, element);
  }

  deleteElement(id) {
    return this.http.delete(`${this.cnt.baseUrl}/api/admin/element/${id}`);
  }

  /**
   * 获取菜单对应的资源
   * @param menuId 
   */
  getElementMenu() {
    return `${this.cnt.baseUrl}/api/admin/group/element/authorize/list`;
  }
}


export class Element {
  id?: string;
  menuId: string;
  [propName: string]: any;
}