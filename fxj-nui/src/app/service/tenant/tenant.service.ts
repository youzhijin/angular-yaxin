import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import { _HttpClient } from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class TenantService {

  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient
  ) { }

  list() {
      return `${this.cnt.baseUrl}/api/admin/tenant/page`
  }

  get(id) {
      return this.http.get(`${this.cnt.baseUrl}/api/admin/tenant/${id}`);
  }

  add(v) {
      return this.http.post(`${this.cnt.baseUrl}/api/admin/tenant`, v);
  }

  modify(v) {
      return this.http.put(`${this.cnt.baseUrl}/api/admin/tenant/${v.id}`, v);
  }

  delete(id) {
      return this.http.delete(`${this.cnt.baseUrl}/api/admin/tenant/${id}`);
  }

  getOwn(id) {
      return this.http.get(`${this.cnt.baseUrl}/api/admin/tenant/${id}/user`)
  }

  setOwn(tenantId, userId) {
      return this.http.put(`${this.cnt.baseUrl}/api/admin/tenant/${tenantId}/user?userId=${userId}`);
  }
}


export class Tenant {
  id?: string;
  code?: string;
  name?: string;  
  description?: string;  
  isSuperTenant?: string;
  [propName: string]: any;
}