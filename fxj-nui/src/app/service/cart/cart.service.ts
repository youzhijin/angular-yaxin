import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import { _HttpClient } from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class CartService {

  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient
  ) { }

  addCart(productCd) {
      return this.http.post(`${this.cnt.baseUrl}/api/sms/prod/cart/add`, { productCd: productCd, qty: 1, isAppend: '1' })
  }

  modifyCart(productCd, qty) {
      return this.http.post(`${this.cnt.baseUrl}/api/sms/prod/cart/add`, { productCd: productCd, qty: qty, isAppend: '0' })
  }

  list() {
      return this.http.get(`${this.cnt.baseUrl}/api/sms/prod/cart/list`);
  }

  delByBrand() {
    return this.http.delete(`${this.cnt.baseUrl}/api/sms/prod/cart/delByBrand`);
  }

  delBySku(productCd) {
    return this.http.delete(`${this.cnt.baseUrl}/api/sms/prod/cart/delBySku`, { productCd: productCd });
  }

  creatOrder() {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/prod/cart/creatOrder`);
  }
  uploadCart(): string {
    return `${this.cnt.baseUrl}/zuul/api/sms/prod/cart/batchUpload`;
  }
}
