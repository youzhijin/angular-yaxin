import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import { _HttpClient } from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class DictService {

  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient
  ) { }

  dictTree() {
      return this.http.get(`${this.cnt.baseUrl}/api/dict/dictType/tree`)
  }

  modifyType(obj) {
      return this.http.put(`${this.cnt.baseUrl}/api/dict/dictType/${obj.id}`, obj);
  }

  addType(obj) {
      return this.http.post(`${this.cnt.baseUrl}/api/dict/dictType`, obj);
  }

  deleteType(id) {
      return this.http.delete(`${this.cnt.baseUrl}/api/dict/dictType/${id}`);
  }

  getDictType(typeId) {
      return this.http.get(`${this.cnt.baseUrl}/api/dict/dictType/${typeId}`);
  }

  dictValues() {
      return `${this.cnt.baseUrl}/api/dict/dictValue/page`;
  }

  addDictValue(v) {
      return this.http.post(`${this.cnt.baseUrl}/api/dict/dictValue`, v);
  }

  modifyDictValue(v) {
      return this.http.put(`${this.cnt.baseUrl}/api/dict/dictValue/${v.id}`, v);
  }

  deleteDictValue(v) {
      return this.http.delete(`${this.cnt.baseUrl}/api/dict/dictValue/${v}`);
  }

  getDictValue(id) {
      return this.http.get(`${this.cnt.baseUrl}/api/dict/dictValue/${id}`);
  }


}

export class DictValue {
    id?: string;
    code?: string;
    labelDefault?: string;
    labelEnUs?: string;
    labelZhCh?: string;
    value?: string;
    orderNum?: number;
    typeId?: string;
    [propName: string]: any;
}
