import { Injectable, Injector } from '@angular/core';

import { Router } from "@angular/router";
import { Constant, ServiceResult } from '@core/constant';
import { _HttpClient } from '@delon/theme';
import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { mergeMap, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { MyHttpClient } from '../MyHttpClient';


@Injectable()
export class AuthService {
    get cnt(): Constant {
        return this.injector.get(Constant);
    }
    
    constructor(
        private injector: Injector,
        private http: _HttpClient,
        private myHttp: MyHttpClient
    ) {
        
    }
    
    list() {
        return `${this.cnt.baseUrl}/api/auth/service/page`;
    }

    get(id) {
        return this.http.get(`${this.cnt.baseUrl}/api/auth/service/${id}`);
    }

    modify(v) {
        return this.http.put(`${this.cnt.baseUrl}/api/auth/service/${v.id}`, v);
    }

    add(v) {
        return this.http.post(`${this.cnt.baseUrl}/api/auth/service`, v);
    } 

    del(id) {
        return this.http.delete(`${this.cnt.baseUrl}/api/auth/service/${id}`);
    }

    getClients(id) {
        return this.http.get(`${this.cnt.baseUrl}/api/auth/service/${id}/client`);
    }

    setClients(id, clients) {
        return this.http.put(`${this.cnt.baseUrl}/api/auth/service/${id}/client?clients=${clients}`)
    }
    
}

export class Client {
    id?: string;
    code?: string;
    name?: string;
    secret?: string;
    description?: string;
    [proname: string]: any; 
}
