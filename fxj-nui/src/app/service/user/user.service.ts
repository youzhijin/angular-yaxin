import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import {_HttpClient, SettingsService} from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class UserService {

  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    public setting: SettingsService,

    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient
  ) { }

  /**
   * 获取用户列表
   */
  list(): string {
    return `${this.cnt.baseUrl}/api/sms/admin/user/page`;
  }

  list2(): string {
    return `${this.cnt.baseUrl}/api/sms/admin/user/page2`;
  }

  secondList(): string {
    return `${this.cnt.baseUrl}/api/sms/admin/user/second/page`;
  }

  /**
   * 获取用户信息
   * @param userId  userinfo_id
   */
  info(username) {
    return this.http.get(`${this.cnt.baseUrl}/rest/base/getuser/${username}`);
  }

  /**
   * 获取用户信息
   * @param userId  user_id
   */
  info2(userId) {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/admin/user/info/${userId}`);
  }

  /**
   * 获取用户信息(仅userinfo)
   * @param userId  user_id
   */
  info3(userId) {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/admin/user/userinfo/${userId}`);
  }

  /**
   * 更新用户
   * @param user
   */
  modify(user) {
    return this.http.put(`${this.cnt.baseUrl}/api/sms/admin/user/${user.id}`, user);
  }
  /**
   * 1级用户注册
   */
  add(user) {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/admin/user/register`, user);
  }
  /**
   * 2级用户注册
   */
  addSecond(user) {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/admin/user/register2`, user);
  }

  addThird(user) {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/admin/user/register3`, user);
  }

  delete(id) {
    return this.http.delete(`${this.cnt.baseUrl}/api/sms/admin/user/${id}`);
  }

  /**
   * 禁用用户
   * @param id
   */
  disable(id) {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/admin/user/disable/${id}`);
  }

  /**
   * 启用用户
   * @param id
   */
  enable(id) {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/admin/user/enable/${id}`);
  }

  /**
   * 获取一级用户树
   */
  agentTree() {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/admin/user/agentTree`)
  }

  childTree(parentId) {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/admin/user/childTree?parentId=${parentId}`)
  }

  getRelation(childId, parentId) {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/admin/user/relation?childId=${childId}&parentId=${parentId}`);
  }

  /**
   * 密码修改
   * @param userName
   * @param oldPassword
   * @param newPassword
   */
  modifyPassword(userName, oldPassword, newPassword) {
      return this.myHttp.post(`${this.cnt.baseUrl}/rest/tokens/changepass`, {
          username: this.setting.user.username,
          oldPwd: oldPassword,
          newPwd: newPassword
      });
  }

  /**
   * 返回用户头像上传路径
   */
  uploadUrl() {
    return `${this.cnt.baseUrl}/zuul/api/admin/user/avatar`;
  }

  setPriceConfig(params) {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/admin/user/priceConfig`, params);
  }

  setCurrency(params) {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/admin/user/currencys`, params);
  }

  setSiteCode(params) {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/admin/user/siteCode`, params);
  }

  setLabel(params) {
    return this.http.post(`${this.cnt.baseUrl}/api/sms/admin/user/modifylabel`, params);
  }

  sync(date) {
    return this.http.get(`${this.cnt.baseUrl}/api/sms/admin/user/sync?date=${date}`)
  }
  batchSetCurrencyCodeLabel(): string {
    return `${this.cnt.baseUrl}/zuul/api/sms/admin/user/batchSetCurrencyCodeLabel`;
  }
}


export class User {
  id?: string;
  username?: string;
  name?: string;
  password?: string;
  email?: string;
  mobilePhone?: string;
  birthday?: string;
  sex?: string;
  description?: string;
  isDeleted?: string;
  isDisabled?: string;
  departId?: string;
  isSuperAdmin?: string;
  isAppManager?: string;
  [propName: string]: any;
}
