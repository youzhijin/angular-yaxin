import { Injectable, Injector } from '@angular/core';
import { Constant, ServiceResult } from '@core/constant';
import { _HttpClient } from '@delon/theme';
import { MyHttpClient } from '../MyHttpClient';

@Injectable()
export class DepartService {

  get cnt(): Constant {
    return this.injector.get(Constant);
  }

  constructor(
    private injector: Injector,
    private http: _HttpClient,
    private myHttp: MyHttpClient
  ) { }

  departTree() {
    return this.http.get<ServiceResult>(`${this.cnt.baseUrl}/api/admin/depart/tree`);
  }

  get(id) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/depart/${id}`);
  }

  add(v) {
    return this.http.post(`${this.cnt.baseUrl}/api/admin/depart`, v);
  }

  modify(v) {
    return this.http.put(`${this.cnt.baseUrl}/api/admin/depart/${v.id}`, v);
  }

  delete(id) {
    return this.http.delete(`${this.cnt.baseUrl}/api/admin/depart/${id}`); 
  }

  departMembers() {
    return `${this.cnt.baseUrl}/api/admin/depart/user`;
  }

  setMember(departId, userIds) {
    return this.http.post(`${this.cnt.baseUrl}/api/admin/depart/user?departId=${departId}&userIds=${userIds}`)
  }

  removeMember(departId, userId) {
    return this.http.delete(`${this.cnt.baseUrl}/api/admin/depart/user?departId=${departId}&userId=${userId}`)
  }

  positions() {
    return `${this.cnt.baseUrl}/api/admin/position/page`;
  }

  getPosition(id) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/position/${id}`);
  }

  addPosition(v) {
    return this.http.post(`${this.cnt.baseUrl}/api/admin/position`, v);
  }

  modifyPosition(v) {
    return this.http.put(`${this.cnt.baseUrl}/api/admin/position/${v.id}`, v);
  }

  deletePosition(id) {
    return this.http.delete(`${this.cnt.baseUrl}/api/admin/position/${id}`);
  }

  getPositionMember(positionId) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/position/${positionId}/user`)
  }

  setPositionMember(positionId, userIds) {    
    return this.http.put(`${this.cnt.baseUrl}/api/admin/position/${positionId}/user?users=${userIds}`);
  }

  getRelateDepart(positionId) {
    return this.http.get(`${this.cnt.baseUrl}/api/admin/position/${positionId}/depart`)
  }

  setRelateDepart(positionId, departs) {
    return this.http.put(`${this.cnt.baseUrl}/api/admin/position/${positionId}/depart?departs=${departs}`)
  }
}

export class Position {
  id?: string;
  code?: string;
  name?: string;
  departId?: string;
  type?: string;
  [proname: string]: any;
}
