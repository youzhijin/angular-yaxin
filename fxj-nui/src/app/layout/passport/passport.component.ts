import { Component, OnInit } from '@angular/core';
import { EmitterService } from 'app/service/emit.service';

@Component({
  selector: 'layout-passport',
  templateUrl: './passport.component.html',
  styleUrls: ['./passport.component.less'],
})
export class LayoutPassportComponent implements OnInit {
  links = [
    
  ];
  
  constructor(private emit: EmitterService) {}

  ngOnInit() {    
  } 
}
