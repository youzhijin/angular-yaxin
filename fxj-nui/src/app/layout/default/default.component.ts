import { Component, OnInit } from '@angular/core';
import {
  Router,
  NavigationEnd,
  RouteConfigLoadStart,
  NavigationError,
  NavigationCancel
} from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { ScrollService, MenuService, SettingsService } from '@delon/theme';
import { EmitterService } from 'app/service/emit.service';

@Component({
  selector: 'layout-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.less']
})
export class LayoutDefaultComponent implements OnInit {
  isFetching = false;  
  showReuse = true;
  constructor(
    router: Router,
    scroll: ScrollService,
    _message: NzMessageService,
    public menuSrv: MenuService,
    public settings: SettingsService,
    private emit: EmitterService
  ) {
    // scroll to top in change page
    router.events.subscribe(evt => {
      if (!this.isFetching && evt instanceof RouteConfigLoadStart) {
        this.isFetching = true;
      }
      if (evt instanceof NavigationError || evt instanceof NavigationCancel) {
        this.isFetching = false;
        if (evt instanceof NavigationError) {
          _message.error(`无法加载${evt.url}路由`, { nzDuration: 1000 * 3 });
        }
        return;
      }
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      setTimeout(() => {
        scroll.scrollToTop();
        this.isFetching = false;
      }, 100);
    });

    this.emit.getMessage().subscribe(data => {
      if (data.text === 'HIDE_REUSE_TAB') {
          this.showReuse = false;
      } else if (data.text === 'SHOW_REUSE_TAB') {
          this.showReuse = true;
      }
    })
  }

  ngOnInit() {
  }
}
