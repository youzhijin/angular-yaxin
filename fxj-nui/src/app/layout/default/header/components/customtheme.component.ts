import { Component, HostListener } from '@angular/core';
import * as screenfull from 'screenfull';
import { SettingsService } from '@delon/theme';
import { EmitterService } from 'app/service/emit.service';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { doChangeColor } from '@shared/theme-util';

@Component({
    selector: 'header-customtheme',
    template: `
    <nz-popover [nzTrigger]="'click'" nzOverlayClassName="theme-color-content">
        <div class="theme-color" nz-popover >
            <div class="theme-color-value">
                <i class="anticon anticon-skin"></i>
            </div>
        </div>
        <ng-template #nzTemplate>
            <div style="width: 200px; margin: -12px -16px;">
                <color-sketch [color]="color" (onChangeComplete)="changeColor($event)"></color-sketch>
            </div>
        </ng-template>
    </nz-popover>
    `
})
export class HeaderCustomThemeComponent implements OnInit {
    // 默认色
    color = localStorage.getItem("sms-theme") || '#1890ff';

    constructor(private settings: SettingsService, private emit: EmitterService) {
    }

    ngOnInit() {
    }

    changeColor(res: any) {
        this.color = res.color.hex;
        console.log(this.color);
        doChangeColor(res.color.hex);
    }

}
