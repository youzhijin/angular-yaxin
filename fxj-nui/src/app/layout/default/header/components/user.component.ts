import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsService } from '@delon/theme';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { NzModalService } from 'ng-zorro-antd';

@Component({
  selector: 'header-user',
  template: `
  <nz-dropdown nzPlacement="bottomRight">
    <div class="item d-flex align-items-center px-sm" nz-dropdown>
      <nz-avatar [nzSrc]="settings.user.avatar" nzSize="small" class="mr-sm"></nz-avatar>
      {{settings.user.name}}
    </div>
    <div nz-menu class="width-sm">      
      <div nz-menu-item (click)="logout()"><i class="anticon anticon-setting mr-sm"></i>Logout</div>
    </div>
  </nz-dropdown>
  `,
})
export class HeaderUserComponent {
  constructor(
    private confirmServ: NzModalService,
    public settings: SettingsService,
    private router: Router,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
  ) {}

  logout() {
      this.confirmServ.confirm({
        nzTitle: `您确定要退出系统?`,
        nzOkText: '确定',
        nzCancelText: '取消',
        nzOnOk: () => {
            let tmp = localStorage.getItem('sms-theme');
            localStorage.clear();
            if (tmp != null) {
              localStorage.setItem("sms-theme", tmp);
            }
            sessionStorage.clear();
            this.tokenService.clear();
            this.router.navigateByUrl(this.tokenService.login_url);
        }
    });
  }
}
