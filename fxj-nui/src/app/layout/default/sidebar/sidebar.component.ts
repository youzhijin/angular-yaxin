import { Component, Inject } from '@angular/core';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { SettingsService } from '@delon/theme';
import { Router } from '../../../../../node_modules/@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '../../../../../node_modules/@delon/auth';

@Component({
  selector: 'layout-sidebar',
  templateUrl: './sidebar.component.html',
})
export class SidebarComponent {
  constructor(
    public settings: SettingsService,
    public msgSrv: NzMessageService,
    private confirmServ: NzModalService,
    private router: Router,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
  ) {}


  logout() {
    this.confirmServ.confirm({
        nzTitle: `您确定要退出系统?`,
        nzOkText: '确定',
        nzCancelText: '取消',
        nzOnOk: () => {
            let tmp = localStorage.getItem('sms-theme');
            localStorage.clear();
            if (tmp != null) {
              localStorage.setItem("sms-theme", tmp);
            }
            sessionStorage.clear();
            this.tokenService.clear();
            this.router.navigateByUrl(this.tokenService.login_url);
        }
    });
  }
}
