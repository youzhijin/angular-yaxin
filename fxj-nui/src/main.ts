import { enableProdMode, ViewEncapsulation } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from 'app/app.module';
import { environment } from '@env/environment';
import { preloaderFinished } from '@delon/theme';
import { hmrBootstrap } from 'hmr';
import { tryLoadTheme, doChangeColor, tryLoadLess } from '@shared/theme-util';

// 加载隐藏preload的方法
preloaderFinished();

if (environment.production) {
  enableProdMode();
}

const bootstrap = () => {
  return platformBrowserDynamic()
    .bootstrapModule(AppModule, {
      defaultEncapsulation: ViewEncapsulation.Emulated,
      preserveWhitespaces: false,
    })
    .then(res => {
      if ((<any>window).appBootstrap) {        
        tryLoadTheme().then(() => {           
          tryLoadLess().then(() => {                        
            if (localStorage.getItem('sms-theme')) {                 
              doChangeColor(localStorage.getItem('sms-theme'), (<any>window).appBootstrap);                       
            } else {
              // 执行隐藏preload方法  
              (<any>window).appBootstrap();
            }
          })
        })                
      }
      return res;
    });
};

if (environment.hmr) {
  if (module['hot']) {
    hmrBootstrap(module, bootstrap);
  } else {
    console.error('HMR is not enabled for webpack-dev-server!');
    console.log('Are you using the --hmr flag for ng serve?');
  }
} else {
  bootstrap();
}
